<?php

declare(strict_types=1);

namespace App\App\Domain\User;

use App\Domain\User\UserRoles;
use PhpSpec\ObjectBehavior;

class UserRolesSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(UserRoles::class);
    }

    public function it_get_roles_test(): void
    {
        $this->getRoleList()->shouldReturn([
            UserRoles::ROLE_SUPER_ADMIN => 'Super admin',
            UserRoles::ROLE_ADMIN => 'Administrator',
            UserRoles::ROLE_REDACTOR => 'Redaktor',
        ]);
    }
}
