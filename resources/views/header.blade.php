<header class="header">
    <nav id="nav" class="text-center pd-zero nav-custom no-ajax-load" role="navigation">
        <div class="menu-opener">
            <div class="menu-opener-inner"></div>
        </div>
        <div class="container clearfix" id="myScrollspy">
            <div class="text-menu">
                <figure><img src="{{ asset('img/svg/responsive.svg') }}" alt="Tablet i telefon"></figure>
            </div>
            <ul class="nav">
                <li><a class="page-scroll ajax" href="{{ route('pages', ['']) }}">Home</a></li>
                <li><a class="page-scroll ajax" href="{{ route('pages', ['o-mnie']) }}">O MNIE</a></li>
                {{--<li class="dropdown"> <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Serwis <span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="">Komutery i laptopy</a></li>--}}
                        {{--<li><a href="">Telefony</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <li class="dropdown"> <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Portfolio <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a class="ajax" href="{{ route('pages', ['portfolio']) }}">Szablony stron</a></li>
                    </ul>
                </li>
                <li><a class="page-scroll ajax" href="{{route('blog')}}">Blog</a></li>
                <li><a class="page-scroll ajax" href="{{route('FastPost')}}">Szybkie posty</a></li>
                <li><a class="page-scroll ajax" href="{{ route('contact.index') }}">Kontakt</a></li>
                <li><a class="page-scroll" href="{{route('post.create')}}">a</a></li>
                @guest
                @else
                    <li><a class="page-scroll" href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Wyloguj</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                @endguest
                <li class="mp3-li">
                    <div class="mp3-container">
                        <div class="change" title="pause/play">
                            <div class="btn-mp3"><span class="eq eq--off" id="link_id"> <span class="eq__bar eq__1"></span><span class="eq__bar eq__2"></span><span class="eq__bar eq__3"></span><span class="eq__bar eq__4"></span><span class="eq__bar eq__5"></span> </span></div>    
                        </div>
                        <div class="mp3-list-container">
                            <div class="text-center control-container">
                                <div class="col-xs-2 control-icon" id="mp3-prev" title="Poprzedni">
                                    <i class="fas fa-step-backward"></i>
                                </div>   
                                <div class="col-xs-2 control-icon" id="mp3-rand" title="Losowe mp3">
                                    <i class="fa fa-random" aria-hidden="true"></i>
                                </div>                
                                <div class="col-xs-2 control-icon" id="mp3-next" title="Następny">
                                    <i class="fas fa-step-forward"></i>
                                </div>           
                                <div class="col-xs-2 control-icon" id="mp3-volume-plus" title="Głośniej">
                                    <i class="fas fa-plus-circle"></i>
                                </div>
                                <div class="col-xs-2 control-icon" id="mp3-volume-minus" title="Ciszej">
                                    <i class="fas fa-minus-circle"></i>
                                </div>    
                                <div class="col-xs-2 control-icon" id="volume-info" title="Poziom głośności od 0-1">1</div> 
                            </div>  
                            <div class="clearfix"></div>
                            <div class="mp3-list"></div>  
                            <div class="clearfix"></div>
                            <div class="text-center">
                                <div id="mp3-time-info" class="mp3-time-info"></div>
                            </div>
                        </div>   
                    </div>
                </li>
                <audio id="mp3Audio"></audio>
            </ul>
        </div>
    </nav>
</header>