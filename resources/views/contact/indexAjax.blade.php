<article class="container-fluid contakt_background">
    <div class="container contact_darkening_background">
        <header class="row">
            <div class="col-lg-12 text-center">
                <h1>Formularz Kontaktowy</h1>
                <hr>
                <h4>Jeżeli chcesz nawiązać ze mną współprace lub masz do mnie jakie kolwiek pytania proszę skorzystaj z poniższego formularza kontaktowego lub zadzwoń na na podany poniżej numer telelefonu.</h4>
            </div>
        </header>
        <section class="row padding-container">
            <div class="col-lg-6 text-center col-lg-offset-3">
                <figure><img src="{{asset('img/svg/smartphone-call.svg')}}" alt="Ikona telefonu" class="img-responsive smallimg animated infinite bounce"></figure>
                <h2>511-152-133</h2>
                <h4>mordimergt@gmail.com</h4>
            </div>
        </section>
        <hr>
        <div class="row">
            <section class="col-lg-6 col-lg-offset-3 pd-zero">
                <form id="contact_form" class="form-horizontal" method="POST" action="{{route('contact.store')}}">
                    @csrf
                    <h4>DANE *</h4>
                    <div class="control-group">
                        <div class="col-lg-6">
                            <div class="controls">
                                <input type="text" name="firstname" class="form-control" placeholder="Wpisz imie" id="firstname" value="{{old('firstname')}}" data-error="#perfirstname">
                            </div>
                            <label class="control-label grey-font" for="firstname">IMIE</label>
                            @if ($errors->has('firstname'))
                                <div class="errormessage invalid-feedback">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="col-lg-6">
                            <div class="controls">
                                <input type="text" name="lastname" class="form-control" placeholder="Wpisz nazwisko" id="lastname" value="{{old('lastname')}}" data-error="#perlastname">
                            </div>
                            <label class="control-label grey-font" for="lastname">NAZWISKO</label>
                            @if ($errors->has('lastname'))
                                <div class="errormessage invalid-feedback">
                                    <strong>{{ $errors->first('lastname') }}</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="control-group col-lg-12">
                        <label class="control-label" for="email">Adres Email *</label>
                        <div class="controls">
                            <input type="text" name="email" placeholder="Wpisz email" class="form-control" id="email" value="{{old('email')}}" data-error="#peremail">
                        </div>
                        @if ($errors->has('email'))
                            <div class="errormessage invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="control-group col-lg-12">
                        <label class="control-label" for="subject">Temat *</label>
                        <div class="controls">
                            <input type="text" name="subject" class="form-control" placeholder="Tu wpisz temat wiadomosci" id="subject" value="{{old('subject')}}" data-error="#persubject">
                        </div>
                         @if ($errors->has('subject'))
                            <div class="errormessage invalid-feedback">
                                <strong>{{ $errors->first('subject') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="control-group col-lg-12">
                        <label class="control-label" for="message">Treść wiadomości *</label>
                        <div class="controls">
                            <textarea type="text" name="message" id="message" rows="10" class="form-control contakt101" placeholder="Tu wpisz treść wiadomosci" data-error="#permessage">{{old('message')}}</textarea>
                        </div>
                        @if ($errors->has('message'))
                            <div class="errormessage invalid-feedback">
                                <strong>{{ $errors->first('message') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="control-group col-lg-12 text-center">
                        <button type="submit" id="send_form" class="sendmessage_button">WYŚLIJ</button>
                    </div>
                </form>
            </section>
        </div>
        <footer>
            <div class="row">
                <div class="col-lg-12 padding-container text-center">
                    <h3 class="white-font">Powered by AJAX!</h3>
                </div>
            </div>
        </footer>
    </div>
</article>