@section('title'){{$title}}@endsection
@section('description'){{$description}}@endsection

@include('titlePage')
<article class="container-fluid bg-white text-center" style="padding-top: 0px;padding-bottom: 0px">
    <div class="row">
        <section class="col-lg-12">
            <h3>Jeżeli coś jest wartego uwagi a nie jest to materiał na pełnoprawnwgo posta na blogu to będę starać się to umieszczać w tym miejscu.</h3>
        </section>
        <section class="col-lg-4 col-lg-offset-4">
            <form method="get" action="{{route('FastPost')}}" id="searchForm">
                <div class="input-group">
                    <input name="title" id="title" class="form-control form-control-special" placeholder="Szukaj..." type="text" value="">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default btn-custom" id="searchButton"><i class="fas fa-search"></i></button>
                    </span>
                </div>
            </form>
        </section>
        <section class="col-lg-12 text-center over-auto">
            @foreach ($tags as $tag)
            <a class="blog-link2 ajax" href="{{route('FastPostTag',$tag->slug)}}">#{{$tag->name}}</a>
            @endforeach
        </section>
        <section class="col-lg-12 pd-top-20" style="padding-bottom: 15px">
            @if(isset($tagSearch))
            <ul class="list-group" style="margin-bottom:5px!important">
                <li class="list-group-item list-group-item active">Szukasz po tagu: <b>{{$tagSearch}}</b></li>
            </ul>
            @endif
            @if(isset($fastPosts))
            <ul class="list-group">
                <li class="list-group-item list-group-item active"><b>Ilość rekordów w bazie: {{$fastPosts->total()}}</b></li>
            </ul>
            @foreach($fastPosts as $post)
            <section class="pd-zero text-left mg-bt-top-10px">
                <h5 class="date-h text-left"><i class="far fa-clock red-font fa-2x red-font" aria-hidden="true"></i>
                    <time datetime="2018-01-31" itemprop="datePublished"> {{$post->publishedAt}}</time></h5>
                <h3 class="text-left text-longshadow"> <a class="ajax" href="{{route('fastPostSingle',$post->slug)}}">{{$post->title}}</a></h3>
                {!! $post->content !!}
            </section>
            <hr class="style-two">
            @endforeach
            {{ $fastPosts->appends(request()->input())->links() }}
            @else
            <ul class="list-group pd-top-20">
                <li class="list-group-item list-group-item-danger custom-item">WPISZ SZUKANĄ FRAZĘ LUB SZUKAJ PO KATEGORII</li>
            </ul>
            <br>
            @endif
        </section>
    </div>    
</article>
<script>
    if (typeof Prism !== "undefined") {
        Prism.highlightAll();
    }
</script>