@section('title'){{$post->title}}@endsection
@section('description'){{$post->description}}@endsection
@include('titlePage')
<article class="container-fluid bg-white text-left" style="padding-top: 10px;padding-bottom: 10px">
    {!!$post->content!!}
</article>
@if(true === $post->disableComments)
    <article class="container-fluid bg-white border-top-2px">
        <div class="container">
            <div class="errormessage"><strong>Komentarze wyłączone</strong></div>
        </div>
    </article>
@elseif(true === $post->justShowComments)
    <article class="container-fluid bg-white border-top-2px">
        <div class="container">
            <div class="errormessage"><strong>Dodawanie komentarzy wyłączone</strong></div>
        </div>
    </article>
    @include('blog.singlePost.comments')
@elseif(true === $post->justAddingComments)
    <article class="container-fluid bg-white border-top-2px">
        <div class="container">
            <div class="errormessage"><strong>Możesz tylko dodawać komentarze</strong></div>
        </div>
    </article>
    @include('blog.singlePost.comments')
@else
    @include('blog.singlePost.comments')
@endif
<script>
    if (typeof Prism !== "undefined") {
        Prism.highlightAll();
    }
</script>