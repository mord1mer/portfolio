@extends('layouts.app')
@section('title', 'Nowa Kategoria')
@section('content')
@include('titlePage')
@include('adminMenu')
<article class="container-fluid grey-cont">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('user.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Nowy post</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </div> <!-- end of .row -->
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <th>id</th>
                <th>imie</th>
                <th>email</th>
                <th>Data utworzenia</th>
                <th>Data ostatniej edycji</th>
                <th>Role</th>
                <th></th>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <th>{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->updated_at }}</td>
                        <td>
                            {{implode(' ',$user->roles)}}
                        </td>
                        <td><a href="{{ route('user.show', $user->id) }}" class="btn btn-default btn-sm">Podgląd</a> <a href="{{ route('user.edit', $user->id) }}" class="btn btn-default btn-sm btn-primary">Edycja</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-center">
                {!! $users->links(); !!}
            </div>
        </div>
    </div>
</article>
@endsection
@section('scripts')
@endsection