<div class="form-group">
    <label class="col-sm-2 control-label" for="name">Imię *</label>
    <div class="col-sm-10">
        <input type="text" name="name" class="contakt100 form-control form-control-special"  placeholder="imię i nazwisko" id="name" value="{{ old('name',  isset($user->name) ? $user->name : null) }}">
    </div>
    @if ($errors->has('name'))
    <div class="errormessage invalid-feedback">
        <strong>{{ $errors->first('name') }}</strong>
    </div>
    @endif
    <div id="name_text"></div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="email">Email *</label>
    <div class="col-sm-10">
        <input type="text" name="email" class="contakt100 form-control form-control-special"  placeholder="email ziomka" id="email" value="{{ old('email',  isset($user->email) ? $user->email : null) }}">
    </div>
    @if ($errors->has('email'))
    <div class="errormessage invalid-feedback">
        <strong>{{ $errors->first('email') }}</strong>
    </div>
    @endif
    <div id="name_text"></div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="password">password *</label>
    <div class="col-sm-10">
        <input type="password" name="password" class="contakt100 form-control form-control-special"  placeholder="hasło" id="password" value="">
    </div>
    @if ($errors->has('password'))
    <div class="errormessage invalid-feedback">
        <strong>{{ $errors->first('password') }}</strong>
    </div>
    @endif
    <div id="name_text"></div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="password_confirmation">Powtórz hasło *</label>
    <div class="col-sm-10">
        <input type="password" name="password_confirmation" class="contakt100 form-control form-control-special"  placeholder="powtorz haslo" id="password_confirmation" value="">
    </div>
    @if ($errors->has('password_confirmation'))
    <div class="errormessage invalid-feedback">
        <strong>{{ $errors->first('password_confirmation') }}</strong>
    </div>
    @endif
    <div id="name_text"></div>
</div>
<div class="form-group">
    <h3><b>Role użytkownika</b></h3>

    @foreach ($userRoles as $key => $role)
    <span class="checkbox-choice">
        <input type="checkbox" class="form-check-input" id="cat_{{$key}}" value="{{$key}}" name="roles[]"
               @if(isset($user->roles))
               @if(is_array($user->roles) && in_array($key, $user->roles))checked @endif
               @else
               @if(is_array(old('roles')) && in_array($key, old('roles')))checked @endif
               @endif
               >
               <label for="cat_{{$key}}"><span>x</span> {{$role}}</label>
    </span>
    @endforeach

</div>
<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary btn-lg btn-block pull-left" name="add-post" >zapisz</button>
    </div>
</div>
<style>
    .checkbox-choice input[type="checkbox"] {
        display: none;
    }
    .checkbox-choice span {
        display: none;
    }
    .checkbox-choice label {
        border: 1px solid #900000;padding: 5px; cursor: pointer;border-radius: 5px;
    }
    .checkbox-choice input[type="checkbox"]:checked+label {
        background-color:#900000;color: white;
    }
    .checkbox-choice input[type="checkbox"]:checked+label span {
        display: inline-block;color: white;
    }
</style>