@extends('layouts.app')
@section('title', 'Edycja danych użytkownika')
@section('content')
@include('titlePage')
@include('adminMenu')
<article class="container-fluid grey-cont">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('user.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Nowy nowego usera</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </div> <!-- end of .row -->
    <div class="row">	
        <div class="container kolor">
            <form action="{{ route('user.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                @include('admin.user.userForm')
            </form>
        </div>
    </div>	
</article>
@endsection
@section('scripts')
@endsection