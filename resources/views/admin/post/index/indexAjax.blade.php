@include('titlePage')
@include('adminMenu')
<article class="container-fluid grey-cont">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('post.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Nowy post</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </div> <!-- end of .row -->
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <th>Id</th>
                    <th>Tytuł</th>
                    <th>Autor</th>
                    <th>Kontent</th>
                    <th>Typ</th>
                    <th>Data utworzenia</th>
                    <th></th>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                    <tr @if($post->user_id!=Auth::id()) style="background-color: grey" @endif >
                        <th>{{ $post->id }}</th>
                        <th>{{ $post->title }}</th>
                        <td>{{ $post->author->name }}</td>
                        <td>{{ substr(strip_tags($post->content), 0, 50) }}{{ strlen(strip_tags($post->content)) > 50 ? "..." : "" }}</td>
                        <td>{{ $pageType[$post->type] }}</td>
                        <td>{{ $post->publishedAt }}</td>
                        <td>
                            <a href="{{ route('post.show', $post->id) }}" class="btn btn-default btn-sm">Podgląd</a>
                            <a href="{{ route('post.edit', $post->id) }}" class="btn btn-default btn-sm btn-primary">Edycja</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-center">
                {!! $posts->links(); !!}
            </div>
        </div>
    </div>
</article>