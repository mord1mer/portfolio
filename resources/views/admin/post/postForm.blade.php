<div class="row">
    <div class="form-group">
        <!--    <label class="col-sm-2 control-label text-left" for="content">POST Text:</label>-->
        <div class="col-sm-12">
            <textarea placeholder="text-posta" class="form-control custom-control form-control-special edytor"
                name="content" id="content"
                rows="50">{{ old('content',  isset($post->content) ? $post->content : null) }}</textarea>
        </div>
        <div class="clearfix"></div>
        @if ($errors->has('content'))
        <div class="errormessage invalid-feedback">
            <strong>{{ $errors->first('content') }}</strong>
        </div>
        @endif
        <div id="content_text"></div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="title">Tytuł *</label>
    <div class="col-sm-10">
        <input type="text" name="title" class="contakt100 form-control form-control-special"
            placeholder="tutuł posta .. np jak zrobić..." id="title"
            value="{{ old('title',  isset($post->title) ? $post->title : null) }}">
    </div>
    @if ($errors->has('title'))
    <div class="errormessage invalid-feedback">
        <strong>{{ $errors->first('title') }}</strong>
    </div>
    @endif
    <div id="title_text"></div>

</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="publishedAt">publishedAt *</label>
    <div class="col-sm-10">
        <input type="text" name="publishedAt" class="contakt100 form-control form-control-special" id="publishedAt"
            value="{{ old('publishedAt',  isset($post->publishedAt) ? $post->publishedAt : null) }}">
    </div>
    @if ($errors->has('publishedAt'))
    <div class="errormessage invalid-feedback">
        <strong>{{ $errors->first('publishedAt') }}</strong>
    </div>
    @endif

</div>
<h1>Dane seo i linki</h1>

<div class="form-group">
    <label class="col-sm-2 control-label" for="headtitle">headtitle *</label>
    <div class="col-sm-10">
        <input type="text" name="headtitle" class="contakt100 form-control form-control-special"
            placeholder="tytuł/headtitle dla sekcji head max 71 znaków" id="headtitle"
            value="{{ old('headtitle',  isset($post->headtitle) ? $post->headtitle : null) }}" maxlength='71'>
    </div>
    @if ($errors->has('headtitle'))
    <div class="errormessage invalid-feedback">
        <strong>{{ $errors->first('headtitle') }}</strong>
    </div>
    @endif
    <div id="headtitle_text"></div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="keyworks">keyworks *</label>
    <div class="col-sm-10">
        <input type="text" name="keyworks" class="contakt100 form-control form-control-special"
            placeholder="keyworks... max 200 znaków" id="keyworks"
            value="{{ old('keyworks',  isset($post->keyworks) ? $post->keyworks : null) }}" maxlength='200'>
    </div>
    @if ($errors->has('keyworks'))
    <div class="errormessage invalid-feedback">
        <strong>{{ $errors->first('keyworks') }}</strong>
    </div>
    @endif
    <div id="keyworks_text"></div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="description">description */Krótki opis do 278 znakow posta na
        blogu</label>
    <div class="col-sm-10">
        <textarea maxlength='278' placeholder="Krótki opis ..." class="form-control custom-control form-control-special"
            name="description" id="description"
            rows="10">{{ old('description',  isset($post->description) ? $post->description : null) }}</textarea>
    </div>
    @if ($errors->has('description'))
    <div class="errormessage invalid-feedback">
        <strong>{{ $errors->first('description') }}</strong>
    </div>
    @endif
    <div id="description_text"></div>
</div>
<div class="form-group">
    <div class="col-xs-12">
        <h3><b>Kategorie</b></h3>
        @foreach ($categories as $category)
        <span class="checkbox-choice">
            <input type="checkbox" class="form-check-input" id="cat_{{$category->id}}" value="{{$category->id}}"
                name="categories[]" @if(isset($selectCategories)) @if(is_array($selectCategories) &&
                in_array($category->id, $selectCategories))checked @endif
            @else
            @if(is_array(old('categories')) && in_array($category->id, old('categories')))checked @endif
            @endif
            >
            <label for="cat_{{$category->id}}"><span>x</span> {{$category->name}}</label>
        </span>
        @endforeach

        @if ($errors->has('categories'))
        <div class="errormessage invalid-feedback">
            <strong>{{ $errors->first('categories') }}</strong>
        </div>
        @endif
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12">
        <h3><b>Tagi</b></h3>
        @foreach ($tags as $tag)
        <span class="checkbox-choice">
            <input type="checkbox" class="form-check-input" id="tag_{{$tag->id}}" value="{{$tag->id}}" name="tags[]"
                @if(isset($selectTags)) @if(is_array($selectTags) && in_array($tag->id, $selectTags)) checked @endif
            @else
            @if(is_array(old('tags')) && in_array($tag->id, old('tags'))) checked @endif
            @endif
            >
            <label for="tag_{{$tag->id}}"><span>x</span> {{$tag->name}}</label>
        </span>
        @endforeach
        @if ($errors->has('tags'))
        <div class="errormessage invalid-feedback">
            <strong>{{ $errors->first('tags') }}</strong>
        </div>
        @endif
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="img">Typ strony</label>
    <div class="col-sm-10">
        <select class="form-control form-control form-control-special" name="type">
            <option value="">-----</option>
            @foreach($types as $typeKey => $type)
            @if( old('type', isset($post->type) ? $post->type : null) == $typeKey)
            <option selected value="{{$typeKey}}">{{$type}}</option>
            @else
            <option value="{{$typeKey}}">{{$type}}</option>
            @endif
            @endforeach
        </select>
    </div>
    @if ($errors->has('type'))
    <div class="errormessage invalid-feedback">
        <strong>{{ $errors->first('type') }}</strong>
    </div>
    @endif
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="img">Zdjecie tla posta</label>
    <div class="col-sm-10">
        <input type="text" name="img" class="contakt100 form-control form-control-special" placeholder="adres zdjecia"
            id="img" value="{{ old('img',  isset($post->img) ? $post->img : null) }}" maxlength='255'>
    </div>
    @if ($errors->has('img'))
    <div class="errormessage invalid-feedback">
        <strong>{{ $errors->first('img') }}</strong>
    </div>
    @endif
    <div id="img_text"></div>
</div>
<div class="form-group">
    <label class="btn btn-default btn-file">
        <input name="image_upload_box" type="file" id="image_upload_box" size="40" />
    </label>
    <div clas="showPhoto">
        <img class="img-responsive" src="" alt="" id="showPhoto">
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12">
        <h3>Komentarze</h3>
    </div>
    <div class="col-xs-12">
        <h6>*Zaznaczenie tej opcji umozliwia pokazanie już dodanych komentarzy.</h6>
        <span class="checkbox-choice">
            <input type="checkbox" class="form-check-input" id="show_added_comments" name="show_added_comments" value="1" 
            @if(old('show_added_comments') || (true === old('show_added_comments', $post->show_added_comments ?? null)))
            checked
            @endif
            >
            <label for="show_added_comments"><span>x</span> Włącz pokazywanie komentarzy</label>
        </span>
    </div>
    <div class="col-xs-12">
        <h6>*Zaznaczenie tej opcji pozwala dodawać komentarze.</h6>
        <span class="checkbox-choice">
            <input type="checkbox" class="form-check-input" id="let_add_comments" name="let_add_comments" value="1" 
            @if(old('let_add_comments') || (true === old('let_add_comments', $post->let_add_comments ?? null)))
            checked
            @endif
            >
            <label for="let_add_comments"><span>x</span>Pozwalaj dodawać komentarze</label>
        </span>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary btn-lg btn-block pull-left" name="add-post">Zapisz</button>
    </div>
</div>

@section('scripts')
<!-- <script src="//cloud.tinymce.com/stable/tinymce.min.js"></script> -->
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=pfir6tw1aejvqvi10svdtwbptl0arnvh4lbkrpg55kumq548">
</script>
<script>
    $(window).on('load', function() {
    tinymce.init({
        selector: '#content',
        height: 700,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
    });
});
$.fn.extend({
    limiter: function(limit, elem) {
        try {
            $(this).on("keyup focus", function() {
                //$('body').find(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html(limit - chars);
            }
            ;
            setCount($(this)[0], elem);
        } catch (err) {
            console.log(err.message);
        }
    }
});

$(window).on('load', function() {
    $("#title").limiter(100, $("#title_text"));
    $("#description").limiter(278, $("#description_text"));
    $("#keyworks").limiter(200, $("#keyworks_text"));
    $("#headtitle").limiter(71, $("#headtitle_text"));
});
</script>
<style>
    .checkbox-choice input[type="checkbox"] {
        display: none;
    }

    .checkbox-choice span {
        display: none;
    }

    .checkbox-choice label {
        border: 1px solid #177CA4;
        padding: 5px;
        cursor: pointer;
        border-radius: 5px;
    }

    .checkbox-choice input[type="checkbox"]:checked+label {
        background-color: #177CA4;
        color: white;
    }

    .checkbox-choice input[type="checkbox"]:checked+label span {
        display: inline-block;
        color: black;
    }
</style>
@endsection