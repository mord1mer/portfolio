@extends('layouts.app')
@section('title', 'Edycja posta')
@section('content')
@include('titlePage')
@include('adminMenu')
<article class="container-fluid grey-cont">
    <div class="row">
        <div class="container kolor">
            <form action="{{ route('post.update',$post->id) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <input name="_method" type="hidden" value="PUT">
                @include('admin.post.postForm')
            </form>
        </div>
    </div>
</article>
@endsection