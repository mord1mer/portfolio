@extends('layouts.app')
@section('title', 'Dodawanie posta')
@section('content')
@include('titlePage')
@include('adminMenu')
<article class="container-fluid grey-cont">
    <div class="row">
        <div class="container kolor">
            <form action="{{ route('post.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                @include('admin.post.postForm')
            </form>
        </div>
    </div>
</article>
@endsection