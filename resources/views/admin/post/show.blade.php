@extends('layouts.app')
@section('title', 'Post')
@section('content')
@include('titlePage')
<article class="container-fluid grey-cont">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="well">
                <div>
                    <label>Url: {{route('blog.single',$post->slug ? $post->slug : '')}}</label>
                    <p><a href="{{route('blog.single',$post->slug ? $post->slug : '')}}">Przejdz do postu</a></p>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Kategorie:</h3>
                        @foreach ($post->categories as $category)
                            <span class="label label-default">{{ $category->name }}</span>
                        @endforeach
                    </div>
                    <div class="col-sm-6 tags">
                        <h3>Tagi</h3>
                        @foreach ($post->tags as $tag)
                            <span class="label label-default">{{ $tag->name }}</span>
                        @endforeach
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        <label>Data utworzenia:</label>
                        <p>{{$post->publishedAt}}</p>
                    </div>
                    <div class="col-sm-6">
                        <label>Data ostatniej modyfikacji:</label>
                        <p>{{$post->updatedAt}}</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{ route('post.edit', $post->id) }}" class="btn btn-primary btn-block">Edit</a>
                    </div>
                    <div class="col-sm-6">
                        <form method="POST" action="{{ route('post.destroy', $post->id) }}" accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE">
                            @csrf
                            <input class="btn btn-danger btn-block" type="submit" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1>{{ $post->title }}</h1>
            <hr style="height: 2px;background-color: black">
        </div>
        <div class="clearfix"></div>
        <div class="lead">{!! $post->content !!}</div>
    </div>
    <hr style="height: 2px;background-color: black">
    <section id="backend-comments" style="margin-top: 50px;">
        <button id="showHidden" class="btn btn-success">pokaz szczegolowe dane</button>
        <h3>Komentarze <b>({{$post->comments()->count()}})</b></h3>
        <table class="table">
            <thead>
                <tr>
                    <th>id</th>
                    <th>comment_parent</th>
                    <th>Autor</th>
                    <th>Email</th>
                    <th>Komentarz</th>
                    <th class="hiddenData">Ip</th>
                    <th class="hiddenData">Data</th>
                    <th width="70px">Akcja</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($post->comments as $comment)
                <tr @if($comment->deleted_at!=null) {{'style=background-color:red;color:white'}} @endif>
                     <th>{{$comment->id}}</th>
                    <th>{{$comment->comment_parent}}</th>
                    <th>{{$comment->comment_author}}</th>
                    <th>{{$comment->comment_author_email}}</th>
                    <th>{{$comment->comment_content}}</th>
                    <th class="hiddenData" >{{$comment->comment_author_ip}}</th>
                    <th class="hiddenData" >{{$comment->comment_agent}}</th>
                    <th width="70px">
                        <a class="btn btn-primary" href="{{route('comment.edit', $comment->id) }}" onclick="return confirm('Napewno usunąć ?');">edycja</a>
                        <form action="{{ URL::route('comment.destroy', $comment->id) }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            @csrf
                            <button class="btn btn-danger">usun</button>
                        </form>
                    </th>
                </tr>
                @endforeach
            </tbody>
        </table>
    </section>
</article>
<style>
    .hiddenData{display: none;}
    .block{display: table-cell;}
</style>

<script>
    document.getElementById('showHidden').addEventListener('click', function () {
        var hiddenData = document.querySelectorAll('.hiddenData');
        hiddenData.forEach(function (item) {
            item.classList.toggle('block');
        })
    })
</script>
@endsection