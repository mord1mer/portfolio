@extends('layouts.app')
@section('title', 'Tagi na blogu')
@section('content')
    @include('titlePage')
    @include('adminMenu')

    <article class="container-fluid grey-cont text-center">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('tags.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Nowy tag</a>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div> <!-- end of .row -->
        <div class="row">
            <div class="table-responsive">
                <table class="table text-center">
                    <thead>
                        <th>id</th>
                        <th>nazwa</th>
                        <th>slug</th>
                        <th>akcja</th>
                    </thead>

                    <tbody>
                    @foreach ($tags as $tag)
                        <tr>
                            <th>{{ $tag->id }}</th>
                            <th>{{ $tag->name }}</th>
                            <th>{{ $tag->slug }}</th>
                            <th>

                                    <form method="POST" action="{{ route('tags.destroy', $tag->id) }}"
                                          accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE">
                                        @csrf
                                        <input class="btn btn-danger" type="submit" value="Usun" onclick="return confirm('Napewno usunac ? usuniecie spowoduje usunieice tagu oraz tagow w postach');">
                                    </form>

                                    <a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-default btn-sm btn-primary">Edycja</a>

                            </th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </article>
@endsection
@section('scripts')
@endsection
