@extends('layouts.app')
@section('title', 'Edycja taga')
@section('content')
@include('titlePage')
@include('adminMenu')
<article class="container-fluid grey-cont">
    <div class="container kolor">
        <form action="{{ route('tags.update',$tag->id) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            <input name="_method" type="hidden" value="PUT">
            <input name="idTag" type="hidden" value="{{$tag->id}}">
            <div class="form-group">
                <label class="col-sm-2 control-label" for="name">Nazwa *</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="contakt100 form-control form-control-special"  placeholder="tutuł posta .. np jak zrobić..." id="name" value="{{$tag->name}}">
                </div>
                @if ($errors->has('name'))
                <div class="errormessage invalid-feedback"><strong>{{ $errors->first('name') }}</strong></div>
                @endif
            </div>
            <div class="form-group">
                <label for="showInPost">
                    <h3>Pokaz w wyszukiwaniu na blogu</h3>
                    <input type="checkbox" name="showInPost" id="showInPost" value="1" @if ($tag->showInPost==true){{'checked'}}@endif>
                    
                </label>
            </div>
            <div class="form-group">
                <label for="showInFastPost">
                    <h3>Pokaz w wyszukiwaniu w szybkich postach</h3>
                    <input type="checkbox" name="showInFastPost" id="showInFastPost" value="1" @if ($tag->showInFastPost==true){{'checked'}}@endif>
                </label>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary btn-lg btn-block pull-left" name="add-post" >zapisz</button>
                </div>
            </div>
        </form>
    </div>
</article>
@endsection
@section('scripts')
@endsection