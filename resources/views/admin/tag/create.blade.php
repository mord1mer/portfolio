@extends('layouts.app')
@section('title', 'Nowy tag')
@section('content')
    @include('titlePage')
    @include('adminMenu')
    <article class="container-fluid grey-cont">
        <div class="container kolor">
            <form action="{{ route('tags.store') }}" method="post" class="form-horizontal">
                @csrf
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">Nazwa *</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="contakt100 form-control form-control-special"
                               placeholder="nazwa tagu" id="name" value="{{ old('name') }}">
                    </div>
                    @if ($errors->has('name'))
                        <div class="errormessage invalid-feedback"><strong>{{ $errors->first('name') }}</strong></div>
                    @endif
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary btn-lg btn-block pull-left" name="add-post">
                            zapisz
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </article>
@endsection
@section('scripts')
@endsection
