@extends('layouts.app')
@section('title', 'Edycja szybkiego posta')
@section('content')
@include('titlePage')
@include('adminMenu')

<article class="container-fluid grey-cont">
    <div class="container kolor">
        <form action="{{ route('fastpost.update',$fastpost->id) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            <input name="_method" type="hidden" value="PUT">
            @include('admin.fastpost.fastPostForm')
        </form>
    </div>
</article>
@endsection

