@extends('layouts.app')
@section('title', 'Dodawanie posta')
@section('content')
@include('titlePage')
@include('adminMenu')
<article class="container-fluid grey-cont">
    <div class="container kolor">
        <form action="{{ route('fastpost.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @include('admin.fastpost.fastPostForm')
        </form>
    </div>
</article>
@endsection

