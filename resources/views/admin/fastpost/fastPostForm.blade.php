<div class="form-group">
    <label class="col-sm-2 control-label" for="title">Tytuł *</label>
    <div class="col-sm-10">
        <input type="text" name="title" class="contakt100 form-control form-control-special"  placeholder="tutuł posta .. np jak zrobić..." id="title" value="{{ old('title',  isset($fastpost->title) ? $fastpost->title : null) }}">
    </div>
    @if ($errors->has('title'))
        <div class="errormessage invalid-feedback">
            <strong>{{ $errors->first('title') }}</strong>
        </div>
    @endif
    <div id="title_text"></div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" for="fast_link">fast_link *</label>
    <div class="col-sm-10">
        <input type="text" name="fast_link" class="contakt100 form-control form-control-special"  placeholder="fast_link" id="fast_link" value="{{ old('fast_link',  isset($fastpost->fast_link) ? $fastpost->fast_link : null) }}">
    </div>
    @if ($errors->has('fast_link'))
        <div class="errormessage invalid-feedback">
            <strong>{{ $errors->first('fast_link') }}</strong>
        </div>
    @endif
    <div id="fast_link_text"></div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label text-left" for="content">POST Text:</label>
    <div class="col-sm-12">
        <textarea placeholder="text-posta" class="form-control custom-control form-control-special edytor" name="content" id="content" rows="50">{{old('content',isset($fastpost->content) ? $fastpost->content : null) }}</textarea>
    </div>
    <div class="clearfix"></div>
    @if ($errors->has('content'))
        <div class="errormessage invalid-feedback">
            <strong>{{ $errors->first('content') }}</strong>
        </div>
    @endif
    <div id="content_text"></div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label" for="publishedAt">publishedAt *</label>
    <div class="col-sm-10">
        <input type="text" name="publishedAt" class="contakt100 form-control form-control-special" id="publishedAt" value="{{ old('publishedAt',  isset($fastpost->publishedAt) ? $fastpost->publishedAt : date("Y-m-d H:i:s")) }}">
    </div>
    @if ($errors->has('publishedAt'))
        <div class="errormessage invalid-feedback">
            <strong>{{ $errors->first('publishedAt') }}</strong>
        </div>
    @endif
</div>
<div class="form-group">
    <h3><b>Tagi</b></h3>
    @foreach ($tags as $tag)
        <span class="checkbox-choice">
                <input type="checkbox" class="form-check-input" id="tag_{{$tag->id}}" value="{{$tag->id}}" name="tags[]"
                        @if(isset($selectTags))
                             @if(is_array($selectTags) && in_array($tag->id, $selectTags)) checked @endif
                        @else
                            @if(is_array(old('tags')) && in_array($tag->id, old('tags'))) checked @endif
                        @endif
                >
            <label for="tag_{{$tag->id}}"><span>x</span> {{$tag->name}}</label>
        </span>
    @endforeach
    @if ($errors->has('tags'))
        <div class="errormessage invalid-feedback">
            <strong>{{ $errors->first('tags') }}</strong>
        </div>
    @endif
</div>
<div class="form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary btn-lg btn-block pull-left" name="add-post" >zapisz</button>
    </div>
</div>
@section('scripts')
    <!-- edytor cms -->
    <!-- <script src="//cloud.tinymce.com/stable/tinymce.min.js"></script> -->
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=pfir6tw1aejvqvi10svdtwbptl0arnvh4lbkrpg55kumq548"></script>
    <script>
        $(window).on('load', function() {
            tinymce.init({
                selector: '#content',
                height: 500,
                theme: 'modern',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
                ],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
                image_advtab: true,
                templates: [
                    { title: 'Test template 1', content: 'Test 1' },
                    { title: 'Test template 2', content: 'Test 2' }
                ],
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'
                ],
            });
        });

        $.fn.extend( {
            limiter: function(limit, elem) {
                try {
                    $(this).on("keyup focus", function() {
                        //$('body').find(this).on("keyup focus", function() {
                        setCount(this, elem);
                    });
                    function setCount(src, elem) {
                        var chars = src.value.length;
                        if (chars > limit) {
                            src.value = src.value.substr(0, limit);
                            chars = limit;
                        }
                        elem.html(limit - chars);
                    };
                    setCount($(this)[0], elem);
                }catch(err) {
                    console.log(err.message);
                }
            }
        });

        $(window).on('load', function() {
            $("#title").limiter(100, $("#title_text"));
            $("#fast_link").limiter(255, $("#fast_link_text"));
            $("#description").limiter(278, $("#description_text"));
        });
    </script>
    <style>
        .checkbox-choice input[type="checkbox"] {
            display: none;
        }
        .checkbox-choice span {
            display: none;
        }
        .checkbox-choice label {
            border: 1px solid #177CA4;padding: 5px; cursor: pointer;border-radius: 5px;
        }
        .checkbox-choice input[type="checkbox"]:checked+label {
            background-color:#177CA4;color: white;
        }
        .checkbox-choice input[type="checkbox"]:checked+label span {
            display: inline-block;color: black;
        }
    </style>
@endsection