@include('titlePage')
@include('adminMenu')
<article class="container-fluid grey-cont">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('fastpost.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Nowy post</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </div> <!-- end of .row -->
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <th>id</th>
                <th>Tytuł</th>
                <th>Kontent</th>
                <th>Data utworzenia</th>
                <th></th>
                </thead>
                <tbody>
                @foreach ($fastPost as $post)
                    <tr>
                        <th>{{ $post->id }}</th>
                        <th>{{ $post->title }}</th>
                        <td>{{ substr(strip_tags($post->content), 0, 50) }}{{ strlen(strip_tags($post->content)) > 50 ? "..." : "" }}</td>
                        <td>{{ $post->publishedAt }}</td>
                        <td><a href="{{ route('fastpost.show', $post->id) }}" class="btn btn-default btn-sm">Podgląd</a> <a href="{{ route('fastpost.edit', $post->id) }}" class="btn btn-default btn-sm btn-primary">Edycja</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-center">
                {!! $fastPost->links(); !!}
            </div>
        </div>
    </div>
</article>