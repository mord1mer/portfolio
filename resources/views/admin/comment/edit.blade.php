@extends('layouts.app')

@section('title', 'Edycja Komentarza')

@section('content')
@include('titlePage')
@include('adminMenu')
<article class="container-fluid grey-cont">
    <div class="container kolor">
        <form action="{{ route('comment.update',$editComment->id) }}" method="post" class="form-horizontal"
              enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="form-group">
                <label class="col-sm-2 control-label" for="post_id">id posta</label>
                <div class="col-sm-10">
                    <input type="text" name="post_id" class="form-control form-control-special" id="post_id"
                           value="{{$editComment->post_id}}">
                </div>
                @if ($errors->has('post_id'))
                <div class="errormessage invalid-feedback">
                    <strong>{{ $errors->first('post_id') }}</strong>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="comment_author">autor </label>
                <div class="col-sm-10">
                    <input type="text" name="comment_author" class="form-control form-control-special"
                           id="comment_author" value="{{$editComment->comment_author}}">
                </div>
                @if ($errors->has('comment_author'))
                <div class="errormessage invalid-feedback">
                    <strong>{{ $errors->first('comment_author') }}</strong>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="comment_author_email">email autora</label>
                <div class="col-sm-10">
                    <input type="text" name="comment_author_email" class="form-control form-control-special"
                           id="comment_author_email" value="{{$editComment->comment_author_email}}">
                </div>
                @if ($errors->has('comment_author_email'))
                <div class="errormessage invalid-feedback">
                    <strong>{{ $errors->first('comment_author_email') }}</strong>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="comment_author_url">url autora</label>
                <div class="col-sm-10">
                    <input type="text" name="comment_author_url" class="form-control form-control-special"
                           id="comment_author_url" value="{{$editComment->comment_author_url}}">
                </div>
                @if ($errors->has('comment_author_url'))
                <div class="errormessage invalid-feedback">
                    <strong>{{ $errors->first('comment_author_url') }}</strong>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label text-left" for="comment_content">Text komentarza:</label>
                <div class="col-sm-12">
                    <textarea placeholder="text-posta"
                              class="form-control custom-control form-control-special edytor" name="comment_content"
                              id="comment_content" rows="5">{{$editComment->comment_content}}</textarea>
                </div>
                <div class="clearfix"></div>
                @if ($errors->has('comment_content'))
                <div class="errormessage invalid-feedback">
                    <strong>{{ $errors->first('comment_content') }}</strong>
                </div>
                @endif
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="comment_parent">Rodzidc komentarza(0 oznacza glowny
                    komentarz)</label>
                <div class="col-sm-10">
                    <input type="text" name="comment_parent" class="form-control form-control-special"
                           id="comment_parent" value="{{$editComment->comment_parent}}">
                </div>
                @if ($errors->has('comment_parent'))
                <div class="errormessage invalid-feedback">
                    <strong>{{ $errors->first('comment_parent') }}</strong>
                </div>
                @endif
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="comment_author_ip">IP</label>
                <div class="col-sm-10">
                    <input type="text" name="comment_author_ip" class="form-control form-control-special"
                           id="comment_author_ip" value="{{$editComment->comment_author_ip}}">
                </div>
                @if ($errors->has('comment_author_ip'))
                <div class="errormessage invalid-feedback">
                    <strong>{{ $errors->first('comment_author_ip') }}</strong>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="comment_agent">comment_agent</label>
                <div class="col-sm-10">
                    <input type="text" name="comment_agent" class="form-control form-control-special"
                           id="comment_agent" value="{{$editComment->comment_agent}}">
                </div>
                @if ($errors->has('comment_agent'))
                <div class="errormessage invalid-feedback">
                    <strong>{{ $errors->first('comment_agent') }}</strong>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="publishedAt">Opublikowany</label>
                <div class="col-sm-10">
                    <input type="text" name="publishedAt" class="form-control form-control-special" id="publishedAt"
                           value="{{$editComment->publishedAt}}">
                </div>
                @if ($errors->has('publishedAt'))
                <div class="errormessage invalid-feedback">
                    <strong>{{ $errors->first('publishedAt') }}</strong>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="deleted_at">Miekkie usuniecie</label>
                <div class="col-sm-10">
                    <input type="text" name="deleted_at" class="form-control form-control-special" id="deleted_at"
                           value="{{$editComment->deleted_at}}">
                </div>
                @if ($errors->has('deleted_at'))
                <div class="errormessage invalid-feedback">
                    <strong>{{ $errors->first('deleted_at') }}</strong>
                </div>
                @endif
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary btn-lg btn-block pull-left" name="add-post">
                        zapisz
                    </button>
                </div>
            </div>
        </form>
        <form action="{{ URL::route('comment.destroy', $editComment->id) }}" method="POST">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="forceDelete" value="DELETE">
            @csrf
            <button class="btn btn-danger btn-lg btn-block" onclick="return confirm('Napewno usunac?');">Usun na
                zawsze
            </button>
        </form>
    </div>
</article>
@endsection
