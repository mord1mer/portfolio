@component('mail::message')

@component('mail::panel')
<h3>Od: {{ $data['firstname'].' '.$data['lastname'] }}</h3>
@endcomponent

<div>
    Wiadomosc: {{ $data['message'] }}
</div>
Pozdrawiam ciepło,<br>
<h3>Grzesiek Tarka</h3>
@endcomponent