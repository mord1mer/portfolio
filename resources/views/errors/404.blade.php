@extends('layouts.app')
@section('title'){{'404-strona nie istnieje'}}@endsection
@section('description'){{'404-strona nie istnieje'}}@endsection
@section('content')
<div class="error-body">
    <div class="div-error">
        <h1 class="big-font">404</h1>
        <h1 class="big-medium">Nie ma takiej strony</h1>
        <a href="{{ route('home') }}" class="ajax sendmessage">STRONA GŁÓWNA</a>
    </div>
</div>
<audio id="my_audio" src="{{ asset('mp3/Space echo.mp3') }}" loop="loop"></audio>
@endsection
<style>
    .error-body {
        background: url('{{ asset('img/moonlight-703553_1920.jpg') }}') no-repeat fixed center;
        background-size:cover;
        height: 100vh
    }
    .big-font {
        font-size: 60px;
        color: #000;
        text-align: center;
        line-height: 20px
    }
    .big-medium {
        font-size: 30px;
        color: #000;
        text-align: center;
    }
    .sendmessage {
        padding: 10px 20px;
        color: #FFF;
        ;
        border: 2px solid #FFF;
        cursor: pointer;
        background-color: rgba(0, 0, 0, 0.2);
        text-decoration: none
    }
    .sendmessage:hover {
        color: black;
        background-color: #FFF;
    }
    .div-error {
        text-align: center;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 100%;
        text-align: center
    }
</style>

@section('scripts')
<script>
    window.onload = function () {
        document.getElementById("my_audio").play();
    }
</script>
@endsection