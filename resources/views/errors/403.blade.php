@extends('layouts.app')
@section('title'){{$exception->getMessage()}}@endsection
@section('description'){{$exception->getMessage()}}@endsection
@section('content')
    <article class="container-fluid over-hiden pd-zero title-post-img top-blog-bg">
        <section class="padding-post-img" style="background: rgba(0, 0, 0, 0.3)">
            <div class="container">
                <div class="row blog-post white-font">
                    <div class="col-lg-12">
                        <h1>403</h1>
                        <h2>{{ $exception->getMessage() }}</h2>
                    </div>
                </div>
            </div>
        </section>
    </article>
@endsection