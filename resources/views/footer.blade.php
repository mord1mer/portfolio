<footer class="container-fluid footer text-center" id="footer">
    <div class=container>
        <div class="col-lg-12 wow zoomInUp" data-wow-delay=0.2s>
            <figure><img src="{{ asset('img/svg/laptop.svg') }}" alt="Ikona laptopa" class=svg-footer></figure>
        </div>
        <div class="col-lg-12 pd-zero">
            <a href="https://www.facebook.com/sharer/sharer.php?u={{route('home')}}" class="icon-link jeden wow rotateIn"
               data-wow-delay=1s>
                <i class="fab fa-facebook-f fa-facebook circle-icon" aria-hidden=true></i>
            </a>
            <a class="icon-link wow rotateIn" data-wow-delay=1s href="https://twitter.com/home?status={{route('home')}}">
                <i class="fab fa-twitter circle-icon" aria-hidden=true></i>
            </a>
            <a class="icon-link wow rotateIn" data-wow-delay=1s href="{{route('home')}}">
                <i class="fab fa-instagram circle-icon" aria-hidden=true></i>
            </a>
            <a class="icon-link wow rotateIn" data-wow-delay=1s href="https://plus.google.com/share?url={{route('home')}}">
                <i class="fab fa-google-plus-g fa-google-plus circle-icon" aria-hidden=true></i>
            </a>
        </div>
        <div class="col-lg-12 white-font-09">
            <h3>COPYRIGHT INSTRMNT LIMITED 2021</h3>
            <h5>Powered by <a href="https://pl.wikipedia.org/wiki/AJAX">AJAX!</a></h5>
        </div>
    </div>
</footer>