<article class="container-fluid pd-tp-bt-0">
    <div class="row ligh-bg blog">
        <div class="col-lg-12 white-font text-center">
            <h1>@yield('title')</h1>
            @if (Auth::check() && isset($post))
            <br>
            <div class="row text-center">
                <a class="sendmessage_button" target="_blank"  href="{{ route('post.edit', $post->id) }}">edytuj</a>
            </div>
            @endif
        </div>
    </div>
</article>
