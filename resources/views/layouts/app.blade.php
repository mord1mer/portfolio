<!DOCTYPE html>
<html lang="{{ app()->getLocale('pl') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <meta name='description' content='@yield('description')' />
    <meta name="author" content="Grzegorz Tarka" lang="pl">
    @yield('metaImage')
    <style>.load-container{width: 100%;height: 100vh;position: relative;position: fixed;background:rgba(0, 0, 0, 0.8) top left repeat;z-index: 10000;display: none;}  .top-50 {position: relative;top: 50%;top: calc(50% - 30px);}
        .start-load-container{background:white top left repeat;display: block;}  .load-container .loadersmall {border: 5px solid #f3f3f3;border-top: 5px solid transparent;-webkit-animation: spin 2s linear infinite;animation: spin 2s linear infinite;border-radius: 50%;width: 60px;height: 60px;margin: auto;}  @keyframes spin { 0% { transform: rotate(0deg); } 40% { transform: rotate(100deg); } 70% { transform: rotate(300deg); } 100% { transform: rotate(720deg); } }  .start-load-container .loadersmall{border: 5px solid #f3f3f3;border-top: 5px solid #333;}</style>
</head>
<body id="body">
<aside><div class="load-container start-load-container"><div class="top-50"><div class="loadersmall"></div></div></div><a href="#body" class="scrollup smoothScroll"><i class="far fa-caret-square-up fa-4x"></i></i></a></aside>
@if(session('message'))
    <div id="myModal" class="modal fade" role="dialog" style="display: block">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Wiadomość</h4>
          </div>
          <div class="modal-body">
            <p>{{session('message')}}</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
@endif
<div id="app">
    @include('header')
    <main class="py-4">@yield('content')</main>
    @include('footer')
</div>
<link href="{{ asset('css/app.css?v=0002') }}" rel="stylesheet">
<script src="{{ asset('js/app.js?v=0002') }}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Aldrich|Noticia+Text|Press+Start+2P" rel="stylesheet" lazyload>
<script type="text/javascript">$('#myModal').modal({backdrop: 'static', keyboard: false})</script>
@yield('scripts')
<script>$(window).bind("load", function() {$( ".load-container" ).animate({opacity: 0,}, 1000, function() {$('.load-container').removeClass('start-load-container').css('opacity',1);});});</script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</body></html>