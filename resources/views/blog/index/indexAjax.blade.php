<article class="container-fluid over-hiden pd-zero title-post-img top-blog-bg">
    <section class="padding-post-img" style="background: rgba(0, 0, 0, 0.3)">
        <div class="container">
            <div class="row blog-post white-font">
                <div class="col-lg-12">
                    <h1 style="margin-bottom: 40px">
                        <div class="black-bg-post">BLOG</div>
                    </h1>
                </div>
            </div>
        </div>
    </section>
</article>
<article class="container-fluid bg-white pd-zero">
    <div class="container">
        <div class="row">
            <h3>Witaj na blogu poświeconym programowaniu i nowoczesnym technologiom.</h3>
        </div>
        <div class="row">
            <article data-mh="my-group" class="col-lg-8 pd-top-20 blog_right_shadow">
                @include('blog.index.include.posts')
            </article>

            <aside data-mh="my-group" class="col-lg-4 pd-top-bt-50px shadowOnRight" id="sidbar-right">
                @include('blog.index.include.aside')
            </aside>
        </div>
    </div>
</article>
<script>
    function watch() {var secondsHand = document.querySelector('.seconds'), minutesHand = document.querySelector('.minutes'), hoursHand = document.querySelector('.hours'), date = new Date();var secs = date.getSeconds(), mins = (date.getMinutes() * 60) + secs, hours = (date.getHours() * 3600) + mins;secondsHand.style.animationDelay = '-' + secs + 's';minutesHand.style.animationDelay = '-' + mins + 's';hoursHand.style.animationDelay = '-' + hours + 's';}watch();
</script>