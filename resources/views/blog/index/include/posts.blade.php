@if(isset($slug))
<ul class="list-group" style="margin-bottom:5px!important">
    <li class="list-group-item list-group-item active">Szukasz po {{$slug['searchTitle']}}: <b>{{$slug['name']}}</b></li>
</ul>
@endif
<aside>
    <ul class="list-group">
        @if ($posts->total() > 0)
            <li class="list-group-item list-group-item active"><b>Ilość rekordów w bazie: {{ $posts->total() }}</b></li>
        @else
            <li class="list-group-item list-group-item-danger custom-item"><b>BRAK WYNIKÓW WYSZUKIWANIA</b></li>
        @endif
    </ul>
</aside>
@foreach ($posts as $post)
    <article class="blog-sidebar-left">
        <header>
            <div class="vertical_align">
                <div class="col-lg-4">
                    <h5 class="date-h text-left text-center-md"><i class="fas fa-pencil-alt fa-1x radius-blog" aria-hidden="true"></i></h5>
                </div>
                <div class="col-lg-8 text-right text-center-md">
                    @foreach ($post->categories as $category)
                        <span class="blog-category">{{$category->name}}</span>
                    @endforeach
                </div>
            </div>
            <div class="vertical_align">
                <div class="col-lg-6">
                    <h5 class="date-h text-left text-center-md"><i class="far fa-clock red-font fa-2x pd-5px" aria-hidden="true"></i>{{date('Y-m-d', strtotime($post->publishedAt))}}</h5>
                </div>
                <div class="col-lg-6 text-right text-center-md">
                    <h5 class="date-h  text-center-md"><i class="fa fa-user red-font fa-2x pd-5px" aria-hidden="true"></i><strong>Autor: {{$post->author->name}}</strong></h5>
                </div>
            </div>
        </header>
        <h3 class="text-left text-longshadow">
            <a class="title-link ajax" href="{{route('blog.single',$post->slug)}}">{{$post->title}}</a>
        </h3>
        @if($post->img)
            <figure>
                @if(filter_var($post->img, FILTER_VALIDATE_URL))
                    <img src="{{asset($post->img)}}" class="img-responsive my-img">
                @else
                    <img src="{{asset($post->img)}}" class="img-responsive my-img">
                @endif
            </figure>
        @endif
        <div class="col-lg-12 text-left pd-zero">{!! $post->description !!}</div>
        <div class="col-lg-12">
            <footer class="row align-items-center" >
                <div class="col-lg-6 pd-zero text-left pd-top-bt-20 text-center-md">
                    <a class="blok-link ajax" href="{{route('blog.single',$post->slug)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>WIĘCEJ</a>
                </div>
                <div class="col-lg-6 pd-top-bt-20 text-right text-center-md">
                    <a title="Opublikuj na facebooku!" href="https://www.facebook.com/sharer/sharer.php?u={{route('blog.single',$post->slug)}}" class="icon-link jeden">
                        <i class="fab fa-facebook-f fa-facebook circle-icon" aria-hidden="true"></i>
                    </a>
                    <a title="Opublikuj na Twitterze!" href="https://twitter.com/home?status={{route('blog.single',$post->slug)}}" class="icon-link dwa">
                        <i class="fab fa-twitter circle-icon" aria-hidden="true"></i>
                    </a>
                    <a title="Opublikuj na W Google+" href="https://plus.google.com/share?url={{route('blog.single',$post->slug)}}" class="icon-link dwa">
                        <i class="fab fa-google-plus-g fa-google-plus circle-icon" aria-hidden="true"></i>
                    </a>
                </div>
            </footer>
        </div>
    </article>
@endforeach
{{ $posts->appends(request()->input())->links() }}