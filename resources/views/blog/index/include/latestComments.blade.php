@foreach ($latestComments as $comment)
    <article class="col-lg-12 text-left pd-zero">
        <div class="panel panel-white post panel-shadow">
            <header>
                <div class="post-heading">
                    <div class="pull-left image">
                        <img src="{{asset('img/avatar/8eb1b522f60d11fa897de1dc6351b7e8.jpg')}}" class="img-circle rounded-circle avatar" alt="user profile image">
                    </div>
                    <div class="pull-left meta">
                        <div class="title h5">
                            <a class="text-capitalize" target="_blank"><b>{{$comment->comment_author}}</b></a>
                        </div>
                        <h6 class="text-muted time">{{$comment->publishedAt}}</h6>
                    </div>
                </div>
            </header>
            <div class="post-description pd-top-0px">
                <p>{{\Illuminate\Support\Str::limit($comment->comment_content,100,'...')}}</p>
                <a class="btn btn-info ajax " href="{{route('blog.single',$comment->slug)}}">Przejdz do posta</a>
            </div>
        </div>
    </article>
@endforeach