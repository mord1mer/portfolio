<h2 class="widget-title text-left">Wyszukiwarka</h2>
<form method="get" id="searchForm" action="{{route('blog')}}">
    <div class="input-group">
        <input name="title" type="text" value="@if(isset($searchTitle)) {{$searchTitle}} @endif" id="title"
            class="form-control form-control-special" placeholder="Szukaj..." required>
        <span class="input-group-btn">
            <button id="searchButton" type="submit" class="btn btn-default btn-custom">
                <span><i class="fas fa-search"></i></span>
            </button>
        </span>
    </div>
</form>
<h2 class="widget-title text-left">Kategorie</h2>
@foreach ($categories as $category)
<a class="blog-link2 ajax" href="{{route('blogCategory',$category->slug)}}">#{{$category->name}}</a>
@endforeach
<div class="clearfix"></div>
<h2 class="widget-title text-left">Najnowsze wpisy</h2>
<div class="clearfix"></div>
<div class="panel panel-white post panel-shadow">
    <div class="clock">
        <div class="hand seconds animate"></div>
        <div class="hand minutes animate"></div>
        <div class="hand hours animate"></div>
    </div>
</div>
<h2 class="widget-title text-left">Ostatnie komentarze</h2>
<div class="clearfix"></div>
<div class="col-lg-12 pd-zero">
    <!--  <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&tabs=timeline&width=280&height=280&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="280" height="280" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe> -->
    @include('blog.index.include.latestComments')
</div>
<div class="clearfix"></div>
<h2 class="widget-title text-left">Tagi</h2>
@foreach ($tags as $tag)
<a class="blog-link2 ajax" href="{{route('blogTag',$tag->slug)}}">#{{$tag->name}}</a>
@endforeach