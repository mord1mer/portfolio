<article class="container-fluid over-hiden pd-zero title-post-img" @if($post->img)
    @if(filter_var($post->img, FILTER_VALIDATE_URL))
    style="background-image: url('{{$post->img}}');"
    @else
    style="background-image: url('{{asset($post->img)}}');"
    @endif
    @else
    style="background-color: #177CA4"
    @endif
    >
    <section class="padding-post-img">
        <div class="container">
            <div class="row blog-post white-font">
                <div class="col-lg-12">
                    <h3 class="wow bounceInDown" style="margin-bottom: 20px" data-wow-delay="0.5s">
                        <div class="black-bg-post">{{$post->title}}</div>
                    </h3>
                    {{--<a target="_blank" class="sendmessage line-height-50" href="" >edytuj full text</a><br>--}}
                </div>
                <div class="col-lg-6">
                    <h5 class="text-left text-center-md wow slideInLeft" data-wow-delay="0.2s"
                        style="margin-bottom: 20px">
                        <div class="black-bg-post"><b>{{$post->publishedAt}}</b></div>
                    </h5>
                </div>
                <div class="col-lg-6">
                    <h5 class="text-right text-center-md wow slideInRight" data-wow-delay="0.2s"
                        style="margin-bottom: 20px">
                        <div class="black-bg-post"><b>Autor: {{$post->author->name}}</b></div>
                    </h5>
                </div>
                @if (Auth::check() && isset($post))
                    <div class="col-lg-12">
                        <a class="sendmessage_button" target="_blank"  href="{{ route('post.edit', $post->id) }}">edytuj</a>
                    </div>
                @endif
            </div>
        </div>
    </section>
</article>
<article style="background-color: white">
    {!!$post->content!!}
</article>
<article class="container-fluid bg-white border-top-2px pd-zero">
    <div class="container">
        <div class="row" style="padding: 15px">
            <header><span>kategorie</span></header>
            @foreach($post->categories as $cat)
            <a href="{{route('blogCategory',$cat->slug)}}" target="_blank" class="btn btn-secondary ajax"
                style="background-color: #5a6268;color:white">{{ $cat->name }}</a>
            @endforeach
        </div>
    </div>
</article>
<article class="container-fluid bg-white border-top-2px pd-zero">
    <div class="container">
        <div class="row" style="padding: 15px">
            <header><span>tagi</span></header>
            @foreach ($post->tags as $tag)
            <a href="{{route('blogTag',$tag->slug)}}" target="_blank" class="btn btn-secondary ajax"
                style="background-color: #5a6268;color:white">{{ $tag->name }}</a>
            @endforeach
        </div>
    </div>
</article>
<article class="container-fluid bg-white border-top-2px pd-zero">
    <div class="container">
        <div class="row">
            <article>
                <div class="col-lg-12 vertical-align-b">
                    <div class="col-lg-6 pd-zero text-left pd-top-bt-20 text-center-md">
                        <h3 style="margin-top: 10px">Opublikuj</h3>
                    </div>
                    <div class="col-lg-6 pd-zero pd-top-bt-20 text-right text-center-md">
                        <a title="Opublikuj na facebooku!"
                            href="https://www.facebook.com/sharer/sharer.php?u={{route('blog.single',$post->slug)}}"
                            class="icon-link jeden"><i class="fab fa-facebook-f fa-facebook circle-icon"
                                aria-hidden="true"></i>
                        </a>
                        <a title="Opublikuj na Twitterze!"
                            href="https://twitter.com/home?status={{route('blog.single',$post->slug)}}"
                            class="icon-link dwa"><i class="fab fa-twitter circle-icon" aria-hidden="true"></i>
                        </a>
                        <a title="Opublikuj w Google+"
                            href="https://plus.google.com/share?url={{route('blog.single',$post->slug)}}"
                            class="icon-link dwa"><i class="fab fa-google-plus-g fa-google-plus circle-icon"
                                aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </article>
        </div>
    </div>
</article>
@if(true === $post->disableComments)
    <article class="container-fluid bg-white border-top-2px">
        <div class="container">
            <div class="errormessage"><strong>Komentarze wyłączone</strong></div>
        </div>
    </article>
@elseif(true === $post->justShowComments)
    <article class="container-fluid bg-white border-top-2px">
        <div class="container">
            <div class="errormessage"><strong>Dodawanie komentarzy wyłączone</strong></div>
        </div>
    </article>
    @include('blog.singlePost.comments')
@elseif(true === $post->justAddingComments)
    <article class="container-fluid bg-white border-top-2px">
        <div class="container">
            <div class="errormessage"><strong>Możesz tylko dodawać komentarze</strong></div>
        </div>
    </article>
    @include('blog.singlePost.comments')
@else
    @include('blog.singlePost.comments')
@endif
<script>
    if (typeof Prism !== "undefined") {
        Prism.highlightAll();
    }
</script>