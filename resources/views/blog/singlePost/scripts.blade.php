<style type="text/css">
    .anser-laoder-container {
        display: none
    }

    .comments {
        padding-left: 1em
    }

    .comments li {
        list-style: none
    }

    #comments ul {
        padding-left: 1em;
    }

    #comment-message {
        background-color: #189a18;
        color: white;
        border-radius: 5px;
        margin: 10px auto;
        padding: 5px;
        display: none
    }
</style>
<script type="text/javascript" src="{{asset('js/post.js')}}"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript">
    function postReply(commentId){
        document.getElementById('comment_parent').value = commentId;
        document.getElementById('add-comment-container').style.display = "block";
        document.getElementById('comment_author').focus();
    }
</script>