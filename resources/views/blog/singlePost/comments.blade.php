@if(false === $post->justShowComments)
    <article class="container-fluid bg-white " id="add-comment-container" style="display: none;">
        <div class="container">
            <div class="row">
                <form action="{{route('comment.store')}}" method="post" class="form-horizontal" id="add-comment">
                    @csrf
                    <input type="hidden" name="post_id" value="{{$post->id}}" id="post_id">
                    <div id="post_id_error" class="ajax-error"></div>
                    <input type="hidden" name="comment_parent" value="{{ old('comment_parent', '0') }}" id="comment_parent">
                    <div id="comment_parent_error" class="ajax-error"></div>
                    <div class="form-group">
                        <label class="col-sm-12 control-label" for="comment_author">Imie/nick (wymagane)</label>
                        <div class="col-lg-6 col-lg-offset-0 col-xs-12">
                            <input type="text" name="comment_author" class="form-control form-control-special"
                                placeholder="Wpisz imie" id="comment_author" value="{{old('comment_author')}}" required>
                            <div id="comment_author_error" class="ajax-error"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 control-label control-label" for="comment_author_email">Email (nie będzie
                            widoczny) (wymagane)</label>
                        <div class="col-lg-6 col-lg-offset-0 col-xs-12">
                            <input name="comment_author_email" class="form-control form-control-special"
                                placeholder="Wpisz email" id="comment_author_email" value="{{old('comment_author_email')}}"
                                type="email" required>
                            <div id="comment_author_email_error" class="ajax-error"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 control-label" for="comment_author_url">Adres www(opcja)</label>
                        <div class="col-lg-6 col-lg-offset-0 col-xs-12">
                            <input type="text" name="comment_author_url" class="form-control form-control-special"
                                placeholder="url" id="comment_author_url" value="{{old('comment_author_url')}}" required>
                            <div id="comment_author_url_error" class="ajax-error"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 control-label" for="comment_content">Komentarz</label>
                        <div class="col-sm-12">
                            <textarea placeholder="Treść komentarza..."
                                class="form-control custom-control form-control-special" name="comment_content"
                                id="comment_content" rows="10" required=""
                                data-error="#pertext">{{old('comment_content')}}</textarea>
                            <div id="comment_content_error" class="ajax-error"></div>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center!important;margin:auto!important">
                        <div class="g-recaptcha" data-sitekey="{{config('app.GOOGLE_RECAPTCHA_KEY')}}"></div>
                        <input type="hidden" class="hiddenRecaptcha required" name="captcha" id="hiddenRecaptcha"
                            data-error="#percaptcha">
                        <div id="g-recaptcha-response_error" class="ajax-error"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div id="comment-message">Komentarz został dodany!</div>
                            <button type="submit" id="add-comment-submit" class="blok-link pull-left"
                                name="edit-text-blog-submit" value="add-comment"><i class="fa fa-pencil-square-o"
                                    aria-hidden="true"></i> Dodaj komentarz
                                <i class='fas fa-circle-notch fa-spin' style='font-size:20px;opacity: 0;'
                                    id="add-comment-loader"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </article>
@endif
<article class="container-fluid bg-white pd-zero border-top-2px" style="" id="comments-container">
    <div class="container">
        <div class="row">
            <div id="postCommentsPage" style="display: none;">1</div>
            <div class="vertical_align">
                <header class="col-lg-6 text-left text-center-md pd-bt-30">
                    <h2>Komentarze
                        <span class="fa-stack">
                            <i class="fa fa-comment fa-stack-2x"></i>
                            <strong class="fa-stack-1x fa-stack-text fa-inverse" style="font-size: 14px"
                                amount="{{ $post->number_of_comments }}"
                                id="comment-lenght">{{ $post->number_of_comments }}</strong>
                        </span>
                    </h2>
                </header>
                <div class="col-lg-6 text-right text-center-md pd-bt-30">
                    @if(false === $post->justShowComments)
                        <button type="button" class="btn btn-info btn-lg" data-target=".myModalHorizontal" id="mainComment"
                            onclick="postReply(0)">Dodaj komentarz</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</article>

@if(false === $post->justAddingComments)
    <div id="post-comments-url" link="{{route('comment.getpostcomments',$post->id)}}" style="display: none;">
        {{route('comment.getpostcomments',$post->id)}}</div>
    <article class="container-fluid bg-white pd-zero border-top-2px">
        <div class="container" id="comments-list-container">
            <div class="row">
                <section class="comments" id="ansvers-0"></section>
            </div>
        </div>
    </article>
    <div id="comments-start-loading"></div>
    <article class="container-fluid bg-white" style="display: none" id="comment-loader-container">
        <i class='fas fa-circle-notch fa-spin' style='font-size:54px;color:#0074D9'></i>
        <h6>Ładowanie komentarzy...</h6>
    </article>
    <article class="container-fluid bg-white border-top-2px" style="display: none" id="comments-error-container">
        <div class="container errormessage">
            <strong>Ups cos poszlo nie tak komentarze nie mogly byc zaladowane</strong></div>
        </div>
    </article>
@endif

@include('blog.singlePost.scripts')