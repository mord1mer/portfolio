@extends('layouts.app')
@section('content')
@section('title'){{$post->title}}@endsection
@section('description'){{$post->description}}@endsection
@section('metaImage')<meta property="og:image" content="http://grzesiekneo2.ct8.pl/img/upload/6/titleimg/laravel-5.5.jpg">@endsection
@include('blog.singlePost.singlePostAjax')
@endsection
