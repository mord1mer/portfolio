function getPageWithAjax(url, saveHistory = true) {
    $('.load-container').show();
    $.ajax({
        url: url,
        dataType: "json",
        contentType: 'application/json',
    }).done(function (response) {
        $('html, body').animate({ scrollTop: 0 }, 250);
        document.title = response.title;
        document.querySelector('meta[name="description"]').setAttribute("content", response.description);
        //====================================================================================================
        if ($(".menu-opener-inner").hasClass("active")) {
            $('.nav').removeClass('black-nav');
            $(".menu-opener-inner").removeClass("active");
            $(".nav-custom").removeClass("nav-overflow");
        }
        //====================================================================================================
        $('.py-4').html(response.content);
        $('.load-container').hide();

        if (saveHistory) {
            window.history.pushState("", "", url);
        }
    }).fail(function () {
        alert('strona nie mogla byc wczytana ');
    });

    // axios.get(url)
    //     .then(function (response) {
    //         $('html, body').animate({scrollTop: 0}, 250);
    //         document.title = response.data.title;
    //         document.querySelector('meta[name="description"]').setAttribute("content", response.data.description);
    //         //====================================================================================================
    //         if ($( ".menu-opener-inner" ).hasClass("active")) {
    //             $('.nav').removeClass('black-nav');
    //             $(".menu-opener-inner").removeClass("active");
    //             $(".nav-custom").removeClass("nav-overflow");
    //         }
    //         //====================================================================================================
    //         $('.py-4').html(response.data.content);
    //         $('.load-container').hide();
    //         window.history.pushState("", "", url);
    //     }).catch(function (error) {
    //         alert('strona nie mogla byc wczytana ');
    //     });
}

document.addEventListener('DOMContentLoaded', function () {
    // $('body').on('click', '.ajax,.page-link', function(e) {
    //      e.preventDefault();
    //      var url = $(this).attr('href');
    //      console.log(url);
    //      getPageWithAjax(url);
    //      window.history.pushState("", "", url);
    //  });
    document.body.addEventListener('click', function (e) {
        if (e.target.classList.contains("ajax") || e.target.classList.contains("page-link")) {
            e.preventDefault();
            const url = e.target.getAttribute('href');
            getPageWithAjax(url);
        }

        if (e.target.id == 'searchButton') {
            e.preventDefault();
            const searchWords = this.querySelector('#title').value;
            const action = this.querySelector('#searchForm').action;
            const url = action + '?title=' + searchWords;
            getPageWithAjax(url);
        }
    });

    if (window.history && window.history.pushState) {
        //window.history.pushState('forward', null, './#forward');
        $(window).on('popstate', function () {
            getPageWithAjax(window.location.href, false);
        });
    }
});