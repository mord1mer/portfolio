$(window).on("scroll", function () {
    if ($(window).scrollTop() > 80) {
        $(".scrollup").addClass("block animated fadeInUp");
        $('.nav-custom').addClass('sticky');
        return;
    }

    $(".scrollup").removeClass("block animated fadeInUp");
    $('.nav-custom').removeClass('sticky');
});

$(document).on('focusin', function (e) {
    if ($(e.target).closest(".mce-window").length) {
        e.stopImmediatePropagation();
    }
});

$(window).on("load ajaxComplete", function () {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
        var target = $(this).attr('href');
        $(target).css('left', '-' + $(window).width() + 'px');
        var left = $(target).offset().left;
        $(target).css({ left: left }).animate({ "left": "0px" }, "10");
    });

    $('.to-footer').css('minHeight', $('html').outerHeight() - ($('.footer').outerHeight()));

    $(".dropdown").on("shown.bs.dropdown", function () {
        $(".dropdown-menu").addClass("animated bounceInDown").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $(".dropdown-menu").removeClass("animated bounceInDown");
        });
    });

    $('#demo').typewrite({
        'delay': 150,
        'extra_char': '<span class="jump">|</span>',
        'trim': true,
        'callback': null
    });

    // setTimeout(function () {
    //     $.fn.matchHeight._apply('.equal_columns');
    //     $.fn.matchHeight._apply('.equal-2');
    // }, 500);
});

$('body').on('click', '.smoothScroll', function (e) {
    e.preventDefault(e);
    e.stopImmediatePropagation();
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html,body').animate({ scrollTop: target.offset().top - 80 }, 600);
            return false;
        }
    }
});

$(".menu-opener").on("click", function (e) {
    e.preventDefault(e);
    $('.nav').toggleClass('black-nav');
    $(".menu-opener-inner").toggleClass("active");
    $(".nav-custom").toggleClass("nav-overflow");
});


// $(document).bind('load ajaxComplete', function () {
//     $('#div').css({'height': (($(window).height())) + 'px'});
//     setTimeout(function () {
//         $('.height-100').height($(window).height());
//     }, 1500);
// });

function Round(n, k) {
    var factor = Math.pow(10, k + 1);
    n = Math.round(Math.round(n * factor) / 10);
    return n / (factor / 10);
}

function arrayNavigation(array, key) {
    var len = array.length;
    var data = [];
    key = parseInt(key);
    data['current'] = key;
    data['prev'] = (key + len - 1) % len;
    data['next'] = (key + 1) % len;
    return data;
}

function randArrayKey(mp3array) {
    return Math.floor(Math.random() * mp3array.length)
}

function timer(numberOfSeconds) {
    var mins = Math.floor(numberOfSeconds / 60);
    var secs = Math.floor(numberOfSeconds % 60);
    if (secs < 10) {
        secs = '0' + String(secs);
    }
    return mins + ':' + secs;
}

function mp3Player(mp3Handler) {
    let activeMp3Key = 0;
    const mp3UrlLocation = window.location.origin + '/mp3/';;

    if ("volume" in localStorage) {
        setVolumeData(localStorage.getItem("volume"));
    }

    const mp3Array = [
        '1.main title) days of thunder original score by hans zimmer.mp3',
        'dash berlin feat. emma hewitt -  waiting (official music video).mp3',
        'royksopp – here she comes again(dj antonio remix).mp3',
        'armin van buuren feat. sharon den adel - in and out of love (lost frequencies remix).mp3',
        'wolf larsen - if i be wrong.mp3',
        'C-bool Feat. Giang Pham - DJ Is Your Second Name (Radio Edit) - [www.nowosci-mp3.eu].mp3',
        'C-BooL - Magic Symphony (feat. Giang Pham).mp3',
        'Alan Walker - The Spectre.mp3',
        'Tina Turner - Simply The Best (Remix)  Shuffle Dance Video.mp3',
        'COLDPLAY live 2012 - Yellow (intro piano) - HD.mp3',
        'Enya - Gladiator Theme - Now We Are Free.mp3',
        'Westbam - Agharta Banging Bones Remix.mp3',
        'Roxette - Dressed for Success.mp3',
        'FACEIT Theme (Original).mp3',
        'Suggs-Cecilia.mp3',
        'Kath Bloom  Come Here (2005).mp3',
        'HIT 2018 - Sara Perche Ti Amo (GK Project Remix).mp3'
    ];

    mp3Array.sort();

    function setVolumeData(volume) {
        $('#volume-info').text(volume);
        mp3Handler.volume = volume;
        localStorage.setItem('volume', volume);
    }

    function startMp3(mp3Handler) {
        total = timer(mp3Handler.duration);

        setInterval(function () {
            let info = timer(mp3Handler.currentTime);
            $('#mp3-time-info').text(info + '/' + total);
        }, 1000);

        mp3Handler.play();
    }

    function activateMp3(mp3Handler, number, scrollTo = true, resume = false) {
        if (!resume) {
            mp3Handler.src = mp3UrlLocation + mp3Array[number];
        }

        $('.mp3-list-item').removeClass('active-mp3')
        $('.eq').removeClass('eq--off').addClass('eq--on');
        $('#mp3_' + number).addClass('active-mp3');

        if (scrollTo) {
            const numberOfPixelsToScrool = $('#mp3_' + number).outerHeight() * number;
            $('.mp3-list').animate({ scrollTop: numberOfPixelsToScrool }, "fast");
        }

        if (!isNaN(mp3Handler.duration)) {
            startMp3(mp3Handler);
            return;
        }

        mp3Handler.addEventListener('loadedmetadata', function () {
            startMp3(mp3Handler);
        });
    }

    setInterval(function () {
        if ($('.eq').hasClass("eq--off")) {
            $('.change').addClass('animated infinite jello');
            setTimeout(function () {
                $('.change').removeClass("animated infinite jello");
            }, 1000);
        }
    }, 10000);

    (function () {
        let mp3ListHtml = '';

        for (var i = 0; mp3Array.length > i; i++) {
            mp3ListHtml += `<p mp3KeyNumber="${i}" id="mp3_${i}" class="mp3-list-item">${(i + 1) + ') ' + mp3Array[i]}</p>`;
        }

        $('.mp3-list').html(mp3ListHtml);
    })();

    $('.mp3-list-item').on("click", function () {
        activeMp3Key = $(this).attr('mp3KeyNumber');
        activateMp3(mp3Handler, activeMp3Key, false);
    });

    $(mp3Handler).on("ended", function () {
        activeMp3Key = randArrayKey(mp3Array);
        activateMp3(mp3Handler, activeMp3Key)
    });

    $(".btn-mp3").on("click", function () {
        if (mp3Handler.src.length === 0) {
            activeMp3Key = randArrayKey(mp3Array);
            mp3Handler.src = mp3UrlLocation + mp3Array[activeMp3Key];
        }

        if (mp3Handler.paused === false) {
            $('.eq').removeClass('eq--on').addClass('eq--off');
            mp3Handler.pause();
            return;
        }

        activateMp3(mp3Handler, activeMp3Key, true, true)
    });

    $('#mp3-prev').on("click", function () {
        activeMp3Key = arrayNavigation(mp3Array, activeMp3Key).prev;
        activateMp3(mp3Handler, activeMp3Key)
    });

    $('#mp3-next').on("click", function () {
        activeMp3Key = arrayNavigation(mp3Array, activeMp3Key).next;
        activateMp3(mp3Handler, activeMp3Key)
    });

    $('#mp3-rand').on("click", function () {
        activeMp3Key = randArrayKey(mp3Array);
        activateMp3(mp3Handler, activeMp3Key)
    });

    $('#mp3-volume-plus').on("click", function () {
        let volume = Round((mp3Handler.volume + 0.1), 1);

        if (volume >= 1) {
            volume = 1;
        }

        setVolumeData(volume);
    });

    $('#mp3-volume-minus').on("click", function () {
        let volume = Round((mp3Handler.volume - 0.1), 1);

        if (volume <= 0) {
            volume = 0;
        }

        setVolumeData(volume);
    });
}

mp3Player(document.getElementById('mp3Audio'));

$('body').on('click', '.scroll_to_content', function (e) {
    e.preventDefault(e);
    $('html,body').animate({
        scrollTop: $('#offer_container').offset().top - 80
    }, 600);
});

(function ($) {
    $.fn.typewrite = function (options) {
        var settings = {
            'selector': this,
            'extra_char': '',
            'delay': 100,
            'trim': false,
            'callback': null
        };
        if (options) $.extend(settings, options);
        function type_next_element(index) {
            var current_element = $(settings.selector[index]);
            var final_text = current_element.text();
            if (settings.trim) final_text = $.trim(final_text);
            current_element.html("").show();

            function type_next_character(element, i) {
                element.html(final_text.substr(0, i) + settings.extra_char);
                if (final_text.length >= i) {
                    setTimeout(function () {
                        type_next_character(element, i + 1);
                    }, settings.delay);
                }
                else {
                    if (++index < settings.selector.length) {
                        type_next_element(index);
                    }
                    else if (settings.callback) settings.callback();
                }
            }
            type_next_character(current_element, 0);
        }
        type_next_element(0);
        return this;
    };
})(jQuery);

// $(window).resize(function () {
//     $('#div').css({'height': (($(window).height())) + 'px'});
//     $.fn.matchHeight._apply('.equal_columns');
//     $.fn.matchHeight._apply('.equal-2');
//     $('.height-100').height($(window).height());
//     $('.to-footer').css('minHeight', $('html').outerHeight() - ($('.footer').outerHeight()));
// });