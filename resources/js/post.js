function isScrolledIntoView(elem) {
    //var elem = document.querySelector(elem);
    var docViewTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
    var docViewBottom = docViewTop + window.innerHeight;
    var elemTop = elem.offsetTop;
    var elemBottom = elemTop + elem.offsetHeight;
    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function getAjaxData(url) {
    //console.log('pobralem');
    const request = new XMLHttpRequest();
    return new Promise((resolve, reject) => {
        request.open('GET', url, true);
        request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

        request.onload = function () {
            if (request.status == 200) {
                const data = JSON.parse(this.response);
                resolve(data[0]);
            } else {
                reject(this.response);
            }
        };

        request.overrideMimeType("application/json");
        request.send();
    });
}

function getCommentErrorAction() {
    setTimeout(() => {
        document.querySelector('#comments-error-container').style.display = "block";
        document.querySelector('#comment-loader-container').style.display = "none";
    }, 1000);
}


function parseComments(responseData) {
    let commentsChunk = document.createElement('div');
    commentsChunk.className = "comments-chunk";

    for (let i = 0; i < responseData.length; i++) {
        let createdli = document.createElement('li');
        let comments = commentsTemplate(
            responseData[i]['id'],
            responseData[i]['comment_author'],
            responseData[i]['publishedAt'],
            responseData[i]['comment_content'],
            responseData[i]['comment_author_url'],
            responseData[i]['comment_author_email_md5'],
            responseData[i]['number_of_responses']
        );

        createdli.innerHTML = comments;
        commentsChunk.appendChild(createdli);
    }

    return commentsChunk;
}

function one(el, type, fn) {
    function handler(event) {
        el.removeEventListener(type, handler);
        fn(event);
    }

    el.addEventListener(type, handler);
}

window.commentsOffset = 0;
window.scroolComments = true;

function loadComments() {
    //console.log('nasluchuje');
    const commentsStartLoading = document.querySelector('#comments-start-loading');

    if (!commentsStartLoading) {
        //console.log('wywalam event !!!');
        return window.removeEventListener('scroll', loadComments);
    }

    if (isScrolledIntoView(commentsStartLoading) && window.scroolComments == true) {
        //console.log('Powienien go wywalić: ' + Date.now());
        window.scroolComments = false;
        //this.removeEventListener('scroll', arguments.callee);
        document.getElementById('comment-loader-container').style.display = "block";
        const url = document.querySelector('#post-comments-url').innerHTML + '/' + 0 + '/' + window.commentsOffset
        getAjaxData(url).then(response => {
            const numberOfGetComments = response.length;
            window.commentsOffset += numberOfGetComments;
            const commentsList = parseComments(response);
            document.getElementById('ansvers-0').appendChild(commentsList);
            document.querySelector('#comment-loader-container').style.display = "none";

            if (numberOfGetComments > 0) {
                //return window.addEventListener('scroll', loadComments);
                return window.scroolComments = true;
            }

            //console.log('wywalam event2222 !!!');
            return window.removeEventListener('scroll', loadComments);
        }).catch((error) => {
            getCommentErrorAction();
        });

        document.querySelector('#comments-container').style.display = "block";
    }

    return false;
}

window.removeEventListener('scroll', loadComments);
window.addEventListener("scroll", loadComments);
//one(window, 'scroll', loadComments);

function commentsTemplate(commentId, commentAuthor, publishedAt, commentContent, commentAuthorUrl, commentAuthorEmailMd5, numberOfResponses) {
    const img = getAvatarImg(commentAuthorEmailMd5);
    let url = '';
    commentAuthorUrl != null && commentAuthorUrl.length > 1 ? url = commentAuthorUrl : url = '';
    let comments = `<article id="comment-${commentId}">
        <div class="col-lg-12 text-left pd-zero">
            <div class="panel panel-white post panel-shadow">
                <header>
                    <div class="post-heading">
                        <div class="pull-left image">
                            <img src="${img}" class="img-circle avatar" alt="user profile image">
                        </div>
                        <div class="pull-left meta">
                            <div class="title h5">
                                <a class="text-capitalize" href="${url}" target="_blank"><b>${commentAuthor}</b></a>
                            </div>
                            <h6 class="text-muted time">${publishedAt}</h6>
                        </div>
                    </div>
                </header>
                <div class="post-description">
                    <p style="">${commentContent}</p>
                    <button class="test btn btn-primary" name="subject" type="submit"  onclick="postReply(${commentId})" data-toggle="modal" data-target=".myModalHorizontal">ODPOWIEDZ</button>
                </div>
            </div>
        </div>
    </article>
    <ul class="comments" id="ansvers-${commentId}"></ul>`;

    if (numberOfResponses > 0) {
        comments += `<div class="text-left"><a parentId="${commentId}" offset="0" numberOfResponsesToGet="${numberOfResponses}" class="btn show-answers">Pokaż odpowiedzi ${numberOfResponses}</a><div class="anser-laoder-container"><i class='fas fa-circle-notch fa-spin' style='font-size:24px;color:#0074D9'></i></div></div>`;
    }

    return comments;
}

function getAvatarImg(commentAuthorEmailMd5) {
    return "https://www.gravatar.com/avatar/" + commentAuthorEmailMd5 + "?d=mp";
}

function increaseNumberInfoOfAllComments(quantityToAdd = 1) {
    const commentLenght = document.getElementById('comment-lenght');
    const quantity = parseInt(commentLenght.innerHTML);

    commentLenght.innerHTML = quantity + quantityToAdd;
}

function showAnswer(event) {
    event.preventDefault();
    if (event.target.classList.contains("show-answers")) {
        const parentLink = event.target;
        const loader = parentLink.parentElement.querySelector('.anser-laoder-container');
        loader.style.display = "block"
        const numberOfResponsesToGet = parseInt(parentLink.getAttribute('numberOfResponsesToGet'));
        const parentId = parentLink.getAttribute("parentId");
        const offset = parseInt(parentLink.getAttribute("offset"));
        const url = document.querySelector('#post-comments-url').getAttribute('link') + '/' + parentId + '/' + offset;
        const parent = parentLink.parentElement.parentElement.querySelector('#ansvers-' + parentId);

        getAjaxData(url).then(response => {
            loader.style.display = "none"
            const numberOfResponsesInfo = numberOfResponsesToGet - response.length

            if (response.length > 0) {
                parentLink.setAttribute('numberOfResponsesToGet', numberOfResponsesInfo);
                parentLink.textContent = 'Pokaż jeszcze ' + numberOfResponsesInfo;
                const commentsHtml = parseComments(response);
                const newOfset = offset + response.length;
                parentLink.setAttribute("offset", newOfset);
                parent.appendChild(commentsHtml);
            }

            if (response.length == 0 || numberOfResponsesInfo <= 0) {
                const toDel = parentLink.parentElement;
                toDel.removeChild(toDel.childNodes[0]);
                return;
            }
        }).catch((error) => {
            getCommentErrorAction();
        });
    }
}


const commentsListContainer = document.getElementById('comments-list-container');

if (commentsListContainer) {
    commentsListContainer.addEventListener('click', showAnswer);
}

function clearCommentErrors(ajaxErrors) {
    for (i = 0; i < ajaxErrors.length; ++i) {
        ajaxErrors[i].innerHTML = "";
    }
}

const addCommentSubmit = document.getElementById('add-comment-submit');

if (addCommentSubmit) {
    addCommentSubmit.addEventListener('click', function (event) {
        event.preventDefault();
        const addCommentLoader = document.getElementById('add-comment-loader');
        addCommentLoader.style.opacity = "1";
        //const form = document.querySelector('form');
        const formData = $('form').serialize();
        document.querySelector('#comment-message').style.display = "none";
        const commentUrl = document.querySelector('#add-comment').action;
        const ajaxErrors = document.querySelectorAll('.ajax-error');

        axios.post(commentUrl, formData)
            .then(function (response) {
                clearCommentErrors(ajaxErrors);
                document.querySelector('#comment-message').style.display = "block";
                const commentHtml = parseComments([response.data.comment]);
                const commentParent = document.getElementById('comment_parent').getAttribute('value');
                const commentAnsversContainer = $('body').find('#ansvers-' + commentParent);
                commentAnsversContainer.append(commentHtml);

                increaseNumberInfoOfAllComments();
            })
            .catch(function (error) {
                if (error.response.status == 422) {
                    clearCommentErrors(ajaxErrors);
                    const errors = error.response.data.errors;
                    grecaptcha.reset();

                    Object.keys(errors).forEach(function (key) {
                        document.querySelector('#' + key + '_error').innerHTML = '<div class="errormessage"><strong>' + errors[key][0] + '</strong></div>'
                    });

                    // for (key in errors) {
                    //     document.querySelector('#' + key + '_error').innerHTML = '<div class="errormessage"><strong>' + errors[key][0] + '</strong></div>'
                    // }

                    return;
                }

                document.querySelector('#comments-error-container').style.display = "block";
            }).then(() => {
                addCommentLoader.style.opacity = "0";
            });
    });
}
// var MD5=function(s){function L(k,d){return(k<<d)|(k>>>(32-d))}function K(G,k){var I,d,F,H,x;F=(G&2147483648);H=(k&2147483648);I=(G&1073741824);d=(k&1073741824);x=(G&1073741823)+(k&1073741823);if(I&d){return(x^2147483648^F^H)}if(I|d){if(x&1073741824){return(x^3221225472^F^H)}else{return(x^1073741824^F^H)}}else{return(x^F^H)}}function r(d,F,k){return(d&F)|((~d)&k)}function q(d,F,k){return(d&k)|(F&(~k))}function p(d,F,k){return(d^F^k)}function n(d,F,k){return(F^(d|(~k)))}function u(G,F,aa,Z,k,H,I){G=K(G,K(K(r(F,aa,Z),k),I));return K(L(G,H),F)}function f(G,F,aa,Z,k,H,I){G=K(G,K(K(q(F,aa,Z),k),I));return K(L(G,H),F)}function D(G,F,aa,Z,k,H,I){G=K(G,K(K(p(F,aa,Z),k),I));return K(L(G,H),F)}function t(G,F,aa,Z,k,H,I){G=K(G,K(K(n(F,aa,Z),k),I));return K(L(G,H),F)}function e(G){var Z;var F=G.length;var x=F+8;var k=(x-(x%64))/64;var I=(k+1)*16;var aa=Array(I-1);var d=0;var H=0;while(H<F){Z=(H-(H%4))/4;d=(H%4)*8;aa[Z]=(aa[Z]|(G.charCodeAt(H)<<d));H++}Z=(H-(H%4))/4;d=(H%4)*8;aa[Z]=aa[Z]|(128<<d);aa[I-2]=F<<3;aa[I-1]=F>>>29;return aa}function B(x){var k="",F="",G,d;for(d=0;d<=3;d++){G=(x>>>(d*8))&255;F="0"+G.toString(16);k=k+F.substr(F.length-2,2)}return k}function J(k){k=k.replace(/rn/g,"n");var d="";for(var F=0;F<k.length;F++){var x=k.charCodeAt(F);if(x<128){d+=String.fromCharCode(x)}else{if((x>127)&&(x<2048)){d+=String.fromCharCode((x>>6)|192);d+=String.fromCharCode((x&63)|128)}else{d+=String.fromCharCode((x>>12)|224);d+=String.fromCharCode(((x>>6)&63)|128);d+=String.fromCharCode((x&63)|128)}}}return d}var C=Array();var P,h,E,v,g,Y,X,W,V;var S=7,Q=12,N=17,M=22;var A=5,z=9,y=14,w=20;var o=4,m=11,l=16,j=23;var U=6,T=10,R=15,O=21;s=J(s);C=e(s);Y=1732584193;X=4023233417;W=2562383102;V=271733878;for(P=0;P<C.length;P+=16){h=Y;E=X;v=W;g=V;Y=u(Y,X,W,V,C[P+0],S,3614090360);V=u(V,Y,X,W,C[P+1],Q,3905402710);W=u(W,V,Y,X,C[P+2],N,606105819);X=u(X,W,V,Y,C[P+3],M,3250441966);Y=u(Y,X,W,V,C[P+4],S,4118548399);V=u(V,Y,X,W,C[P+5],Q,1200080426);W=u(W,V,Y,X,C[P+6],N,2821735955);X=u(X,W,V,Y,C[P+7],M,4249261313);Y=u(Y,X,W,V,C[P+8],S,1770035416);V=u(V,Y,X,W,C[P+9],Q,2336552879);W=u(W,V,Y,X,C[P+10],N,4294925233);X=u(X,W,V,Y,C[P+11],M,2304563134);Y=u(Y,X,W,V,C[P+12],S,1804603682);V=u(V,Y,X,W,C[P+13],Q,4254626195);W=u(W,V,Y,X,C[P+14],N,2792965006);X=u(X,W,V,Y,C[P+15],M,1236535329);Y=f(Y,X,W,V,C[P+1],A,4129170786);V=f(V,Y,X,W,C[P+6],z,3225465664);W=f(W,V,Y,X,C[P+11],y,643717713);X=f(X,W,V,Y,C[P+0],w,3921069994);Y=f(Y,X,W,V,C[P+5],A,3593408605);V=f(V,Y,X,W,C[P+10],z,38016083);W=f(W,V,Y,X,C[P+15],y,3634488961);X=f(X,W,V,Y,C[P+4],w,3889429448);Y=f(Y,X,W,V,C[P+9],A,568446438);V=f(V,Y,X,W,C[P+14],z,3275163606);W=f(W,V,Y,X,C[P+3],y,4107603335);X=f(X,W,V,Y,C[P+8],w,1163531501);Y=f(Y,X,W,V,C[P+13],A,2850285829);V=f(V,Y,X,W,C[P+2],z,4243563512);W=f(W,V,Y,X,C[P+7],y,1735328473);X=f(X,W,V,Y,C[P+12],w,2368359562);Y=D(Y,X,W,V,C[P+5],o,4294588738);V=D(V,Y,X,W,C[P+8],m,2272392833);W=D(W,V,Y,X,C[P+11],l,1839030562);X=D(X,W,V,Y,C[P+14],j,4259657740);Y=D(Y,X,W,V,C[P+1],o,2763975236);V=D(V,Y,X,W,C[P+4],m,1272893353);W=D(W,V,Y,X,C[P+7],l,4139469664);X=D(X,W,V,Y,C[P+10],j,3200236656);Y=D(Y,X,W,V,C[P+13],o,681279174);V=D(V,Y,X,W,C[P+0],m,3936430074);W=D(W,V,Y,X,C[P+3],l,3572445317);X=D(X,W,V,Y,C[P+6],j,76029189);Y=D(Y,X,W,V,C[P+9],o,3654602809);V=D(V,Y,X,W,C[P+12],m,3873151461);W=D(W,V,Y,X,C[P+15],l,530742520);X=D(X,W,V,Y,C[P+2],j,3299628645);Y=t(Y,X,W,V,C[P+0],U,4096336452);V=t(V,Y,X,W,C[P+7],T,1126891415);W=t(W,V,Y,X,C[P+14],R,2878612391);X=t(X,W,V,Y,C[P+5],O,4237533241);Y=t(Y,X,W,V,C[P+12],U,1700485571);V=t(V,Y,X,W,C[P+3],T,2399980690);W=t(W,V,Y,X,C[P+10],R,4293915773);X=t(X,W,V,Y,C[P+1],O,2240044497);Y=t(Y,X,W,V,C[P+8],U,1873313359);V=t(V,Y,X,W,C[P+15],T,4264355552);W=t(W,V,Y,X,C[P+6],R,2734768916);X=t(X,W,V,Y,C[P+13],O,1309151649);Y=t(Y,X,W,V,C[P+4],U,4149444226);V=t(V,Y,X,W,C[P+11],T,3174756917);W=t(W,V,Y,X,C[P+2],R,718787259);X=t(X,W,V,Y,C[P+9],O,3951481745);Y=K(Y,h);X=K(X,E);W=K(W,v);V=K(V,g)}var i=B(Y)+B(X)+B(W)+B(V);return i.toLowerCase()};