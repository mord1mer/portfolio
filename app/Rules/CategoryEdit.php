<?php

declare(strict_types=1);

namespace App\Rules;

use App\Http\Model\Category;
use Illuminate\Contracts\Validation\Rule;

class CategoryEdit implements Rule
{
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function passes($attribute, $value): bool
    {
        $countValidTag = Category::where('id', '!=', $this->id)->where('name', '=', $value)->count();

        return 0 === $countValidTag;
    }

    public function message(): string
    {
        return 'Kategoria juz istnieje';
    }
}
