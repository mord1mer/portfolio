<?php

declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Captcha implements Rule
{
    public function __construct()
    {
    }

    public function passes($attribute, $value): bool
    {
        $check = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . config('app.GOOGLE_RECAPTCHA_SECRET') . '&response=' . $value);
        $answer = json_decode($check);

        return $answer->success;
    }

    public function message(): string
    {
        return 'Potwierdz że nie jesteś botem!';
    }
}
