<?php

declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Domain\Pages\PageTypes;

class PageTypeRules implements Rule
{
    public function __construct()
    {
    }

    public function passes($attribute, $value): bool
    {
        return null != PageTypes::findType((int) $value);
    }

    public function message(): string
    {
        return 'Typ strony nie jest poprawny!';
    }
}
