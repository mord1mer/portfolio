<?php

declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Http\Model\Comment;

class CommentParent implements Rule
{
    public function __construct()
    {
    }

    public function passes($attribute, $value): bool
    {
        if (0 == $value) {
            return true;
        }

        return Comment::where('id', $value)->count() == 1;
    }

    public function message(): string
    {
        return 'Komentarz do którego chcesz udzielić odpowiedzi nie istnieje';
    }
}
