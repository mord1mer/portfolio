<?php

declare(strict_types=1);

namespace App\Http\Repository;

use App\Http\Requests\PostRequest;
use App\Http\Model\Post;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use App\Http\Repository\RepositoryAbstract;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Domain\Pages\PageTypes;
use Illuminate\Support\Str;

class PostRepository extends RepositoryAbstract
{
    /**
     * limit posts per page
     */
    const PAGINATE = 15;

    public function __construct(Post $post)
    {
        $this->model = $post;
    }

    public function createPost(PostRequest $postRequest): void
    {
        $this->createOrUpdatePost(null, $postRequest);
    }

    public function update(Post $post, PostRequest $postRequest): void
    {
        $this->createOrUpdatePost($post, $postRequest);
    }

    private function createOrUpdatePost(?Post $post, PostRequest $postRequest): void
    {
        if (!$post) {
            $post = new $this->model;
            $post->user_id = Auth::id();
            $post->publishedAt = $postRequest->publishedAt ? $postRequest->publishedAt : new \DateTime();
        }

        $post->content = $postRequest->content;
        $post->title = $postRequest->title;
        $post->slug = Str::slug($postRequest->title);
        $post->updatedAt = new \DateTime();
        $post->headtitle = $postRequest->headtitle;
        $post->keyworks = $postRequest->keyworks;
        $post->description = $postRequest->description;
        $post->img = $postRequest->img;
        $post->type = $postRequest->type;
        $post->show_added_comments = (bool) $postRequest->show_added_comments;
        $post->let_add_comments = (bool) $postRequest->let_add_comments;
        $post->save();

        $post->tags()->sync($postRequest->tags);
        $post->categories()->sync($postRequest->categories);

        $tagsData = [];

        if ($postRequest->tags) {
            foreach ($postRequest->tags as $tag) {
                $tagsData[] = [
                    'tag_id' => $tag,
                    'type' => $postRequest->type,
                ];
            }
        }

        $post->tags()->sync($tagsData);
        $post->categories()->sync($postRequest->categories);
    }

    public function delete(Post $post): void
    {
        $post->comments()->forceDelete();
        $post->tags()->detach();
        $post->categories()->detach();
        $post->delete();
    }

    public function getPostsByCategory(int $catId, int $paginate = null, int $page): LengthAwarePaginator
    {
        /* better performance for many database data */
        $paginate = $paginate ? $paginate : self::PAGINATE;

        $postsByCategoryQuery = $this->model->select('posts.id', 'posts.user_id', 'posts.slug', 'posts.title', 'posts.description', 'posts.publishedAt', 'posts.img')
            //                        ->whereHas('categories', function ($query) use ($catId) {
            //                            $query->where('id', $catId);
            //                        })
            ->where('type', PageTypes::BLOG_POST)
            ->leftjoin('category_post', 'posts.id', '=', 'category_post.post_id')
            ->where('category_post.category_id', $catId);

        //$countPostsByCategory = $postsByCategoryQuery->count();
        // ->whereExists(function ($query) use ($catId) {
        //     $query->select(DB::raw(1))
        //         ->from('category_post')
        //         ->whereRaw('category_post.post_id = posts.id')
        //         ->whereRaw('category_post.category_id ='.$catId.'');
        // })
        $postsByCategoryQuery = $postsByCategoryQuery->with('categories')->with('author')->orderBy('posts.id', 'desc')->forPage($page, $paginate)->get();

        return new LengthAwarePaginator(
            $postsByCategoryQuery,
            $this->countPostsByCategoryAndType($catId),
            $paginate,
            $page,
            [
                'path' => Paginator::resolveCurrentPath(),
                'pageName' => 'page',
            ]
        );
    }

    public function getPostsByTagAndType(
        int $tagId,
        int $pageType,
        array $fields,
        int $page,
        bool $getCategories = false,
        bool $getAuthor = false,
        int $paginate = null
    ): LengthAwarePaginator {
        /* better performance for many database data */
        $paginate = $paginate ? $paginate : self::PAGINATE;
        $postsByTagQuery = $this->model->select(...$fields)
            //->where('posts.type', $pageType)
            //                        ->whereHas('tags', function ($query) use ($tagId) {
            //                             $query->where('id', $tagId);
            //                        })
            //==========================
            //naszybsze
            //        $postsByTagQuery = $this->model
            //                         ->from('post_tag')
            //                ->select(DB::raw('*'))
            //                     ->selectRaw('tag_id,post_id')
            //                ->where('post_tag.tag_id', $tagId)
            //                ->whereRaw('post_tag.post_id = posts.id')
            ->join('post_tag', function ($join) use ($tagId, $pageType) {
                $join->on('posts.id', '=', 'post_tag.post_id');
                $join->where('post_tag.tag_id', '=', $tagId);
                $join->where('post_tag.type', '=', $pageType);
            });
        //$countPostsByTag = $postsByTagQuery->count();
        // ->whereExists(function ($query) use ($tagId) {
        //     $query->select(DB::raw(1))
        //         ->from('post_tag')
        //         ->whereRaw('post_tag.tag_id ='.$tagId.'')
        //         ->whereRaw('posts.id = post_tag.post_id')
        //         ;
        // })
        if ($getCategories) {
            $postsByTagQuery->with('categories');
        }
        if ($getAuthor) {
            $postsByTagQuery->with('author');
        }

        $postsByTagQuery = $postsByTagQuery->orderBy('posts.id', 'desc')->forPage($page, $paginate)->get();

        $countPostsByTag = DB::table('post_tag')->where('tag_id', $tagId)->where('type', $pageType)->count();

        return new LengthAwarePaginator($postsByTagQuery, $countPostsByTag, $paginate, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);
    }

    public function getFastPostByTitle(string $title, int $paginate = null): LengthAwarePaginator
    {
        $paginate = $paginate ? $paginate : self::PAGINATE;

        return $this->model
            ->select('posts.id', 'posts.slug', 'posts.title', 'posts.content', 'posts.publishedAt')
            ->where('type', PageTypes::FAST_POST)
            ->where('title', 'LIKE', "%$title%")
            ->paginate($paginate);
    }

    public function getSinglePost(
        string $slug,
        int $pageType,
        array $fields = ['*'],
        bool $withTags = false,
        bool $withCategories = false,
        bool $withAuthor = false
    ): Post {
        $post = $this->model->select($fields)->where('slug', '=', $slug)->where('type', $pageType);

        if ($withTags) {
            $post->with('tags');
        }

        if ($withCategories) {
            $post->with('categories');
        }

        if ($withAuthor) {
            $post->with('author');
        }

        return $post->firstOrFail();
    }

    private function countPostsByCategoryAndType(int $catId, int $pageType = PageTypes::BLOG_POST): int
    {
        return DB::table('category_post')
            ->join('posts', 'posts.id', '=', 'category_post.post_id')
            ->where('posts.type', $pageType)
            ->where('category_id', $catId)->count();
    }
}
