<?php

declare(strict_types=1);

namespace App\Http\Repository\Upload;

use App\Http\Model\Upload\UploadedFile;
use Illuminate\Database\Eloquent\Collection;

class UploadedFileRepository
{
    private UploadedFile $model;

    public function __construct(UploadedFile $model)
    {
        $this->model = $model;;
    }

    public function getUploadedFiles(int $offset = 0): Collection
    {
        return $this->model
            ->offset($offset)
            ->limit(10)
            ->get();
    }
}
