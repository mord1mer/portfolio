<?php

declare(strict_types=1);

namespace App\Http\Repository;

use App\Domain\Pages\PageTypes;
use App\Http\Model\Comment;
use App\Http\Model\Post;
use App\Http\Requests\CommentRequest;
use DateTime;
use Illuminate\Database\Eloquent\Collection;

class CommentRepository
{
    /**
     * remove forever true|false default false
     */
    private bool $forceDelete = false;

    public function __construct(private Comment $model)
    {
    }

    public function setForceDelete(bool $forceDelete): void
    {
        $this->forceDelete = $forceDelete;
    }

    public function getLatestComments(int $quantity, int $pageType = PageTypes::BLOG_POST): Collection
    {
        return $this->model::select('post_id', 'comment_author', 'comment_author_email', 'comment_author_url', 'comment_content', 'comments.publishedAt', 'posts.slug')
            ->where('comment_parent', 0)
            ->join('posts', 'posts.id', '=', 'comments.post_id')
            ->where('posts.type', $pageType)
            //with('post:id,slug')
            ->orderBy('comments.id', 'desc')
            ->limit($quantity)
            ->get();
    }

    public function createComment(CommentRequest $request): Comment
    {
        $comment = new $this->model;
        $comment->post_id = $request->post_id;
        $comment->comment_author = $this->parseProtectedElements($request->comment_author);
        $comment->comment_author_email = $this->parseProtectedElements($request->comment_author_email);
        $comment->comment_author_email_md5 = $this->parseProtectedElements($request->comment_author_email);
        $comment->comment_author_url = $this->parseProtectedElements($request->comment_author_url);
        $comment->comment_author_ip = $request->ip();
        $comment->comment_content = $this->parseProtectedElements($request->comment_content);
        $comment->comment_parent = $request->comment_parent;
        $comment->comment_agent = $request->server('HTTP_USER_AGENT');
        $comment->publishedAt = new DateTime();
        $comment->save();

        Post::where('id', '=', $request->post_id)->increment('number_of_comments');

        if ($request->comment_parent > 0) {
            $this->model::where('id', '=', $request->comment_parent)->increment('number_of_responses');
        }

        return $comment;
    }
    /**
     * todo transfer to service
     */
    private function parseProtectedElements(?string $value): ?string
    {
        return null === $value ? null : trim(strip_tags(($value)));
    }

    public function delete(int $commentId): void
    {
        $this->deleteSingleComment($commentId);

        $allChildrensOfComment = $this->model::where('comment_parent', $commentId)->withTrashed()->get();

        if ($allChildrensOfComment) {
            foreach ($allChildrensOfComment as $comment) {
                //delete all childs and recurrence delete next childs
                $this->deleteSingleComment($comment->id);
                $this->delete($comment->id);
            }
        }
    }

    public function deleteSingleComment(int $commentId): void
    {
        if ($this->forceDelete == true) {
            $this->model::where('id', '=', $commentId)->forceDelete();
        } else {
            $this->model::where('id', '=', $commentId)->delete();
        }
    }

    public function getPostComments(int $postId, int $commentParent = 0, int $offset = 0): Collection
    {
        return $this->model::select('id', 'comment_parent', 'number_of_responses', 'comment_author', 'comment_author_email_md5', 'comment_author_url', 'comment_content', 'publishedAt')
            ->where('post_id', '=', $postId)
            ->where('comment_parent', '=', $commentParent)
            ->offset($offset)
            ->limit(25)
            ->get();
    }
}
