<?php

namespace App\Http\Repository;

use Illuminate\Database\Eloquent\Model;

abstract class RepositoryAbstract
{
    protected Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function findAll()
    {
        return $this->model->all();
    }

    /**
     * @param array $data
     * @return type
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    // public function update(array $data, $id) {
    // 	$record = $this->find($id);
    // 	return $record->update($data);
    // }
    // remove record from the database
    // public function delete($id) {
    // 	return $this->model->destroy($id);
    // }

    /**
     * show the record with the given id
     * 
     * @param type $id
     * @param array $columns
     * 
     * @return Model|null
     */
    public function find($id, array $columns = null): ?Model
    {
        $query = $this->model;

        if ($columns) {
            $query = $query->select($columns);
        }
        return $query->find($id);
    }

    /**
     * 
     * @param type $id
     * @param array $columns
     * 
     * @return Model
     */
    public function findOrFail($id, array $columns = null): Model
    {
        $query = $this->model;

        if ($columns) {
            $query = $query->select($columns);
        }

        return $query->findOrFail($id);
    }

    /**
     * [id =>['a','=','23432']]
     * 
     * @param array $criteria
     * @param array $columns
     * @param array $orderBy
     * 
     * @return type
     */
    public function findOneBy(array $criteria, array $columns = null, array $orderBy = null)
    {

        $query = $this->model;

        if ($columns) {
            $query = $query->select($columns);
        }

        foreach ($criteria as $key => $value) {
            $query = $query->where($value[0], $value[1], $value[2]);
        }

        if ($orderBy) {
            $query->orderBy($orderBy[0], $orderBy[1]);
        }

        return $query->first();
    }

    /**
     * 
     * @param array $criteria
     * @param array $columns
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * 
     * @return type
     */
    public function findBy(array $criteria, array $columns = null, array $orderBy = null, int $limit = null, int $offset = null)
    {
        $query = $this->model;

        if ($columns) {
            $query = $query->select($columns);
        }

        foreach ($criteria as $key => $value) {
            $query = $query->where($value[0], $value[1], $value[2]);
        }

        if ($orderBy) {
            $query->orderBy($orderBy[0], $orderBy[1]);
        }

        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }

        return $query->get();
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }
}
