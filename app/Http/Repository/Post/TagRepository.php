<?php

declare(strict_types=1);

namespace App\Http\Repository\Post;

use App\Http\Model\Tag;
use App\Http\Repository\RepositoryAbstract;
use Illuminate\Database\Eloquent\Collection;

class TagRepository extends RepositoryAbstract
{
    public function __construct(Tag $model)
    {
        $this->model = $model;
    }

    public function getTagsByCondition(array $condition = ['showInFastPost', 1], array $fields = ['slug', 'name']): Collection
    {
        return  $this->model::select(...$fields)->where(...$condition)->orderBy('name', 'asc')->get();
    }

    public function getTagBySlug(string $slug, array $fields = ['id', 'name']): ?Tag
    {
        return $this->model::select(...$fields)->where('slug', '=', $slug)->firstOrFail();
    }
}
