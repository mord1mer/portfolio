<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Http\Model\Post;
use App\Domain\User\UserRoles;

class CheckPostPermission
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $post = Post::findOrFail($request->post->id);

        if (($post->user_id != Auth::id()) && !UserRoles::checkRole(UserRoles::ROLE_SUPER_ADMIN, Auth::user()->roles)) {
            abort(403, 'Brak dostępu');
        }

        return $next($request);
    }
}
