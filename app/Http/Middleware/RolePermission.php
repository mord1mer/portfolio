<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Domain\User\UserRoles;

class RolePermission
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string $role - require role
     * @return mixed
     */
    public function handle($request, Closure $next, string $role)
    {
        if (Auth::guest()) {
            return redirect()->route('login');
        }

        if (!UserRoles::checkRole($role, Auth::user()->roles)) {
            abort(403, 'Brak dostepu');
        }

        return $next($request);
    }
}
