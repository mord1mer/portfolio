<?php

declare(strict_types=1);

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * @return view|JsonResponse
     */
    public function index(Request $request)
    {
        $title = 'Kontakt » Grzegorz Tarka';
        $description = 'Skontaktuj się z ! Grzegorz Tarka - Tworzenie wyjątkowych stron internetowych, marketing internetowy, pełna obsługa firm w internecie. Myszków, Zawiercie, Częstochowa Polska.';

        if ($request->ajax()) {
            return response()->json([
                'content' => view('contact.indexAjax')->render(),
                'title' => $title,
                'description' => $description
            ])->header("Vary", "Accept");
        }

        return view('contact.index')->with(['title' => $title, 'description' => $description]);
    }

    /**
     * @return view
     */
    public function store(ContactRequest $request)
    {
        Mail::to(config('mail.username'))->send(new Contact($request));

        return back()->with('message', 'Twój email został wysłany');
    }
}
