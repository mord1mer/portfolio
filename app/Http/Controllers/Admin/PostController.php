<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Model\Category;
use App\Http\Controllers\Controller;
use App\Http\Repository\PostRepository;
use App\Http\Requests\PostRequest;
use App\Http\Model\Post;
use App\Http\Model\Tag;
use Illuminate\Support\Facades\Auth;
use App\Domain\User\UserRoles;
use App\Domain\Pages\PageTypes;
use App\Domain\Post\Content\BladeToHtmlGenerator;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('post_permission', ['except' => ['index', 'store', 'show', 'create']]);
    }

    /**
     * @return View|JsonResponse
     */
    public function index(Request $request)
    {
        if (UserRoles::checkRole(UserRoles::ROLE_SUPER_ADMIN, Auth::user()->roles)) {
            $posts = Post::orderBy('id', 'desc');
        } else {
            $posts = Post::where('user_id', '=', Auth::id())->orderBy('id', 'desc');
        }

        if ($request->type) {
            $posts->where('type', '=', $request->type);
        }

        if ($request->title) {
            $posts->where('title', 'LIKE', "%$request->title%");
        }

        $posts = $posts->with('author')->paginate(100);

        $title = 'Wszystkie posty';
        $description = 'Lista postów';

        $pageType = PageTypes::getTypes();

        if ($request->ajax()) {
            return response()->json([
                'content' => view('admin.post.index.indexAjax', compact('posts', 'pageType'))->render(),
                'title' => $title,
                'description' => $description
            ])->header("Vary", "Accept");
        }

        return view('admin.post.index.index', compact('posts', 'title', 'description', 'pageType'));
    }

    public function store(PostRequest $request, PostRepository $postRepository): RedirectResponse
    {
        $postRepository->createPost($request);

        return redirect()->route('post.index')->with('message', 'Dodałem post');
    }

    public function create(): View
    {
        return view('admin.post.create', [
            'categories' => Category::select('id', 'name')->orderBy('name', 'asc')->get(),
            'tags' => Tag::select('id', 'name')->orderBy('name', 'asc')->get(),
            'types' => PageTypes::getTypes()
        ]);
    }

    public function edit(Post $post): View
    {
        return view('admin.post.edit', [
            'post' => $post,
            'categories' => Category::select('id', 'name')->orderBy('name', 'asc')->get(),
            'tags' => Tag::select('id', 'name')->orderBy('name', 'asc')->get(),
            'selectTags' => $post->tags->pluck('id')->all(),
            'selectCategories' => $post->categories->pluck('id')->all(),
            'types' => PageTypes::getTypes(),
        ]);
    }

    public function update(PostRequest $request, Post $post, PostRepository $postRepository): RedirectResponse
    {
        $postRepository->update($post, $request);

        return redirect()->route('post.index')->with('message', 'Zapisałem zmiany');
    }

    public function show(Post $post): View
    {
        $post->content = BladeToHtmlGenerator::generate($post->content);

        return view('admin.post.show', [
            'post' => $post
        ]);
    }

    public function destroy(Post $post, PostRepository $postRepository): RedirectResponse
    {
        $postRepository->delete($post);

        return redirect()->route('post.index')->with('message', 'Post został usuniety');
    }
}
