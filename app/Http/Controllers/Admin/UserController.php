<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\UserRequest;
use App\Domain\User\UserRoles;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

class UserController extends Controller
{
    public function index(): View
    {
        return view('admin.user.index', [
            'users' => User::paginate(10)
        ]);
    }

    public function edit(User $user): View
    {
        return view('admin.user.edit', [
            'user' => $user,
            'userRoles' => UserRoles::getRoleList()
        ]);
    }

    public function update(UserRequest $request, User $user): RedirectResponse
    {
        $userData = $request->all();
        $userData['roles'] = $userData['roles'] ?? [];

        if (isset($userData['password'])) {
            $userData['password'] = bcrypt($userData['password']); //if changing of password docode it to bcrypt
        } else {
            unset($userData['password']); //remove password key if empty
        }

        $user->fill($userData)->save();

        return redirect()->route('user.index')->with('message', 'Zapisałem zmiany');
    }

    public function create(): View
    {
        return view('admin.user.create', [
            'userRoles' => UserRoles::getRoleList()
        ]);
    }

    public function store(UserRequest $tagRequest): RedirectResponse
    {
        $userData = $tagRequest->all();
        $userData['roles'] = $userData['roles'] ?? [];

        if (isset($userData['password'])) {
            $userData['password'] = bcrypt($userData['password']); //if changing of password docode it to bcrypt
        } else {
            unset($userData['password']); //remove password key if empty
        }
        $user = new User();
        $user->fill($userData)->save();

        return redirect()->route('user.index')->with('message', 'Zapisałem nowego typa');
    }
}
