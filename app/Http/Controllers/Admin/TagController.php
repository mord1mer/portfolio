<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TagRequest;
use App\Http\Model\Tag;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Str;

class TagController extends Controller
{
    public function index(): View
    {
        return view('admin.tag.index', [
            'tags' => Tag::orderBy('id', 'asc')->get()
        ]);
    }

    public function create(): View
    {
        return view('admin.tag.create');
    }

    public function store(TagRequest $tagRequest, Tag $tag): RedirectResponse
    {
        $tag->createNew($tagRequest);

        return redirect()->route('tags.index')->with('message', 'Dodałem tag');
    }

    public function edit(Tag $tag): View
    {
        return view('admin.tag.edit', [
            'tag' => $tag
        ]);
    }

    public function update(TagRequest $tagRequest, Tag $tag): RedirectResponse
    {
        $tag->update([
            'name' => $tagRequest->name,
            'slug' => Str::slug($tagRequest->name),
            'showInPost' => isset($tagRequest->showInPost) ? 1 : 0,
            'showInFastPost' => isset($tagRequest->showInFastPost) ? 1 : 0,
        ]);

        return back()->with('message', 'Update taga');
    }

    public function destroy(int $id): RedirectResponse
    {
        DB::table('post_tag')->where('tag_id', '=', $id)->delete();
        Tag::where('id', '=', $id)->delete();

        return back()->with('message', 'usunąłem tag i tagi z postow');
    }
}
