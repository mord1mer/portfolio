<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Model\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function index(): View
    {
        return view('admin.category.index', [
            'categories' =>  Category::orderBy('id', 'asc')->get()
        ]);
    }

    public function create(): View
    {
        return view('admin.category.create');
    }

    public function store(CategoryRequest $categoryRequest, Category $category): RedirectResponse
    {
        $category->createNew($categoryRequest);

        return redirect()->route('categories.index')->with('message', 'Dodałem kategorie');
    }

    public function edit(Category $category): View
    {
        return view('admin.category.edit', ['category' => $category]);
    }

    public function update(CategoryRequest $categoryRequest, Category $category): RedirectResponse
    {
        $category->update([
            'name' => $categoryRequest->name,
            'slug' => Str::slug($categoryRequest->name),
        ]);

        return back()->with('message', 'Update kategori');
    }

    public function destroy(int $id): RedirectResponse
    {
        DB::table('category_post')->where('category_id', '=', $id)->delete();
        Category::where('id', '=', $id)->delete();

        return back()->with('message', 'usunąłem kategorie i kategorie z postow');
    }
}
