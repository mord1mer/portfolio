<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Model\Category;
use App\Http\Repository\PostRepository;
use App\Http\Model\Post;
use App\Http\Repository\CommentRepository;
use App\Domain\Pages\PageTypes;
use App\Domain\Response\Response;
use Illuminate\Http\Request;
use App\Http\Repository\Post\TagRepository;
use Illuminate\Config\Repository;

class PostController extends Controller
{
    private string $title;

    private string $description;

    private int $paginate;

    private Response $response;

    public function __construct(Response $response, Repository $repository)
    {
        $this->response = $response;
        $this->paginate = $repository->get('app.BLOG_CONFIG.BLOG_PAGINATE');
        $this->title = $repository->get('app.BLOG_CONFIG.BLOG_TITLE');
        $this->description = $repository->get('app.BLOG_CONFIG.BLOG_DESCRIPTION');
    }

    /**
     * @return View|JsonResponse
     */
    public function index(Request $request, CommentRepository $commentRepository, TagRepository $tagRepository)
    {
        $posts = Post::select('posts.id', 'posts.user_id', 'posts.slug', 'posts.title', 'posts.description', 'posts.publishedAt', 'posts.img');

        $searchTitle = '';

        if ($request->title) {
            $posts = $posts->where('title', 'LIKE', "%$request->title%");
            $searchTitle = $request->title;
        }

        $posts = $posts->where('type', PageTypes::BLOG_POST)->with('categories')->with('author')->orderBy('posts.id', 'desc')->paginate($this->paginate);

        return $this->response->render($request->ajax(), [
            'posts' => $posts,
            'tags' => $tagRepository->getTagsByCondition(['showInPost', 1]),
            'categories' => Category::select('name', 'slug')->get(),
            'latestComments' => $commentRepository->getLatestComments(5),
            'searchTitle' => $searchTitle,
            'title' => $this->title,
            'description' => $this->description,
        ], 'blog.index.indexAjax', 'blog.index.index');
    }

    /**
     * @return View|JsonResponse
     */
    public function postsByCategory(
        Request $request,
        string $slug,
        PostRepository $postRepository,
        CommentRepository $commentRepository,
        TagRepository $tagRepository
    ) {
        $category = Category::select('id', 'name')->where('slug', '=', $slug)->firstOrFail();
        $page = (int) $request->input('page') ?? 1;

        return $this->response->render($request->ajax(), [
            'posts' => $postRepository->getPostsByCategory($category->id, $this->paginate, $page),
            'tags' => $tagRepository->getTagsByCondition(['showInPost', 1]),
            'categories' => Category::all(),
            'latestComments' => $commentRepository->getLatestComments(5),
            'slug' => [
                'name' => $category->name,
                'searchTitle' => 'kategori'
            ],
            'title' => $this->title,
            'description' => $this->description
        ], 'blog.index.indexAjax', 'blog.index.index');
    }

    /**
     * @return View|JsonResponse
     */
    public function postsByTag(
        Request $request,
        string $slug,
        PostRepository $postRepository,
        CommentRepository $commentRepository,
        TagRepository $tagRepository
    ) {
        $tagSearch = $tagRepository->getTagBySlug($slug);
        $page = (int) $request->input('page') ?? 1;

        return $this->response->render($request->ajax(), [
            'posts' => $postRepository->getPostsByTagAndType(
                $tagSearch->id,
                PageTypes::BLOG_POST,
                ['posts.id', 'posts.user_id', 'posts.slug', 'posts.title', 'posts.description', 'posts.publishedAt', 'posts.img', 'posts.content'],
                $page,
                true,
                true,
                $this->paginate,
            ),
            'tags' => $tagRepository->getTagsByCondition(['showInPost', 1]),
            'categories' => Category::all(),
            'latestComments' =>  $commentRepository->getLatestComments(5),
            'slug' => [
                'name' => $tagSearch->name,
                'searchTitle' => 'tagu'
            ],
            'title' => $this->title,
            'description' => $this->description,
        ], 'blog.index.indexAjax', 'blog.index.index');
    }

    /**
     * @return View|JsonResponse
     */
    public function singlePost(Request $request, string $slug, PostRepository $postRepository)
    {
        $post = $postRepository->getSinglePost($slug, PageTypes::BLOG_POST, ['*'], true, true, true);

        return $this->response->render($request->ajax(), [
            'post' => $post,
            'title' => $post->title,
            'description' => $post->description
        ], 'blog.singlePost.singlePostAjax', 'blog.singlePost.singlePost');
    }
}
