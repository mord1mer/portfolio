<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Repository\PostRepository;
use App\Domain\Pages\PageTypes;
use App\Domain\Response\Response;
use Illuminate\Http\Request;
use App\Http\Repository\Post\TagRepository;

class FastPostController extends Controller
{
    private string $title;

    private string $description;

    private int $paginate;

    private Response $response;

    public function __construct(Response $response)
    {
        $this->title = config('app.FASTPOST_CONFIG.FASTPOST_TITLE');
        $this->description = config('app.FASTPOST_CONFIG.FASTPOST_DESCRIPTION');
        $this->paginate = config('app.FASTPOST_CONFIG.FASTPOST_PAGINATE');

        $this->response = $response;
    }

    /**
     * @return View|JsonResponse
     */
    public function index(Request $request, PostRepository $postRepository, TagRepository $tagRepository)
    {
        $fastPosts = $searchTitle = null;

        if ($request->title) {
            $fastPosts = $postRepository->getFastPostByTitle($request->title, $this->paginate);
            $searchTitle = $request->title;
        }

        return $this->response->render($request->ajax(), [
            'title' => $this->title,
            'description' => $this->description,
            'tags' => $tagRepository->getTagsByCondition(),
            'fastPosts' => $fastPosts,
            'searchTitle' => $searchTitle
        ], 'fastpost.index.ajaxIndex', 'fastpost.index.index');
    }

    /**
     * @return View|JsonResponse
     */
    public function fastPostsByTag(Request $request, string $slug, PostRepository $postRepository, TagRepository $tagRepository)
    {
        $tagSearch = $tagRepository->getTagBySlug($slug);
        $page = (int) $request->input('page') ?? 1;

        return $this->response->render(
            $request->ajax(),
            [
                'title' => $this->title,
                'description' => $this->description,
                'fastPosts' => $postRepository->getPostsByTagAndType(
                    $tagSearch->id,
                    PageTypes::FAST_POST,
                    ['posts.id', 'posts.slug', 'posts.title', 'posts.content', 'posts.publishedAt'],
                    $page,
                    false,
                    false,
                    $this->paginate,
                ),
                'tags' => $tagRepository->getTagsByCondition(),
                'tagSearch' => $tagSearch->name
            ],
            'fastpost.index.ajaxIndex',
            'fastpost.index.index'
        );
    }

    /**
     * @return View|JsonResponse
     */
    public function singleFastPost(Request $request, string $slug, PostRepository $postRepository)
    {
        return $this->response->render($request->ajax(), [
            'title' => $this->title,
            'description' => $this->description,
            'post' => $postRepository->getSinglePost($slug, PageTypes::FAST_POST, ['id', 'slug', 'content', 'title', 'description', 'number_of_comments', 'show_added_comments', 'let_add_comments'])
        ], 'fastpost.singleFastPost.ajaxIndex', 'fastpost.singleFastPost.index');
    }
}
