<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Model\Comment;
use App\Http\Requests\CommentRequest;
use Illuminate\Http\Request;
use App\Http\Repository\CommentRepository;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function store(CommentRequest $request, CommentRepository $commentRepository): JsonResponse
    {
        $comment = $commentRepository->createComment($request);
        $comment->publishedAt = $comment->publishedAt->format('Y-m-d H:i:s');

        return response()->json([
            'success' => 'comment has been added',
            'comment' => $comment
        ], 200);
        //return back()->with('message', 'Twój komentarz został dodany');
    }

    public function getPostComments(
        int $postId,
        int $commentParent = 0,
        int $offset = 0,
        CommentRepository $commentRepository
    ): JsonResponse {
        return response()->json([
            $commentRepository->getPostComments($postId, $commentParent, $offset)->toArray()
        ], 200);
    }

    public function edit(int $id): View
    {
        return view('admin.comment.edit', [
            'editComment' => Comment::withTrashed()->findOrFail($id)
        ]);
    }

    public function update(CommentRequest $request, int $id): RedirectResponse
    {
        $commentUpdate = Comment::withTrashed()->findOrFail($id);
        $commentUpdate->fill($request->all())->save();

        return back()->with('message', 'Zapisałem zmiany');
    }

    public function destroy(int $id, Request $request, CommentRepository $commentRepository): RedirectResponse
    {
        if ($request->forceDelete) {
            $commentRepository->setForceDelete(true);
            $commentRepository->delete($id);

            return redirect()->route('post.store')->with('message', 'Komentarz został usuniety wraz z wszystkimi odpowiedziami');
        }

        $commentRepository->setForceDelete(false);
        $commentRepository->delete($id);

        return back()->with('message', 'Komentarz został miekko usunięty');
    }
}
