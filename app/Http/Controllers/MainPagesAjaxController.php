<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Domain\Pages\PageTypes;
use App\Domain\Post\Content\BladeToHtmlGenerator;
use App\Domain\Response\Response;
use App\Http\Repository\PostRepository;
use Illuminate\Http\Request;

class MainPagesAjaxController extends Controller
{
    private Response $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * main action in cms - all addresses go through this method
     *
     * @param string $slug
     * @return View|JsonResponse
     * @throws \Exception
     */
    public function index(Request $request, PostRepository $postRepository, $slug = 'home')
    {
        $post = $postRepository->getSinglePost($slug, PageTypes::MAIN_PAGE);
        $post->content = BladeToHtmlGenerator::generate($post->content);

        return $this->response->render($request->ajax(), [
            'content' => $post->content,
            'title' => $post->title,
            'description' =>  $post->description
        ], '', 'MainPagesAjax.index.index', true);
    }
}
