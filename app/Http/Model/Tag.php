<?php

declare(strict_types=1);

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\TagRequest;
use Illuminate\Support\Str;
use App\Http\Model\Post;

class Tag extends Model
{
    protected $table = 'tags';

    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * @var array - The attributes that are mass assignable.
     */
    protected $fillable = [
        'name',
        'slug',
        'showInPost',
        'showInFastPost',
    ];

    /**
     * Many to Many relation
     *
     * @return Illuminate\Database\Eloquent\Relations\belongToMany
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function createNew(TagRequest $tagRequest): void
    {
        $category = new Tag();
        $category->name = $tagRequest->name;
        $category->slug = Str::slug($tagRequest->name);
        $category->save();
    }
}
