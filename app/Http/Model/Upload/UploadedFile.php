<?php

declare(strict_types=1);

namespace App\Http\Model\Upload;

use Illuminate\Database\Eloquent\Model;

class UploadedFile extends Model
{
    /**
     * @var string - table name
     */
    protected $table = 'uploaded_file';

    /**
     * @var string  - primary key in table
     */
    protected $primaryKey = 'id';
}
