<?php

declare(strict_types=1);

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Model\Post;

class Comment extends Model
{
    use SoftDeletes;

    protected $table = 'comments';

    public $timestamps = false;

    /**
     * @var array - The attributes that are mass assignable.
     */
    protected $fillable = [
        'post_id',
        'comment_author',
        'comment_author_email',
        'comment_author_email_md5',
        'comment_author_url',
        'comment_author_ip',
        'comment_content',
        'comment_parent',
        'comment_agent',
        'publishedAt',
        'deleted_at',
    ];

    /**
     * Many to one relation - one post has many comments/many comments belongs to one post
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
