<?php

declare(strict_types=1);

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Str;
use App\Http\Model\Post;

class Category extends Model
{
    protected $table = 'categories';

    public $timestamps = false;

    /**
     * @var array - The attributes that are mass assignable.
     */
    protected $fillable = [
        'name',
        'slug',
    ];

    /**
     * Many to Many relation
     *
     * @return 
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function createNew(CategoryRequest $catRequest): void
    {
        $category = new Category;
        $category->name = $catRequest->name;
        $category->slug = Str::slug($catRequest->name);
        $category->save();
    }
}
