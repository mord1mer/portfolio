<?php

declare(strict_types=1);

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use App\Http\Model\Comment;
use App\User;

class PostTag extends Model
{
    protected $table = 'post_tag';

    /**
     * post has many comments
     */
    public function comments()
    {
        return $this->hasMany(Comment::class)->withTrashed();
    }

    /**
     * post has/belongs to one author
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id')->select('name', 'id');
    }
}
