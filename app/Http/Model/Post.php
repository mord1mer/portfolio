<?php

declare(strict_types=1);

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use App\Http\Model\Tag;
use App\Http\Model\Category;
use App\Http\Model\Comment;
use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Post extends Model
{
    protected $table = 'posts';

    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * @var array - The attributes that are mass assignable.
     */
    protected $fillable = [
        'content',
        'title',
        'slug',
        'headtitle',
        'keyworks',
        'description',
        'img',
        'type',
        'show_added_comments',
        'let_add_comments'
    ];

    protected $casts = [
        'show_added_comments' => 'boolean',
        'let_add_comments' => 'boolean',
    ];

    protected $appends = [
        'disableComments',
        'justShowComments',
        'justAddingComments'
    ];
    /**
     * Many to Many relation
     *
     * @return Illuminate\Database\Eloquent\Relations\belongToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Many to Many relation
     *
     * @return Illuminate\Database\Eloquent\Relations\belongToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * post has many comments
     */
    public function comments()
    {
        return $this->hasMany(Comment::class)->withTrashed();
    }

    /**
     * post has/belongs to one author
     */
    public function author(array $fields = ['name', 'id']): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id')->select(...$fields);
    }

    public function getDisableCommentsAttribute(): bool
    {
        return false === $this->let_add_comments && false === $this->show_added_comments;
    }

    public function getJustShowCommentsAttribute(): bool
    {
        return false === $this->let_add_comments && true === $this->show_added_comments;
    }

    public function getJustAddingCommentsAttribute(): bool
    {
        return true === $this->let_add_comments && false === $this->show_added_comments;
    }
}
