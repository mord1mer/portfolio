<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Rules\TagEdit;
use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = ['name' => ['required', 'string', 'min:3', 'max:70']];

        if (null !== $this->idTag) {
            $this->idTag = (int) $this->idTag;
        }

        if ($this->method() == 'PUT') {
            $rules['name'][] = new TagEdit($this->idTag);
        } else {
            $rules['name'][] = 'unique:tags';
        }

        return $rules;
    }
}
