<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\PageTypeRules;

class PostRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'content' => 'required|string|min:30|max:65000',
            'title' => 'required|string|min:3|max:100',
            'publishedAt' => 'nullable|date_format:Y-m-d H:i:s',
            'headtitle' => 'nullable|string|min:3|max:100',
            'keyworks' => 'nullable|string|min:3|max:200',
            'description' => 'nullable|string|min:3|max:278',
            'categories' => 'array',
            'tags' => 'array',
            'type' => [
                'required',
                'integer',
                'min:1',
                new PageTypeRules(),
            ],
        ];
    }
}
