<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $ignoreId = null;

        if (isset($this->user)) {
            $ignoreId = $this->user->id;
        }

        return [
            'name' => 'required|string|min:3|max:191',
            'email' => 'required|email|min:3|max:191|unique:users,email,' . $ignoreId,
            'password' => 'nullable|min:3|max:191|confirmed'
        ];
    }
}
