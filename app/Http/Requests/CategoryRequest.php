<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CategoryEdit;

class CategoryRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = ['name' => ['required', 'string', 'min:3', 'max:70']];

        if (null !== $this->idCat) {
            $this->idCat = (int) $this->idCat;
        }

        if ($this->method() == 'PUT') {
            $rules['name'][] = new CategoryEdit($this->idCat);
        } else {
            $rules['name'][] = 'unique:categories';
        }

        return $rules;
    }
}
