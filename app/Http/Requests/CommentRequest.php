<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Rules\Captcha;
use App\Rules\CommentParent;
use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'comment_author' => 'required|string|min:3|max:100',
            'comment_author_url' => 'url|nullable|max:100',
            'comment_author_email' => 'required|email|max:100',
            'comment_content' => 'required|string|min:5|max:65000',
            'post_id' => 'required|integer|exists:posts,id',
            'comment_parent' => [
                'required',
                'integer',
                new CommentParent(),
            ],
            'g-recaptcha-response' => new Captcha,
        ];
    }
}
