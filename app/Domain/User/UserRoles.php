<?php

declare(strict_types=1);

namespace App\Domain\User;

class UserRoles
{
    private static array $allRoles = [];

    const ROLE_REDACTOR = 'ROLE_REDACTOR';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    public static function getRoleList(): array
    {
        return [
            static::ROLE_SUPER_ADMIN => 'Super admin',
            static::ROLE_ADMIN => 'Administrator',
            static::ROLE_REDACTOR => 'Redaktor',
        ];
    }

    protected static array $roleHierarchy = [
        self::ROLE_REDACTOR => [self::ROLE_REDACTOR],
        self::ROLE_ADMIN => [self::ROLE_REDACTOR],
        self::ROLE_SUPER_ADMIN => [self::ROLE_ADMIN],
    ];

    public static function getAllowedRolesByRoleName(string $role): array
    {
        return self::$roleHierarchy[$role] ?? [];
    }

    public static function getRoleWithAllChild(string $role): array
    {
        if (!empty(self::getAllowedRolesByRoleName($role))) {
            if (!in_array($role, self::$allRoles)) {
                self::$allRoles[] = $role;
            }

            $getAllowedRolesByRoleName = self::getAllowedRolesByRoleName($role);

            foreach ($getAllowedRolesByRoleName as $role) {
                if (!in_array($role, self::$allRoles)) {
                    self::$allRoles[] = $role;
                    self::getRoleWithAllChild($role);
                }
            }
        }

        return self::$allRoles;
    }

    public static function checkRole(string $requireRole, array $userRolesFromDb): bool
    {
        if (empty($userRolesFromDb)) {
            return false;
        }

        if (in_array($requireRole, $userRolesFromDb)) {
            return true;
        }

        $allRolesInHierarchy = [];

        foreach ($userRolesFromDb as $singleRole) {
            $allRolesInHierarchy = array_unique(array_merge($allRolesInHierarchy, self::getRoleWithAllChild($singleRole)));
        }

        if (in_array($requireRole, $allRolesInHierarchy)) {
            return true;
        }

        return false;
    }
}
