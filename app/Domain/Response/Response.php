<?php

declare(strict_types=1);

namespace App\Domain\Response;

use App\Domain\Response\ResponseInterface;
use Illuminate\Routing\ResponseFactory;

class Response implements ResponseInterface
{
    private ResponseFactory $responseFactory;

    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    public function render(bool $isAjax, array $data, string $ajaxViewName, string $viewName, ?bool $renderContent = null)
    {
        if ($isAjax) {
            return $this->responseFactory->json([
                'content' => $renderContent ? $data['content'] : view($ajaxViewName, $data)->render(),
                'title' => $data['title'],
                'description' =>  $data['description']
            ])->header("Vary", "Accept");
        }

        return $this->responseFactory->view($viewName, $data);
    }
}
