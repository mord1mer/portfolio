<?php

declare(strict_types=1);

namespace App\Domain\Response;

interface ResponseInterface
{
    public function render(bool $isAjax, array $data, string $ajaxView, string $view, ?bool $customContent);
}
