<?php

declare(strict_types=1);

namespace App\Domain\Post\Content;

use Illuminate\Support\Facades\Blade;

class BladeToHtmlGenerator
{
    public static function generate(string $bladeContent): string
    {
        ob_start() and extract([], EXTR_SKIP);

        try {
            eval('?>' .  Blade::compileString($bladeContent));
        } catch (\Exception $e) {
            ob_get_clean();
            throw $e;
        }

        return ob_get_clean();
    }
}
