<?php

declare(strict_types=1);

namespace App\Domain\Pages;

class PageTypes
{
    const BLOG_POST = 1;
    const FAST_POST = 2;
    const MAIN_PAGE = 3;

    private static array $types = [
        self::BLOG_POST => 'Post na blogu',
        self::FAST_POST => 'Szybki post',
        self::MAIN_PAGE => 'Normalna podstrona',
    ];

    public static function getTypes(): array
    {
        return self::$types;
    }

    public static function findType(int $number): ?string
    {
        return self::getTypes()[$number] ?? null;
    }
}
