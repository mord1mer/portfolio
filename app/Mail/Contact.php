<?php

declare(strict_types=1);

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use Queueable,
        SerializesModels;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->request['firstname'] . ' ' . $this->request['lastname'];
        return $this->markdown('emails.contact.send')
            ->from($this->request['email'], $name)
            //->cc($this->request['email'], $name)
            //->bcc($this->request['email'], $name)
            //->to($this->request['email'], $name)
            ->replyTo($this->request['email'], $name)
            ->subject($this->request['subject'])
            ->with(['data' => $this->request]);
    }
}
