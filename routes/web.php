<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PostController;
use App\Http\Controllers\Admin\PostController as AdminPostController;
use App\Http\Controllers\FastPostController;
use App\Http\Controllers\MainPagesAjaxController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\Contact\ContactController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Upload\UploadFilesController;
use App\Http\Controllers\Admin\UserController;

// Auth::routes();
Auth::routes(['register' => false]);
//Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
//Route::post('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
//Route::post('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
//Route::get('password/reset', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
//===============================================================================================
Route::get('/blog', [PostController::class, 'index'])->name('blog');
Route::get('/blog/category/{slug}', [PostController::class, 'postsByCategory'])->name('blogCategory');
Route::get('/blog/tag/{slug}', [PostController::class, 'postsByTag'])->name('blogTag');
Route::get('blog/{slug}', [PostController::class, 'singlePost'])->name('blog.single');

Route::get('fastpost', [FastPostController::class, 'index'])->name('FastPost');
Route::get('fastpost/{slug}', [FastPostController::class, 'singleFastPost'])->name('fastPostSingle');
Route::get('/fastpost/tag/{slug}', [FastPostController::class, 'fastPostsByTag'])->name('FastPostTag');
//===============================================================================================
Route::get('comment/getpostcomments/{postId}/{parentId?}/{offset?}', [CommentController::class, 'getPostComments'])->name('comment.getpostcomments');
Route::resource('comment', CommentController::class)->only('store');
Route::resource('contact', ContactController::class)->only(['index', 'store']);
//===============================================================================================
//===============================================================================================
Route::group(['middleware' => ['role:ROLE_REDACTOR']], function () {
    Route::resource('/admin/post', AdminPostController::class)->only('create', 'store', 'index', 'show', 'edit', 'update', 'destroy');
    Route::resource('/admin/upload-files/get', UploadFilesController::class);
});
//===============================================================================================
Route::group(['middleware' => ['role:ROLE_ADMIN']], function () {
    Route::resource('/admin/categories', CategoryController::class)->only(['index', 'edit', 'update', 'create', 'store']);
    Route::resource('/admin/comment',  CommentController::class)->only(['edit', 'update']);
    Route::resource('/admin/tags', TagController::class)->only(['index', 'edit', 'update', 'create', 'store']);
});
//===============================================================================================
Route::group(['middleware' => ['role:ROLE_SUPER_ADMIN']], function () {
    Route::resource('/admin/categories', CategoryController::class)->only(['destroy']);
    Route::resource('/admin/comment', CommentController::class)->only('destroy');
    Route::resource('/admin/tags', TagController::class)->only(['destroy']);
    Route::resource('/admin/user', UserController::class)->only(['create', 'store', 'index', 'show', 'edit', 'update', 'destroy']);
});
//Route::resource('/adminfastpost', 'Admin\FastPostController', ['as' => 'fastpost','only' => ['index','edit', 'update','create','store','show']]);
Route::get('/', [MainPagesAjaxController::class, 'index'])->name('home');
Route::get('/{any}', [MainPagesAjaxController::class, 'index'])->name('pages')->where('any', '.*');
