<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Auth;

class LinksTest extends TestCase
{

    /**
     * @dataProvider noneAuthLinksProvider
     */
    public function testNoneAuthLinks($expect, $link): void
    {
        $data = $this->get($link);
        $data->assertStatus($expect);
    }

    public function testAuthLinksNoneAuth(): void
    {
        foreach ($this->authLinks() as $value) {
            $data = $this->get($value);
            $data->assertStatus(302);
        }
    }

    public function testLoginPost(): void
    {
        $response = $this->call('POST', '/login', [
            'email' => 'grzesiekneo2@o2.pl',
            'password' => 'tajne',
            '_token' => csrf_token()
        ]);

        $response->assertSessionMissing('errors');
        $authUser = Auth::user();
        $this->assertEquals('ROLE_SUPER_ADMIN', $authUser->roles[0]);
        $this->assertEquals('Grzesiek Tarka', $authUser->name);
        $this->assertEquals('grzesiekneo2@o2.pl', $authUser->email);

        foreach ($this->authLinks() as $value) {
            $data = $this->get($value);
            $data->assertStatus(200);
        }
    }

    private function authLinks()
    {
        return [
            '/admin/post/create',
            '/admin/post',
            '/admin/post/1',
            '/admin/post/1/edit',
            '/admin/tags',
            '/admin/comment/1/edit',
            '/admin/tags/1/edit',
            '/admin/tags/create',
            '/admin/categories',
            '/admin/categories/1/edit',
            '/admin/user',
            '/admin/user/1/edit',
        ];
    }

    public static function noneAuthLinksProvider(): array
    {
        return [
            [200, '/'],
            [200, '/o-mnie'],
            [200, '/portfolio'],
            [200, '/blog'],
            [200, '/fastpost'],
            [200, '/contact'],
            [200, '/login'],
            [200, '/blog/jak-stworzyc-swoj-wlasny-helper-w-laravelu-55'],
            [200, '/blog/category/programowanie'],
            [200, '/blog/tag/css'],
            [200, '/comment/getpostcomments/1/0/0'],
            //======================================
            [404, 'blog/none-isset-post'],
            [404, '/blog/category/none-isset'],
            [404, '/blog/tag/janusz'],
            [404, '/none/isset/path'],
            [404, '/none-isset-path'],
            [404, '/czterystacztery'],
        ];
    }
}
