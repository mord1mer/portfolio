<?php

namespace Tests\Feature;

use Tests\TestCase;
use Session;
use Illuminate\Support\Facades\Auth;

class GetCommentsTest extends TestCase {

    public function testGetComments() {
        $request = $this->get('/comment/getpostcomments/1/0/0', ['HTTP_X-Requested-With' => 'XMLHttpRequest']);
        $data = json_decode($request->getContent(), true);
        
        $this->assertEquals($data[0][0], [
            "id" => 1,
            "comment_parent" => "0",
            "number_of_responses" => 0,
            "comment_author" => "Grzesiek T",
            "comment_author_email_md5" => "05a87867a89b56e201309c2224e201bb",
            "comment_author_url" => null,
            "comment_content" => "Fajny komentarz nie ?",
            "publishedAt" => "2018-08-09 21:18:14",
        ]);
    }

    // public function testJanusz()
    // {
    //     $request=$this->get('https://grzesiekneo2.ct8.pl/comment/getpostcomments/1', array('HTTP_X-Requested-With' => 'XMLHttpRequest'));
    //     $data = json_decode($request->getContent(), true);
    //     dump($data);
    // }
}
