<?php

use App\Domain\Pages\PageTypes;
use App\Http\Model\Post;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CommentSeeder extends Seeder
{
    public function run()
    {
        $phpUniTestComment = new \App\Http\Model\Comment();
        $phpUniTestComment->post_id = 1;
        $phpUniTestComment->comment_author = 'Grzesiek T';
        $phpUniTestComment->comment_author_email = 'grzesiekneo2@o2.pl';
        $phpUniTestComment->comment_author_email_md5 = md5('grzesiekneo2@o2.pl');
        $phpUniTestComment->comment_author_ip = '192.168.110';
        //========================================================================================
        $phpUniTestComment->comment_content = 'Fajny komentarz nie ?';
        $phpUniTestComment->comment_parent = 0;
        $phpUniTestComment->comment_agent = 'user agent';
        $phpUniTestComment->publishedAt = date('2018-08-09 21:18:14');
        $phpUniTestComment->save();

        require base_path('database/seeders/posts/posts.php');

        $postsIdsArray = [];

        foreach ($posts as $post) {
            if ($post['type'] == PageTypes::BLOG_POST) {
                $postsIdsArray[] = $post['id'];
            }
        }

        $faker = Faker::create('pl_PL');
        $numberOfComments = 2000;

        for ($comments = 1; $comments <= $numberOfComments; $comments++) {

            $randPostId = $postsIdsArray[array_rand($postsIdsArray, 1)];

            Post::where('id', '=', $randPostId)->increment('number_of_comments');

            $comment = new \App\Http\Model\Comment();
            $comment->post_id = $randPostId;
            $comment->comment_author = $faker->userName;
            $comment->comment_author_email = $faker->email;
            $comment->comment_author_email_md5 = md5($faker->email);
            $comment->comment_author_ip = $faker->ipv4;
            $comment->comment_content = $faker->text($maxNbChars = 200);
            $comment->comment_parent = 0;
            $comment->comment_agent = $faker->userAgent;
            $comment->publishedAt = date('Y-m-d H:i:s');
            $comment->save();
        }
    }
}
