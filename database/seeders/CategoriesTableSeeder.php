<?php

use App\Http\Model\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        $Tags = [
            ['name' => 'programowanie'],
            ['name' => 'windows'],
        ];

        foreach ($Tags as $value) {
            Category::insert([
                'name' => $value['name'],
                'slug' => $value['name'],
            ]);
        }
    }
}
