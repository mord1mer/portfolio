<?php

use App\Http\Model\PostTag;
use Illuminate\Database\Seeder;

class PostTagSeeder extends Seeder
{
    public function run()
    {
        require base_path('database/seeders/post_tag/post_tag.php');

        foreach ($post_tag as $tag) {
            PostTag::insert($tag);
        }
    }
}
