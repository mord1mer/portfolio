<?php

use App\Http\Model\Post;
use Illuminate\Database\Seeder;


class NewPostsTableSeeder extends Seeder
{
    public function run()
    {
        require base_path('database/seeders/posts/posts.php');

        foreach ($posts as $post) {
            Post::insert($post);
        }
    }
}
