<?php

use App\Http\Model\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    public function run()
    {
        require base_path('database/seeders/tags/tags.php');

        foreach ($tags as $tag) {
            Tag::insert($tag);
        }
    }
}
