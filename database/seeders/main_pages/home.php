<article class="vertical dark-container">
    <div class="crossfade">
        <figure></figure>
        <figure></figure>
        <figure></figure>
        <figure></figure>
        <figure></figure>
    </div>
    <div class="container-fluid table-cell text-center" id="welcome">
        <div class="container write-text">
        <h1 class="hellow-world wow bounceInDown hidden-xs" data-wow-delay="0.3s">&#60;&#8725;Hellow World&gt</h1>
        <h3 class="main-title tp-splitted" id="demo">Nazywam sie Grzegorz Tarka. Witam cie na mojej stronie</h3>
        </div>
        <a href="#oferta" class="sendmessage_button line-height-50 smoothScroll wow bounceInUp no-ajax-load" data-wow-delay="9s" style="display: inline-block">OFERTA</a>
        <div class="arrow animated infinite bounce no-ajax-load">
            <a class="smoothScroll" style="display: block" href="#projects">
                <img src="{{asset('img/down-arrow.svg')}}" class="ikonaproject" alt="Strzałka">
            </a>
        </div>
    </div>
</article>
<article class="container-fluid bg-white pd black-font background_cutout" id="projects">
    <div class="row">
        <div class="container vertical_align">
            <div class="row">
                <div class="col-lg-5 text-right text-center-md wow zoomInLeft" data-wow-delay="0s"> <img src="{{asset('img/home/39fae7201b008036d5d46fde08364d63.jpg')}}" alt="Kompas"> </div>
                <div class="col-lg-7 text-left wow zoomInRight" data-wow-delay="0s">
                    <h2>Potrzebujesz strony internetowej ?</h2>
                    <h4>Przenieś swój bizes na niespotykany dotąd poziom!</h4>
                    <p>Internet w oczach twoich klientów to gigantyczna porównywarka cen usług i produktów ... tysiące podobnych ofert i wśród nich Ty - zastanów się, czy jesteś dostatecznie widoczny na tle konkurencji ? Profesjonalnie stworzona strona to najlepszy sposób aby twoja marka wyróżniała sie w internecie oraz gwarancja tego, że produkty i usługi oferowane przez ciebie znacznie lepiej będą się prezentować. Czas to pieniądz i ja szanuje twój dlatego proponuje ci w tym miejscu wszystko to czego potrzebujesz! Profesjonalnie stworzona strona na zawsze odmieni twój wizerunek w sieci ! </p>
                </div>
            </div>
        </div>
    </div>
</article>
<article class="container-fluid gradient_bg">
    <div class="container pd-lf-rg-0">
        <header class="row">
            <div class="col-lg-12">
                <h1>Zalety nowoczesnej strony WWW</h1>
            </div>
        </header>
        <div class="row">
            <section class="col-lg-4 wow zoomIn" data-wow-delay="0.1s">
                <h3>Intuicyjna i wygodna</h3>
                <p class="text-left">Zaprojektowana jako podróż dla Klienta po Twojej ofercie, nie wymaga od Klienta szukania i dokonywania niezrozumiałych wyborów - intuicyjnie przeprowadza go krok po kroku, od najważniejszych zalet po szczegółową ofertę, wraz z najbardziej oczywistą czynnością na stronie WWW - przewijaniem.</p>
            </section>
            <section class="col-lg-4 wow zoomIn" data-wow-delay="0.1s">
                <h3>Przejrzysta i czytelna</h3>
                <p class="text-left">Dzięki liniowej organizacji informacji oraz subtelnym animacjom, przewijana strona pomaga Klientowi zapoznać się z produktem. Konieczność zwięzłej prezentacji cech produktu sprawia, że komunikacja z Klientem staje się łatwiejsza.</p>
            </section>
            <section class="col-lg-4 wow zoomIn" data-wow-delay="0.1s">
                <h3>Przyjazna i mobilna</h3>
                <p class="text-left">W erze technologii Twoja oferta powinna być łatwo dostępna z każdego urządzenia - komputera, telewizora, tabletu oraz telefonu. Tworzone przeze mnie strony współpracują z każdym urządzeniem !</p>
            </section>
        </div>
    </div>
</article>
<article>
    <header class="container-fluid offer_conatiner white-font over-hiden" id="oferta">
        <div class="container">
            <div class="row text-left">
                <div class="col-lg-12">
                    <h3 class=" wow fadeInLeft" data-wow-delay="0.5s">Spójrz jakie rzeczy mogę dla ciebie zrobić</h3>
                    <h1 class="bigfont wow fadeInRight" data-wow-delay="0.5s">OFERTA</h1>
                    <hr class="my-hr wow fadeInUp" data-wow-delay="0.8s">
                </div>
            </div>
        </div>
    </header>
    <article class="container-fluid pd-tp-bt-0 bg-white offer offer pd-zero">
        <section class="col-lg-3 col-md-6 col-sm-12 col-xs-12 hover-services" data-mh="my-group">
            <a href="#1" class="scroll_to_content pd-top-bt-50px deskop-padding" data-toggle="tab">
                <i class="fas fa-tv fa-4x" aria-hidden="true"></i>
                <h3>Strony Internetowe</h3>
                <hr class="my-hr-2">
                <p class="spcial-p">Strony internetowe dostosowane do Twoich potrzeb.</p>
            </a>
        </section>
        <section class="col-lg-3 col-md-6 col-sm-12 col-xs-12 hover-services" data-mh="my-group">
            <a href="#2" class="scroll_to_content pd-top-bt-50px deskop-padding" data-toggle="tab">
                <i class="far fa-gem fa-4x" aria-hidden="true"></i>
                <h3>Aplikacje Internetowe</h3>
                <hr class="my-hr-2">
                <p class="spcial-p">Skrypty i Aplikacje bazodanowe.</p>
            </a>
        </section>
        <section class="col-lg-3 col-md-6 col-sm-12 col-xs-12 hover-services" data-mh="my-group">
            <a href="#3" class="scroll_to_content pd-top-bt-50px deskop-padding" data-toggle="tab">
                <i class="fab fa-wordpress fa-4x" aria-hidden="true"></i>
                <h3>Wordpress development</h3>
                <hr class="my-hr-2">
                <p class="spcial-p">Strony oparte na najpopularniejszym na świecie systemie CMS.</p>
            </a>
        </section>
        <section class="col-lg-3 col-md-6 col-sm-12 col-xs-12 hover-services" data-mh="my-group">
            <a href="#4" class="scroll_to_content pd-top-bt-50px deskop-padding" data-toggle="tab">
                <i class="fab fa-free-code-camp fa-4x" aria-hidden="true"></i>
                <h3>Optymalizacja stron i aplikacji</h3>
                <hr class="my-hr-2">
                <p class="spcial-p">Optymalizacja i dodawanie nowych funkcji do stron i istniejących aplikacji.</p>
            </a>
        </section>
    </article>
    <div class="clearfix"></div>
    <article class="container-fluid pd-zero black-font" id="offer_container" style="background-color: #fff">
        <div id="exTab2">
            <div class="tab-content">
                <div class="tab-pane fade in active" id="1">
                    <section class="vertical_align">
                        <div class="col-lg-6 pd-zero text-left padding-left-s">
                            <h1 class="bigfont">STRONY INTERNETOWE</h1>
                            <hr class="my-hr-3">
                            <div class="clearfix"></div>
                            <p class="spcial-p">W dzisiejszym czasie biznes bez strony internetowej skazany jest na porażkę. Każde nowoczesne przedsiębiorstwo stawiające na rozwój buduje swój wizerunek w internecie. Strona internetowa to nie tylko zbiór informacji o firmie. Jest to również potężne narzędzie do budowania długotrwałych relacji firmy z klientem.</p>
                            <p class="spcial-p">Mogę zaprojektować od podstaw uszyta na miarę stronę dostosowaną do twoich potrzeb. Wszystkie projekty są w pełni responsywne (RWD) i dobrze wyglądają na każdym urządzeniu.</p>
                        </div>
                        <div class="col-lg-6 pd-zero">
                            <figure><img src="{{asset('img/home/oferta/1/ipad-820272_1920.jpg')}}" class="img-responsive my-img"></figure>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="2">
                    <section class="vertical_align">
                        <div class="col-lg-6 pd-zero text-left padding-left-s">
                            <h1 class="bigfont">APLIKACJE INTERNETOWE</h1>
                            <hr class="my-hr-3">
                            <div class="clearfix"></div>
                            <p class="spcial-p ">Oferuję wykonanie autorskich, dostosowanych do indywidualnych potrzeb aplikacji internetowych. Dopasowuje funkcjonalność oprogramowania do indywidualnego charakteru firmy, stosując przy tym frameworki PHP, Laravel Framework bądź Codeigniter. Dedykowana aplikacja to duży krok w usprawnianiu działania twojej firmy! </p>
                        </div>
                        <div class="col-lg-6 pd-zero">
                            <figure><img src="{{asset('img/home/oferta/2/blur-1853262_1920.jpg')}}" class="img-responsive my-img"></figure>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="3">
                    <section class="vertical_align">
                        <div class="col-lg-6 pd-zero text-left padding-left-s">
                            <h1 class="bigfont">WORDPRESS</h1>
                            <hr class="my-hr-3">
                            <div class="clearfix"></div>
                            <p class="spcial-p ">WordPress miał swój początek w 2003 roku i posiadał wówczas zaledwie kilku użytkowników. Dziś to najpopularniejszy system CMS na świecie i zasila ponad 27% wszystkich stron internetowych!. Ze względu na na jego funkcjonalność, ogromną liczbę wtyczek i szablonów z powodzeniem możemy użyć go do zarządzania treścią twojej strony, dzięki czemu dodawanie treści na stonie stanie się dziecinne proste!.</p>
                            <p class="spcial-p ">Z moją pomocą uruchomisz własny serwis oparty na tym narzędziu. Mogę stworzyć dla ciebie szablon całkowicie od podstaw lub wykorzystać już gotowy darmowy lub komercyjny szablon.</p>
                        </div>
                        <div class="col-lg-6 pd-zero">
                            <figure><img src="{{asset('img/home/oferta/3/wordpress-923188_1920.jpg')}}" class="img-responsive my-img"></figure>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="4">
                    <section class="vertical_align">
                        <div class="col-lg-6 pd-zero text-left padding-left-s">
                            <h1 class="bigfont">OPTYMALIZACJA</h1>
                            <hr class="my-hr-3">
                            <div class="clearfix"></div>
                            <p class="spcial-p ">Ile razy wchodząc na stronę internetową z powodu długiego wczytywania po kilku chwilach frustracji przechodzisz do innej strony w wyszukiwarce. Mimo tego że strona może zawierać potrzebne rzeczy czy informacje pomijasz ją z powodu długiego czasu ładowania. </p>
                            <p class="spcial-p">Zadbaj o to aby twoja strona była należycie zoptymalizowana i sytuacje tego typu nie dotyczyły twoich wizytówek w sieci.</p>
                        </div>
                        <div class="col-lg-6 pd-zero">
                            <figure><img src="{{asset('img/home/oferta/4/military-jet-1053394_1280.jpg')}}" class="img-responsive my-img"></figure>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </article>
</article>
<article class="container-fluid home_contact">
    <div class="row vertical_align">
        <div class="col-lg-6">
            <h4 class="white-font">Masz pytania?</h4>
        </div>
        <div class="col-lg-6"> <a class="home-link ajax" href="{{ route('contact.index') }}">Skontaktuj się</a> </div>
    </div>
</article>
<article class="container-fluid bg-white hidden-sm hidden-xs">
       <i class="fab fa-html5 custom-icon wow slideInLeft" data-wow-delay="0.2s" aria-hidden="true"></i> <i class="fab fa-css3-alt custom-icon wow slideInLeft" data-wow-delay="0.3s" aria-hidden="true"></i> <i class="fas fa-database custom-icon wow slideInLeft" data-wow-delay="0.4s" aria-hidden="true"></i> <i class="fab fa-wordpress custom-icon wow slideInLeft" data-wow-delay="0.5s" aria-hidden="true"></i> <i class="fab fa-facebook custom-icon  wow slideInLeft" data-wow-delay="0.6s" aria-hidden="true"></i> <i class="fab fa-google custom-icon  wow slideInLeft" data-wow-delay="0.7s" aria-hidden="true"></i> <i class="fab fa-firefox custom-icon wow slideInLeft" data-wow-delay="0.8s" aria-hidden="true"></i> <i class="fab fa-chrome custom-icon wow slideInLeft" data-wow-delay="0.9s" aria-hidden="true"></i> <i class="fab fa-opera custom-icon wow slideInLeft" data-wow-delay="1s" aria-hidden="true"></i> <i class="fab fa-internet-explorer custom-icon wow slideInLeft" data-wow-delay="1.1s" aria-hidden="true"></i> <i class="fab fa-safari custom-icon wow slideInLeft" data-wow-delay="1.2s" aria-hidden="true"></i> <i class="fab fa-github custom-icon wow slideInLeft" data-wow-delay="1.3s" aria-hidden="true"></i> <i class="fab fa-windows custom-icon wow slideInLeft" data-wow-delay="1.4s" aria-hidden="true"></i> <i class="fab fa-linux custom-icon wow slideInLeft" data-wow-delay="1.5s" aria-hidden="true"></i>
</article>