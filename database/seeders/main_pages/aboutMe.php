<article class="container-fluid pd-tp-bt-0">
    <div class="row ligh-bg blog">
        <div class="col-lg-12 white-font">
            <h1>O MNIE</h1>
        </div>
    </div>
</article>
<article>
    <section class="container-fluid black-font grey-cont pd-lf-rg-0">
        <div class="container kolor">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <figure>
                        <img src="{{asset('img/ja/20141101_173642.jpg')}}" class="img-circle" width="204" height="254"
                             alt="Grzegorz avatar">
                    </figure>
                </div>
                <div class="col-lg-6 col-lg-offset-3 text-left">
                    <p>
                        <strong>Witam nazywam się Grzegorz,</strong> programowaniem zajmuje się od około 5 lat, jednak trudno powiedzieć mi kiedy to wszystko się zaczęło bo w zasadzie od zawsze interesowałem sie informatyką i programowaniem. Z czasem zainteresowanie to przełożyło poprostu na prace w tym zakresie. Oprócz programowania zajmuje się naprawą szeroko pojętej elektroniki. 
                    </p>
                    <p><strong>System szkolnictwa</strong> nie jest w Polsce idealny a kiedyś przez moje nie
                        zdecydowanie co chcę w życiu robić skończyłem studia całkowicie w innym kierunku niż informatyka
                        i programowanie jako takie. Kierunek jaki ukończyłem był dla mnie za nudny więc w między czasie
                        przewinąłem się przez kilka różnych zawodów :). Postanowiłem wrócić jednak do informatyki, którą
                        zawsze lubiłem. Stosunkowo szybko nadrobiłem "zaległości" i gdy poczułem się już pewnie w
                        Fron/Back End postanowiłem uderzyć na głęboką wodę :). </p>
                    <p><strong>Jestem samoukiem</strong> stale dążącym do celu. Programowanie w różnych językach stało
                        się od jakiegoś już czasu takim celem. Chętnie uczę się nowych technologii i szybko
                        przyzwyczajam sie do wszelkich nowości. </p>
                    <p><strong>Poza programowaniem</strong> 
                    interesuje sie motoryzacją podróżami i historią. Lubię również jeździć na rowerze, siatkówkę, góry i czasem obejrzeć jakiś dobry film. Jestem też wielkim fanem starych samochodów i klimatu lat 80 i 90. Należe do otwartych osób  i łatwo nawiązuje znajomości. 
                    </p>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4 col-md-offset-2">
                    <a href="https://github.com/GrzesiekTa" target="_blank">
                        <i class="fab fa-7x fa-github-square"></i>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="https://www.linkedin.com/in/grzegorz-tarka-547444148/" target="_blank">
                        <i class="fab fa-7x fa-linkedin"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="quotation white-font container-fluid dark-bg">
        <div class="col-lg-6 col-lg-offset-3">
            <h2 class="text-center">—</h2>
            <h1 class="text-center">“<strong>Wyobraźnia bez wiedzy może stworzyć rzeczy piękne. Wiedza bez wyobraźni
                    najwyżej doskonałe.”&nbsp;</strong></h1>
            <h3>—Albert Einstein—</h3>
        </div>
    </section>
</article>
<article class="container-fluid  grey-cont text-center">
    <div class="container">
        <header><h2 class="elegantshadow"><b>Umiejętności</b></h2></header>
        <br>
        <div class="row skils-container">
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>HTML</b></h2>
                <i class="fab fa-html5"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>CSS3</b></h2>
                <i class="fab fa-css3-alt"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>BOOTSTRAP</b></h2>
                <img src="{{asset('img/skils/Boostrap_logo.png')}}" alt="placeholder+image">
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>PHP</b></h2>
                <i class="fab fa-php"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>LARAVEL</b></h2>
                <i class="fab fa-laravel"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>SYMFONY</b></h2>
                <img src="{{asset('img/skils/img_437055.png')}}" alt="placeholder+image">
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>JAVASCRIPT</b></h2>
                <i class="fab fa-js"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>JQUERY</b></h2>
                <img src="{{asset('img/skils/jquery.png')}}" alt="placeholder+image">
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>VUE</b></h2>
                <i class="fab fa-vuejs"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>GIT</b></h2>
                <i class="fab fa-git-square"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>SQL</b></h2>
                <i class="fas fa-database"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>SEO</b></h2>
                <i class="fas fa-search"></i>
            </section>
        </div>
    </div>
</article>