<article class="container-fluid pd-tp-bt-0">
    <div class="row ligh-bg blog">
        <div class="col-lg-12 white-font">
            <h1>Szablony stron</h1>
        </div>
    </div>
</article>
<article class="container-fluid pd-top-bt-50px black-font grey-cont">
    <header>
        <nav class="nav_portfolio row">
            <ul class="simplefilter">
                <li class="active" data-filter="all">WSZYSTKIE</li>
                <li data-filter="1">PHOTO</li>
                <li data-filter="2">RESTAURANT</li>
                <li data-filter="3">SPORT</li>
                <li data-filter="4">MUSIC</li>
                <li data-filter="5">WIZYTÓWkI</li>
                <li data-filter="7">INNE</li>
            </ul>
        </nav>
    </header>
    <section class="container">
        <!-- Shuffle & Sort Controls -->
        <div class="row mg-bt-top-10px">
            <ul class="sortandshuffle pd-zero">
                <li class="shuffle-btn" data-shuffle>Shuffle</li>
            </ul>
        </div>
        <!-- Search control -->
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <div class="form-group">
                    <div class="controls"> 
                        <input type="text" placeholder="Szukaj po tytule..." class="form-control form-control-special" name="filtr-search" data-search> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row filtr-container">
            <a href="portfolio/szablony/29" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme29 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Photo#1</span> 
                </div>
            </a>
            <a href="portfolio/szablony/28" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Ecology#1">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme28 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Ecology#1</span> 
                </div>
            </a>
            <a href="portfolio/szablony/27" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#1">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme27 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#1</span> 
                </div>
            </a>
            <a href="portfolio/szablony/26" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme26 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#2</span> 
                </div>
            </a>
            <a href="portfolio/szablony/25" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="3" data-sort="Sport#1">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme25 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Sport#1</span> 
                </div>
            </a>
            <a href="portfolio/szablony/24" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme24 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Photo#2</span> 
                </div>
            </a>
            <a href="portfolio/szablony/23" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo#3">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme23 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Photo#3</span> 
                </div>
            </a>
            <a href="portfolio/szablony/22" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Dreamhouse">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme22 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">dreamhouse</span> 
                </div>
            </a>
            <a href="portfolio/szablony/21" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Restless machines">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme21 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Wizytówki#1</span> 
                </div>
            </a>
            <a href="portfolio/szablony/20" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Restless machines">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme20 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">restless machines</span> 
                </div>
            </a>
            <a href="portfolio/szablony/19" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo#4">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme19 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Photo#4</span> 
                </div>
            </a>
            <a href="portfolio/szablony/18" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#3">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme18 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#3</span> 
                </div>
            </a>
            <a href="portfolio/szablony/17" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="7" data-sort="Inne#1">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme17 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Inne#1</span> 
                </div>
            </a>
            <a href="portfolio/szablony/16" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Wizytówki#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme16 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Wizytówki#2</span> 
                </div>
            </a>
            <a href="portfolio/szablony/15" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#4">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme15 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#4</span> 
                </div>
            </a>
            <a href="portfolio/szablony/14" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#5">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme14 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#5</span> 
                </div>
            </a>
            <a href="portfolio/szablony/13" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="7" data-sort="Inne#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme13 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Inne#2</span> 
                </div>
            </a>
            <a href="portfolio/szablony/12" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo#5">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme12 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Photo#5</span> 
                </div>
            </a>
            <a href="portfolio/szablony/11" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="3" data-sort="Sport#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme11 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Sport#2</span> 
                </div>
            </a>
            <a href="portfolio/szablony/10" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="7" data-sort="Inne#3">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme10 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Inne#3</span> 
                </div>
            </a>
            <a href="portfolio/szablony/9" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Wizytówki#3">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme9 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Wizytówki#3</span> 
                </div>
            </a>
            <a href="portfolio/szablony/8" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo#6">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme8 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Photo#6</span> 
                </div>
            </a>
            <a href="portfolio/szablony/6" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="4" data-sort="Music#1">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme6 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Music#1</span> 
                </div>
            </a>
            <a href="portfolio/szablony/5" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="4" data-sort="Music#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme5 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Music#2</span> 
                </div>
            </a>
            <a href="" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#6">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset('img/szablony/theme4 - kopia.jpg')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#6</span> 
                </div>
            </a>
        </div>
    </section>
</article>
<script type="text/javascript">
    function initSort() {
        $.getScript("js/sort/jquery.filterizr.js");
        //Simple filter controls
        $('.simplefilter li').click(function () {
            $('.simplefilter li').removeClass('active');
            $(this).addClass('active');
        });
        //Multifilter controls
        $('.multifilter li').click(function () {
            $(this).toggleClass('active');
        });
        //Shuffle control
        $('.shuffle-btn').click(function () {
            $('.sort-btn').removeClass('active');
        });
        //Sort controls
        $('.sort-btn').click(function () {
            $('.sort-btn').removeClass('active');
            $(this).addClass('active');
        });
        setTimeout(function () {
            $('.filtr-container').filterizr();
        }, 1000);
    }

    window.onload = function () {
        initSort();
    };

    if (window.jQuery) {
        initSort();
    }
</script>