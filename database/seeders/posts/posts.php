<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.9.5
 */

/**
 * Database `m6035_portfolio_moja`
 */

/* `m6035_portfolio_moja`.`posts` */
$posts = array(
  array('id' => '1','user_id' => '1','title' => 'Opcje odczytu pliku, odczyt pliku jquery','slug' => 'opcje-odczytu-pliku-odczyt-pliku-jquery','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
    &lt;meta charset="UTF-8"&gt;
    &lt;title&gt;Document&lt;/title&gt;
    &lt;script src="http://code.jquery.com/jquery-1.11.2.min.js"&gt;&lt;/script&gt;
&lt;/head&gt;
&lt;body&gt;
    &lt;script&gt;
        $( document ).ready(function() {
            alert( "document loaded" );
        });
        $( window ).load(function() {
            alert( "window loaded" );
        });
    &lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-12 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Opcje odczytu pliku, odczyt pliku jquery','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '2','user_id' => '1','title' => 'funkcja resize outer height','slug' => 'funkcja-resize-outer-height','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
    &lt;meta charset="UTF-8"&gt;
    &lt;title&gt;Document&lt;/title&gt;
    &lt;script src="http://code.jquery.com/jquery-1.11.2.min.js"&gt;&lt;/script&gt;
&lt;/head&gt;
&lt;body&gt;
    &lt;script&gt;
        if ($("#nav").outerHeight() &gt; $( window ).height()) {
            $(\'#nav\').addClass(\'test-over\');
        }else{
            $(\'#nav\').removeClass(\'test-over\');
        }
        $(window).resize(function() {
            if ($("#nav").outerHeight() &gt; $( window ).height()) {
                $(\'#nav\').addClass(\'test-over\');
            }else{
                $(\'#nav\').removeClass(\'test-over\');
            }
        });
    &lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-12 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'funkcja resize outer height','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '3','user_id' => '1','title' => 'Losowanie koloru w php','slug' => 'losowanie-koloru-w-php','content' => '<pre class="language-php"><code>&lt;?php
function rand_color() {
  $r = str_pad(dechex(rand(0, 255)), 2, \'0\', STR_PAD_LEFT);
  $g = str_pad(dechex(rand(0, 255)), 2, \'0\', STR_PAD_LEFT);
  $b = str_pad(dechex(rand(0, 255)), 2, \'0\', STR_PAD_LEFT);
  return \'#\'.$r.$g.$b;
}
echo \'&lt;div style="background: \'.rand_color().\';height:600px"&gt;treść&lt;/div&gt;\';</code></pre>','publishedAt' => '2017-02-12 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Losowanie koloru w php','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '4','user_id' => '1','title' => 'Licznik znaków liczenie','slug' => 'licznik-znakow-liczenie','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
   &lt;head&gt;
      &lt;meta charset="UTF-8"&gt;
      &lt;title&gt;Document&lt;/title&gt;
   &lt;/head&gt;
   &lt;body&gt;
      &lt;textarea id="customInput" placeholder="Wpisz tutaj sw&oacute;j tekst!"&gt;&lt;/textarea&gt;
      &lt;div id="charCount"&gt;&lt;/div&gt;
      &lt;script&gt;
         var input = document.querySelector("#customInput"),
             charCount = document.querySelector("#charCount");
         
         var onKeyDown = function() {
             charCount.textContent = input.value.length+1;
         };
         
         input.addEventListener("keydown", onKeyDown);
      &lt;/script&gt;
   &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-12 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Licznik znaków liczenie','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '5','user_id' => '1','title' => 'Scroll menu overflow pojawiający się suwak','slug' => 'scroll-menu-overflow-pojawiajacy-sie-suwak','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
   &lt;head&gt;
      &lt;meta charset="UTF-8"&gt;
      &lt;title&gt;Document&lt;/title&gt;
      &lt;style&gt;
         .nav-overflow {
         background-color: green
         }
         @media only screen and (max-height: 557px) {
         .nav-overflow {
         list-style-type: none;
         overflow-y: auto;
         white-space: nowrap;
         -webkit-overflow-scrolling: touch;
         height: 100vh;
         background-color: red
         }
         }
      &lt;/style&gt;
   &lt;/head&gt;
   &lt;body&gt;
      &lt;div class="nav-overflow"&gt;
         &lt;ul&gt;
            &lt;li&gt;
               &lt;a href=""&gt;jeden&lt;/a&gt;
            &lt;/li&gt;
            &lt;li&gt;
               &lt;a href=""&gt;jeden&lt;/a&gt;
            &lt;/li&gt;
            &lt;li&gt;
               &lt;a href=""&gt;jeden&lt;/a&gt;
            &lt;/li&gt;
            &lt;li&gt;
               &lt;a href=""&gt;jeden&lt;/a&gt;
            &lt;/li&gt;
            &lt;li&gt;
               &lt;a href=""&gt;jeden&lt;/a&gt;
            &lt;/li&gt;
            &lt;li&gt;
               &lt;a href=""&gt;jeden&lt;/a&gt;
            &lt;/li&gt;
            &lt;li&gt;
               &lt;a href=""&gt;jeden&lt;/a&gt;
            &lt;/li&gt;
         &lt;/ul&gt;
      &lt;/div&gt;
   &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-12 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Scroll menu overflow pojawiający się suwak','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '6','user_id' => '1','title' => 'Custom style css wyrównywania tekstu textu','slug' => 'custom-style-css-wyrownywania-tekstu-textu','content' => '<pre class="language-css"><code>/*customowe style textu*/
.text-left-not-xs, .text-left-not-sm, .text-left-not-md, .text-left-not-lg { text-align: left; } .text-center-not-xs, .text-center-not-sm, .text-center-not-md, .text-center-not-lg { text-align: center; } .text-right-not-xs, .text-right-not-sm, .text-right-not-md, .text-right-not-lg { text-align: right; } .text-justify-not-xs, .text-justify-not-sm, .text-justify-not-md, .text-justify-not-lg { text-align: justify; } @media (max-width: 767px) { .text-left-not-xs, .text-center-not-xs, .text-right-not-xs, .text-justify-not-xs { text-align: inherit; } .text-left-xs { text-align: left; } .text-center-xs { text-align: center; } .text-right-xs { text-align: right; } .text-justify-xs { text-align: justify; } } @media (min-width: 768px) and (max-width: 991px) { .text-left-not-sm, .text-center-not-sm, .text-right-not-sm, .text-justify-not-sm { text-align: inherit; } .text-left-sm { text-align: left; } .text-center-sm { text-align: center; } .text-right-sm { text-align: right; } .text-justify-sm { text-align: justify; } } @media (min-width: 992px) and (max-width: 1199px) { .text-left-not-md, .text-center-not-md, .text-right-not-md, .text-justify-not-md { text-align: inherit; } .text-left-md { text-align: left; } .text-center-md { text-align: center; } .text-right-md { text-align: right; } .text-justify-md { text-align: justify; } } @media (min-width: 1200px) { .text-left-not-lg, .text-center-not-lg, .text-right-not-lg, .text-justify-not-lg { text-align: inherit; } .text-left-lg { text-align: left; } .text-center-lg { text-align: center; } .text-right-lg { text-align: right; } .text-justify-lg { text-align: justify; } }
/*customowe style textu*/</code></pre>','publishedAt' => '2017-02-12 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Custom style css wyrównywania tekstu textu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '7','user_id' => '1','title' => 'Vertical center fixed element','slug' => 'vertical-center-fixed-element','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
    &lt;meta charset="UTF-8"&gt;
    &lt;title&gt;Document&lt;/title&gt;
    &lt;style&gt;
        .window-container {
            background-color: rgba(0, 0, 0, .8);
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            -webkit-padding-start: 0px;
            margin: auto;
            text-align: center;
            height: 100vh;
            z-index: 8000;
        }
        
        .window-col {
            background-color: rgba(23, 124, 164, 0.9);
            text-align: center;
            padding: 15% 15px 15% 15px;
            border-radius: 5px;
            color: #fff;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            text-align: center;
            margin: auto;
            width: 70%;
            -webkit-padding-start: 0px;
            padding-left: 0px;
        }
        
        .window-close {
            position: absolute;
            top: 0;
            right: 22px;
            font-size: 70px;
            cursor: pointer;
        }
        
        .contact-img {
            width: 110px;
            height: 110px;
            margin: auto;
        }
        
        .content {
            width: 40px
        }
    &lt;/style&gt;
&lt;/head&gt;

&lt;body&gt;
    &lt;div class="content"&gt;
        &lt;p&gt;
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur maiores autem, vel voluptas! Quaerat enim minima, delectus. Impedit dolorem atque nobis iusto quas excepturi est facilis eos voluptas eaque veniam, repellat, totam, beatae. Explicabo aut perferendis architecto optio officia alias distinctio recusandae quis autem, debitis est id cum earum harum, voluptatibus odit, reiciendis minus unde similique at dolorem. Error possimus delectus nesciunt illum molestiae eligendi incidunt, quaerat enim alias officiis ipsum sint debitis accusamus! Assumenda repudiandae accusamus est excepturi adipisci ex aspernatur omnis labore minima. Architecto rerum natus id nisi provident ipsum beatae molestiae eaque sapiente, facilis numquam expedita quidem perferendis tempora harum dolor doloremque dicta culpa adipisci quisquam.
        &lt;/p&gt;
    &lt;/div&gt;
    &lt;div id="window-1" class="window-container"&gt;
        &lt;div class="window-col"&gt;
            &lt;span class="window-close"&gt;&amp;times;&lt;/span&gt;
            &lt;img src="http://drive.google.com/uc?export=download&amp;id=0Bw176tQtKlp1WUd5ZGJTNmVOZUU" class="contact-img"&gt;
            &lt;h2 class="big-font aldrich" style="margin-top:0px"&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis saepe necessitatibus, autem esse aperiam sed &lt;/h2&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/body&gt;

&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-12 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Vertical center fixed element','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '8','user_id' => '1','title' => 'Vertical center element 100% okna','slug' => 'vertical-center-element-100-okna','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;

&lt;head&gt;
    &lt;meta charset="UTF-8"&gt;
    &lt;title&gt;Document&lt;/title&gt;
    &lt;link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"&gt;
    &lt;style&gt;
        body {
            padding: 0px;
            margin: 0px
        }
        
        .vertical {
            display: table;
            height: 100vh;
            width: 100%;
            text-align: center;
            overflow: hidden;
        }
        
        .table-cell {
            display: table-cell;
            vertical-align: middle;
            margin: auto;
            width: 100%
        }
        
        .sectionclass1 {
            background: url(\'https://images81.fotosik.pl/340/42b35b22f5e2a029.jpg\') center center no-repeat fixed;
            background-size: cover;
            position: relative;
            width: 100%!important;
            height: 100vh!important;
        }
        
        .arrow {
            position: absolute;
            left: 0;
            bottom: 0;
            text-align: center;
            margin: auto;
            width: 100%;
            z-index: 3000
        }
    &lt;/style&gt;
&lt;/head&gt;

&lt;body&gt;
    &lt;section class="sectionclass1 overflow-none vertical"&gt;
        &lt;div class="container-fluid height-100 table-cell text-left white-font" id="welcome"&gt;
            &lt;h3 class="wow zoomIn" data-wow-delay="0.5s"&gt;&lt;span class="border-bt big-font"&gt;FITNESS CENTER&amp;&lt;/span&gt;&lt;/h3&gt;
            &lt;h3 class="wow zoomIn" data-wow-delay="0.5s"&gt;&lt;span class="border-bt big-font"&gt;PROFESSIONAL GYM STUDIO&lt;/span&gt;&lt;/h3&gt;
        &lt;/div&gt;
        &lt;div class="arrow"&gt;
            &lt;a href="#about" class="chewron-down smoothScroll"&gt; &lt;i class="fa fa-angle-down fa-5x  div-a" aria-hidden="true"&gt;&lt;/i&gt;&lt;/a&gt;
        &lt;/div&gt;
    &lt;/section&gt;
&lt;/body&gt;

&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-12 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Vertical center element 100% okna','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '9','user_id' => '1','title' => 'Wyrażenia regularne walidacja kodu pocztowego ale mało elegancko','slug' => 'wyrazenia-regularne-walidacja-kodu-pocztowego-ale-malo-elegancko','content' => '<pre class="language-php"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
    &lt;head&gt;
        &lt;meta charset="UTF-8"&gt;
        &lt;title&gt;Walidacja pesla ale mało elegancko :]&lt;/title&gt;
    &lt;/head&gt;
    &lt;body&gt;
        &lt;?php
        if (preg_match(\'@^[0-9][0-9]-[0-9][0-9][0-9]$@\', $_POST[\'pole\'])) {
          echo \'ok\';
        } else {
          echo \'error\';
        }
        ?&gt;
        &lt;form action="pierwsze pesel ale chamsko.php" method="post"&gt;
            &lt;input type="text" name="pole"&gt;
            &lt;input type="submit"&gt;
        &lt;/form&gt;
    &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-19 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Wyrażenia regularne walidacja kodu pocztowego ale mało elegancko','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '10','user_id' => '1','title' => 'Wyrażenia regularne walidacja kodu pocztowego','slug' => 'wyrazenia-regularne-walidacja-kodu-pocztowego','content' => '<pre class="language-php"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;
        &lt;title&gt;Wyrażenia regularne walidacja kodu pocztowego&lt;/title&gt;
    &lt;/head&gt;
    &lt;body&gt;
        &lt;form action="dwa pesel elegancko.php" method="post"&gt;
            &lt;input type="text" name="pole"&gt;
            &lt;input type="submit" value="wyslij"&gt;
        &lt;/form&gt;
        &lt;?php
        if (preg_match(\'@^[0-9]{2}-[0-9]{3}$@\', $_POST[\'pole\'])) {
          echo (\'ok\');
        } else {
          echo (\'error\');
        }
        ?&gt;
    &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-19 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Wyrażenia regularne walidacja kodu pocztowego','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '11','user_id' => '1','title' => 'Wyrażenia regularne czy pole zawiera liczbę','slug' => 'wyrazenia-regularne-czy-pole-zawiera-liczbe','content' => '<pre class="language-php"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;
        &lt;title&gt;&lt;/title&gt;
    &lt;/head&gt;
    &lt;body&gt;
        &lt;form action="jedenhaslocyfra.php" method="post"&gt;
            &lt;input type="text" name="pole"&gt;
            &lt;input type="submit" value="wyslij"&gt;
        &lt;/form&gt;
        &lt;?php
        if (preg_match(\'@[0-9]@\', $_POST[\'pole\'])) {
          echo (\'ok\');
        } else {

          echo (\'error\');
        }
        ?&gt;
    &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-19 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Wyrażenia regularne czy pole zawiera liczbę','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '12','user_id' => '1','title' => 'Walidacja imienia i nazwiska z dużej litery i od 2 do 10 znaków wyrażenia regularne','slug' => 'walidacja-imienia-i-nazwiska-z-duzej-litery-i-od-2-do-10-znakow-wyrazenia-regularne','content' => '<pre class="language-php"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;
        &lt;title&gt;Walidacja imienia i nazwiska z dużej litery i od 2 do 10 znak&oacute;w &lt;/title&gt;
    &lt;/head&gt;
    &lt;body&gt;
        &lt;form action="imieinazwisko.php" method="post"&gt;
            &lt;input type="text" name="pole"&gt;
            &lt;input type="submit" value="wyslij"&gt;
        &lt;/form&gt;
        &lt;?php
        if (preg_match(\'@^[A-Z][a-z]{2,10} [A-Z][a-z]{2,10}$@\', $_POST[\'pole\'])) {
          echo \'ok\';
        } else {
          echo \'error\';
        }
        ?&gt;
    &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-20 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Walidacja imienia i nazwiska z dużej litery i od 2 do 10 znaków wyrażenia regularne','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '13','user_id' => '1','title' => 'Wyrażenie regularne negacja zawartości','slug' => 'wyrazenie-regularne-negacja-zawartosci','content' => '<pre class="language-php"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;
        &lt;title&gt;&lt;/title&gt;
    &lt;/head&gt;
    &lt;body&gt;
        &lt;form action="ostatninegacje.php" method="post"&gt;
            &lt;input type="text" name="pole"&gt;
            &lt;input type="submit" value="wyslij"&gt;
        &lt;/form&gt;
        &lt;?php
        if (preg_match(\'@^[^a-z]+$@\', $_POST[\'pole\'])) {//+oznacza minimum jeden raz(min jedne znak)
          echo (\'ok\');
        } else {
          echo (\'error\');
        }
        ?&gt;
    &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-20 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Wyrażenie regularne negacja zawartości','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '14','user_id' => '1','title' => 'Wyrażenie regularne czy pole zawiera 0 lub 1 i ma długość 4','slug' => 'wyrazenie-regularne-czy-pole-zawiera-0-lub-1-i-ma-dlugosc-4','content' => '<pre class="language-php"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
    &lt;head&gt;
        &lt;title&gt;&lt;/title&gt;
    &lt;/head&gt;
    &lt;body&gt;
        &lt;form action="ostatniprzyklad.php" method="post"&gt;
            &lt;input type="text" name="pole"&gt;
            &lt;input type="submit" value="wyslij"&gt;
        &lt;/form&gt;
        &lt;?php
        if (preg_match(\'@^[01]{4}$@\', $_POST[\'pole\'])) {
          echo (\'ok\');
        } else {
          echo (\'error\');
        }
        ?&gt;
    &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-02-20 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Wyrażenie regularne czy pole zawiera 0 lub 1 i ma długość 4','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '15','user_id' => '1','title' => 'Sprawdzanie koloru z obrazka w css','slug' => 'sprawdzanie-koloru-z-obrazka-w-css','content' => '<a target="_blank" href="http://imagecolorpicker.com/pl" class="blok-link">Pierwszy</a>
          <a target="_blank" href="http://www.ginifab.com/feeds/pms/pms_color_in_image.php" class="blok-link">DRUGI</a>','publishedAt' => '2017-02-24 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Sprawdzanie koloru z obrazka w css','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '16','user_id' => '1','title' => 'Wyciąganie ostatniego id z bazy','slug' => 'wyciaganie-ostatniego-id-z-bazy','content' => '<pre class="language-php"><code>&lt;?php
$stmt=$pdo-&gt;prepare("INSERT INTO orders (id, customer, address, email) VALUES (null, :customer, :address, :email)");
$stmt-&gt;bindValue(\':customer\', $_POST[\'customer\'],  PDO::PARAM_STR);
$stmt-&gt;bindValue(\':email\', $_POST[\'email\'],  PDO::PARAM_STR);
$stmt-&gt;bindValue(\':address\', $_POST[\'address\'],  PDO::PARAM_STR);
$stmt-&gt;execute();

$id =$pdo-&gt;lastInsertId(); 
echo $id;</code></pre>','publishedAt' => '2017-02-28 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Wyciąganie ostatniego id z bazy','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '17','user_id' => '1','title' => 'Przekierowanie wstecz','slug' => 'przekierowanie-wstecz','content' => '<pre class="language-php"><code>&lt;?php
header(\'Location: \' . $_SERVER[\'HTTP_REFERER\']);
?&gt;</code></pre>','publishedAt' => '2017-02-28 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Przekierowanie wstecz','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '18','user_id' => '1','title' => 'Add class/remove class jquery','slug' => 'add-classremove-class-jquery','content' => '<pre class="language-markup"><code>&lt;script&gt;
    $(document).ready(function() {
        $(\'.btn-mp3\').click(function(){
            if ($(\'.eq\').hasClass("eq--off")) {
                $(\'.eq\').removeClass(\'eq--off\');
                $(\'.eq\').addClass(\'eq--on\');
            } else {

                $(\'.eq\').removeClass(\'eq--on\');
                $(\'.eq\').addClass(\'eq--off\');
            }
        });
    });
&lt;/script&gt;</code></pre>','publishedAt' => '2017-02-28 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Add class/remove class jquery','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '19','user_id' => '1','title' => 'Kompresja mod_deflate w pliku htacces','slug' => 'kompresja-mod-deflate-w-pliku-htacces','content' => '<pre class="language-markup"><code>&lt;IfModule mod_deflate.c&gt;
AddOutputFilterByType DEFLATE text/css text/x-component application/x-javascript application/javascript text/javascript text/x-js text/html text/richtext image/svg+xml text/plain text/xsd text/xsl text/xml image/x-icon application/json
&lt;/IfModule&gt;

&lt;IfModule mod_deflate.c&gt;
AddOutputFilterByType DEFLATE text/plain
AddOutputFilterByType DEFLATE text/html
AddOutputFilterByType DEFLATE text/css
AddOutputFilterByType DEFLATE text/javascript
AddOutputFilterByType DEFLATE application/javascript
AddOutputFilterByType DEFLATE application/x-javascript
&lt;/IfModule&gt;</code></pre>','publishedAt' => '2017-03-01 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Kompresja mod_deflate w pliku htacces','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '20','user_id' => '1','title' => 'Dodawanie grupy inputów do formularza','slug' => 'dodawanie-grupy-inputow-do-formularza','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
  &lt;meta charset="UTF-8"&gt;
  &lt;title&gt;add group forms&lt;/title&gt;
  &lt;script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"&gt;&lt;/script&gt;
&lt;/head&gt;
&lt;body&gt;
  &lt;div class=\'add-container\'&gt;
    &lt;div class="form-group-register"&gt;
      &lt;label for="key_people_data_name"&gt;imie&lt;/label&gt;
      &lt;input type=\'text\' name=\'key_people_data_name[]\' class="register-input"&gt;
    &lt;/div&gt;

    &lt;div class="form-group-register"&gt;
      &lt;label for="key_people_data_surname"&gt;nazwisko&lt;/label&gt;
      &lt;input type=\'text\' name=\'key_people_data_surname[]\' class="register-input"&gt;
    &lt;/div&gt;
    &lt;div class="form-group-register"&gt;
      &lt;label for="key_people_data_name"&gt;funkcja&lt;/label&gt;
      &lt;select name="key_people_data_function[]" id="" class="register-input"&gt;
        &lt;option value=""&gt;--------------&lt;/option&gt;
        &lt;option value=""&gt;jeden&lt;/option&gt;
        &lt;option value=""&gt;dwa&lt;/option&gt;
        &lt;option value=""&gt;trzy &lt;/option&gt;
        &lt;option value=""&gt;tzry&lt;/option&gt;
        &lt;option value=""&gt;cztery &lt;/option&gt;
        &lt;option value=""&gt;piec&lt;/option&gt;
        &lt;option value=""&gt;szesc&lt;/option&gt;
      &lt;/select&gt;
    &lt;/div&gt;
    &lt;div class="form-group-register"&gt;
      &lt;label for="key_people_data_email"&gt;email&lt;/label&gt;
      &lt;input type=\'text\' name=\'key_people_data_function[]\' class="register-input"&gt;
    &lt;/div&gt;
    &lt;div class="form-group-register"&gt;
      &lt;label for="key_people_data_phone"&gt;telefon&lt;/label&gt;
      &lt;input type=\'text\' name=\'key_people_data_function[]\' class="register-input"&gt;
    &lt;/div&gt;
    &lt;hr&gt;
  &lt;/div&gt;
  &lt;button id=\'add_group_form\'&gt;Add&lt;/button&gt;

  &lt;script&gt;
    $(document).ready(function() {
      var remove_Button = "&lt;button class=\'remove_group_form\'&gt;Remove&lt;/button&gt;";
      var max_fields = 5;//max add group inputs
      var lenght = 1;
      $(\'#add_group_form\').click(function() {
        if(lenght &lt; max_fields){
          lenght++;
          $(\'div.add-container:last\').after($(\'div.add-container:first\').clone());
          $(\'div.add-container:last\').append(remove_Button);
        }
      });
      $(\'body\').on(\'click\',\'.remove_group_form\', function() {
        lenght--;
        $(this).closest(\'div.add-container\').remove();
      });
    });
  &lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-07-25 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Dodawanie grupy inputów do formularza','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '21','user_id' => '1','title' => 'Usuwanie cache i plików cookie ','slug' => 'usuwanie-cache-i-plikow-cookie','content' => 'W konsoli projektu:
<h1>php artisan view:clear</h1>','publishedAt' => '2017-03-17 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Usuwanie cache i plików cookie ','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '22','user_id' => '1','title' => 'Szybki var_dump requesta','slug' => 'szybki-var-dump-requesta','content' => '<pre class="language-php"><code>&lt;?php
public function save(Request $request)
{
   dd($request);
   //lub
   dump($request);
}

//lub same pola formularza
public function save(Request $request)
{
   dd($request-&gt;all());
}</code></pre>','publishedAt' => '2017-03-17 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Szybki var_dump requesta','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '23','user_id' => '1','title' => 'Globalna instalacja laravela przy urzyciu comosera i tworzenie czystego projektu','slug' => 'globalna-instalacja-laravela-przy-urzyciu-comosera-i-tworzenie-czystego-projektu','content' => '<p>Globalna instalacja laravela przebiega bardzo prosto po instalacji composera wystarczy w consoli CMD wpisać:</p>
<h1>composer global require "laravel/installer"</h1>
<p>Wchodzimy teraz do miejsca  którym jest folder htdocss i console wywołujemy w ten sposób ... najeżdżamy na folder htdocs  i z wciśniętym przyciskiem \'shift\' przytrzymujemy drugi przycisk od myszki i wybieramy "Otwórz okno polecenia tutaj" ... w consoli wpisujemy:</p>
<h1>laravel new nazwa_aplikacji</h1>
<p>Trochę to zajmie nim composer przemieli to wszystko a na końcu mamy czysty projekt laravela</p>


','publishedAt' => '2017-03-21 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Globalna instalacja laravela przy urzyciu comosera i tworzenie czystego projektu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '24','user_id' => '1','title' => 'Tworzenie kontrolera','slug' => 'tworzenie-kontrolera','content' => '<p>Kontroler w laravelu można stworzyć na dwa sposoby (oczywiście wpisujemy to w consoli):</p>
<h1>1)  php artisan make:controller nazwakontrolera</h1>
<h5>Tworzenie  czystego kontrolera</h5>
<h1>2 ) php artisan make:controller nazwakontrolera --resource</h1>
<h5>Tworzenie  kontrolera z metodami</h5>','publishedAt' => '2017-03-21 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Tworzenie kontrolera','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '25','user_id' => '1','title' => 'Tworzenie modelu','slug' => 'tworzenie-modelu','content' => '<p>Model teź można stworzyć na 2 sposoby:</p>
<h4>Czysty model:</h4>
<h1>php artisan make:model nazwamodelu</h1>
<h4>Model wraz z migracjami :</h4>
<h1>php artisan make:model nazwanazwa --migration
<p>Model zostanie stworzony w folderze "App/" a migracja do modelu  w pliku "database/migrations/"</p>','publishedAt' => '2017-03-21 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Tworzenie modelu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '26','user_id' => '1','title' => 'Dodanie klasy logowania ','slug' => 'dodanie-klasy-logowania','content' => '<p>Laravel posiada gotową klasę służącą do autoryzacji użytkowników.Jest to bardzo przydatna klasa, która oszczędza dużo czasu ponieważ nie musimy jej pisać sami :). Dodaje ona również standardowy layout szablonów blade w laravelu. Dodajemy ją komenda w consoli: </p>
<h1>php artisan make:auth</h1>','publishedAt' => '2017-03-21 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Dodanie klasy logowania ','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '27','user_id' => '1','title' => 'Dodawanie zmiennej do widoku','slug' => 'dodawanie-zmiennej-do-widoku','content' => '<pre class="language-php"><code>&lt;?php
public function index()
{
   return view(\'hello.world\')-&gt;with(\'test\',\'text przeniesiony do zmiennej\');
}</code></pre>','publishedAt' => '2017-03-21 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Dodawanie zmiennej do widoku','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '28','user_id' => '1','title' => 'Przekazanie tablicy do widoku','slug' => 'przekazanie-tablicy-do-widoku','content' => '<pre class="language-php"><code>&lt;?php
public function foo()
{
 $data=[\'seth saren\',\'petro alex\',\'nov thida\',\'aeu\',\'laravel\'];
 return view(\'hello.world\')-&gt;with(\'data\',$data);
}

//widok
&lt;ol&gt;
    @foreach ($data as $key=&gt;$d)
    &lt;li&gt;{{$d}}&lt;/li&gt;
    @endforeach
&lt;/ol&gt;</code></pre>','publishedAt' => '2017-03-21 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Przekazanie tablicy do widoku','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '29','user_id' => '1','title' => 'Róznice {{ }} {!! !!} w szablonach blade','slug' => 'roznice-w-szablonach-blade','content' => '<p>{{ }} pokazuje encje htmla</p>
<p>{!! !!} nie pokazuje htmla</p>','publishedAt' => '2017-03-28 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Róznice {{ }} {!! !!} w szablonach blade','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '30','user_id' => '1','title' => 'Tworzenie tabeli z migracja','slug' => 'tworzenie-tabeli-z-migracja','content' => '<p>php artisan make:migration create_reoles_table --create=roles</p>','publishedAt' => '2017-03-28 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Tworzenie tabeli z migracja','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '31','user_id' => '1','title' => 'Tworzenie seederów','slug' => 'tworzenie-seederow','content' => 'php artisan make:seeder UsersTableSeeder','publishedAt' => '2017-03-28 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Tworzenie seederów','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '32','user_id' => '1','title' => 'Zdarzenia w javascript','slug' => 'zdarzenia-w-javascript','content' => '<p><b>onclick</b> - Zdarzenie występuje, gdy użytkownik kliknie element.</p>
<p><b>ondblclick</b> - Zdarzenie występuje, gdy użytkownik dwukrotnie kliknie element.</p>
<p><b>onmousedown</b> - Zdarzenie występuje, gdy użytkownik naciśnie przycisk myszy, gdy kursor
znajduje się nad elementem.</p>
<p><b>onmouseenter</b> - Zdarzenie występuje, gdy kursor jest przesuwany na elemencie.</p>
<p><b>onmouseleave</b> - Zdarzenie występuje, gdy użytkownik przesunie kursor z elementu.</p>
<p><b>onmousemove</b> - Zdarzenie występuje, gdy kursor, będąc w ruchu, znajduje się nad elementem.</p>
<p><b>onmouseover</b> - Zdarzenie występuje, gdy wskaźnik jest przesuwany na elemencie lub na jednym
z jego dzieci.</p>
<p><b>onmouseout</b> - Zdarzenie występuje, gdy użytkownik przesuwa wskaźnik myszy na element
lub z jednego z jego dzieci.</p>
<p><b>onmouseup</b> - Zdarzenie występuje, gdy użytkownik zwolni przycisk myszy na elemencie.</p>
<h1>Zdarzenia HTML DOM związane z klawiaturą</h1>
<p><b>onkeydown</b> - Zdarzenie ma miejsce, gdy użytkownik trzyma wciśnięty klawisz.</p>
<p><b>onkeypress</b> -Zdarzenie wystąpiło, gdy użytkownik nacisnął klawisz.</p>
<p><b>onkeyup</b> - Zdarzenie wystąpiło, gdy użytkownik zwolnił klawisz.</p>
<h1>Zdarzenia HTML DOM związane z obiektami (ramkami)</h1>
<p><b>onabort</b> - Ładowanie obrazu przerwano, zanim został załadowany w całości.</p>
<p><b>onerror</b> - Obraz nie został załadowany prawidłowo (dla &lt;object&gt;, &lt;body&gt; i &lt;frameset&gt;).</p>
<p><b>onload</b> - Dokument, frameset lub &lt;object&gt; został załadowany.</p>
<p><b>onresize</b> - Zmieniono rozmiary widoku dokumentu.</p>
<p><b>onscroll</b> - Przewinięto dokument.</p>
<p><b>onunload</b> - Strona nie została załadowana (ma zastosowanie zarówno dla &lt;body&gt;, jak i &lt;frameset&gt;).</p>
<h1>Zdarzenia HTML DOM związane z formularzami</h1>
<p><b>onblur</b> - Zdarzenie występuje, gdy element formularza traci fokus.</p>
<p><b>onchange</b> - Zdarzenie występuje, gdy zawartość elementu formularza, selekcja lub sprawdzony status uległy zmianie (ma zastosowanie do &lt;input&gt;, &lt;select&gt; oraz &lt;textarea&gt;).</p>
<p><b>onfocus</b> - Zdarzenie występuje, gdy element uzyskuje fokus (ma zastosowanie dla &lt;label&gt;, &lt;input&gt;, &lt;select&gt;, &lt;textarea&gt; oraz &lt;button&gt;).</p>
<p><b>onreset</b> - Zdarzenie występuje, gdy formularz jest resetowany.</p>
<p><b>onselect</b> - Zdarzenie nastąpi, gdy użytkownik wybierze tekst (ma zastosowanie dla &lt;input&gt; and &lt;textarea&gt;).</p>
<p><b>onsubmit</b> - Zdarzenie nastąpi, gdy formularz zostanie wysłany.</p>
<h1>Zdarzenia HTML DOM związane z właściwościami</h1>
<p><b>bubbles</b> - Zwraca informację, czy zdarzenie może przechodzić fazę propagacji(tzw. bąbelkowanie).</p>
<p><b>cancelable</b> - Zwraca informację, czy zdarzenie może mieć zablokowane działanie w sposóbdomyślny.</p>
<p><b>currentTarget</b> - Zwraca element, którego słuchacz zdarzeń wywołał zdarzenie.</p>
<p><b>eventPhase</b> - Zwraca informację, która faza przepływu zdarzeń jest obecnie oceniana.</p>
<p><b>target</b> - Zwraca element, który wywołał zdarzenie.</p>
<p><b>timeStamp</b> - Zwraca czas mierzony w milisekundach w stosunku do momentu, w którym zdarzenie zostało utworzone.</p>
<p><b>type</b> - Zwraca nazwę zdarzenia.</p>
<h1>Zdarzenia MouseEvent/KeyboardEvent HTML DOM związane z właściwościami</h1>
<p><b>altKey</b> - Zwraca informację, czy klawisz Alt został wciśnięty, gdy zdarzenie zostało zarejestrowane.</p>
<p><b>button</b> - Zwraca informację, który klawisz myszy został kliknięty, gdy zdarzenie zostało zarejestrowane.</p>
<p><b>clientX</b> - Zwraca współrzędną poziomą wskaźnika myszy w stosunku do bieżącego okna, gdy zdarzenie zostało zarejestrowane.</p>
<p><b>clientY</b> - Zwraca współrzędną pionową wskaźnika myszy w stosunku do bieżącego okna, gdy zdarzenie zostało zarejestrowane.</p>
<p><b>ctrlKey</b> - Zwraca informację, czy klawisz Ctrl został wciśnięty, gdy zdarzenie zostało zarejestrowane.</p>
<p><b>keyIdentifier</b> - Zwraca identyfikator klawisza.</p>
<p><b>keyLocation</b> - Zwraca położenie klawisza w urządzeniu.</p>
<p><b>metaKey</b> - Zwraca informację, czy klawisz meta został wciśnięty, gdy zdarzenie zostało zarejestrowane.</p>
<p><b>relatedTarget</b> - Zwraca element związany z elementem, który spowodował zdarzenie.</p>
<p><b>screenX</b> - Zwraca współrzędną poziomą wskaźnika myszy w stosunku do ekranu, gdy zdarzenie zostało zarejestrowane.</p>
<p><b>screenY</b> - Zwraca współrzędną pionową wskaźnika myszy w stosunku do ekranu, gdy zdarzenie zostało zarejestrowane.</p>
<p><b>shiftKey</b> - Zwraca informację, czy klawisz Shift został wciśnięty, gdy zdarzenie zostałozarejestrowane.</p>','publishedAt' => '2017-03-29 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Zdarzenia w javascript','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '33','user_id' => '1','title' => 'Zapis seedera do bazy','slug' => 'zapis-seedera-do-bazy','content' => 'php artisan db:seed --class=RolesTableSeeder','publishedAt' => '2017-04-03 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Zapis seedera do bazy','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '34','user_id' => '1','title' => 'Zbiór komend laravel','slug' => 'zbior-komend-laravel','content' => '<a target="_blank" href="http://cheats.jesse-obrien.ca/" class="blok-link">Zbiór komend</a>','publishedAt' => '2017-04-06 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Zbiór komend laravel','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '35','user_id' => '1','title' => 'System formularzy w laravelu','slug' => 'system-formularzy-w-laravelu','content' => '<a target="_blank" href="https://laravelcollective.com/docs/5.3/html#installation" class="blok-link">LINK</a>
','publishedAt' => '2017-04-19 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'System formularzy w laravelu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '36','user_id' => '1','title' => 'Reset tabeli z usunięciem auto incrementowanych wartości','slug' => 'reset-tabeli-z-usunieciem-auto-incrementowanych-wartosci','content' => 'TRUNCATE table nazwatabeli','publishedAt' => '2017-04-20 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Reset tabeli z usunięciem auto incrementowanych wartości','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '37','user_id' => '1','title' => 'Przekierowania w javascript','slug' => 'przekierowania-w-javascript','content' => '<pre class="language-markup"><code>&lt;!-- przekierowanie na adres--&gt;
&lt;script&gt;window.location.replace("http://adresurl.pl)";&lt;/script&gt;

&lt;!-- przekierowanie wstecz--&gt;
&lt;script&gt;window.history.back();&lt;/script&gt;

&lt;!-- przekierowanie w prz&oacute;d--&gt;
&lt;script&gt;window.history.forward();&lt;/script&gt;

&lt;!-- przekierowanie jedna stronę wstecz--&gt;
&lt;script&gt;window.history.go(-1);&lt;/script&gt;

&lt;!-- przekierowanie jedna stronę w prz&oacute;d--&gt;
&lt;script&gt;window.history.go(1);&lt;/script&gt;</code></pre>','publishedAt' => '2017-04-20 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Przekierowania w javascript','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '38','user_id' => '1','title' => 'Inicjowanie autoloadingu ps-4','slug' => 'inicjowanie-autoloadingu-ps-4','content' => 'composer dump-autoload','publishedAt' => '2017-04-24 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Inicjowanie autoloadingu ps-4','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '39','user_id' => '1','title' => 'Losowy ciąg znaków','slug' => 'losowy-ciag-znakow','content' => '<pre class="language-php"><code>&lt;?php
class RandomCodeGenerator {

    /**
     * rand code(integer or alphanumeric) by lenght
     * 
     * @param int $length
     * @return string|integer
     */
    public static function generate($length = 4, $alphanumeric = false) {

        if ($alphanumeric) {
            $characters = \'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\';
        } else {
            $characters = \'0123456789\';
        }

        $characterLength = strlen($characters);
        $randomcode = \'\';
        for ($i = 0; $i &lt; $length; $i++) {
            $randomcode .= $characters[rand(0, $characterLength - 1)];
        }

        return $randomcode;
    }

}</code></pre>','publishedAt' => '2017-04-25 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Losowy ciąg znaków','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '40','user_id' => '1','title' => 'Zablokowanie formularza js/jquery','slug' => 'zablokowanie-formularza-jsjquery','content' => '<pre class="language-markup"><code>&lt;!-- javascript --&gt;
&lt;script&gt;
  window.onload=function() {
    document.getElementById(\'form_name\').addEventListener(\'submit\', function (e) {
      alert(\'submit intercepted\');
      e.preventDefault(e);
    });
  };
&lt;/script&gt;

&lt;!-- jquery --&gt;
&lt;script&gt;
  $(document).ready(function() {
    $(\'#form_name\').on("submit",function(e) {
      alert(\'submit intercepted\');
      e.preventDefault(e);
    });
  });
&lt;/script&gt;</code></pre>','publishedAt' => '2017-05-24 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Zablokowanie formularza js/jquery','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '41','user_id' => '1','title' => 'Pobranie elementu po id i dodanie styli css','slug' => 'pobranie-elementu-po-id-i-dodanie-styli-css','content' => '<pre class="language-markup"><code>&lt;div id="div_one"&gt;&lt;p&gt;lorem ipusm&lt;/p&gt;&lt;/div&gt;
&lt;!-- javascript --&gt;
&lt;script&gt;
  var element = document.getElementById("div_one");
  element.style.color = "red";
&lt;/script&gt;
&lt;!-- jquery --&gt;
&lt;script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"&gt;&lt;/script&gt;
&lt;script&gt;
  var element = $("#div_one");
  element.css(\'color\',\'red\');
&lt;/script&gt;</code></pre>','publishedAt' => '2017-05-24 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Pobranie elementu po id i dodanie styli css','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '42','user_id' => '1','title' => 'Zdarzenie click','slug' => 'zdarzenie-click','content' => '<pre class="language-markup"><code>&lt;!-- javascript  --&gt;
&lt;div id="js-id"&gt;Click Me js&lt;/div&gt;
&lt;script&gt;
    document.getElementById(\'js-id\').addEventListener(\'click\', function() {
        alert(\'Hello js!\');
    }, false);
&lt;/script&gt;
&lt;!-- jquery --&gt;
&lt;div id="jq_id"&gt;jquery&lt;/div&gt;
&lt;script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"&gt;&lt;/script&gt;
&lt;script&gt;
    $( "#jq_id" ).click(function() {
        alert( "Hello jquery!" );
    });
&lt;/script&gt;</code></pre>','publishedAt' => '2017-05-24 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Zdarzenie click','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '43','user_id' => '1','title' => 'Sprawdzanie płci','slug' => 'sprawdzanie-plci','content' => '<pre class="language-php"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
  &lt;meta charset="UTF-8"&gt;
  &lt;title&gt;Document&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
  &lt;?php
  if (isset($_POST) &amp;&amp; $_SERVER[\'REQUEST_METHOD\'] == "POST") {
    $sex = $_POST[\'sex\'];
    $check_sign = strlen($sex);
    $pattern = $sex[$check_sign - 1];
    if ($pattern == "a" || $pattern == "A") {
      echo "kobieta";
    }else {
      echo "męzczyzna";
    }
  }
  ?&gt;
  &lt;form action="" method="post"&gt;
    &lt;input type="text" name="sex" placeholder="Wpisz imie"&gt;
    &lt;input type="submit" value="sprawdz płeć" &gt;
  &lt;/form&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-05-13 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Sprawdzanie płci','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '44','user_id' => '1','title' => 'Operatory w wyrażeniach regularnych','slug' => 'operatory-w-wyrazeniach-regularnych','content' => '<p><strong>\\s\\S = ???? dowolny znak 26.07.2019</strong></p>
<p><strong>.</strong> - kropka symbolizuje dowolny znak (za wyjątkiem przełamania linii).</p>
<p><strong>\\d</strong> - dowolna cyfra dziesiętna</p>
<p><strong>\\D</strong> - dowolny znak niebędący cyfrą</p>
<p><strong>\\s</strong> - biały znak (np. spacja, tabulator)</p>
<p><strong>^</strong> - Początek linii</p>
<p><strong>$</strong> - Koniec linii</p>
<p><strong>\\b (PCRE)</strong> - Znak wyrazu pomiędzy znakiem wyrazu a pomiędzy znakiem niewyrazu, czyli pomiędzy \\w\\W lub \\W\\w</p>
<p><strong>\\B (PCRE)</strong> -Wszystko to, czego nie dopasowuje \\b</p>
<p>* - dowolną ilośc razy</p>','publishedAt' => '2017-07-09 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Operatory w wyrażeniach regularnych','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '45','user_id' => '1','title' => 'Pobieranie wszystkiego między dwoma znakami','slug' => 'pobieranie-wszystkiego-miedzy-dwoma-znakami','content' => '<pre class="language-php"><code>&lt;?php
function GetBetween($var1="",$var2="",$pool){
  $temp1 = strpos($pool,$var1)+strlen($var1);
  $result = substr($pool,$temp1,strlen($pool));
  $dd=strpos($result,$var2);
  if($dd == 0){
    $dd = strlen($result);
  }
  return substr($result,0,$dd);
}
//===============================================
$str =\'chce wszystko od @to chce wyciagnąć@\';
echo GetBetween(\'@\',\'@\',$str);//to chce wyciagnąć
?&gt;</code></pre>','publishedAt' => '2017-07-21 00:00:00','updatedAt' => '2019-12-14 12:26:33','headtitle' => 'Pobieranie wszystkiego między dwoma znakami','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '46','user_id' => '1','title' => 'Wyciąganie ciągu znaków do wystąpienia znaku','slug' => 'wyciaganie-ciagu-znakow-do-wystapienia-znaku','content' => '<pre class="language-php"><code>&lt;?php 
$mystring = \'ten text chcę=a tego nie\';
$first = strtok($mystring, \'=\');
echo $first.\'&lt;br&gt;&lt;br&gt;&lt;br&gt;\'; // ten text chcę</code></pre>','publishedAt' => '2017-07-21 00:00:00','updatedAt' => '2019-12-14 12:25:51','headtitle' => 'Wyciąganie ciągu znaków do wystąpienia znaku','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '47','user_id' => '1','title' => 'Sprawdzanie czy istnieje folder i tworzenie go jak nie istnieje','slug' => 'sprawdzanie-czy-istnieje-folder-i-tworzenie-go-jak-nie-istnieje','content' => '<pre class="language-php"><code>&lt;?php
if (!file_exists(\'folder/\'.\'1\')) {
  mkdir(\'folder/\'.\'1\', 0777, true);
  echo \'folder został stworzony\';
}else{
  echo "folder juz istnieje";
}
?&gt;</code></pre>','publishedAt' => '2017-07-24 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Sprawdzanie czy istnieje folder i tworzenie go jak nie istnieje','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '48','user_id' => '1','title' => 'Sprawdzanie zaznaczenia checkboxa','slug' => 'sprawdzanie-zaznaczenia-checkboxa','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
  &lt;meta charset="UTF-8"&gt;
  &lt;title&gt;check input&lt;/title&gt;
  &lt;script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"&gt;&lt;/script&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;label for="id_checbox"&gt;
  &lt;input type="checkbox" id="id_checbox"&gt;Zaznacz
&lt;/label&gt;
  &lt;script&gt;
    $(\'#id_checbox\').change(function() {
      if ($(this).is(\':checked\')) {
        console.log(\'Checked\');
      } else {
        console.log(\'Unchecked\');
      }
    });
  &lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-07-27 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Sprawdzanie zaznaczenia checkboxa','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '49','user_id' => '1','title' => 'Sprawdzanie czy co najmniej jeden checkbox o danej klasie jest zaznaczony','slug' => 'sprawdzanie-czy-co-najmniej-jeden-checkbox-o-danej-klasie-jest-zaznaczony','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
  &lt;head&gt;
    &lt;meta charset="UTF-8"&gt;
    &lt;title&gt;Document&lt;/title&gt;
  &lt;/head&gt;
  &lt;body&gt;
    &lt;form&gt;
      &lt;div class="container-checkbox"&gt;
        &lt;input class="check_test" type="checkbox"&gt;
        &lt;input class="check_test" type="checkbox"&gt;
        &lt;input class="check_test" type="checkbox"&gt;
        &lt;input class="check_test" type="checkbox"&gt;
        &lt;input class="check_test" type="submit" value="Submit"&gt;
      &lt;/div&gt;
      &lt;script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"&gt;&lt;/script&gt;
      &lt;script&gt;  
      $(\'.check_test\').change(function() {
        if($(\'.container-checkbox input[type=checkbox]:checked\').length) {
          console.log(\'co najmniej jeden checkbox jest zaznaczony\');
        }else{
          console.log(\'nic nnie jest zaznczone\');
        }
      });
      &lt;/script&gt;
    &lt;/form&gt;
  &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-07-28 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Sprawdzanie czy co najmniej jeden checkbox o danej klasie jest zaznaczony','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '50','user_id' => '1','title' => 'Zdarzenie mouseleave/opuszczenie elementu o danym id','slug' => 'zdarzenie-mouseleaveopuszczenie-elementu-o-danym-id','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
  &lt;meta charset="UTF-8"&gt;
  &lt;title&gt;Document&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;div id="js-id"&gt;jestem divem&lt;/div&gt;
  &lt;script&gt;
      document.getElementById(\'js-id\').addEventListener(\'mouseleave\', function() {
          console.log(\'Opusciłeś div js-id!\');
      });
  &lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;
</code></pre>','publishedAt' => '2017-07-29 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Zdarzenie mouseleave/opuszczenie elementu o danym id','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '51','user_id' => '1','title' => 'Losowanie elementu z tablicy','slug' => 'losowanie-elementu-z-tablicy','content' => '<pre class="language-php"><code>&lt;?php
$input = array("Neo", "Morpheus", "Trinity", "Cypher", "Tank");
$rand_keys = array_rand($input, 2);
echo $input[$rand_keys[0]] . "\\n";
?&gt;
</code></pre>','publishedAt' => '2017-07-29 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Losowanie elementu z tablicy','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '52','user_id' => '1','title' => 'Menu zmieniające text w divie','slug' => 'menu-zmieniajace-text-w-divie','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
  &lt;meta charset="UTF-8"&gt;
  &lt;title&gt;Document&lt;/title&gt;
  &lt;style&gt;
  .change-form-link{padding:20px;display:inline-block;font-weight:900;border-bottom:3px solid transparent;text-decoration:none;cursor: pointer;}
  .change-form-link:hover{text-decoration: none;}
  .active{border-bottom:3px solid #ff7b33;color:#ff7b33;}
&lt;/style&gt;
&lt;/head&gt;
&lt;body&gt;
  &lt;article&gt;
    &lt;header&gt;
      &lt;nav&gt;
        &lt;a target="1" class="change-form-link active"&gt;DIABLO&lt;/a&gt;
        &lt;a target="2" class="change-form-link"&gt;STARCRAFT&lt;/a&gt;
      &lt;/nav&gt;
    &lt;/header&gt;
    &lt;section class="container  container-show" id="1"&gt;
      &lt;p&gt;Diablo 2 podzielone zostało na 4 akty. Każdy z nich rozgrywać się będzie w zupełnie inne scenerii. Raz jest to zwyczajny teren pełen zieleni i łąk, innym razem jest to wielka pustynia, bagna, aż w końcu samo piekło. Każdy z rozdział&oacute;w przygody rozpoczniemy w osiedlu będącym naszą niejako bazą. To właśnie tutaj przebywać będą wszystkie ważne postacie, takie jak handlarze, kowale czy też NPC zlecający nam misje. W prawdzie ich liczba nie jest zbyt wielka, bo to ledwie kilka osobistości na akt, ale w zupełności starczą na potrzeby gry. Bardzo fajną sprawą jest losowe generowanie lokacji, znane już z pierwszej części Diablo. Dzięki temu rozgrywka zawsze wygląda nieco inaczej i nie ma możliwości gry &bdquo;na pamięć&rdquo;. Rzecz jasna, nasza podr&oacute;ż nie toczy się jedynie na otwartym terenie: nie raz przyjdzie nam zejść do głębokich jaskiń, do mrocznych grobowc&oacute;w, zwiedzić jakąś świątynie czy też więzienie.&lt;/p&gt;
    &lt;/section&gt;
    &lt;section class="container  container-show" id="2" style="display: none"&gt;
      &lt;p&gt;O StarCraft słyszał chyba każdy chociaż odrobinę interesujący się grami. Ta wiekowa, ale wciąż popularna produkcja doczeka się wkr&oacute;tce odświeżonego wydania. Producent przybliżył kwestię tego, kto będzie m&oacute;gł się nią cieszyć.
        I zgodnie z przypuszczeniami, do zabawy przystąpi chyba każdy zainteresowany. Ujawnione przez Blizzard wymagania sprzętowe są bardzo niskie, nawet te zalecane z bardzo dużą nawiązką spełniają kilkuletnie komputery..&lt;/p&gt;
    &lt;/section&gt;
  &lt;/article&gt;
  &lt;script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"&gt;&lt;/script&gt;
  &lt;script&gt;
    $(\'.change-form-link\').click(function(e) {
      e.preventDefault();
      $(\'.container-show\').hide();
      $(\'#\'+this.target).show();
      $(\'.change-form-link\').removeClass(\'active\');
      $(this).addClass(\'active\');
    });
  &lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2017-07-29 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Menu zmieniające text w divie','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '53','user_id' => '1','title' => 'Funkcje usuwające niepotrzebne spacje','slug' => 'funkcje-usuwajace-niepotrzebne-spacje','content' => '<h3 id="mcetoc_1bm7ed6bb0"><strong>Funkcje TRIM</strong></h3>
<p>Wiele baz danych obsługuje trzy funkcje odcinające niepotrzebne spacje. Funkcja<br /><strong>RTRIM()</strong> usuwa spacje z prawej strony tekstu, funkcja LTRIM() usuwa je<br />z lewej strony, natomiast funkcja <strong>TRIM()</strong> usuwa spacje z obu stron tekstu.Przykład bez użycia:</p>
<pre class="language-php"><code>SELECT Concat(dost_nazwa, \' (\', dost_kraj, \')\')
FROM Dostawcy
ORDER BY dost_nazwa;</code></pre>
<p>Fun and Games &nbsp; &nbsp; &nbsp; &nbsp; (Anglia &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; )<br />Furball Inc. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (USA &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; )<br />Jouets et ours &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;(Francja &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; )<br />Lalki S.A. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (Polska &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; )<br />Misie Pysie &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;(Polska &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; )<br />Misiowe Imperium (Polska &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; )</p>
<p>Po użyciu funkcji&nbsp;<strong>RTRIM():</strong>&nbsp;</p>
<pre class="language-php"><code>SELECT RTRIM(dost_nazwa) + \' (\' + RTRIM(dost_kraj) + \')\'
FROM Dostawcy
ORDER BY dost_nazwa;</code></pre>
<p>Fun and Games (Anglia)<br />Furball Inc. (USA)<br />Jouets et ours (Francja)<br />Lalki S.A. (Polska)<br />Misie Pysie (Polska)<br />Misiowe Imperium (Polska)</p>','publishedAt' => '2017-07-29 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Funkcje usuwające niepotrzebne spacje','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '54','user_id' => '1','title' => 'Funkcje agregujące','slug' => 'funkcje-agregujace','content' => '<h3>Funkcja agregująca:</h3>
<p>Funkcja działająca w zbiorze wierszy w celu obliczenia i zwr&oacute;cenia tylko<br />jednej wartości.</p>
<div class="table-responsive">
<table class="table table-bordered" style="width: 1182px;">
<tbody>
<tr>
<td style="width: 515.867px;">Funkcja</td>
<td style="width: 663.133px;">Opis</td>
</tr>
<tr>
<td style="width: 515.867px;">AVG()</td>
<td style="width: 663.133px;">zwraca średnią wartość kolumny</td>
</tr>
<tr>
<td style="width: 515.867px;">COUNT()</td>
<td style="width: 663.133px;">zwraca liczbę wierszy w kolumnie</td>
</tr>
<tr>
<td style="width: 515.867px;">MAX()</td>
<td style="width: 663.133px;">zwraca największą wartość w kolumnie</td>
</tr>
<tr>
<td style="width: 515.867px;">MIN()</td>
<td style="width: 663.133px;">zwraca najmniejszą wartość w kolumnie</td>
</tr>
<tr>
<td style="width: 515.867px;">SUM()</td>
<td style="width: 663.133px;">zwraca sumę wartości w kolumnie</td>
</tr>
</tbody>
</table>
</div>
<p>&nbsp;</p>','publishedAt' => '2017-08-06 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Funkcje agregujące','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '55','user_id' => '1','title' => 'Dodawanie klasy do obróbki zdjęc i udostępnienie publicznie folderu storage','slug' => 'dodawanie-klasy-do-obrobki-zdjec-i-udostepnienie-publicznie-folderu-storage','content' => '<ol>
<li>Po pierwsze musimy udostęnić folder storage... robimy to komendą&nbsp;
<pre class="language-php"><code>php artisan storage:link​</code></pre>
</li>
<li>Pobieramy klase do obr&oacute;bki zdjęc komendą :&nbsp;
<pre class="language-php"><code>composer require intervention/image​</code></pre>
</li>
<li>W pliku&nbsp;config/app.php dodajemy to tablicy providers : Intervention\\Image\\ImageServiceProvider::class i do tablicy aliases:&nbsp;\'Image\' =&gt; Intervention\\Image\\Facades\\Image::class &nbsp;</li>
<li>Na końcu :
<pre class="language-php"><code>php artisan vendor:publish --provider="Intervention\\Image\\ImageServiceProviderLaravel5"​​</code></pre>
<p><a class="blok-link" title="przejdz" href="http://image.intervention.io/getting_started/installation#laravel" target="_blank" rel="noopener">więcej informacji</a></p>
</li>
</ol>','publishedAt' => '2017-09-22 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Dodawanie klasy do obróbki zdjęc i udostępnienie publicznie folderu storage','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '56','user_id' => '1','title' => 'Przydatne informacje na temat języka php','slug' => 'przydatne-informacje-na-temat-jezyka-php','content' => '<p><a class="blok-link" href="http://pl.phptherightway.com/#site-header" target="_blank" rel="noopener">LINK</a></p>','publishedAt' => '2017-10-26 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Przydatne informacje na temat języka php','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '57','user_id' => '1','title' => 'Funkcje przy wczytywaniu drzewa DOM','slug' => 'funkcje-przy-wczytywaniu-drzewa-dom','content' => '<pre class="language-javascript"><code>window.addEventListener(\'DOMContentLoaded\', function() {
  console.log(\'window - DOMContentLoaded - capture\'); // 1st
}, true);
document.addEventListener(\'DOMContentLoaded\', function() {
  console.log(\'document - DOMContentLoaded - capture\'); // 2nd
}, true);
document.addEventListener(\'DOMContentLoaded\', function() {
  console.log(\'document - DOMContentLoaded - bubble\'); // 2nd
});
window.addEventListener(\'DOMContentLoaded\', function() {
  console.log(\'window - DOMContentLoaded - bubble\'); // 3rd
});

window.addEventListener(\'load\', function() {
  console.log(\'window - load - capture\'); // 4th
}, true);
document.addEventListener(\'load\', function(e) {
  /* Filter out load events not related to the document */
  if([\'style\',\'script\'].indexOf(e.target.tagName.toLowerCase()) &lt; 0)
    console.log(\'document - load - capture\'); // DOES NOT HAPPEN
}, true);
document.addEventListener(\'load\', function() {
  console.log(\'document - load - bubble\'); // DOES NOT HAPPEN
});
window.addEventListener(\'load\', function() {
  console.log(\'window - load - bubble\'); // 4th
});

window.onload = function() {
  console.log(\'window - onload\'); // 4th
};
document.onload = function() {
  console.log(\'document - onload\'); // DOES NOT HAPPEN
};</code></pre>','publishedAt' => '2017-10-26 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Funkcje przy wczytywaniu drzewa DOM','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '58','user_id' => '1','title' => 'Dubugowananie zapytania','slug' => 'dubugowananie-zapytania','content' => '<pre class="language-markup"><code>https://github.com/panique/pdo-debug</code></pre>','publishedAt' => '2017-10-30 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Dubugowananie zapytania','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '59','user_id' => '1','title' => 'Informacje o statusie domeny','slug' => 'informacje-o-statusie-domeny','content' => '<pre class="language-php"><code>$url = \'http://allegro.pl\';
print_r(get_headers($url, 1));
////////////////lub//////////////////
$strona = file_get_contents(\'http://zalando.pl/\');
echo "&lt;pre&gt;";
var_dump($http_response_header);
echo "&lt;/pre&gt;";
exit();</code></pre>','publishedAt' => '2017-11-08 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Informacje o statusie domeny','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '60','user_id' => '1','title' => 'Wzór wystapnia na końcu stringa','slug' => 'wzor-wystapnia-na-koncu-stringa','content' => '<pre class="language-php"><code>// create a string
$string = \'abcdefghijklmnopqrstuvwxyz0123456789.pl\';

// try to match our pattern at the end of the string
if(preg_match("/.pl\\z/i", $string))
        {
    // if our pattern matches we echo this 
        echo \'The string ends with .pl\';
        }
else
        {
    // if no match is found we echo this line
        echo \'No match found\';
        }</code></pre>
<p>https://www.phpro.org/tutorials/Introduction-to-PHP-Regex.html</p>','publishedAt' => '2017-11-16 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Wzór wystapnia na końcu stringa','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '61','user_id' => '1','title' => 'Ładowanie wszystkich zdjęć z folderu','slug' => 'ladowanie-wszystkich-zdjec-z-folderu','content' => '<p>Z np lighboxem:</p>
<pre class="language-php"><code>&lt;?php
$images = glob("{small/*.jpg,*.bmp,*.gif,*.png,*.jpeg}",GLOB_BRACE);
$images_big = glob("{400/*.jpg,*.bmp,*.gif,*.png,*.jpeg}",GLOB_BRACE);?&gt;
&lt;?php foreach (array_combine($images, $images_big) as $image =&gt; $image_big): ?&gt;
  &lt;div class="img_item"&gt;&lt;img class="slider_img" src="&lt;?php echo $image; ?&gt;" alt=""&gt;&lt;/div&gt;
  &lt;div class="img_item"&gt;&lt;img class="slider_img" src="&lt;?php echo $image_big; ?&gt;" alt=""&gt;&lt;/div&gt;
&lt;?php endforeach ?&gt;</code></pre>
<p>Ładowanie samych pojedyńczych zdjęć:</p>
<pre class="language-php"><code>&lt;?php
$images = glob("{*.jpg,*.bmp,*.gif,*.png,*.jpeg}",GLOB_BRACE);?&gt;
&lt;?php foreach ($images as $image): ?&gt;
  &lt;div class="img_item"&gt;&lt;img class="slider_img" src="&lt;?php echo $image; ?&gt;" alt=""&gt;&lt;/div&gt;
&lt;?php endforeach ?&gt;</code></pre>','publishedAt' => '2018-12-06 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Ładowanie wszystkich zdjęć z folderu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '62','user_id' => '1','title' => 'Kopiowanie tabeli','slug' => 'kopiowanie-tabeli','content' => '<p>Kopiowanie całej tabeli</p>
<pre class="language-php"><code>INSERT INTO newTable
SELECT * FROM oldTable</code></pre>
<p>Kopiowanie wybranych wierszy</p>
<pre class="language-php"><code>INSERT INTO newTable (col1, col2, col3)
SELECT column1, column2, column3
FROM oldTable</code></pre>','publishedAt' => '2018-01-11 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Kopiowanie tabeli','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '63','user_id' => '1','title' => 'Funkcja kopiująca folder na serwerze','slug' => 'funkcja-kopiujaca-folder-na-serwerze','content' => '<pre class="language-php"><code>function copy_directory($source, $dest)
{
    // Check for symlinks
    if (is_link($source)) {
        return symlink(readlink($source), $dest);
    }
    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }
    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest);
    }
    // Loop through the folder
    $dir = dir($source);
    while (false !== $entry = $dir-&gt;read()) {
        // Skip pointers
        if ($entry == \'.\' || $entry == \'..\') {
            continue;
        }
        // Deep copy directories
        copy_directory("$source/$entry", "$dest/$entry");
    }
    // Clean up
    $dir-&gt;close();
    return true;
}</code></pre>','publishedAt' => '2018-01-17 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Funkcja kopiująca folder na serwerze','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '64','user_id' => '1','title' => 'Funkcja usuwająca folder','slug' => 'funkcja-usuwajaca-folder','content' => '<pre class="language-php"><code>function deleteDir($dirPath) {
    if (! is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != \'/\') {
        $dirPath .= \'/\';
    }
    $files = glob($dirPath . \'*\', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}</code></pre>','publishedAt' => '2018-01-17 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Funkcja usuwająca folder','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '65','user_id' => '1','title' => 'Update composera','slug' => 'update-composera','content' => '<pre class="language-php"><code>composer self-update</code></pre>','publishedAt' => '2018-01-31 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Update composera','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '66','user_id' => '1','title' => 'Długość największego pola ','slug' => 'dlugosc-najwiekszego-pola','content' => '<pre class="language-php"><code>SELECT MAX(LENGTH(field_to_query)) FROM table_to_query;</code></pre>','publishedAt' => '2018-03-20 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Długość największego pola ','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '67','user_id' => '1','title' => 'Naprawa błędu długości pola w laravelu na windowsie ','slug' => 'naprawa-bledu-dlugosci-pola-w-laravelu-na-windowsie','content' => '<p>Jeżeli uraczy cię taki błąd podczas migracji w Laravelu:</p>
<pre class="language-php"><code>  [PDOException]
  SQLSTATE[42000]: Syntax error or access violation: 1071 Specified key was too long; max key length is 767 bytes</code></pre>
<p><strong>w App/Providers/AppServiceProvider.php dodaj do funkcji boot()</strong></p>
<pre class="language-php"><code>public function boot()
{
  Schema::defaultStringLength(191);
}</code></pre>
<p><strong>i nie zapomnij o:</strong></p>
<pre class="language-php"><code>use Illuminate\\Support\\Facades\\Schema;</code></pre>
<pre class="language-php"><code>&lt;?php

namespace App\\Providers;

use Illuminate\\Support\\ServiceProvider;
use Illuminate\\Support\\Facades\\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
</code></pre>','publishedAt' => '2018-03-21 00:00:00','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Naprawa błędu długości pola w laravelu na windowsie ','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '68','user_id' => '1','title' => 'Symfony komendy','slug' => 'symfony-komendy','content' => '<p><strong>php bin/console make:migration</strong> ::dodanie wersji migracji<br /><strong>php bin/console doctrine:migrations:diff:</strong>:wykrycie zmian w migracjach<br /><strong>php bin/console doctrine:migrations:migrate</strong> ::i jak jakies sa<br /><strong>php bin/console doctrine:fixtures:load</strong> ::ladowanie fakera<br /><strong>bin/console make:crud SchoolClass</strong> ::tworzenie cruda<br /><strong>php bin/console doctrine:database:create</strong> ::tworzenie bazy danych<br /><strong>php bin/console fos:user:create testuser test@example.com p@ssword</strong> ::tworzenie usera</p>
<pre>php bin/console make:entity --regenerate App //regeneracja wszystkich encji<br /><br /><a href="https://stackoverflow.com/questions/10038581/query-builder-join-on-many-to-many-relationship">https://stackoverflow.com/questions/10038581/query-builder-join-on-many-to-many-relationship</a><br />//defenestracja</pre>','publishedAt' => '2018-08-02 17:56:38','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Symfony komendy','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '69','user_id' => '1','title' => 'Symfony przyklady zapytan','slug' => 'symfony-przyklady-zapytan','content' => '<p>https://stackoverflow.com/questions/34640661/doctrine-query-one-to-many-unidirectional-with-join-table-association-from-in</p>
<pre class="language-php"><code>$qb = $this-&gt;getEntityManager()-&gt;createQueryBuilder();

$qb-&gt;select(\'p\')
-&gt;from(\'SomeBundle:Post\', \'p\')
-&gt;join(\'p.categories\', \'c\')
-&gt;where(\'c.id = :categoryId\')
-&gt;setParameter(\'categoryId\', $categoryId)
-&gt;getQuery()
-&gt;getResult();</code></pre>
<p><br />Get all posts with a range of categories:</p>
<pre class="language-php"><code>$qb = $this-&gt;getEntityManager()-&gt;createQueryBuilder();

$qb-&gt;select(\'p\')
-&gt;from(\'SomeBundle:Post\', \'p\')
-&gt;join(\'p.categories\', \'c\')
-&gt;where($qb-&gt;expr()-&gt;in(\'c.id\', \':categoryIds\'))
-&gt;setParameter(\'categoryIds\', $categoryIds) // array of ids
-&gt;getQuery()
-&gt;getResult();</code></pre>
<p>//===============================================================================</p>
<pre class="language-php"><code>//normalne zapytanie w symfony
$tag = $tag-&gt;getId();
echo $tag;
$conn = $this-&gt;get(\'database_connection\');
$res = $conn-&gt;prepare(\'
SELECT p.id,p.title,p.slug,p.shortcontent,p.published_at
FROM post p, blog_post_tag chuj
where p.id=chuj.post_id
AND chuj.tag_id=:tag
ORDER BY p.published_at DESC
\');
$res-&gt;bindParam(\':tag\', $tag, PDO::PARAM_INT);
$res-&gt;execute();
dump($res-&gt;fetchAll(PDO::FETCH_ASSOC));</code></pre>
<p><br />//===============================================================================<br />$post = $this-&gt;getDoctrine()-&gt;getRepository(Post::class)-&gt;find($id);<br />if (!$post) {<br />throw $this-&gt;createNotFoundException(<br />\'post nie istnieje\'<br />);<br />}<br />dump($post);<br />//===============================================================================</p>
<pre class="language-php"><code>$query = $this-&gt;getDoctrine()-&gt;getManager()
-&gt;createQuery(\'
SELECT p, t
FROM App:Post p
left JOIN p.tags t
ORDER BY p.publishedAt DESC
\');</code></pre>
<p><br /><br /></p>','publishedAt' => '2018-08-02 18:00:37','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Symfony przyklady zapytan','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '70','user_id' => '1','title' => 'Symfony custom form','slug' => 'symfony-custom-form','content' => '<pre class="language-php"><code>-&gt;add(\'firstname\', TextType::class, array(\'attr\' =&gt; array(\'class\' =&gt; \'form-control\')))
            -&gt;add(\'lastname\', TextType::class, array(\'attr\' =&gt; array(\'class\' =&gt; \'form-control\')))
            -&gt;add(\'class\', EntityType::class, array(
                \'class\' =&gt; SchoolClass::class,
                \'multiple\' =&gt; false,
                \'attr\' =&gt; array(\'class\' =&gt; \'form-control\'),
            ));</code></pre>','publishedAt' => '2018-08-02 18:03:45','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Symfony custom form','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '71','user_id' => '1','title' => 'Git przydatne komendy','slug' => 'git-przydatne-komendy','content' => '<h5>reset plikow</h5>
<pre class="language-markup"><code>git clean -fd</code></pre>
<h4><strong>git checkout -b nazwa_brancha </strong>:::tworzenie nowego brancha i odrazu przelaczenie sie na niego<br /><strong>git checkout nazwa_brancha </strong>:::przelaczeie sie na branch o danej nazwie<br /><strong>git push -u origin nazwa_brancha</strong> :::aktualizacja danego brancha w zdalnym repozytorium</h4>
<h4>Usunięcie folderu z cached</h4>
<pre class="language-markup"><code>git rm --cached -r public/css</code></pre>
<h5>Reset pojedyńczego pliku do stanu z początku commita</h5>
<pre class="language-markup"><code>git checkout HEAD -- src/DM/DashboardBundle/Controller/DashboardController.php</code></pre>
<h5>Nadpisanie commita</h5>
<pre class="language-markup"><code>git commit --amend
git push --force</code></pre>
<h5>Git patche</h5>
<pre class="language-markup"><code>git diff --patch &gt; mypatch.patch // to generate the patch
git apply mypatch.patch // to apply the patch</code></pre>
<h5>Usunięcie brancha</h5>
<pre class="language-markup"><code>git branch -d nazwa_brancha //usuniecie lokalnego brancha
git push -d origin nazwa_brancha //usuniecie brancha z repo</code></pre>
<h5>Cherry pick</h5>
<pre class="language-markup"><code>git cherry-pick numer_commita
git cherry-pick commit-first^..commit-last
git cherry-pick fdafe120a394be52491201b39a71395ef19c9977</code></pre>
<h5>Stash</h5>
<pre class="language-markup"><code>git stash save "my_stash"
git stash list
git stash drop n
git stash apply n</code></pre>
<h5>Clone</h5>
<pre class="language-markup"><code>git clone -b nazwa_brancha --single-branch https://adress.www</code></pre>
<h5>Init</h5>
<pre class="language-markup"><code>git init .
git remote add origin git@github.com:user/repo.git
git fetch origin
git checkout master</code></pre>','publishedAt' => '2018-08-02 18:05:05','updatedAt' => '2021-04-14 20:05:16','headtitle' => 'Git przydatne komendy','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '72','user_id' => '1','title' => 'Start testu po tagu','slug' => 'start-testu-po-tagu','content' => '<p><strong>vendor/bin/behat.bat --tags=getClientAndAgreement</strong></p>
<p><strong>bash: vendor/bin/behat.bat: No such file or directory</strong></p>
<p>git branch --set-upstream-to=origin/master master ::co robi</p>
<p><strong>&nbsp;sprawdzic : bin/czy jest behat</strong></p>
<p><strong>i vendor/bin/czy jest behat</strong></p>
<p><strong>//behat bat</strong></p>
<p>a:2:{i:0;s:10:"ROLE_ADMIN";i:1;s:8:"ROLE_API";}</p>','publishedAt' => '2018-08-07 05:19:18','updatedAt' => '2021-01-17 19:21:24','headtitle' => 'Start testu po tagu','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '73','user_id' => '1','title' => 'statyczny budowniczy','slug' => 'statyczny-budowniczy','content' => '<pre class="language-php"><code>&lt;?php
//Programowanie obiektowe w języku PHP5 / Moduł 4. Algorytmy i struktury danych / Cz. 3
// wzorzec budowniczy
class HTMLBuilder {
  private $html;

  private function __construct($html) {
    $this-&gt;html = $html;
  }

  public static function begin() {
    $builder = new HTMLBuilder(\'&lt;html&gt;&lt;head&gt;&lt;/head&gt;&lt;body&gt;\');
    return $builder;
  }

  public function p($paragraph) {
    $this-&gt;html .= \'&lt;p&gt;\' . $paragraph . \'&lt;/p&gt;\';
    return $this;
  }

  public function hr() {
    $this-&gt;html .= \'&lt;hr&gt;\';
    return $this;
  }

  public function pre($contents) {
    $safecontents = htmlspecialchars($contents);
    $this-&gt;html .= "&lt;pre&gt;$safecontents&lt;/pre&gt;";
    return $this;
  }

  public function end() {
    $this-&gt;html .= \'&lt;/body&gt;&lt;/html&gt;\';
    return $this-&gt;html;
  }

}

$test = HTMLBuilder::begin()

  -&gt;p(\'paragraf\')
  -&gt;hr()
  -&gt;pre(\'class HTMLBuilder\')
  -&gt;p(\'kolejny paragraf\')
  -&gt;end();

echo \'&lt;pre&gt;\';
var_dump($test);
echo \'&lt;/pre&gt;\';</code></pre>','publishedAt' => '2018-08-08 19:49:40','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'statyczny budowniczy','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '74','user_id' => '1','title' => 'Wzorzec komendy','slug' => 'wzorzec-komendy','content' => '<pre class="language-php"><code>&lt;?php
//Programowanie obiektowe w języku PHP5 / Moduł 5. SOLID / Cz. 2
require_once \'lib/logic/Aplication.php\';
require_once \'lib/logic/AplicationModel.php\';

interface Command {
  public function execute($parameters);
  public function printHelp();
}

class Action {
  public function execute() {}
}

class HellowAction extends Action {
  public function __construct($app) {
    $this-&gt;app = $app;
  }

  function execute() {
    $this-&gt;app-&gt;hellow();
  }
}

class LogoutAction extends Action {
  public function __construct($app) {
    $this-&gt;app = $app;
  }

  function execute() {
    $this-&gt;app-&gt;logout();
  }
}

//wzorzec komendy

class RequestProcess {
  public function process() {
    session_start();
    $app = new Aplication;
    $aplicationmodel = new AplicationModel;

    $actions[\'hellow\'] = new HellowAction($app);
    $actions[\'logout\'] = new LogoutAction($app);

    include \'template/header.php\';
    include \'template/menu.php\';

    $actionName = $_GET[\'action\'];
    $action = $actions[$actionName];

    $action-&gt;execute();

    include \'template/footer.php\';

  }
}
?&gt;</code></pre>
<p>To samo bez zastosowania wzoraca:</p>
<pre class="language-php"><code>&lt;?php

require_once \'lib/logic/Aplication.php\';
require_once \'lib/logic/AplicationModel.php\';

class RequestProcess {

  function __construct() {

  }

  public function process() {
    session_start();
    $app = new Aplication;
    $aplicationmodel = new AplicationModel;

    include \'template/header.php\';
    include \'template/menu.php\';

    if (isset($_GET[\'action\']) &amp;&amp; $_GET[\'action\'] == \'hellow\') {
      $app-&gt;Hellow();
    } elseif (isset($_GET[\'action\']) &amp;&amp; $_GET[\'action\'] == \'logout\') {
      $app-&gt;logout();
    }

    //z

    include \'template/footer.php\';

  }
}
?&gt;</code></pre>','publishedAt' => '2018-08-08 19:54:28','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Wzorzec komendy','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '75','user_id' => '1','title' => 'Vhost','slug' => 'vhost','content' => '<pre class="language-markup"><code># Virtual Hosts
#
&lt;VirtualHost *:80&gt;
  ServerName localhost
  ServerAlias localhost
  DocumentRoot "${INSTALL_DIR}/www"
  &lt;Directory "${INSTALL_DIR}/www/"&gt;
    Options +Indexes +Includes +FollowSymLinks +MultiViews
    AllowOverride All
    Require local
  SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
  &lt;/Directory&gt;
&lt;/VirtualHost&gt;
</code></pre>','publishedAt' => '2018-08-09 05:10:28','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Vhost','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '76','user_id' => '1','title' => 'Refaktoryzacja','slug' => 'refaktoryzacja','content' => '<pre class="language-markup"><code>przed_refaktoryzacja      MainIdModel metoda refreshIdentifiers ::: i ClientFilterFactory metoda create

po refaktoryzacji :</code></pre>
<pre class="language-markup"><code>https://ccc.eu/cz/cart/map/zasilkovna</code></pre>
<p>https://hotfix.brico.enp.pl/silikon-sanitarny-silton-s-001-bialy-280-ml-atlas</p>','publishedAt' => '2018-08-09 20:36:41','updatedAt' => '2021-04-28 08:17:43','headtitle' => 'Refaktoryzacja','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '77','user_id' => '1','title' => 'Crawler symfony','slug' => 'crawler-symfony','content' => '<pre class="language-php"><code>$crawler = $this-&gt;client-&gt;request(\'GET\', \'/awplans/homepage\');
         
$errorTag = $crawler-&gt;filter(\'h1\')-&gt;each(function($node) {
   return $node-&gt;html();
});
        
if (in_array(trim(\'AW Plany\'),$errorTag)) {
  echo \'istnieje\';
}</code></pre>
<pre class="language-php"><code>/**
    * @param $crawler - Crawler object  
    * @param $errorTag tag name 
    * @param $tagValue tag value 
    * 
    */
    public function checkTagError(object $crawler, string $tagName,string $tagValue){
                
        $errorTag = $crawler-&gt;filter($tagName)-&gt;each(function($node) {
            return $node-&gt;html();
        });
        
        if (in_array(trim($tagValue),$errorTag)) {
           return true;
        }else{
           return false;
        }
    }</code></pre>
<p>//api bundle</p>
<pre class="language-php"><code>https://github.com/uecode/api-key-bundle

https://packagist.org/packages/ma27/api-key-authentication-bundle

https://medium.com/@_Ma27_/authenticate-users-with-api-keys-using-symfony-and-doctrine-b2270752261a</code></pre>','publishedAt' => '2018-08-23 11:26:29','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Crawler symfony','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '78','user_id' => '1','title' => 'synfony assert','slug' => 'synfony-assert','content' => '<pre class="language-php"><code>use Doctrine\\ORM\\Mapping\\AttributeOverrides;
 
 * @Assert\\Expression(
 *      "this.email() != null ",
 *     message="The price for 2 pax standard is required",
 * )</code></pre>','publishedAt' => '2018-09-06 13:44:47','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'synfony assert','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '79','user_id' => '1','title' => 'Fos bundle zastąpienie pola swoimi atrybutami','slug' => 'fos-bundle-zastapienie-pola-swoimi-atrybutami','content' => '<pre class="language-php"><code>* @ORM\\AttributeOverrides({
 *      @ORM\\AttributeOverride(name="username",
 *         column=@ORM\\Column(
 *             name="username",
 *             type="string",
 *             length=180,
 *             nullable=false
 *         )
 *     ),})</code></pre>','publishedAt' => '2018-09-07 06:46:14','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Fos bundle zastąpienie pola swoimi atrybutami','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '81','user_id' => '1','title' => 'Symfony fosuserbundle zmiana hasla','slug' => 'symfony-fosuserbundle-zmiana-hasla','content' => '<pre class="language-php"><code>php bin/console fos:user:change-password</code></pre>','publishedAt' => '2018-09-10 17:59:47','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Symfony fosuserbundle zmiana hasla','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '82','user_id' => '1','title' => 'Monitor temperatury na linuxie','slug' => 'monitor-temperatury-na-linuxie','content' => '<pre class="language-php"><code>sudo sensors-detect</code></pre>','publishedAt' => '2018-09-10 20:11:26','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Monitor temperatury na linuxie','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '83','user_id' => '1','title' => 'Short isset w php 7','slug' => 'short-isset-w-php-7','content' => '<pre class="language-php"><code>//  PHP 5.3+
$fruit= isset($_GET[\'fruit\']) ? $_GET[\'fruit\'] : \'banana\';

// PHP 7
$fruit = $_GET[\'fruit\'] ?? \'banana\';</code></pre>','publishedAt' => '2018-09-11 22:57:47','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Short isset w php 7','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '85','user_id' => '1','title' => 'Synfony fos user tworzenie nowego usera','slug' => 'synfony-fos-user-tworzenie-nowego-usera','content' => '<pre class="language-php"><code>php bin/console fos:user:create test test@example.com test</code></pre>','publishedAt' => '2018-09-17 12:35:55','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Synfony fos user tworzenie nowego usera','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '86','user_id' => '1','title' => 'Przekazywanie wybranego klucza tablicy','slug' => 'przekazywanie-wybranego-klucza-tablicy','content' => '<pre class="language-php"><code>$array=[
	0 =&gt; [\'id\' =&gt; 1,\'name\' =&gt; \'janusz\'],
	1 =&gt; [\'id\' =&gt; 2,\'name\' =&gt; \'pioter\'],

];

var_dump(array_column($array, \'name\'));

//array(2) { [0]=&gt; string(6) "janusz" [1]=&gt; string(6) "pioter" }</code></pre>','publishedAt' => '2018-09-18 19:56:05','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Przekazywanie wybranego klucza tablicy','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '87','user_id' => '1','title' => 'Wlacznie htaccess','slug' => 'wlacznie-htaccess','content' => '<p>W pliku&nbsp;<strong><code>/etc/apache2/</code><code>apache2.conf</code></strong></p>
<p><strong><code>Zamienic :</code></strong></p>
<pre><code>&lt;Directory /var/www/&gt;
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
&lt;/Directory&gt;<br /><br />i na&nbsp;koncu : <br /></code></pre>
<pre><strong><code>sudo a2enmod rewrite 
</code></strong></pre>
<pre><strong><code>sudo service apache2 restart
</code></strong></pre>
<pre><code>&nbsp;</code></pre>
<p><strong><code></code></strong></p>','publishedAt' => '2018-09-27 19:08:44','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Wlacznie htaccess','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '88','user_id' => '1','title' => 'Przykładowa klasa z configiem','slug' => 'przykladowa-klasa-z-configiem','content' => '<pre class="language-php"><code>namespace App\\Domain\\Log\\Statuses;

class Statuses {

    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 2;

    private static $types = [
        self::STATUS_SUCCESS =&gt; \'success\',
        self::STATUS_ERROR =&gt; \'error\',
    ];

    /**
     * get types
     * 
     * @return array
     */
    public static function getTypes(): array {
        return self::$types;
    }

}</code></pre>','publishedAt' => '2018-03-28 11:37:13','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Przykładowa klasa z configiem','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '89','user_id' => '1','title' => 'Array merge','slug' => 'array-merge','content' => '<pre class="language-php"><code>$customReceivers = $this-&gt;receiversRepository-&gt;findAll();

        if (empty($customReceivers)) {
            return;
        }

        foreach ($customReceivers as $singleReceivers) {
            $receiverConfig = $this-&gt;dataProviders[$singleReceivers-&gt;getType()];
            array_push($this-&gt;receivers, new Receiver(
                $receiverConfig[\'app\'],
                $receiverConfig[\'account\'],
                array_merge($receiverConfig, [
                    \'value\' =&gt; $singleReceivers-&gt;getData(),
                ]),
                $receiverConfig[\'methodName\']
            ));
        }</code></pre>','publishedAt' => '2018-10-02 13:24:52','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Array merge','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '90','user_id' => '1','title' => 'Wysylka smsow','slug' => 'wysylka-smsow','content' => '<p>Jak chcesz dużo wysyłać a masz mało pieniędzy i trochę czasu to zaproponuje następujące rozwiązanie:<br />1. Stawiasz serwer z Ubuntu.<br />2. Bierzesz najtańszy abonament kt&oacute;ry daje Ci nielimitowane SMSy(T-mobile nie ma problemu z automatyczną wysyłką, Play może mieć, przeczytaj regulamin).&nbsp;<br />3. Podpinasz modem(najprostszy wystarczy ale musi być kompatybilny z Gammu) do serwera.<br />4. Na serwerze instalujesz Gammu i MySQL i konfigurujesz Gammu pod obsługę MySQL(szablon bazy trzeba rzucić i skonfigurować jakiegoś usera do bazy).<br />5. Odpalasz Gammu jako demon żeby czytał nowe SMSy do wysłania z MySQL<br />6. Nowe SMSy wrzucasz przez komendę gammu-smsd-inject. Jak chcesz gdzieś z zewnątrz to wystawiasz proste API do wrzucania nowych SMS&oacute;w<br /><br />Jest obsługa Unicode, długich wiadomości(sam dzieli), raport&oacute;w dostarczenia i odbierania wiadomości.<br /><br />Zrobiłem tak u klienta 4 lata temu i działa bez zarzutu z minimalnymi poprawkami do dzisiaj. Klient wysyła średnio 6000 sms&oacute;w miesięcznie co w SMSapi by kosztowało 500zł na miesiąc. Abonament kosztuje jakieś 40 + podstawowy koszt modemu kilka dych.<br /><br />Pytanie jeszcze czy SMSapi liczy te 8.5 grosza za jeden SMS w rozumieniu operatora(ograniczona liczba znak&oacute;w) czy po prostu za jedną wiadomość niezależnie od długości. Jeżeli pierwsza opcja do przebitka jest dużo większa przy długich wiadomościach(a takie m&oacute;j klient wysyła).</p>','publishedAt' => '2018-10-02 15:56:50','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Wysylka smsow','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '91','user_id' => '1','title' => 'Wylaczenie ekranu dotykowego','slug' => 'wylaczenie-ekranu-dotykowego','content' => '<pre class="language-php"><code>xinput</code></pre>','publishedAt' => '2018-10-02 18:30:55','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Wylaczenie ekranu dotykowego','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '92','user_id' => '1','title' => 'Routing sposob','slug' => 'routing-sposob','content' => '<div id="crayon-5bbf0d3a46f43763212056" class="crayon-syntax crayon-theme-twilight crayon-font-consolas crayon-os-pc print-yes notranslate" data-settings=" touchscreen minimize scroll-mouseover">
<div class="crayon-toolbar" data-settings=" show">
<div class="crayon-tools">&nbsp;</div>
<div class="crayon-tools">
<div class="crayon-button crayon-plain-button" title="Włącz/wyłącz czysty kod">
<div class="crayon-button-icon">
<pre class="language-php"><code>//router.php
Route::any(\'{controller}/{action?}\', function($controller = \'home\', $action = \'index\')
{
    $action = \'get\'.ucfirst($action);
    $controller = ucfirst($controller).\'Controller\';

    return App::make($controller)-&gt;{$action}();
})-&gt;where(\'controller\', \'\\w+\')-&gt;where(\'action\', \'\\w+\');</code></pre>
</div>
</div>
<div class="crayon-button crayon-popup-button" title="Otw&oacute;rz kod w nowym oknie">
<div class="crayon-button-icon">&nbsp;</div>
</div>
Kr&oacute;tkie wytłumaczenie tego kodu: w pierwszej linii łapiemy wywołania (ANY &ndash; dotyczy wszystkich, możecie to zawęzić tylko do np. GET lub POST) gdzie pierwszy człon traktujemy jako kontroler, a drugi jako jego metodę (zwr&oacute;ćcie uwagę na znak zapytania na końcu &ndash; oczywiście oznacza to, że akcja nie musi występować). Ustawiamy podstawowe wartości na&nbsp;<code>$controller = \'home\'</code>&nbsp;i&nbsp;<code>$action = \'index\'</code>, a niżej ładujemy dany kontroler ograniczając jednocześnie (where) nazwy do sł&oacute;w (litery i cyfry). Oczywiście możecie to rozszerzyć o kolejne parametry dla danej akcji.</div>
</div>
</div>','publishedAt' => '2018-10-15 20:21:47','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Routing sposob','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '93','user_id' => '1','title' => 'szkolenie','slug' => 'szkolenie','content' => '<p><a href="https://www.percona.com/doc/percona-toolkit/LATEST/pt-duplicate-key-checker.html">https://www.percona.com/doc/percona-toolkit/LATEST/pt-duplicate-key-checker.html</a></p>
<p><a href="https://github.com/datacharmer/test_db">https://github.com/datacharmer/test_db</a></p>
<p><a href="http://nosql-database.org/">http://nosql-database.org/</a></p>
<p>http://tsung.erlang-projects.org/user_manual/features.html</p>','publishedAt' => '2018-10-22 12:44:47','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'szkolenie','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '94','user_id' => '1','title' => 'Restart mysql na linuxie','slug' => 'restart-mysql-na-linuxie','content' => '<pre class="language-php"><code>sudo /etc/init.d/mysql restart</code></pre>','publishedAt' => '2018-10-22 18:30:50','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Restart mysql na linuxie','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '95','user_id' => '1','title' => 'Restart apache na linuxie','slug' => 'restart-apache-na-linuxie','content' => '<pre class="language-php"><code>sudo /etc/init.d/apache2 restart
</code></pre>','publishedAt' => '2018-10-22 18:34:13','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Restart apache na linuxie','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '96','user_id' => '1','title' => 'Pokazywanie wartosci danej zmiennej w mysql','slug' => 'pokazywanie-wartosci-danej-zmiennej-w-mysql','content' => '<pre class="language-php"><code>mysql -u root -p</code></pre>
<pre class="language-php"><code>SHOW VARIABLES LIKE "max_connections";</code></pre>','publishedAt' => '2018-10-22 21:25:34','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Pokazywanie wartosci danej zmiennej w mysql','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '97','user_id' => '1','title' => 'Partycjonowanie','slug' => 'partycjonowanie','content' => '<pre class="language-php"><code>CREATE TABLE `salaries_2` (
  `emp_no` int(11) NOT NULL,
  `salary` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL
) 

PARTITION BY RANGE(YEAR(from_date)) (
    PARTITION p0 VALUES LESS THAN (1985),
    PARTITION p1 VALUES LESS THAN (1995),
    PARTITION p2 VALUES LESS THAN (2000),
    PARTITION p3 VALUES LESS THAN (2002),
    PARTITION p4 VALUES LESS THAN MAXVALUE
)

explain select count(*) from salaries_2 where from_date = \'2001-01-01\';

CREATE TABLE `salaries_3` (
  `emp_no` int(11) NOT NULL,
  `salary` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL
) 

PARTITION BY HASH(emp_no)
PARTITIONS 6;</code></pre>','publishedAt' => '2018-10-23 09:50:28','updatedAt' => '2020-11-07 17:19:40','headtitle' => 'Partycjonowanie','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '98','user_id' => '1','title' => 'Sprawdzanie czy tablica jest pusta','slug' => 'sprawdzanie-czy-tablica-jest-pusta','content' => '<pre class="language-php"><code>&lt;?php

$array = [\'key\' =&gt; \'\'];

if (empty(array_filter($array))){
    echo \'empty\';
}else{
    echo \'not empty\';
}</code></pre>','publishedAt' => '2018-10-26 17:39:55','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Sprawdzanie czy tablica jest pusta','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '99','user_id' => '1','title' => 'Pusta pierwsza wartość w formie typu select','slug' => 'pusta-pierwsza-wartosc-w-formie-typu-select','content' => '<pre class="language-php"><code>-&gt;add(\'status\', ChoiceType::class, [\'attr\' =&gt; array(\'class\' =&gt; \'form-control\'), \'choices\' =&gt; ClientStatuses::getTypes(), \'required\' =&gt; false, \'label\' =&gt; \'Status:\', \'placeholder\' =&gt; \'-------\'])
</code></pre>','publishedAt' => '2018-10-29 07:21:38','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Pusta pierwsza wartość w formie typu select','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '100','user_id' => '1','title' => 'Symfony 2 generowanie encji z pliku w danym bandlu','slug' => 'symfony-2-generowanie-encji-z-pliku-w-danym-bandlu','content' => '<pre class="language-php"><code>php app/console doctrine:generate:entities MultiStepFormBundle</code></pre>','publishedAt' => '2018-10-29 12:50:10','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Symfony 2 generowanie encji z pliku w danym bandlu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '101','user_id' => '1','title' => 'Tuning php','slug' => 'tuning-php','content' => '<pre class="language-php"><code>//php.ini
php_value upload_max_filesize 120M //file size
php_value post_max_size 120M
php_value max_execution_time 200
php_value max_input_time 200</code></pre>','publishedAt' => '2018-10-29 21:51:45','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Tuning php','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '102','user_id' => '1','title' => 'Parser inputa','slug' => 'parser-inputa','content' => '<pre class="language-php"><code> &lt;?php
$attribs = \' id= "header" class = "foobar" style ="background-color:#fff; color: red; "\';
$attribs = \'type="text"      name="janusz"  class="form-control"          onclick="myFunction()"\';

$x = new SimpleXMLElement("&lt;element $attribs /&gt;");
echo \'&lt;pre&gt;\';
var_dump($x);
echo \'&lt;/pre&gt;\';

?&gt;</code></pre>','publishedAt' => '2018-10-29 21:56:50','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Parser inputa','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '103','user_id' => '1','title' => 'Parser tagow inputa do tablicy','slug' => 'parser-tagow-inputa-do-tablicy','content' => '<pre class="language-php"><code>&lt;?php

/**
 * Class parse data from string - example:
 * [ input value="asdasd" type="text" name="imie" placeholder="" class="" style="" validators="onlyNumbers, min(3), max(128)" items="1|aaa, 2|bbb" ]
 * Result getParseTags() = array:
    [
        \'value\' =&gt; \'asdasd\',
        \'type\' =&gt; \'text\',
        \'name\' =&gt; \'imie\',
        \'placeholder\' =&gt; \'\',
        \'class\' =&gt; \'\',
        \'style\' =&gt; \'\',
        \'validators\' =&gt; [
            \'onlyNumbers\' =&gt; \'\',
            \'min\' =&gt; \'3\',
            \'max\' =&gt; \'128\'
        ],
        \'items\' =&gt; [
            1 =&gt; \'aaa\',
            2 =&gt; \'bbb\'
        ]
    ]
 */
class TagRecognizer {

    /**
     * @var string 
     */
    private $string;

    /**
     * 
     * @param string $string
     */
    public function __construct($string) {
        $this-&gt;string = $string;
    }

    /**
     * get array tags
     * 
     * @return array
     */
    public function getParseTags() {
        //get all between example="example" ::: (\\w+) creates array $result[1]-----------([^"]*) creates array $result[2]
        preg_match_all(\'@(\\w+)="([^"]*)"@\', $this-&gt;string, $result);
        //new array created from $result[1](key) =&gt; $result[2](Value)
        $arrayTags = array_combine($result[1], $result[2]);

        return $this-&gt;parseTags($arrayTags);
    }

    /**
     * parse tags array to new array
     * 
     * @param array $arrayTags
     * 
     * @return array
     */
    private function parseTags(array $arrayTags) {

        $parseArrayTags = [];

        foreach ($arrayTags as $key =&gt; $value) {
            //items
            if ($key === \'items\') {
                $parseArrayTags[$key] = $this-&gt;parseItems($value);
                //validators
            } elseif ($key === \'validators\') {
                $parseArrayTags[$key] = $this-&gt;parseValidators($value);
                //simple tag
            } else {
                $parseArrayTags[$key] = $value;
            }
        }

        return $parseArrayTags;
    }

    /**
     *  parse items in string
     * 
     * @param string $value
     * 
     * @return array
     */
    private function parseItems($value) {
        $arrInfo = explode("#", $value);

        $newArray = [];
        foreach ($arrInfo as $item) {
            $values = explode("|", $item);
            $newArray[trim($values[0])] = $values[1];
        }

        return $newArray;
    }

    /**
     * parse validators in string
     * 
     * @param string $value
     * 
     * @return array
     */
    private function parseValidators($value) {
        $arrInfo = explode(",", $value);
        $newArray = [];
        foreach ($arrInfo as $item) {

            // if item has () in value - $keyValidatorValue is equal text between this brackets
            if (strpos($item, \'(\') !== false &amp;&amp; strpos($item, \')\') !== false) {

                preg_match(\'@\\((.*?)\\)@\', $item, $match);

                $keyValidatorValue = $match[1];
            } else {
                $keyValidatorValue = \'\';
            }

            //$newArray[$item] - delete bracket with text and trim $item
            $newArray[trim(preg_replace("@\\([^)]+\\)@", "", $item))] = $keyValidatorValue;
        }

        return $newArray;
    }

}
</code></pre>
<pre class="language-php"><code>&lt;?php

namespace MultiStepFormBundle\\Tests\\TagRecognizer;

use Symfony\\Bundle\\FrameworkBundle\\Test\\WebTestCase;
use MultiStepFormBundle\\Domain\\Tag\\Recognizer\\TagRecognizer;

class TagRecognizerTest extends WebTestCase {

    /**
     * 
     * @dataProvider dataProvider
     */
    public function testTagRecognizer($expect, $string) {
        $result = new TagRecognizer($string);
        $result = $result-&gt;getParseTags();

        return $this-&gt;assertEquals($expect, $result);
    }

    public function dataProvider() {
        return [
            [   
                [
                    \'value\' =&gt; \'asdasd\',
                    \'type\' =&gt; \'text\',
                    \'name\' =&gt; \'imie\',
                    \'placeholder\' =&gt; \'\',
                    \'class\' =&gt; \'\',
                    \'style\' =&gt; \'\',
                    \'validators\' =&gt; [
                        \'onlyNumbers\' =&gt; \'\',
                        \'min\' =&gt; \'3\',
                        \'max\' =&gt; \'128\'
                    ],
                    \'items\' =&gt;[
                        1 =&gt; \'aaa\',
                        2 =&gt; \'bbb\'
                    ]
                ] ,
                \'[ input value="asdasd" type="text" name="imie" placeholder="" class="" style="" validators="onlyNumbers, min(3), max(128)" items="1|aaa# 2|bbb"      ]\'
            ],
            //==========================================================================================================================================================
            //==========================================================================================================================================================
            [   
                [
                    \'value\' =&gt; \'12323435\',
                    \'type\' =&gt; \'integer\',
                    \'name\' =&gt; \'example name\',
                    \'placeholder\' =&gt; \'example placeholder\',
                    \'class\' =&gt; \'form-controll\'
                ],
                \'[ input value="12323435" type="integer" name="example name" placeholder="example placeholder" class="form-controll" ]\'
            ],
            //==========================================================================================================================================================
            //==========================================================================================================================================================
            [   
                [
            
                    \'type\' =&gt; \'hidden\',
                    \'id\' =&gt; \'container\',
                    \'placeholder\' =&gt; \'bummmmmm\',
                    \'class\' =&gt; \'form-custom\',
                    \'validators\' =&gt; [
                        \'example\' =&gt; \'\',
                        \'email\' =&gt; \'\',
                        \'phone\' =&gt; \'\',
                        \'lenght\' =&gt; \'200\'
                    ],
                ],
                \'[ input type="hidden" id="container"  placeholder="bummmmmm" class="form-custom" validators="example, email, phone, lenght(200)" ]\'
            ],
            //==========================================================================================================================================================
            //==========================================================================================================================================================
            [   
                [
                    \'type\' =&gt; \'date\',
                    \'id\' =&gt; \'data_container\',
                    \'placeholder\' =&gt; \'2019-01-01\',
                    \'class\' =&gt; \'date-custom\',
                    \'exampleTag\' =&gt; \'lorem ipsum\',
                    \'items\' =&gt;[
                        1 =&gt; \'one\',
                        2 =&gt; \'two\',
                        3 =&gt; \'three\'
                    ],
                    \'validators\' =&gt; [
                        \'date\' =&gt; \'\',
                        \'lenght\' =&gt; \'202\'
                    ],
                ],
                \'[ input type="date" id="data_container"  placeholder="2019-01-01" class="date-custom" exampleTag="lorem ipsum" items="1|one# 2|two# 3|three" validators="date, lenght(202)" ]\'
            ],
            //==========================================================================================================================================================
            //==========================================================================================================================================================
            [   
                [
                    \'name\' =&gt; \'example_name_value\',
                    \'otherTag\' =&gt; \'example_value\', 
                    \'id\' =&gt; \'my-id\', 
                    \'class\' =&gt; \'my_amazing_class\', 
                    
                ],
                \'[input name="example_name_value" otherTag="example_value" id="my-id" class="my_amazing_class"]\'
            ],
            //==========================================================================================================================================================
            //==========================================================================================================================================================
            [   
                [
                    \'name\' =&gt; \'example_name_value\',
                    \'otherTag\' =&gt; \'example_value\', 
                    \'id\' =&gt; \'my-id\', 
                    \'class\' =&gt; \'my_amazing_class\',
                    \'validators\' =&gt; [
                        \'custom_validator\' =&gt; \'\',
                        \'lenght\' =&gt; \'18\',
                        \'other_validator\' =&gt; \'\',
                        \'expectedValue\' =&gt; \'12\',
                    ]
                ],
                \'[input    name="example_name_value"   otherTag="example_value"   id="my-id"   class="my_amazing_class"   validators="custom_validator, lenght(18), other_validator, expectedValue(12)"]\'
            ],
            //==========================================================================================================================================================
            //==========================================================================================================================================================
            [   
                [],
                \'[    ]\'
            ],
            //==========================================================================================================================================================
            //==========================================================================================================================================================
            [   
                [],
                \'\'
            ],
            //==========================================================================================================================================================
            //==========================================================================================================================================================
            [   
                [],
                \'[input]\'
            ],
            //==========================================================================================================================================================
            //==========================================================================================================================================================
            [   
                [
                    \'name\' =&gt; \'example_name_value\',
                    \'otherTag\' =&gt; \'example_value\', 
                    \'id\' =&gt; \'my-id\', 
                    \'validators\' =&gt; [
                        \'required\' =&gt; \'\',
                    ]                    
                ],
                \'[input name="example_name_value" otherTag="example_value" id="my-id" validators="required"]\'
            ],
            //==========================================================================================================================================================
            //==========================================================================================================================================================
            
        ];
    }

}

?&gt;</code></pre>','publishedAt' => '2018-10-30 11:53:43','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Parser tagow inputa do tablicy','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '104','user_id' => '1','title' => 'Ignorowanie wersji php','slug' => 'ignorowanie-wersji-php','content' => '<pre class="language-php"><code>--ignore-platform-reqs</code></pre>','publishedAt' => '2018-10-30 14:14:37','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Ignorowanie wersji php','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '105','user_id' => '1','title' => 'Pobranie tekstu między dwoma nawisami []','slug' => 'pobranie-tekstu-miedzy-dwoma-nawisami','content' => '<pre class="language-php"><code>//get all between [] to array     
preg_match_all(\'@\\[.*?\\]@\', $this-&gt;multiStepForm-&gt;getCustomContent(), $inputsString);</code></pre>','publishedAt' => '2018-11-02 12:37:04','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Pobranie tekstu między dwoma nawisami []','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '106','user_id' => '1','title' => 'Aktualna data plus liczba dni','slug' => 'aktualna-data-plus-liczba-dni','content' => '<pre class="language-php"><code>&lt;?php

class ConvertIntToDateByDayNumbers {
    
    /**
     * if parameter is integer and is greater than zero adds to actual date number days == $expire otherwise function return default value
     * 
     * @param string $expire
     * @return string
     */
    public static function init(string $expire):string {

        if (!is_numeric($expire) || $expire &lt; 0) {
            return $expire;
        }

        return date(\'Y-m-d\', strtotime("+" . $expire . "days"));
    }

}</code></pre>','publishedAt' => '2018-11-02 13:44:28','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Aktualna data plus liczba dni','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '107','user_id' => '1','title' => 'Optymalizacja laravela','slug' => 'optymalizacja-laravela','content' => '<pre class="language-php"><code>php artisan cache:clear
php artisan config:clear
php artisan route:clear

php artisan config:cache
php artisan route:cache
php artisan optimize
</code></pre>','publishedAt' => '2018-11-03 16:37:59','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Optymalizacja laravela','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '108','user_id' => '1','title' => 'Przydatne info mysql','slug' => 'przydatne-info-mysql','content' => '<pre class="language-css"><code>https://x-coding.pl/blog/developers/mysql-explain-uzywac-czytac/</code></pre>','publishedAt' => '2018-11-03 17:21:07','updatedAt' => '2018-11-03 18:00:56','headtitle' => 'Przydatne info mysql','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '109','user_id' => '1','title' => 'Naprawa bledu po update','slug' => 'naprawa-bledu-po-update','content' => '<p>/usr/share/phpmyadmin/libraries/config.default.php</p>
<pre class="language-php"><code>$cfg[\'blowfish_secret\'] = \'qtdRoGmbc9{8IZr323xYcSN]0s)r$9b_JUnb{~Xz\';</code></pre>
<p>Jeżeli wystąpi błąd z tmp nalezy stworzyć folder tmp&nbsp; w/usr/share/phpmyadmin/tmp i nadac mu uprawnienia do zapisu.</p>','publishedAt' => '2018-11-07 20:15:46','updatedAt' => '2018-11-07 20:15:46','headtitle' => 'Naprawa bledu po update','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '110','user_id' => '1','title' => 'Tylko litery i spacje','slug' => 'tylko-litery-i-spacje','content' => '<pre class="language-php"><code>if (!preg_match("@^[a-zA-ZĄąĘę&Oacute;&oacute;ĆćŁłŃńŚśŹźŻż\\s]+$@", $value)) {
   $this-&gt;errorMessage = \'Name must only contain letters!\';
}</code></pre>','publishedAt' => '2018-11-09 07:40:44','updatedAt' => '2018-11-09 07:40:44','headtitle' => 'Tylko litery i spacje','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '111','user_id' => '1','title' => 'Slug','slug' => 'slug','content' => '<pre class="language-php"><code>&lt;?php

namespace Utils\\Slug;

Class Slug {

    public static function slugify($url, $maxLength = 500) {

        $S = array(\'/ą/\', \'/ż/\', \'/ź/\', \'/ę/\', \'/ć/\', \'/ń/\', \'/&oacute;/\', \'/ł/\', \'/ś/\', \'/Ą/\', \'/Ż/\', \'/Ź/\', \'/Ę/\', \'/Ć/\', \'/Ń/\', \'/&Oacute;/\', \'/Ł/\', \'/Ś/\');
        $R = array(\'a\', \'z\', \'z\', \'e\', \'c\', \'n\', \'o\', \'l\', \'s\', \'A\', \'Z\', \'Z\', \'E\', \'C\', \'N\', \'O\', \'L\', \'S\');
        $url = preg_replace($S, $R, $url);
        $url = strtolower(trim($url));
        $url = preg_replace(\'/\\?/\', \'\', $url);
        $url = preg_replace(\'/[^a-z0-9-]/\', \'-\', $url);
        $url = preg_replace(\'/-+/\', "-", $url);
        $url = substr($url, 0, $maxLength);
        return $url;
    }

}
</code></pre>','publishedAt' => '2018-11-13 14:31:23','updatedAt' => '2018-11-13 14:31:23','headtitle' => 'Slug','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '112','user_id' => '1','title' => 'Rozbudowany form w symfony','slug' => 'rozbudowany-form-w-symfony','content' => '<pre class="language-php"><code>&lt;?php

namespace AdvancedVerifyFieldBundle\\Form\\AdvancedVerifyFieldConfig;

use Symfony\\Component\\Form\\AbstractType;

class AdvancedVerifyFieldConfigType extends AbstractType {

    // uchwyt do entity managera
    protected $em;

    public function __construct($em = null) {
        $this-&gt;em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                -&gt;add(\'name\', TextType::class, array(\'label\' =&gt; \'Nazwa:\', \'required\' =&gt; \'true\', \'attr\' =&gt; array(\'class\' =&gt; \'form-control\')))
                -&gt;add(\'description\', TextType::class, array(\'label\' =&gt; \'Opis:\', \'attr\' =&gt; array(\'class\' =&gt; \'form-control\')))
                -&gt;add(\'slug\', TextType::class, array(\'label\' =&gt; \'Slug:\', \'required\' =&gt; \'true\', \'attr\' =&gt; array(\'class\' =&gt; \'form-control\', \'id\' =&gt; \'slug\')))
                -&gt;add(\'status\', CheckboxType::class, array(\'attr\' =&gt; array(\'class\' =&gt; \'checkbox\', \'data-toggle\' =&gt; \'toggle\')))
                -&gt;add(\'defaultHeader\', TextareaType::class, array(\'label\' =&gt; \'Domyślny Header:\', \'required\' =&gt; false, \'attr\' =&gt; array(\'class\' =&gt; \'form-control\')))
                -&gt;add(\'defaultFooter\', TextareaType::class, array(\'label\' =&gt; \'Domyślny Footer:\', \'required\' =&gt; false, \'attr\' =&gt; array(\'class\' =&gt; \'form-control\')))
                -&gt;add(\'css\', TextareaType::class, array(\'label\' =&gt; \'Domyślny CSS:\', \'required\' =&gt; false, \'attr\' =&gt; array(\'class\' =&gt; \'form-control\')))
                -&gt;add(\'scripts\', TextareaType::class, array(\'label\' =&gt; \'Domyślny JS:\', \'required\' =&gt; false, \'attr\' =&gt; array(\'class\' =&gt; \'form-control\')))
                -&gt;add(\'checkPhone\', CheckboxType::class, array(\'label\' =&gt; \'Sprawdzanie numeru telefon&oacute;w:\', \'required\' =&gt; false, \'attr\' =&gt; array(\'class\' =&gt; \'checkbox\', \'data-toggle\' =&gt; \'toggle\')))
                -&gt;add(\'checkPhoneFields\', TextType::class, array(\'label\' =&gt; \'checkPhoneFields:\', \'required\' =&gt; false, \'attr\' =&gt; array(\'class\' =&gt; \'form-control\')))
                -&gt;add(\'checkMail\', CheckboxType::class, array(\'label\' =&gt; \'Sprawdzanie numeru telefon&oacute;w:\', \'required\' =&gt; false, \'attr\' =&gt; array(\'class\' =&gt; \'checkbox\', \'data-toggle\' =&gt; \'toggle\')))
                -&gt;add(\'checkMailFields\', TextType::class, array(\'label\' =&gt; \'checkMailFields:\', \'required\' =&gt; false, \'attr\' =&gt; array(\'class\' =&gt; \'form-control\')))
                -&gt;add(\'steps\', CollectionType::class, [
                    \'entry_type\' =&gt; MultiStepFormStepType::class,
                    \'entry_options\' =&gt; [\'label\' =&gt; false],
                    \'allow_add\' =&gt; true,
                    \'allow_delete\' =&gt; true,
                    \'by_reference\' =&gt; false,
                    \'required\' =&gt; true,
                ])
                -&gt;add(\'integrations\', CollectionType::class, [
                    \'entry_type\' =&gt; MultiStepFormIntegrationType::class,
                    \'entry_options\' =&gt; [\'label\' =&gt; false],
                    \'allow_add\' =&gt; true,
                    \'allow_delete\' =&gt; true,
                    \'by_reference\' =&gt; false,
                    \'required\' =&gt; true,
                ])
                -&gt;add(\'submit\', SubmitType::class, [\'label\' =&gt; \'Zapisz\']);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver-&gt;setDefaults([
            \'data_class\' =&gt; MultiStepForm::class,
        ]);
    }

}
</code></pre>','publishedAt' => '2018-11-14 10:00:31','updatedAt' => '2018-11-14 10:00:31','headtitle' => 'Rozbudowany form w symfony','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '113','user_id' => '1','title' => 'symfony2 dodanie indexu','slug' => 'symfony2-dodanie-indexu','content' => '<pre class="language-markup"><code>AdvancedVerifyFieldBundle\\Entity\\AdvancedVerifyField:
    type:  entity
    repositoryClass: MultiStepFormBundle\\Repository\\MultiStepFormClientData\\MultiStepFormClientDataRepository
    table: advanced_verify_field
    id :
        transaction_id :
            type: uuid
            generator:
                strategy: CUSTOM
            customIdGenerator:
                class: Ramsey\\Uuid\\Doctrine\\UuidGenerator
    fields:
        searchHash:
            type: string
            nullable: false
            length: 32
        code:
            type: string
            nullable: false
            length: 16
        createdAt :
            type: datetime
            nullable: false
        confirmedAt :
            type: datetime
            nullable: true
        type:
            type: smallint
            nullable: false
        value:
            type: string
            nullable: false
            length: 255
    indexes:
        search_index:
          columns: [ searchHash ]

            </code></pre>','publishedAt' => '2018-11-16 07:27:01','updatedAt' => '2018-11-16 07:27:01','headtitle' => 'symfony2 dodanie indexu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '114','user_id' => '1','title' => 'Przyjazne adresy url','slug' => 'przyjazne-adresy-url','content' => '<p>https://codingcyber.org/create-pretty-clean-urls-php-htaccess-7259/?fbclid=IwAR3EhPONIOdVLCOZRbggUmDei0ciDoFeiZwvCHwwpPfGjiWyXuaUfTk2Vjk</p>','publishedAt' => '2018-11-17 17:52:38','updatedAt' => '2018-11-17 17:52:38','headtitle' => 'Przyjazne adresy url','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '115','user_id' => '1','title' => 'Md5 wyszukiwanie w mysql po dwóch kolumnach','slug' => 'md5-wyszukiwanie-w-mysql-po-dwoch-kolumnach','content' => '<p><a href="https://stackoverflow.com/questions/1764733/check-a-field-in-mysql-from-a-php-md5-string" target="_blank" rel="noopener">link</a></p>
<div id="gtx-trans" style="position: absolute; left: 273px; top: 31.2344px;">
<div class="gtx-trans-icon">&nbsp;</div>
</div>','publishedAt' => '2018-11-17 18:09:26','updatedAt' => '2020-11-07 17:18:35','headtitle' => 'Md5 wyszukiwanie w mysql po dwóch kolumnach','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '116','user_id' => '1','title' => 'Vertical align','slug' => 'vertical-align','content' => '<pre class="language-css"><code>.vertical_align {display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-align: center;-ms-flex-align: center;align-items: center;}


@media (max-width: 1199px) {
  .vertical_align {display: block; }
}</code></pre>','publishedAt' => '2018-12-04 20:49:27','updatedAt' => '2018-12-04 20:49:27','headtitle' => 'Vertical align','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '117','user_id' => '1','title' => 'Skosne tlo','slug' => 'skosne-tlo','content' => '<p>https://codepen.io/anon/pen/regaRE?editors=1100</p>','publishedAt' => '2018-12-04 22:37:27','updatedAt' => '2018-12-04 22:37:27','headtitle' => 'Skosne tlo','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '118','user_id' => '1','title' => 'Border zaokraglony','slug' => 'border-zaokraglony','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
	&lt;head&gt;
		&lt;meta charset="UTF-8"&gt;
		&lt;title&gt;Document&lt;/title&gt;
		&lt;style type="text/css"&gt;
			.border{
				border: 4px solid #40B55C;
			border-radius: 5px;
			position: absolute;
			height: 100%;
			}
		&lt;/style&gt;
	&lt;/head&gt;
	&lt;body&gt;
		
		&lt;div class="container"&gt;
			&lt;div class="row" style="position: relative;"&gt;
				&lt;div class="border"&gt;&lt;/div&gt;
				&lt;div class="col-lg-12"&gt;
					&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique sapiente vitae eius doloribus repudiandae unde repellat, quam saepe! Earum, cum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam perferendis provident ipsam aliquid delectus quia omnis nemo libero dicta maiores obcaecati, ut sit quasi laborum reiciendis soluta architecto veritatis incidunt rerum neque, doloribus magni. Architecto, consequuntur impedit eum magni consequatur numquam? Doloremque minus a vel nostrum, culpa reiciendis ipsam ea, explicabo ad in sint velit illum, et possimus repellat praesentium maxime repellendus! Impedit ab, quod tempora tempore obcaecati voluptatum laborum mollitia eius ducimus similique eum dicta rem nulla, sunt ullam magnam, cumque. Officia ex, quod optio numquam libero quibusdam vel, velit dolor dolorum deserunt quia voluptate inventore pariatur nobis blanditiis!&lt;/p&gt;
				&lt;/div&gt;
			&lt;/div&gt;
		&lt;/div&gt;
		&lt;!-- Latest compiled and minified CSS --&gt;
		&lt;link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"&gt;
		&lt;!-- Optional theme --&gt;
		&lt;link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"&gt;
		&lt;!-- Latest compiled and minified JavaScript --&gt;
		&lt;script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"&gt;&lt;/script&gt;

	&lt;/body&gt;
	&lt;/html&gt;</code></pre>','publishedAt' => '2018-12-10 22:05:11','updatedAt' => '2018-12-10 22:05:11','headtitle' => 'Border zaokraglony','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '119','user_id' => '1','title' => 'Obracajace się kółko','slug' => 'obracajace-sie-kolko','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
  &lt;head&gt;
    &lt;meta charset="UTF-8"&gt;
    &lt;title&gt;Document&lt;/title&gt;
    &lt;style type="text/css"&gt;
    
    .circle {
      position: relative;
      margin: 20px;
      height: 120px;
      width: 120px;
      background-color: orange;
      border-radius: 50%;
    }
    .circle__spin {
      position: absolute;
      left: -10px;
      top: -10px;
      width: 140px;
      height: 140px;
    }
    .circle__spin svg {
      width: 100%;
      height: 100%;
      animation: spin 10s linear infinite;
    }
    .circle__spin circle {
      stroke-width: 5;
      stroke-dasharray: 120, 20;
      fill: none;
      stroke: orange;
    }
    @keyframes spin {
      100% {
        transform: rotate(360deg);
      }
    }
    &lt;/style&gt;
  &lt;/head&gt;
  &lt;body&gt;
    &lt;div class="circle"&gt;
      &lt;div class="circle__spin"&gt;
        &lt;svg&gt;
          &lt;circle cx="50%" cy="50%" r="67px"&gt;&lt;/circle&gt;
        &lt;/svg&gt;
      &lt;/div&gt;
    &lt;/div&gt;
  &lt;/body&gt;
&lt;/html&gt;</code></pre>
<p><a href="https://jsfiddle.net/mordimer/1psg8yov/">LINK</a></p>','publishedAt' => '2018-12-16 11:55:47','updatedAt' => '2018-12-16 11:55:47','headtitle' => 'Obracajace się kółko','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '120','user_id' => '1','title' => 'Hover okrągłego zdjęcia','slug' => 'hover-okraglego-zdjecia','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
  &lt;head&gt;
    &lt;meta charset="UTF-8"&gt;
    &lt;title&gt;Document&lt;/title&gt;
    &lt;style type="text/css"&gt;
    .circle{
      width:500px;
      height:500px;
      border-radius:500px;
      overflow:hidden;
      position:relative;
      z-index:1;
    }
    img{
      -webkit-transition:all .2s ease;
      transition:all .2s ease;
    }
    .circle:hover img{
      -webkit-transform:scale(1.1);
      transform:scale(1.1);
    }
    &lt;/style&gt;
  &lt;/head&gt;
  &lt;body&gt;
    &lt;div class=\'circle\'&gt;
      &lt;img src="http://rack.3.mshcdn.com/media/ZgkyMDEzLzA1LzA4L2JlL2JhYmllc193aXRoLjA5NGNjLnBuZwpwCXRodW1iCTg1MHg1OTA-CmUJanBn/8f195417/e44/babies_with_swagg.jpg" alt="" /&gt;
    &lt;/div&gt;
  &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2018-12-16 12:00:22','updatedAt' => '2018-12-16 12:00:22','headtitle' => 'Hover okrągłego zdjęcia','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '121','user_id' => '1','title' => 'Skośne tła','slug' => 'skosne-tla','content' => '<p><a href="https://tympanus.net/codrops/2011/12/21/slopy-elements-with-css3/">LINK</a></p>','publishedAt' => '2018-12-16 12:34:36','updatedAt' => '2018-12-16 12:34:36','headtitle' => 'Skośne tła','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '122','user_id' => '1','title' => 'Niesamowite efekty','slug' => 'niesamowite-efekty','content' => '<p><a href="https://www.webdesignerdepot.com/2017/09/11-experimental-css-projects-thatll-blow-your-mind/">Link</a> <br /><a href="http://caniuse.com/">link di strony z testowaniem przegldarek</a><br /><a href="https://validator.w3.org/nu/?doc=http%3A%2F%2Fw90dni.it%2F">walidator html</a><br /><a href="http://kursjs.pl/kurs/dodatki/js_i_php.php"> java script i php</a><br /><a href="https://tympanus.net/Development/HoverEffectIdeas/"> hover img!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</a><br /><a href="https://designshack.net/tutorialexamples/imagehovers/index.html"> hover img!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</a><br /><a href="http://www.fanaticalcode.com/blog/nowe-podejscie-do-walidacji-formularzy-w-html5/">walidacja !!!!!!!</a><br /><a href="http://strefakodera.pl/poradniki/wordpress/paginacja-w-wordpressie-wczytywanie-postow-w-tle-nieskonczone-przewijanie-infinite-scroll">fajnz blog !!!!!!!</a><br /><a href="http://shebang.pl/kursy/podstawy-jquery/r3/">jquery!!!!!!!!!!!!</a><br /><a href="http://kursjs.pl/kurs/jquery/jquery.php">jquery2222222222222!!!!!!!!!!!!</a><br /><a href="http://www.marcin-gajewski.eu">portfolio typa</a><br /><a href="http://gsoftware.pl/czytaj/blog/php/page/2/">programsta</a><br /><a href="https://artykuly.softonic.pl/windows-xp-musi-odejaa">blog</a><br /><a href="https://codemyui.com/stripe-com-header-navigation-code/">fajne rozne rzeczy</a><br /><a href="http://tutorials.comandeer.pl/html5-blog.html">sematyczny kod</a><br /><a href="https://developers.facebook.com/tools/debug/?hc_location=ufi">facebook debuger</a></p>','publishedAt' => '2018-12-16 13:03:13','updatedAt' => '2018-12-16 13:03:13','headtitle' => 'Niesamowite efekty','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '123','user_id' => '1','title' => 'Scroll menu','slug' => 'scroll-menu','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
    &lt;head&gt;
        &lt;meta charset="utf-8"&gt;
        &lt;meta http-equiv="X-UA-Compatible" content="IE=edge"&gt;
        &lt;meta name="viewport" content="width=device-width, initial-scale=1"&gt;
        &lt;!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags --&gt;
        &lt;title&gt;Bootstrap 101 Template&lt;/title&gt;
        &lt;!-- Bootstrap --&gt;
        &lt;style type="text/css"&gt;
        .nav {
            background-color: #FFF;
            width:100%;
            font-size: 14px;
            font-weight:900;
            letter-spacing: 1px;
            min-width: 50px;
            overflow-x: auto;
            margin-left: 0px;
            text-transform: uppercase;
            z-index:9000;
            -webkit-box-shadow: 0px 2px 12px 0px rgba(0, 79, 180, 1);
            -moz-box-shadow: 0px 2px 12px 0px rgba(0, 79, 180, 1);
            box-shadow: 0px 2px 12px 0px rgba(0, 79, 180, 1);
        }
        .nav li {
            display: inline-block;
            padding: 10px 15px 10px 15px;
            overflow: visible;
            height:auto;
            font-weight: 500;
            letter-spacing: .1em;
        }
        .nav li:nth-child(1){}
        .nav li:last-child {margin-right:60px;}
        .nav ul {
            list-style: none;
            text-align: center;
            white-space: nowrap;
            margin:15px 73px 10px 23px;
        }
        .nav li a {
            color:#000
        }
        .nav li a:hover {
            text-decoration: none;
            opacity: 0.5;
        }
        .logo{
            width:30px;
            float:left;
            padding-bottom:10px;
            height:20px;
            margin-right:50px;
        }
        body {
            margin: 0px;
        }
        &lt;/style&gt;
    &lt;/head&gt;
    &lt;body&gt;
        &lt;nav class="nav" data-sticky=""&gt;
            &lt;ul class=""&gt;
                &lt;div class="logo animated infinite pulse"&gt;
                &lt;img src="img/html-5.svg" height="30" width="30" /&gt;&lt;/div&gt;
                &lt;li&gt;&lt;a href="#omnie" &gt;O mnie&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href="#opis" &gt;Opis&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href="#wersion" &gt;Wersion&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href="#yourteam" &gt;Your team&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href="#opinie" &gt;Opinie&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href="#location" &gt;Lokation&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href="#contaktimage" &gt;kontakt&lt;/a&gt;&lt;/li&gt;
            &lt;/ul&gt;
        &lt;/nav&gt;
    &lt;/body&gt;
&lt;/html&gt;</code></pre>
<p><a href="https://jsfiddle.net/mordimer/n5137802/">link</a></p>','publishedAt' => '2018-12-16 13:34:20','updatedAt' => '2018-12-16 13:34:20','headtitle' => 'Scroll menu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '124','user_id' => '1','title' => 'Tło z video w tle z podzielonym ekranem','slug' => 'tlo-z-video-w-tle-z-podzielonym-ekranem','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
    &lt;head&gt;
        &lt;meta charset="UTF-8"&gt;
        &lt;title&gt;Document&lt;/title&gt;
    &lt;/head&gt;
    &lt;body&gt;
        &lt;div class="banner"&gt;
            &lt;div class="image-container"&gt;
                &lt;div class="text text-test"&gt;NATURE&lt;/div&gt;
            &lt;/div&gt;
            &lt;video autoplay loop muted class="banner__video" poster="https://pawelgrzybek.com/photos/2015-12-06-codepen.jpg"&gt;
                &lt;source src="https://pawelgrzybek.com/photos/2015-12-06-codepen.webm" type="video/webm"&gt;
                &lt;source src="https://pawelgrzybek.com/photos/2015-12-06-codepen.mp4" type="video/mp4"&gt;
            &lt;/video&gt;
        &lt;/div&gt;
        &lt;style type="text/css"&gt;

            body {
                margin: 0;
            }

            .banner {
                position: relative;
                overflow: hidden;
                font-family: \'Raleway\', sans-serif;
                font-size: 1em;
                font-weight: 700;
                text-transform: uppercase;
                text-align: center;
                letter-spacing: 2px;
            }
            .banner__video {
                position: absolute;
                top: 50%;
                left: 50%;
                width: auto;
                min-width: 100%;
                height: auto;
                min-height: 100%;
                transform: translateX(-50%) translateY(-50%);
                z-index: -10;
            }
            .banner::before{
                background: black;
                content: \'\';
                width: 100%;
                height: 100%;
                position: absolute;
            }
            .image-container {
                background-size: cover;
                position: relative;
                height: 500px;
                /*  background-color: black;
                */
            }
            .text {
                background-color: white;
                color: black;
                font-size: 10vw; 
                font-weight: bold;
                margin: 0 auto;
                padding: 10px;
                width: 50%;
                text-align: center;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                mix-blend-mode: screen;
            }
        &lt;/style&gt;
    &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2018-12-21 13:22:42','updatedAt' => '2018-12-21 13:22:42','headtitle' => 'Tło z video w tle z podzielonym ekranem','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '125','user_id' => '1','title' => 'Parser pobierający dane między dwoma nazwisami','slug' => 'parser-pobierajacy-dane-miedzy-dwoma-nazwisami','content' => '<pre class="language-php"><code>&lt;?php

class InputsParser {

    /**
     * @param string $content
     * @return array
     */
    public static function initParser($content) {
        $inputsArray = [];
        //get all between [] to array     
        preg_match_all(\'@\\[\\s*input .*?\\]@\', $content, $inputsArray);
        return $inputsArray;
    }

}
</code></pre>','publishedAt' => '2018-12-24 08:30:53','updatedAt' => '2018-12-24 08:30:53','headtitle' => 'Parser pobierający dane między dwoma nazwisami','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '126','user_id' => '1','title' => 'tuning mysqla','slug' => 'tuning-mysqla','content' => '<h3>/etc/mysql/my.cnf</h3>
<p><a href="https://www.percona.com/blog/2014/01/28/10-mysql-performance-tuning-settings-after-installation/">link</a></p>
<p>Koniecznie pod tagiem [mysqld] !!!!</p>
<p>&nbsp;</p>
<pre class="language-markup"><code>#
# The MySQL database server configuration file.
#
# You can copy this to one of:
# - "/etc/mysql/my.cnf" to set global options,
# - "~/.my.cnf" to set user-specific options.
# 
# One can use all long options that the program supports.
# Run program with --help to get a list of available options and with
# --print-defaults to see which it would actually understand and use.
#
# For explanations see
# http://dev.mysql.com/doc/mysql/en/server-system-variables.html

# This will be passed to all mysql clients
# It has been reported that passwords should be enclosed with ticks/quotes
# escpecially if they contain "#" chars...
# Remember to edit /etc/mysql/debian.cnf when changing the socket location.

# Here is entries for some specific programs
# The following values assume you have at least 32M ram

!includedir /etc/mysql/conf.d/


[mysqld]
max_connections = 500

max_allowed_packet=768M
key_buffer_size=2048M //dla mysam !!! malo wazne

innodb_buffer_pool_size=512M
innodb_log_file_size=128M
innodb_log_buffer_size=512M

query_cache_limit		= 0M
query_cache_size		= 0M

innodb_buffer_pool_instances=2

#tmp_table_size=2G
#max_heap_table_size=2G
</code></pre>','publishedAt' => '2018-12-25 13:03:49','updatedAt' => '2020-10-22 22:23:22','headtitle' => 'mariadb.cnf','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '127','user_id' => '1','title' => 'Sprawdzanie czy istnieje wartość w json','slug' => 'sprawdzanie-czy-istnieje-wartosc-w-json','content' => '<pre class="language-javascript"><code>isAleradyUsed(tagName) {
return Object.values(this.jsonTagsVariance).indexOf(tagName) &gt; -1 ? true : false;
}</code></pre>','publishedAt' => '2019-01-04 14:43:42','updatedAt' => '2019-01-04 14:43:42','headtitle' => 'Sprawdzanie czy istnieje wartość w json','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '128','user_id' => '1','title' => 'Data urodzenia z numeru pesel','slug' => 'data-urodzenia-z-numeru-pesel','content' => '<pre class="language-javascript"><code> function getBirthdayDateFromPesel(pesel, sign){
      pesel = pesel.split(\'\');

      var year = 1900+parseInt(pesel[0])*10+parseInt(pesel[1]);

      if (pesel[2]&gt;=2 &amp;&amp; pesel[2]&lt;8)
        year+=Math.floor(pesel[2]/2)*100;
      if (pesel[2]&gt;=8)
        year-=100;

      var month = (pesel[2]%2)*10+pesel[3];
      var day = pesel[4]*10+pesel[5];

      return day+sign+month+sign+year;
}</code></pre>','publishedAt' => '2019-01-08 10:42:03','updatedAt' => '2019-01-08 10:42:03','headtitle' => 'Data urodzenia z numeru pesel','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '129','user_id' => '1','title' => 'Dodanie myślnika do kodu pocztowego','slug' => 'dodanie-myslnika-do-kodu-pocztowego','content' => '<pre class="language-javascript"><code>function autoCompletePostCode(inputId){
	$(\'body\').on(\'keyup\', inputId, function(event) {
		var liveInputValue = $(this).val();

		//if input lenght i equal 2 and keaborad key != backspace
		if (liveInputValue.length === 2  &amp;&amp; event.keyCode !== 8) {
			$(inputId).val(liveInputValue+\'-\');
			return;
		}

		//if 4nth char in value is equal \'-\' delete it 
		if (liveInputValue.length === 4 &amp;&amp; liveInputValue.charAt(3) === \'-\') {
			$(inputId).val(liveInputValue.slice(0,-1));
			return;
		}
		//limited lenght to five chars
		if (liveInputValue.length &gt; 6){
			$(inputId).val(liveInputValue.slice(0,-1));
		}

	});
}</code></pre>','publishedAt' => '2019-01-08 10:43:08','updatedAt' => '2019-01-08 10:43:08','headtitle' => 'Dodanie myślnika do kodu pocztowego','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '130','user_id' => '1','title' => 'Stan cywilny po płci','slug' => 'stan-cywilny-po-plci','content' => '<pre class="language-javascript"><code>function showMaritalStatusesBySex(sexName, selectDefault = true) {

    if (sexName === \'\') {
        $(\'#maritalStatus\').hide();
        return;
    }

    $(\'body\').find(\'#maritalStatus\').show();

    //select default value if true 
    if (selectDefault) {
        $(\'#stan_cywilny\').find(\'option\').filter(\'[value=""]\').prop(\'selected\', true);
    }

    var maritalStatuses = [];

    maritalStatuses[\'male\'] = [\'kawaler\', \'żonaty\', \'rozwiedziony\', \'wdowiec\', \'konkubent\', \'separacja\'];
    maritalStatuses[\'female\'] = [\'panna\', \'mężatka\', \'rozwiedziona\', \'wdowa\', \'konkubina\', \'separacja\'];

    $(\'#stan_cywilny\').find(\'option\').not("[value=\'\']").hide();

    $.each(maritalStatuses[sexName], function(key, value) {
        $(\'#stan_cywilny\').find(\'option\').filter(\'[value=\' + value + \']\').show();
    });
}</code></pre>','publishedAt' => '2019-01-08 14:23:11','updatedAt' => '2019-01-08 14:23:11','headtitle' => 'Stan cywilny po płci','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '131','user_id' => '1','title' => 'Przekierowanie na plik php','slug' => 'przekierowanie-na-plik-php','content' => '<pre class="language-markup"><code>RewriteRule ^info/([0-9a-z/-]*)$ ./info/info.php?slug=$1</code></pre>','publishedAt' => '2019-01-08 19:59:41','updatedAt' => '2019-01-08 19:59:41','headtitle' => 'Przekierowanie na plik php','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '132','user_id' => '1','title' => 'Dodawanie walidacji podczas budowania forma','slug' => 'dodawanie-walidacji-podczas-budowania-forma','content' => '<pre class="language-php"><code> -&gt;add(\'field_name\', \'integer\', array(
     \'label\' =&gt; \'Your label here\', 
     \'data\' =&gt; 0, // default value
     \'precision\' =&gt; 0, // disallow floats
     \'constraints\' =&gt; array(
         new Assert\\NotBlank(), 
         new Assert\\Type(\'integer\'), 
         new Assert\\Regex(array(
             \'pattern\' =&gt; \'/^[0-9]\\d*$/\',
             \'message\' =&gt; \'Please use only positive numbers.\'
             )
         ),
         new Assert\\Length(array(\'max\' =&gt; 2))
     )
 ))</code></pre>','publishedAt' => '2019-01-10 14:26:10','updatedAt' => '2019-01-10 14:26:10','headtitle' => 'Dodawanie walidacji podczas budowania forma','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '133','user_id' => '1','title' => 'Dostep do entity menagera w formie','slug' => 'dostep-do-entity-menagera-w-formie','content' => '<pre class="language-php"><code>&lt;?php

namespace App\\Utils\\Campaign;

use App\\Lib\\CrudManager\\Form\\FilterDtoType as BaseFilterDtoType;
use Symfony\\Component\\Form\\FormBuilderInterface;
use Symfony\\Component\\OptionsResolver\\OptionsResolver;
use Symfony\\Component\\Form\\Extension\\Core\\Type\\TextType;
use Symfony\\Component\\Form\\Extension\\Core\\Type\\ButtonType;
use Symfony\\Component\\Form\\Extension\\Core\\Type\\ChoiceType;
use App\\DTO\\Campaign\\CampaignFilterDTO;
use Symfony\\Component\\Form\\CallbackTransformer;
use App\\Repository\\TagProduct\\TagProductRepository;
use App\\Domain\\Tags\\ProductTagsProvider;

class FilterType extends BaseFilterDtoType{

    /**
     * translate prefix 
     */
    const TRANSLATE_PREFIX = \'campaign.filter.\';
    
    
    
    protected $productTagProvider;

    public function __construct(ProductTagsProvider $productTagProvider)
    {
        $this-&gt;productTagProvider = $productTagProvider;
    }

    
    
    

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        
        /** @var \\Doctrine\\ORM\\EntityManager $entityManager */
        $entityManager = $options[\'entity_manager\'];
          
       
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        
        //$resolver-&gt;setRequired(\'entity_manager\');
        
        $resolver-&gt;setDefaults(array(
            \'data_class\' =&gt; CampaignFilterDTO::class,
            \'csrf_protection\' =&gt; false,
            \'allow_extra_fields\' =&gt; true,
            \'entity_manager\' =&gt; true,
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return \'campaign_filter\';
    }

}
</code></pre>','publishedAt' => '2019-01-16 14:33:31','updatedAt' => '2019-01-16 14:33:31','headtitle' => 'Dostep do entity menagera w formie','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '134','user_id' => '1','title' => 'Skośny gradient','slug' => 'skosny-gradient','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
    &lt;meta charset="UTF-8"&gt;
    &lt;title&gt;Document&lt;/title&gt;
    &lt;link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet"&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;br&gt;
&lt;div class="row"&gt;
    &lt;div class="container-fluid"&gt;
        &lt;div class="bg-gradient-green"&gt;&lt;/div&gt;
        &lt;div class="bg-green"&gt;
            &lt;div class="container"&gt;
                &lt;h1&gt;Lorem ipsum dolor sit amet.&lt;/h1&gt;
                &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur eaque quos nihil sint dolores at voluptatem cumque unde illo mollitia, tempora ea ut. Aliquam cumque voluptatem sed magnam deleniti tenetur vero odit dicta rem quis? Quae sunt veritatis iusto, officia sequi beatae eum quisquam error veniam rerum quis, asperiores aspernatur ducimus quo eligendi recusandae autem ratione facere adipisci ipsam. Enim at quae architecto et ipsum temporibus dolorum nisi qui minima doloribus aspernatur nobis explicabo voluptatibus fuga, nam a, consectetur fugit possimus est! Ratione est voluptates tempora, quos ducimus quo consequuntur pariatur dolore minus eveniet. Facere magni, delectus incidunt explicabo quibusdam placeat quia necessitatibus eveniet tempore harum architecto suscipit voluptas ullam nemo in totam exercitationem! Quia cupiditate fugiat soluta, dolor qui sunt, nobis pariatur veniam saepe exercitationem, blanditiis! Aliquid incidunt at quod optio, vel! Iure sunt, sapiente sint tempore voluptatibus fugiat earum, blanditiis veritatis. Deserunt perferendis, iure aperiam ex eum nemo. Perferendis ad itaque inventore molestiae blanditiis. Sunt esse impedit illum. Minima veniam deserunt, facilis facere enim. Obcaecati quia, libero ducimus enim cupiditate mollitia, deserunt alias quas, voluptatum commodi deleniti culpa eveniet. Voluptatibus pariatur esse laboriosam, hic quod recusandae dolorum porro, placeat. Est a adipisci quisquam atque incidunt in ipsa, tempore?&lt;/p&gt;
            &lt;/div&gt;
        &lt;/div&gt;
            &lt;div class="bg-gradient-orange"&gt;&lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;
&lt;style type="text/css"&gt;
.bg-gradient-green {
    margin-top: -125px;
    height: 240px;
    background: #40b55c;
    background: -webkit-repeating-linear-gradient(277deg, transparent, transparent 49.9%, #40b55c 50.1%, #40b55c 100%);
    background: -o-repeating-linear-gradient(277deg, transparent, transparent 49.9%, #40b55c 50.1%, #40b55c 100%);
    background: repeating-linear-gradient(173deg, transparent, transparent 49.9%, #40b55c 50.1%, #40b55c 100%);
}

.bg-green {
    padding: 0 0 15px 0;
    background: #40b55c;
}
.bg-gradient-orange {
    height: 196px;
    background: -webkit-repeating-linear-gradient(94deg, transparent, transparent 49.9%, #ffcc32 50.1%, #ffcc32 100%);
    background: -o-repeating-linear-gradient(94deg, transparent, transparent 49.9%, #ffcc32 50.1%, #ffcc32 100%);
    background: repeating-linear-gradient(-4deg, transparent, transparent 49.9%, #ffcc32 50.1%, #ffcc32 100%);
}
&lt;/style&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>
<p><a title="przejdz" href="https://jsfiddle.net/mordimer/f5scw6p7/">LINK</a></p>','publishedAt' => '2019-01-19 11:56:51','updatedAt' => '2019-01-19 11:56:51','headtitle' => 'Skośny gradient','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '135','user_id' => '1','title' => 'Wykrycie przesunięcia do diva','slug' => 'wykrycie-przesuniecia-do-diva','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
  &lt;meta charset="UTF-8"&gt;
  &lt;title&gt;Document&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;div class="container"&gt;
  &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum eum consequatur vitae, a nisi atque ad autem ullam, aperiam velit at sunt laudantium tempore, fuga rem veniam dolore aut omnis fugit quam. Consequuntur numquam illo placeat officiis fugiat earum, quia expedita sapiente alias, perferendis quisquam, impedit recusandae. Tenetur, autem qui quidem quam harum incidunt, ut, labore ad facere magni minus consequatur dolores itaque saepe tempore eveniet laudantium quibusdam. Esse, at. Vel aperiam harum modi aliquid, distinctio? Corporis dicta veritatis labore incidunt sed eos distinctio aspernatur consequatur delectus, est magnam quod odio aperiam aliquam voluptates perferendis, eaque quo quisquam placeat velit repellat iure esse. Placeat alias assumenda eveniet, animi in pariatur sit, dolores facilis hic aliquam at sint. Repellat deserunt placeat velit ipsa dicta neque maxime aliquam molestias, optio itaque earum dolorem, voluptate porro soluta nemo, minus impedit cumque animi ab &lt;/p&gt;
&lt;/div&gt;

&lt;div id="startLoading"&gt;Hej&lt;/div&gt;

&lt;style type="text/css"&gt;
  .container{
    text-align: center;
    max-width: 100px;
    margin:auto;
  }
&lt;/style&gt;

&lt;script type="text/javascript"&gt;
function isScrolledIntoView(elem) {
    var docViewTop = (document.documentElement &amp;&amp; document.documentElement.scrollTop) || document.body.scrollTop;
    var docViewBottom = docViewTop + window.innerHeight;
    var elemTop = elem.offsetTop;
    var elemBottom = elemTop + elem.offsetHeight;
    return ((elemBottom &lt;= docViewBottom) &amp;&amp; (elemTop &gt;= docViewTop));
}

window.addEventListener(\'scroll\', function LoadDiv(){
    if (isScrolledIntoView(document.querySelector(\'#startLoading\'))) {
      alert(\'Hej wlasnie przewinięto do diva #startLoading\');
      //uncoment to remove event
      //this.removeEventListener(\'scroll\', LoadDiv);
    }
});

&lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>
<p><a title="przejdz" href="https://jsfiddle.net/mordimer/m8ouhqwe/" target="_blank" rel="noopener">Link</a></p>','publishedAt' => '2019-01-19 12:31:34','updatedAt' => '2019-01-19 12:31:34','headtitle' => 'Wykrycie przesunięcia do diva','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '136','user_id' => '1','title' => 'Wyszukiwanie wielu pól w relacji many_to_many','slug' => 'wyszukiwanie-wielu-pol-w-relacji-many-to-many','content' => '<pre class="language-markup"><code>SELECT c0_.id AS id_0, c0_.created AS created_1, c0_.affiliate_network AS affiliate_network_2, c0_.affiliate_account AS affiliate_account_3, c0_.description AS description_4, c0_.full_content AS full_content_5, c0_.tag_product AS tag_product_6 FROM campaign c0_ 
LEFT JOIN campaign_taggeneral c2_ ON c0_.id = c2_.campaign_id 
LEFT JOIN tag_general t1_ ON t1_.id = c2_.tag_general_id
WHERE t1_.id IN (?) 
AND c0_.created &gt;= ? AND c0_.created &lt;= ? ORDER BY c0_.created ASC

//symfony
$query-&gt;leftJoin(\'i.tagsGeneral\', \'j\')
-&gt;where(\'j.id in (:subCompanyId)\')-&gt;setParameter("subCompanyId", $filterData-&gt;getFilterTagsGeneral());</code></pre>','publishedAt' => '2019-01-24 08:33:13','updatedAt' => '2019-01-24 08:33:13','headtitle' => 'Wyszukiwanie wielu pól w relacji many_to_many','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '137','user_id' => '1','title' => 'Sortowanie tablicy po kluczy według wartości','slug' => 'sortowanie-tablicy-po-kluczy-wedlug-wartosci','content' => '<p><a title="Przejdz" href="https://stackoverflow.com/questions/1597736/how-to-sort-an-array-of-associative-arrays-by-value-of-a-given-key-in-php" target="_blank" rel="noopener">Przedz do linku</a></p>','publishedAt' => '2019-02-05 08:33:07','updatedAt' => '2019-02-05 08:33:07','headtitle' => 'Sortowanie tablicy po kluczy według wartości','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '138','user_id' => '1','title' => 'Hover menu','slug' => 'hover-menu','content' => '<p><a href="https://codepen.io/3city/pen/KJywpJ" target="_blank" rel="noopener">przejdz</a></p>','publishedAt' => '2019-02-06 18:15:53','updatedAt' => '2019-02-06 18:15:53','headtitle' => 'Hover menu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '139','user_id' => '1','title' => 'Pętla do określonej liczby z dodaniem zer','slug' => 'petla-do-okreslonej-liczby-z-dodaniem-zer','content' => '<pre class="language-php"><code>&lt;?php
$maxNumber = 99999;
$intLenght = strlen($maxNumber);

for ($number = 0; $number &lt;= $maxNumber; $number++) {
    $lenght = strlen($number);
    $repeat = str_repeat(0, $intLenght - $lenght);
    $test = $repeat . $number;
    echo substr_replace($test, \'-\', 2, 0) . \'&lt;br&gt;\';
}</code></pre>','publishedAt' => '2019-02-06 19:00:23','updatedAt' => '2019-02-06 19:00:23','headtitle' => 'Pętla do określonej liczby z dodaniem zer','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '140','user_id' => '1','title' => 'Gradient generator','slug' => 'gradient-generator','content' => '<p><a title="przejdz" href="http://bennettfeely.com/clippy/" target="_blank" rel="noopener">link</a></p>','publishedAt' => '2019-02-11 18:15:29','updatedAt' => '2019-02-11 18:15:29','headtitle' => 'Gradient generator','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '141','user_id' => '1','title' => 'Filtrowanie tablicy bez wywalania 0 w wartości','slug' => 'filtrowanie-tablicy-bez-wywalania-0-w-wartosci','content' => '<pre class="language-php"><code> $parameters = array_filter($parameters,function($value) {
      return ($value !== null &amp;&amp; $value !== false &amp;&amp; $value !== \'\'); 
 });</code></pre>','publishedAt' => '2019-02-12 13:52:23','updatedAt' => '2019-02-12 13:52:23','headtitle' => 'Filtrowanie tablicy bez wywalania 0 w wartości','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '142','user_id' => '1','title' => 'Domyślna aktualna data dla pola datetime w symfony','slug' => 'domyslna-aktualna-data-dla-pola-datetime-w-symfony','content' => '<p>//yaml</p>
<pre class="language-php"><code> two_step_last_auth:
            type: datetime
            nullable: true  
            options:
                default: CURRENT_TIMESTAMP</code></pre>
<p>//adnotacje</p>
<div class="post-text">
<pre class="lang-php prettyprint prettyprinted"><code><span class="lit">@ORM</span><span class="pln">\\Column</span><span class="pun">(</span><span class="pln">name</span><span class="pun">=</span><span class="str">"creation_date"</span><span class="pun">,</span><span class="pln"> type</span><span class="pun">=</span><span class="str">"datetime"</span><span class="pun">,</span><span class="pln"> options</span><span class="pun">={</span><span class="str">"default"</span><span class="pun">=</span><span class="str">"CURRENT_TIMESTAMP"</span><span class="pun">})</span></code></pre>
</div>
<div class="grid mb0 fw-wrap ai-start jc-end gs8 gsy"><time datetime="2016-10-18T11:53:03"></time>
<div class="grid--cell mr16">&nbsp;</div>
</div>','publishedAt' => '2019-02-14 09:57:00','updatedAt' => '2019-02-14 09:57:00','headtitle' => 'Domyślna aktualna data dla pola datetime w symfony 2','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '143','user_id' => '1','title' => 'Crud symfony','slug' => 'crud-symfony','content' => '<pre class="language-php"><code>&lt;?php

namespace DM\\KlienciBundle\\Controller;

use Symfony\\Bundle\\FrameworkBundle\\Controller\\Controller;
use Symfony\\Component\\HttpFoundation\\Request;
use Symfony\\Component\\HttpFoundation\\Response;
use Symfony\\Component\\HttpFoundation\\JsonResponse;
use DM\\KlienciBundle\\Entity\\ClientSource;
use DM\\KlienciBundle\\Form\\ClientSource\\ClientSourceType;
use DM\\KlienciBundle\\Form\\ClientSource\\ClientSourceFilterType;

class ClientSourceController extends Controller {

    const TEMPLATE_PREFIX = \'DMKlienciBundle:ClientSource:\';
    const ACTIVE_BUNDLE = \'klienci\';
    const PAGINATION_PER_PAGE = 100;

    /**
     * Client Source list
     * 
     * @param Request $request
     * 
     * @return Response
     */
    public function indexAction(Request $request) {
        $em = $this-&gt;getDoctrine()-&gt;getManager();

        $paginationLimitPerPage = self::PAGINATION_PER_PAGE;
        // form data
        $data = [];
        $form = $this-&gt;createForm(ClientSourceFilterType::class, $data);
        $resetPaginationPage = false;

        if ($request-&gt;isMethod(\'POST\')) {
            $form-&gt;bind($request);
            $data = $form-&gt;getData();

            if ($form-&gt;get(\'submit\')-&gt;isClicked()) {
                $resetPaginationPage = true;
            }
        }

        $paginationPage = (!$resetPaginationPage &amp;&amp; isset($data[\'paginationPage\']) ? $data[\'paginationPage\'] : 1);
        $itemsCount = $em-&gt;getRepository(ClientSource::class)-&gt;findByMultiParameters(true, $data, []);
        $items = $em-&gt;getRepository(ClientSource::class)-&gt;findByMultiParameters(false, $data, [\'t.id\' =&gt; \'DESC\'], (($paginationPage - 1) * $paginationLimitPerPage), $paginationLimitPerPage);

        // return view
        return $this-&gt;render(self::TEMPLATE_PREFIX . \'list.html.twig\', array(
                    \'active_bundle\' =&gt; self::ACTIVE_BUNDLE,
                    \'form\' =&gt; $form-&gt;createView(),
                    \'items\' =&gt; $items,
                    \'itemsCount\' =&gt; $itemsCount,
                    \'paginationPage\' =&gt; $paginationPage,
                    \'paginationCount\' =&gt; ceil($itemsCount / $paginationLimitPerPage),
        ));
    }

    /**
     * add new client source
     * 
     * @param Request $request
     * 
     * @return Response
     */
    public function addAction(Request $request) {
        return $this-&gt;addeditAction($request, \'add\');
    }

    /**
     * edit client source
     * 
     * @param Request $request
     * @param IframeLink|null $item
     * 
     * @return Response
     */
    public function editAction(Request $request, ClientSource $item) {
        return $this-&gt;addeditAction($request, \'edit\', $item);
    }

    /**
     * crud add/edit
     * 
     * @param Request $request
     * @param string $mode
     * @param IframeLink $clientSource
     * 
     * @return Resposne|Redirect
     */
    public function addeditAction(Request $request, $mode, ClientSource $clientSource = null) {
        $em = $this-&gt;getDoctrine()-&gt;getManager();
        //edit or add 
        if ($mode == \'add\') {
            $clientSource = new ClientSource();
        } elseif ($mode == \'edit\' &amp;&amp; !$clientSource) {
            $this-&gt;addFlash(\'error\', \'Wystąpił błąd, nie można znaleźć wybranej pozycji w systemie. Spr&oacute;buj ponownie.\');
            return $this-&gt;redirectToRoute(\'dm_client_source_homepage\');
        }

        $form = $this-&gt;createForm(ClientSourceType::class, $clientSource);
        $form-&gt;handleRequest($request);

        if ($form-&gt;isSubmitted() &amp;&amp; $form-&gt;isValid()) {
            try {
                $em-&gt;persist($clientSource);
                $em-&gt;flush();

                if ($mode == \'add\') {
                    $this-&gt;addFlash(\'success\', \'Klient został pomyślnie dodany.\');
                } elseif ($mode == \'edit\') {
                    $this-&gt;addFlash(\'success\', \'Klient został pomyślnie zapisany.\');
                }

                return $this-&gt;redirect($this-&gt;generateUrl(\'dm_client_source_homepage\'));
            } catch (\\Exception $ex) {
                $this-&gt;addFlash(\'error\', \'Wystąpił błąd (ex: \' . $ex-&gt;getMessage() . \')\');
            }
        }

        return $this-&gt;render(self::TEMPLATE_PREFIX . \'addEdit.html.twig\', [
                    \'active_bundle\' =&gt; self::ACTIVE_BUNDLE,
                    \'form\' =&gt; $form-&gt;createView(),
                    \'mode\' =&gt; $mode,
        ]);
    }

    /**
     * remove client source
     * 
     * @param Request $request
     * @param ClientSource $item
     * 
     * @return redirect
     */
    public function removeAction(Request $request, ClientSource $item) {
        $em = $this-&gt;getDoctrine()-&gt;getManager();
        $session = $this-&gt;getRequest()-&gt;getSession();

        if (!$item) {
            $session-&gt;getFlashBag()-&gt;add(\'error\', \'Nie znaleziono pozycji w systemie.\');
            return $this-&gt;redirect($this-&gt;generateUrl(\'dm_client_source_homepage\'));
        }

        $em-&gt;remove($item);
        $em-&gt;flush();
        $session-&gt;getFlashBag()-&gt;add(\'success\', \'Klient został pomyślnie usunięty.\');

        return $this-&gt;redirect($this-&gt;generateUrl(\'dm_client_source_homepage\'));
    }

}
</code></pre>
<p>//repository</p>
<pre class="language-php"><code>&lt;?php

namespace DM\\KlienciBundle\\Repository;

use DM\\KlienciBundle\\Entity\\ClientSource;

class ClientSourceRepository extends \\Doctrine\\ORM\\EntityRepository {

    /**
     * find items by mylti parameters with pagination
     *
     * @param boolean $onlyCount - true if return only count
     * @param array $filter - data array with filter parameters
     * @param array $orderBy - order param (key is field, value is direction, defulat id DESC)
     * @param integer $offset - pagination offset
     * @param integer $limit - pagination limit
     * @param string $groupBy
     * 
     * @return array
     */
    public function findByMultiParameters($onlyCount, array $filter = [], array $orderBy = [\'t.id\' =&gt; \'DESC\'], $offset = null, $limit = null, $groupBy = null) {
        $select = ($onlyCount ? \'COUNT(t.id)\' . ($groupBy ? \', \' . $groupBy : \'\') : ($groupBy ? $groupBy : \'t\'));
        $query = $this-&gt;getEntityManager()-&gt;createQueryBuilder()
                -&gt;from(ClientSource::class, \'t\')
                -&gt;select($select)
        ;
        
        // name
        if (isset($filter[\'name\']) &amp;&amp; $filter[\'name\'] != null) {
            $query-&gt;andWhere(\'t.name LIKE :name\')-&gt;setParameter(\'name\', \'%\'.$filter[\'name\'].\'%\');
        }

        if ($groupBy) {
            $query = $query-&gt;groupBy($groupBy);
        }

        if (!empty($orderBy)) {
            foreach ($orderBy as $orderField =&gt; $orderDirection) {
                $query-&gt;addOrderBy($orderField, $orderDirection);
            }
        }

        if (!$onlyCount) {
            $query = $query-&gt;setFirstResult($offset)
                    -&gt;setMaxResults($limit);
        }

        try {
            if ($onlyCount) {
                if ($groupBy) {
                    return $query-&gt;getQuery()-&gt;getResult();
                } else {
                    return $query-&gt;getQuery()-&gt;getSingleScalarResult();
                }
            } else {
                return $query-&gt;getQuery()-&gt;getResult();
            }
        } catch (\\Doctrine\\ORM\\NoResultException $e) {
            if ($onlyCount) {
                return 0;
            } else {
                return array();
            }
        }
    }

}
</code></pre>','publishedAt' => '2019-02-20 13:33:15','updatedAt' => '2019-02-20 13:33:15','headtitle' => 'Crud symfony','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '144','user_id' => '1','title' => 'Crud widoki','slug' => 'crud-widoki','content' => '<pre class="language-php"><code>{% extends \'DMDashboardBundle::layout.html.twig\' %}

{% set title = "Źr&oacute;dło klient&oacute;w - lista" %}
{% set icon = "menu_sieciafiliacyjne_icon.png" %}
{% block title %}{{ title }} {{ parent() }}{% endblock %}
{% block code_header %}
{% endblock %}
{% block body %}
    &lt;header&gt;
        &lt;div class="icon"&gt;&lt;div class="img"&gt;&lt;img src="{{ asset(\'bundles/dmdashboard/images/\' ~ icon) }}" alt="{{ title }}"&gt;&lt;/div&gt;&lt;/div&gt;
        &lt;h1&gt;{{ title }}&lt;/h1&gt;
        &lt;div class="btn-group"&gt;
            &lt;a href="{{ path(\'dm_client_source_add\') }}" class="btn btn-primary"&gt;Dodaj&lt;/a&gt;
        &lt;/div&gt;
    &lt;/header&gt;
    {% include \'DMDashboardBundle::modules/alerts.html.twig\' %}
    &lt;div class="container-fluid"&gt;
        &lt;div class="row"&gt;
            &lt;div class="col-md-3"&gt;
                &lt;div class="row"&gt;
                    {{form_start(form)}}
                        &lt;div class="col-md-6"&gt;
                            {{ form_label(form.name) }}
                            {{ form_widget(form.name) }}
                        &lt;/div&gt;
                        &lt;div class="col-md-6"&gt;
                            {{ form_label(form.submit) }}
                            {{ form_widget(form.submit) }}
                        &lt;/div&gt;
                    {{form_end(form)}}
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
    {% if items|length == 0 %}
        &lt;big&gt;&lt;i&gt;Brak pozycji&lt;/i&gt;&lt;/big&gt;
    {% else %}
        Wynik&oacute;w: &lt;strong&gt;{{ itemsCount }}&lt;/strong&gt;
        {% if items|length == 0 %}
            &lt;big&gt;&lt;i&gt;Brak pozycji&lt;/i&gt;&lt;/big&gt;
        {% else %}
            &lt;table class="table table-striped table-list" style="width: 98%;"&gt;
                &lt;theader&gt;
                    &lt;tr&gt;
                        &lt;th style="width: 5%;"&gt;ID&lt;/th&gt;
                        &lt;th style="width: 50%;"&gt;Nazwa&lt;/th&gt;
                        &lt;th style="width: 5%;"&gt;Akcje&lt;/th&gt;
                    &lt;/tr&gt;
                &lt;/theader&gt;
                &lt;tbody&gt;
                    {% for item in items%}
                        &lt;tr&gt;
                            &lt;td class=""&gt;
                                {{ item.id }}            
                            &lt;/td&gt;
                            &lt;td class=""&gt;
                                {{ item.name }}            
                            &lt;/td&gt;
                            &lt;td&gt;
                                &lt;a href="{{ path(\'dm_client_source_edit\',{\'item\': item.id}) }}" class="btn btn-primary"&gt;Edytuj&lt;/a&gt;
                                &lt;a href="{{ path(\'dm_client_source_remove\',{\'item\':item.id}) }}" class="btn btn-danger" onclick="return confirm(\'Napewno usunąc?\');"&gt;Usuń&lt;/a&gt;
                            &lt;/td&gt;
                        &lt;/tr&gt;
                    {% endfor %}

                &lt;/tbody&gt;
            &lt;/table&gt; 
        {% endif %}

        {% if paginationCount &gt; 1 %}
            &lt;div style="width: 100%; text-align:center; margin-bottom: 20px;"&gt;
                {% if paginationPage != 1 %}
                    &lt;a href="javascript:void(0)" onclick="javascript:changePaginationStrona({{ paginationPage - 1}})" class="elem back"&gt;{{ \'&lt; poprzednia\' }}&lt;/a&gt;
                {% endif %}

                {% for i in 1..paginationCount %}
                    &lt;a href="javascript:void(0)" onclick="javascript:changePaginationStrona({{ i }})" class="elem" {% if i == paginationPage %}style="color:#000; font-weight:bold; font-size: 16px;"{% endif %}&gt;{{ i }}&lt;/a&gt;
                {% endfor %}

                {% if paginationPage != paginationCount %}
                    &lt;a href="javascript:void(0)" onclick="javascript:changePaginationStrona({{ paginationPage + 1}})" class="elem next"&gt;{{ \'nastepna &gt;\' }}&lt;/a&gt;
                {% endif %}
            &lt;/div&gt;
        {% endif %}

        &lt;script type="text/javascript"&gt;
            function changePaginationStrona(newStr) {
                $(\'input#client_source_filter_paginationPage\').val(newStr);
                $("form[name=\'client_source_filter\']").submit();
            }
        &lt;/script&gt;
    {% endif %}

{% endblock body %}</code></pre>
<p>//add edit</p>
<pre class="language-php"><code>{% extends \'DMDashboardBundle::layout.html.twig\' %}

{% if mode == \'edit\' %}
    {% set title = "Źr&oacute;dło klient&oacute;w edycja" %}
{% else %}
    {% set title = "Źr&oacute;dło klient&oacute;w dodaj nową pozycję" %}
{% endif %}

{% set icon = "menu_sieciafiliacyjne_icon.png" %}
{% block title %}{{ title }} {{ parent() }}{% endblock %}

{% block code_header %}
{% endblock %}

{% block body %}

    &lt;header&gt;
        &lt;div class="icon"&gt;&lt;div class="img"&gt;&lt;img src="{{ asset(\'bundles/dmdashboard/images/\' ~ icon) }}" alt="{{ title }}"&gt;&lt;/div&gt;&lt;/div&gt;
        &lt;h1&gt;{{ title }}&lt;/h1&gt;
        &lt;div class="btn-group"&gt;
            &lt;a href="{{ path(\'dm_client_source_add\') }}" class="btn btn-primary"&gt;Dodaj&lt;/a&gt;
        &lt;/div&gt;
    &lt;/header&gt;

    {% include \'DMDashboardBundle::modules/alerts.html.twig\' %}

    &lt;div class="container-fluid"&gt;
        &lt;div class="row"&gt;
            &lt;div class="col-md-6 col-md-offset-2"&gt;
                &lt;div class="panel-group"&gt;
                    &lt;div class="panel panel-default"&gt;
                        &lt;div class="panel-heading"&gt; &lt;h4&gt;{% if mode == \'add\' %}Dodaj nowy{% endif %}{% if mode == \'edit\' %}Edycja{% endif %}&lt;/h4&gt;&lt;/div&gt;
                        &lt;div class="panel-body body-form"&gt;
                            {{form_start(form)}}
                            {{form_end(form)}}
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
{% endblock body %}</code></pre>
<p>//nowy</p>
<pre class="language-markup"><code>{% extends \'DMDashboardBundle::layout.html.twig\' %}
{% block title %} {{ parent() }} - Automatyczne raporty walidacyjne - konfiguracja {% endblock %}
{% block code_header %}
    &lt;style&gt;
        .checkbox-container &gt; div &gt; label{
            width: 35%
        } 
        
        .checkbox-container .has-switch {
            width: 10%;
            margin-right: 1%;
        }
    &lt;/style&gt;
{% endblock %}
{% block body %}
    &lt;header&gt;
        &lt;div class="icon"&gt;&lt;div class="img"&gt;&lt;img src="{{ asset(\'bundles/dmdashboard/images/menu_kategorie_icon.png\') }}" alt="Group"&gt;&lt;/div&gt;&lt;/div&gt;
        &lt;h1&gt;Automatyczne raporty walidacyjne - dodawanie/edycja&lt;/h1&gt;
        &lt;a href="{{ path(\'dm_reportvalidation_auto_report_configuration\')}}" class="btn btn-default btn-add"&gt;&amp;laquo; Wstecz&lt;/a&gt;
    &lt;/header&gt;
    {% include \'DMDashboardBundle::modules/alerts.html.twig\' %}
    
    {{ form_start(form) }}
    {{ form_errors(form) }}
    &lt;div class="left"&gt;
        &lt;div class="panel-group" id="ustawienia"&gt;
            &lt;div class="panel panel-default"&gt;
                &lt;div class="panel-heading"&gt;
                    &lt;h4 class="panel-title"&gt;
                        &lt;a data-toggle="collapse" data-parent="#accordion" href="#Parametry"&gt;
                            Parametry
                        &lt;/a&gt;
                    &lt;/h4&gt;
                &lt;/div&gt;
                &lt;div id="Parametry" class="panel-collapse collapse in"&gt;
                    &lt;div class="panel-body"&gt;
                        &lt;div class="form_row"&gt;
                            {{ form_label(form.mailTitle) }}
                            {{ form_widget(form.mailTitle) }}
                        &lt;/div&gt;
                        &lt;div class="form_row"&gt;
                            {{ form_label(form.mailsToSend) }}
                            {{ form_widget(form.mailsToSend) }}
                        &lt;/div&gt;
                        &lt;div class="form_row"&gt;
                            {{ form_label(form.mailContent) }}
                            {{ form_widget(form.mailContent) }}
                        &lt;/div&gt;
                        &lt;div class="form_row"&gt;
                            {{ form_label(form.report_validation_report) }}
                            {{ form_widget(form.report_validation_report) }}
                        &lt;/div&gt;
                        &lt;div class="row"&gt;
                            &lt;div class="col-sm-6 col-xs-12"&gt;
                                &lt;div class="form_row"&gt;
                                    {{ form_label(form.dateRange) }}
                                    {{ form_widget(form.dateRange) }}
                                &lt;/div&gt;
                            &lt;/div&gt;
                            &lt;div class="col-sm-6 col-xs-12"&gt;
                                &lt;div class="form_row"&gt;
                                    {{ form_label(form.sendDay) }}
                                    {{ form_widget(form.sendDay) }}
                                &lt;/div&gt;
                            &lt;/div&gt;
                        &lt;/div&gt;
                        &lt;div class="form_row checkbox-container"&gt;
                            {{ form_label(form.celsToSend) }}
                            {{ form_widget(form.celsToSend) }}
                            &lt;br&gt;
                            &lt;a class="btn btn-primary" id="cell" &gt;Zaznacz wszystkie pola do wyświetlania&lt;/a&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
    &lt;div class="right"&gt;
        &lt;div class="panel-group" id="ustawienia"&gt;
            &lt;div class="panel panel-default"&gt;
                &lt;div class="panel-heading"&gt;
                    &lt;h4 class="panel-title"&gt;
                        &lt;a data-toggle="collapse" data-parent="#accordion" href="#Akcje"&gt;
                            Akcje
                        &lt;/a&gt;
                    &lt;/h4&gt;
                &lt;/div&gt;
                &lt;div id="Akcje" class="panel-collapse collapse in"&gt;
                    &lt;div class="panel-body"&gt;

                        &lt;div class="btn-group" style="float: right; width: 100%;"&gt;
                            &lt;a class="btn btn-default btn-lg" style="width:40%;" href = "{{ path(\'dm_reportvalidation_auto_report_configuration\') }}" &gt;Anuluj&lt;/a&gt;
                            {{ form_widget(form.submit,{\'attr\':{\'style\':\'width:60%\'}}) }} 
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
                        
    {{ form_end(form) }}
    &lt;script type="text/javascript"&gt;
        $(\'#cell\').click(function () {
            $(\'#auto_report_validation_configuration_celsToSend\').find(\'input[type=checkbox]\').trigger(\'click\');
        });
    &lt;/script&gt;
{% endblock body %}</code></pre>','publishedAt' => '2019-02-20 13:37:30','updatedAt' => '2019-02-20 13:37:30','headtitle' => 'Crud widoki','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '145','user_id' => '1','title' => 'Usunięcie wszystkich znaków oprócz liter, liczb i spacji','slug' => 'usuniecie-wszystkich-znakow-oprocz-liter-liczb-i-spacji','content' => '<pre class="language-php"><code>return preg_replace(\'/[^\\d a-zA-ZĄąĘę&Oacute;&oacute;ĆćŁłŃńŚśŹźŻż]/\', \'\', $value);</code></pre>
<p>\\d - to dowoalna liczba naturalna&nbsp;</p>','publishedAt' => '2019-02-22 07:55:52','updatedAt' => '2019-02-22 07:55:52','headtitle' => 'Usunięcie wszystkich znaków oprócz liter, liczb i spacji','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '146','user_id' => '1','title' => 'Sub zapytanie symfony','slug' => 'sub-zapytanie-symfony','content' => '<pre class="language-php"><code> /**
    * get oldest client category by date import having clientsource
    * 
    * @param int $clientId
    * @param array $categories (values example -&gt;  [ 159 =&gt; 182 , 160 =&gt; "182_167"] )
    * @param \\DateTime $datetimeBegin
    * @param \\DateTime $datetimeEnd
    * 
    * @return array|null
    */
   public function getOldestClientCategoryByDateImportDateOfEntryHavingClientSource(int $clientId, array $categories, \\DateTime $datetimeBegin, \\DateTime $datetimeEnd) {
        $query = $this-&gt;createQueryBuilder(\'kk\')
                -&gt;where(\'kk.client = :client\')-&gt;setParameter(\'client\', $clientId)
                -&gt;andWhere(\'kk.client_source IS not NULL\');

        $categoriesWhere = \'\';

        if (!empty($categories)) {
            foreach ($categories as $category) {
                if (is_integer($category)) {
                    $categoriesWhere .= ($categoriesWhere != \'\' ? \' or \' : \'\') . \'( kk.kategorie_nadrzedna = \\\'\' . $category . \'\\\') \';
                } else {
                    $categoryPart = explode(\'_\', $category);
                    if (count($categoryPart) == 2) {
                        $categoriesWhere .= ($categoriesWhere != \'\' ? \' or \' : \'\') . \'( kk.kategorie_nadrzedna = \\\'\' . $categoryPart[0] . \'\\\' and kk.kategorie = \\\'\' . $categoryPart[1] . \'\\\') \';
                    }
                }
            }

            if ($categoriesWhere != \'\') {
                $query-&gt;andWhere($categoriesWhere);
            }
        }
        
        $query-&gt;andWhere(\'kk.data_wpisania &gt;= :DatetimeBegin\')-&gt;setParameter(\'DatetimeBegin\', $datetimeBegin)
                -&gt;andWhere(\'kk.data_wpisania &lt;= :DatetimeEnd\')-&gt;setParameter(\'DatetimeEnd\', $datetimeEnd);

        $query-&gt;orderBy(\'kk.data_importu\', \'asc\')-&gt;setMaxResults(1);

        try {
            return $query-&gt;getQuery()-&gt;getOneOrNullResult();
        } catch (\\Doctrine\\ORM\\NoResultException $ex) {
            return null;
        }
    }
</code></pre>','publishedAt' => '2019-03-01 13:27:51','updatedAt' => '2019-03-01 13:27:51','headtitle' => 'Sub zapytanie symfony','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '147','user_id' => '1','title' => 'Pobieranie tekstu po wystąpieniu ostatniego znaku','slug' => 'pobieranie-tekstu-po-wystapieniu-ostatniego-znaku','content' => '<pre class="language-php"><code>//get id from string example - bestloan.pl-12 -&gt; 12
$dataTypeId = substr(strrchr(rtrim($searchIdValue, \'-\'), \'-\'), 1);</code></pre>','publishedAt' => '2019-03-25 10:44:13','updatedAt' => '2019-12-14 12:25:45','headtitle' => 'Pobieranie tekstu po wystąpieniu ostatniego znaku','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '148','user_id' => '1','title' => 'Pesel parser','slug' => 'pesel-parser','content' => '<pre class="language-php"><code>&lt;?php

class PeselParser
{
    /**
     * @var string 
     */
    private $pesel;

    /**
     * @param string $pesel
     */
    public function __construct(string $pesel)
    {
        $this-&gt;pesel = $pesel;
    }
    /**
     * get client age from pesel
     *
     * @return integer|null
     */
    public function getAge(): ?int
    {
        if ($this-&gt;extractDate() != null) {
            $today = new \\DateTime;
            $age = $today-&gt;diff($this-&gt;extractDate())-&gt;y;
            return $age;
        } else {
            return null;
        }
    }

    /**
     *  get date from pesel
     *
     * @return \\DateTime|null
     */
    public function extractDate(): ?\\DateTime
    {
        try {
            list($year, $month, $day) = sscanf($this-&gt;pesel, \'%02s%02s%02s\');

            switch (substr($month, 0, 1)) {
                case 2:
                case 3:
                    $month -= 20;
                    $year += 2000;
                    break;
                case 4:
                case 5:
                    $month -= 40;
                    $year += 2100;
                case 6:
                case 7:
                    $month -= 60;
                    $year += 2200;
                    break;
                case 8:
                case 9:
                    $month -= 80;
                    $year += 1800;
                    break;
                default:
                    $year += 1900;
                    break;
            }

            return checkdate($month, $day, $year) ? new \\DateTime($year . "-" . $month) : null;
        } catch (\\Exception $ex) {
            return null;
        }
    }

    /**
     * check valid pesel 
     * 
     * return bool
     */
    public function checkValid(): bool
    {
        //if is too short or too long
        if (mb_strlen($this-&gt;pesel, \'UTF-8\') &lt;= 10) {
            return false;
        }
        if (mb_strlen($this-&gt;pesel, \'UTF-8\') &gt; 11) {
            return false;
        }

        //check that string is only numbers
        if (!ctype_digit($this-&gt;pesel)) {
            return false;
        }

        try {
            $w = [1, 3, 7, 9];
            $wk = 0;
            for ($i = 0; $i &lt;= 9; $i++) {
                $wk = ($wk + $this-&gt;pesel[$i] * $w[$i % 4]) % 10;
            }
            $k = (10 - $wk) % 10;
            if (!($this-&gt;pesel[10] == $k)) {
                return false;
            }

            //if date from pesel is empty pesel is incorrect
            if ($this-&gt;extractDate() === null) {
                return false;
            }
        } catch (\\Exception $ex) {
            return false;
        }

        return true;
    }
}​</code></pre>
<pre class="language-php"><code>&lt;?php

class PeselParser {

    /**
     * @var type 
     */
    private $pesel;

    /**
     * @param string $pesel
     */
    public function __construct($pesel) {
        $this-&gt;pesel = $pesel;
    }

    /**
     * get client age from pesel
     */
    public function getAge() {
        if ($this-&gt;extractDate() != null) {
            $today = new \\DateTime;
            $age = $today-&gt;diff($this-&gt;extractDate())-&gt;y;
            return $age;
        } else {
            return null;
        }
    }

    /**
     * get date from pesel
     * 
     * @return DateTime|null
     */
    public function extractDate() {
        list($year, $month, $day) = sscanf($this-&gt;pesel, \'%02s%02s%02s\');

        switch (substr($month, 0, 1)) {
            case 2:
            case 3:
                $month -= 20;
                $year += 2000;
                break;
            case 4:
            case 5:
                $month -= 40;
                $year += 2100;
            case 6:
            case 7:
                $month -= 60;
                $year += 2200;
                break;
            case 8:
            case 9:
                $month -= 80;
                $year += 1800;
                break;
            default:
                $year += 1900;
                break;
        }

        return checkdate($month, $day, $year) ? new \\DateTime($year . "-" . $month) : null;
    }

    /**
     * check valid pesel 
     * 
     * return bool
     */
    public function checkValid() {
        //if is too short or too long
        if (strlen($this-&gt;pesel) &lt;= 10) {
            return false;
        }
        if (strlen($this-&gt;pesel) &gt; 11) {
            return false;
        }

        //check that string is only numbers
        if (!ctype_digit($this-&gt;pesel)) {
            return false;
        }

        try {
            $w = array(1, 3, 7, 9);
            $wk = 0;
            for ($i = 0; $i &lt;= 9; $i++) {
                $wk = ($wk + $this-&gt;pesel[$i] * $w[$i % 4]) % 10;
            }
            $k = (10 - $wk) % 10;
            if (!($this-&gt;pesel[10] == $k)) {
                return false;
            }
        } catch (\\Exception $ex) {
            return false;
        }
        //if date from pesel is empty pesel is incorrect
        if ($this-&gt;extractDate() === null) {
            return false;
        }

        return true;
    }

}
</code></pre>','publishedAt' => '2019-03-28 10:25:41','updatedAt' => '2020-04-08 14:51:19','headtitle' => 'Pesel parser','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '149','user_id' => '1','title' => 'Router początek','slug' => 'router-poczatek','content' => '<pre class="language-php"><code>&lt;?php

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

print_r(parse_url($actual_link));
echo \'&lt;br&gt;\';
echo parse_url($actual_link, PHP_URL_PATH);

?&gt;</code></pre>
<p>/htacces</p>
<pre class="language-php"><code>Options +FollowSymLinks
RewriteEngine On
RewriteRule ^(.*)$ index.php [NC,L]</code></pre>','publishedAt' => '2019-03-29 15:01:36','updatedAt' => '2019-03-29 15:01:36','headtitle' => 'Router początek','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '150','user_id' => '1','title' => 'Usuwanie symfony','slug' => 'usuwanie-symfony','content' => '<pre class="language-php"><code>$query = $this-&gt;getEntityManager()-&gt;createQueryBuilder();
            $query-&gt;delete(Ip::class, \'ip\')
                    -&gt;where(\'ip.date BETWEEN :startDate AND :endDate\')
                    -&gt;setParameters([
                        \'startDate\' =&gt; $dateStart,
                        \'endDate\' =&gt; $dateEnd,
                    ])
                    -&gt;andWhere(\'ip.wpisywacz_id = :wpisywacz_id\')-&gt;setParameter(\'wpisywacz_id\', $userName)
                    -&gt;andWhere(\'ip.category_id = :category_id\')-&gt;setParameter(\'category_id\', $categoryId)
                    -&gt;andWhere(\'ip.ip = :ip ORDER BY ip.id  \')-&gt;setParameter(\'ip\', $ip)
                    //-&gt;&gt;orderBy(\'ip.id\', \'desc\')
               
                    //-&gt;addOrderBy(\'ip.id\',\'DESC\')
                    //-&gt;addOrderBy(\'ip.id\',\'desc\')
                    -&gt;setMaxResults(1);
            ;
            dump($query-&gt;getQuery()-&gt;getSQL());
            
            die;
            
            $query-&gt;getQuery()-&gt;execute();</code></pre>','publishedAt' => '2019-04-03 06:30:46','updatedAt' => '2019-04-03 06:30:46','headtitle' => 'Usuwanie symfony','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '151','user_id' => '1','title' => 'Komenda curl w windowsie','slug' => 'komenda-curl-w-windowsie','content' => '<pre class="language-php"><code>curl -XPOST \'https://google.pl\'
</code></pre>','publishedAt' => '2019-04-12 10:01:59','updatedAt' => '2019-04-12 10:01:59','headtitle' => 'Komenda curl w windowsie','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '152','user_id' => '1','title' => 'Sprawdzenie wspólnych kluczy tablicy','slug' => 'sprawdzenie-wspolnych-kluczy-tablicy','content' => '<pre class="language-php"><code>$config[\'parameters\'] = [\'test1 =&gt;123,\'test2\'=333];

$parameters = [\'test1 =&gt;123];


$arrayIntersect = array_intersect_assoc($config[\'parameters\'], $parameters);

ksort($arrayIntersect);
ksort($parameters);

if ($arrayIntersect == $parameters) {
   return $config[\'returnValue\'];
}</code></pre>','publishedAt' => '2019-04-12 12:25:02','updatedAt' => '2019-04-12 12:25:02','headtitle' => 'Sprawdzenie wspólnych kluczy tablicy','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '153','user_id' => '1','title' => 'Obcięcie stringa do max ilości znaków','slug' => 'obciecie-stringa-do-max-ilosci-znakow','content' => '<pre class="language-php"><code>mb_substr($var,0,142, "utf-8");</code></pre>','publishedAt' => '2019-04-15 06:46:00','updatedAt' => '2019-12-14 12:25:39','headtitle' => 'Obcięcie stringa do max ilości znaków','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '154','user_id' => '1','title' => 'Mielenie każdej wartości w tablicy przy użyciu funkcji (array_map)','slug' => 'mielenie-kazdej-wartosci-w-tablicy-przy-uzyciu-funkcji-array-map','content' => '<pre class="language-markup"><code>&lt;?php
function myfunction($num)
{
  return($num*$num);
}

$a=array(1,2,3,4,5);
print_r(array_map("myfunction",$a));

// Array
// (
//     [0] =&gt; 1
//     [1] =&gt; 4
//     [2] =&gt; 9
//     [3] =&gt; 16
//     [4] =&gt; 25
// )
?&gt;</code></pre>','publishedAt' => '2019-04-16 07:47:56','updatedAt' => '2019-04-16 07:47:56','headtitle' => 'Mielenie każdej wartości w tablicy przy użyciu funkcji (array_map)','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '155','user_id' => '1','title' => 'Quake woldcam cfg','slug' => 'quake-woldcam-cfg','content' => '<p>bind F5 "cg_draw2D 1;cg_drawStatus 1"<br />bind F6 "cg_draw2D 1;cg_drawStatus 0"<br />bind F7 "cg_draw2D 0;cg_drawStatus 1"<br />bind F8 "cg_draw2D 0;cg_drawStatus 0"</p>
<p>bind UPARROW "fastforward 60"<br />bind DOWNARROW "rewind 60"<br />bind LEFTARROW "rewind 5"<br />bind RIGHTARROW "fastforward 5" <br />//pokazywanie damagu<br />cg_damagePlum ""</p>
<p>///////////////////////<br />//modele przeciwnik&oacute;w//<br />///////////////////////<br />seta cg_forceEnemyModel "keel/bright"<br />seta cg_forceModel "0"<br />seta cg_gibs "0" <br />seta cg_deadBodyDarken "1"</p>
<p><br />//spawarka<br />seta cg_altligthing "2"<br />seta cg_altLightning "2"<br />seta cg_lightningImpactCap "192"<br />seta cg_impactSparksVelocity "128"<br />seta cg_impactSparksLifetime "255"<br />seta cg_impactMarkTime "10000"</p>
<p><br />////////////////<br />////celwonik////<br />///////////////</p>
<p>seta cg_crosshairHitStyle "2"<br />seta cg_drawcrosshair "2"<br />//seta cg_crosshairColor 4<br />seta cg_crosshairSize 25</p>
<p>////////////<br />//grafika///<br />////////////<br />seta r_fullscreen "1"<br />seta r_mode "-2"<br />seta cg_fov "120"<br />seta cg_railTrailTime "1500"<br />seta cg_lagometer "0"<br />seta cg_drawFPS "0"<br />seta cg_weaponBar "0"<br />seta cl_consoleAsChat "0"<br />seta cg_teamChatTime "-1"<br />seta cg_teamChatHeight "0"<br />seta cg_chatBeep "0"<br />seta cg_drawTeamOverlay "0"</p>
<p>//seta mme_blurframes 20<br />//seta cl_aviframerate 200<br />bind v "+acc"</p>
<p>seta cg_drawCrosshair "2"<br />seta cg_drawCrosshairNames "1"<br />seta cg_chatTime "5000"<br />seta cg_crosshairHitStyle "2"<br />seta cg_crosshairHitColor "0xff0000"</p>
<p>bind TAB "+scores"<br />bind ESCAPE "togglemenu"<br />bind SPACE "+moveup"<br />bind 0 "weapon 14"<br />bind 1 "weapon 1"<br />bind 2 "weapon 2"<br />bind 3 "weapon 3"<br />bind 4 "weapon 4"<br />bind 5 "weapon 5"<br />bind 6 "weapon 6"<br />bind 7 "weapon 7"<br />bind 8 "weapon 8"<br />bind 9 "weapon 9"<br />bind [ "follow 0"<br />bind ] "follow killer"<br />bind ` "toggleconsole"<br />bind a "+moveleft"<br />bind c "+movedown"<br />bind d "+moveright"<br />bind f "weapon toggle"<br />bind g "+button3"<br />bind h "+chat"<br />bind i "follow 3"<br />bind k "kill"<br />bind o "follow 2"<br />bind p "follow 1"<br />bind s "+back"<br />bind t "follow 6"<br />bind u "follow 4"<br />bind v "+acc"<br />bind w "+forward"<br />bind y "follow 5"<br />bind ~ "toggleconsole"</p>','publishedAt' => '2019-04-18 19:31:14','updatedAt' => '2019-04-18 19:31:14','headtitle' => 'Quake woldcam cfg','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '156','user_id' => '1','title' => 'Data początek i koniec dnia','slug' => 'data-poczatek-i-koniec-dnia','content' => '<pre class="language-php"><code>&lt;?php
// default and list items
$dateNow = new \\DateTime(\'now\');
$createdAtBeginDefault = new \\DateTime($dateNow-&gt;format(\'Y-m-d\') . \' 00:00:00\');
$createdAtEndDefault = new \\DateTime($dateNow-&gt;format(\'Y-m-d\') . \' 23:59:59\');</code></pre>','publishedAt' => '2019-04-25 11:14:39','updatedAt' => '2019-11-27 08:21:41','headtitle' => 'Data początek i koniec dnia','keyworks' => NULL,'description' => 'Data początek i koniec dnia w języku php','img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '157','user_id' => '1','title' => 'Pokazywanie rezultatu przed końcem wykonania skryptu','slug' => 'pokazywanie-rezultatu-przed-koncem-wykonania-skryptu','content' => '<pre class="language-php"><code>flush();
ob_flush();</code></pre>','publishedAt' => '2019-05-08 06:45:49','updatedAt' => '2019-05-08 06:45:49','headtitle' => 'Pokazywanie rezultatu przed końcem wykonania skryptu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '158','user_id' => '1','title' => 'Walidacja dowodu osobistego','slug' => 'walidacja-dowodu-osobistego','content' => '<pre class="language-php"><code>&lt;?php

class DowOsValidator {

    public static function valid($value) {
        //delete white chars
        $value = str_replace(\' \', \'\', $value);

        //if is too short or too long
        if (strlen($value) &lt;= 8 || strlen($value) &gt; 9) {
            return false;
        }

        $defValue = [
            \'0\' =&gt; 0, \'1\' =&gt; 1, \'2\' =&gt; 2, \'3\' =&gt; 3, \'4\' =&gt; 4, \'5\' =&gt; 5, \'6\' =&gt; 6, \'7\' =&gt; 7, \'8\' =&gt; 8, \'9\' =&gt; 9,
            \'A\' =&gt; 10, \'B\' =&gt; 11, \'C\' =&gt; 12, \'D\' =&gt; 13, \'E\' =&gt; 14, \'F\' =&gt; 15, \'G\' =&gt; 16, \'H\' =&gt; 17, \'I\' =&gt; 18, \'J\' =&gt; 19,
            \'K\' =&gt; 20, \'L\' =&gt; 21, \'M\' =&gt; 22, \'N\' =&gt; 23, \'O\' =&gt; 24, \'P\' =&gt; 25, \'Q\' =&gt; 26, \'R\' =&gt; 27, \'S\' =&gt; 28, \'T\' =&gt; 29,
            \'U\' =&gt; 30, \'V\' =&gt; 31, \'W\' =&gt; 32, \'X\' =&gt; 33, \'Y\' =&gt; 34, \'Z\' =&gt; 35
        ];

        $importance = [7, 3, 1, 9, 7, 3, 1, 7, 3];

        try {
            //change to upper
            $value = strtoupper($value);
            
            $identityCardSum = 0;

            for ($i = 0; $i &lt; 9; $i++) {
                if (!isset($defValue[$value[$i]]) || ($i &lt; 3 &amp;&amp; $defValue[$value[$i]] &lt; 10)) {
                    return false;
                } elseif (($i &gt; 2 &amp;&amp; $defValue[$value[$i]] &gt; 9)) {
                    return false;
                }

                $identityCardSum += ((int) $defValue[$value[$i]]) * $importance[$i];
            }

 
            if ($identityCardSum % 10 != 0) {
                return false;
            }
            
        } catch (\\Exception $ex) {
            return false;
        }

        return true;
    }
}</code></pre>','publishedAt' => '2019-05-23 11:47:34','updatedAt' => '2019-05-23 11:47:34','headtitle' => 'Walidacja dowodu osobistego','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '159','user_id' => '1','title' => 'Symfony form query builder','slug' => 'symfony-form-query-builder','content' => '<pre class="language-php"><code>&lt;?php

namespace App\\AccessBundle\\Form;

use Symfony\\Component\\Form\\AbstractType;
use Symfony\\Component\\Form\\FormBuilderInterface;
use Symfony\\Component\\OptionsResolver\\OptionsResolver;
use Symfony\\Component\\Form\\Extension\\Core\\Type\\TextType;
use Symfony\\Bridge\\Doctrine\\Form\\Type\\EntityType;
use Symfony\\Component\\Form\\Extension\\Core\\Type\\CheckboxType;
use Symfony\\Component\\Form\\Extension\\Core\\Type\\FileType;
use Symfony\\Component\\Form\\Extension\\Core\\Type\\SubmitType;
use Symfony\\Component\\Form\\Extension\\Core\\Type\\TextareaType;
use Doctrine\\ORM\\EntityRepository;
use App\\AccessBundle\\Entity\\Application;
use App\\AccessBundle\\Entity\\Access;
use App\\UserBundle\\Entity\\User;
use Symfony\\Component\\Form\\Extension\\Core\\Type\\ChoiceType;
use Symfony\\Component\\Form\\Extension\\Core\\Type\\DateTimeType;

class AccessType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $departmentId = $options[\'departmentId\'];

        $builder
                -&gt;add(\'application\', EntityType::class, [\'class\' =&gt; Application::class, \'choice_label\' =&gt; \'name\', \'required\' =&gt; true])
                -&gt;add(\'user\', EntityType::class, [\'class\' =&gt; User::class, \'query_builder\' =&gt; function (EntityRepository $er) use ($departmentId) {
                        return $er-&gt;createQueryBuilder(\'i\')
                                -&gt;where(\'i.department = :Department\')-&gt;setParameter(\'Department\', $departmentId)
                                -&gt;orderBy(\'i.name\', \'ASC\')
                                -&gt;addOrderBy(\'i.surname\', \'ASC\');
                    }, \'choice_label\' =&gt; function (User $user) {
                        return $user-&gt;getName() . \' \' . $user-&gt;getSurname();
                    },
                    \'multiple\' =&gt; false, \'required\' =&gt; true])
                -&gt;add(\'description\', TextareaType::class, [\'required\' =&gt; false, \'attr\' =&gt; [\'maxlength\' =&gt; 255]])
                -&gt;add(\'datetime_add\', DateTimeType::class, [\'required\' =&gt; true, \'widget\' =&gt; \'single_text\'])
                -&gt;add(\'datetime_block\', DateTimeType::class, [\'required\' =&gt; false, \'widget\' =&gt; \'single_text\'])
                -&gt;add(\'datetime_remove\', DateTimeType::class, [\'required\' =&gt; false, \'widget\' =&gt; \'single_text\'])
                -&gt;add(\'login\', TextType::class, [\'required\' =&gt; true, \'attr\' =&gt; [\'maxlength\' =&gt; 255]])
                -&gt;add(\'submit\', SubmitType::class)
                -&gt;add(\'submitAndClose\', SubmitType::class)
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver-&gt;setDefaults([
            \'data_class\' =&gt; Access::class,
            \'departmentId\' =&gt; null,
        ]);
    }

}
</code></pre>','publishedAt' => '2019-05-30 06:41:32','updatedAt' => '2019-05-30 06:41:32','headtitle' => 'Symfony form query builder','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '160','user_id' => '1','title' => 'Kasowanie wszystkiego w folderze','slug' => 'kasowanie-wszystkiego-w-folderze','content' => '<pre class="language-markup"><code>rm -rf app/cache/prod/twig/*</code></pre>','publishedAt' => '2019-07-01 06:43:01','updatedAt' => '2019-07-01 06:43:01','headtitle' => 'Kasowanie wszystkiego w folderze','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '161','user_id' => '1','title' => 'Moje konta email','slug' => 'moje-konta-email','content' => '<p>grz.tarka@wp.pl<br />grz.tarka@interia.pl<br /><a href="mailto:grzegorz.tarka22@onet.pl">grzegorz.tarka22@onet.pl</a></p>
<p><a href="mailto:jacek.kawka123@onet.pl">jacek.kawka123@onet.pl</a></p>
<p>ram andrzej :</p>
<p>Nazwa modułu Samsung M471B5273DH0-CH9</p>','publishedAt' => '2019-07-02 13:29:01','updatedAt' => '2020-06-13 09:44:42','headtitle' => 'Moje konta email','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '162','user_id' => '1','title' => 'Basic Auth php przykład','slug' => 'basic-auth-php-przyklad','content' => '<pre class="language-php"><code>        $parameters = json_encode($parameters);
        
        $headers = [
            \'Content-Type: application/json\',
            \'Accept: application/json\',
        ];

        try {
            // curl setup, disabling certificate check
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_USERPWD, $this-&gt;login . ":" . $this-&gt;pass);

            $responseJson = curl_exec($ch);
            $response = json_decode($responseJson, true);

            $this-&gt;lastHttpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
        } catch (\\Exception $ex) {
            throw new \\Exception(\'Curl call failed! HttpCode: \' . $this-&gt;lastHttpCode . \' (Ex: \' . $ex-&gt;getMessage() . \')\');
        }

        return $response;</code></pre>','publishedAt' => '2019-07-05 07:05:45','updatedAt' => '2019-07-05 07:05:45','headtitle' => 'Basic Auth php przykład','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '163','user_id' => '1','title' => 'Symfony 3 regeneracja encji w danym bundlu','slug' => 'symfony-3-regeneracja-encji-w-danym-bundlu','content' => '<pre class="language-php"><code>php bin/console doctrine:generate:entities App/Recruitment
</code></pre>','publishedAt' => '2019-07-11 11:04:39','updatedAt' => '2019-07-11 11:04:39','headtitle' => 'Symfony 3 regeneracja encji w danym bundlu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '164','user_id' => '1','title' => 'Ustawienie końca dnia dla obiektu datetime','slug' => 'ustawienie-konca-dnia-dla-obiektu-datetime','content' => '<pre class="language-php"><code>$date = new DateTime();
      
var_dump($date-&gt;setTime(23, 59, 59, 999999));</code></pre>','publishedAt' => '2019-07-11 13:31:31','updatedAt' => '2019-07-11 13:31:31','headtitle' => 'Ustawienie końca dnia dla obiektu datetime','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '165','user_id' => '1','title' => 'Wzorzec strategia','slug' => 'wzorzec-strategia','content' => '<p>1/</p>
<pre class="language-php"><code>&lt;?php
interface AbstractStrategy{
    function task();
}
class ConcreteStrategy1 implements AbstractStrategy{
    public function task() {
        echo "Strategy 1";
    }
}
class ConcreteStrategy2 implements AbstractStrategy{
    public function task() {
        echo "Strategy 2";
    }
}
class Context{
    private $strategy;
 
    public function setStrategy(AbstractStrategy $obj) {
        $this-&gt;strategy=$obj;
    }
    public function getStrategy() {
        return $this-&gt;strategy;
    }
}
// testy
$obj = new Context();
$obj-&gt;setStrategy(new ConcreteStrategy1);
$obj-&gt;getStrategy()-&gt;task(); // wyswietla &bdquo;Strategy 1&rdquo;
$obj-&gt;setStrategy(new ConcreteStrategy2);
$obj-&gt;getStrategy()-&gt;task(); // wyswietla &bdquo;Strategy 2&rdquo;
?&gt;</code></pre>
<p>2/</p>
<pre class="language-php"><code>&lt;?php
 
interface Tax{
    public function count($net);
}
 
class TaxPL implements Tax{
    public function count($net) {
        return 0.23*$net;
    }
}
 
class TaxEN implements Tax{
    public function count($net) {
        return 0.15*$net;
    }
}
 
class TaxDE implements Tax{
    public function count($net) {
        return 0.3*$net;
    }
}
 
class Context{
    private $tax;
 
    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this-&gt;tax;
    }
 
    /**
     * @param mixed $tax
     */
    public function setTax(Tax $tax)
    {
        $this-&gt;tax = $tax;
    }
 
}
 
// testy
$tax = new Context();
$tax-&gt;setTax(new TaxPL());
echo $tax-&gt;getTax()-&gt;count(100); // wyswietla "23"
$tax-&gt;setTax(new TAXEN());
echo $tax-&gt;getTax()-&gt;count(100); // wyswietla "15"
$tax-&gt;setTax(new TAXDE());
echo $tax-&gt;getTax()-&gt;count(100); // wyswietla "30"
 
?&gt;</code></pre>
<pre class="language-php"><code>&lt;?php

interface Promotion {

    public function getPriseAfterPromotion(float $price): float;
}

class BlackFriday implements Promotion {

    public function getPriseAfterPromotion(float $price): float {
        return $price * 0.2;
    }

}

class SpanishKangaroosFestival implements Promotion {

    public function getPriseAfterPromotion(float $price): float {
        $discount = floor($price / 10) * 2;
        return $price - $discount;
    }

}

class Checkout {

    /**
     * @var type 
     */
    private $price;

    /**
     * @var Promotion 
     */
    private $promotion;

    /**
     * @param float $price
     */
    public function setPrice(float $price): void {
        $this-&gt;prise = $price;
    }

    /**
     * @param Promotion $promition
     */
    public function setPromotion(Promotion $promition): void {
        $this-&gt;promition = $promition;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float {
        return $this-&gt;promition-&gt;getPriseAfterPromotion($this-&gt;prise);
    }

}

$checkout = new Checkout();
$checkout-&gt;setPrice(22.333);
$checkout-&gt;setPromotion(new BlackFriday());

echo $checkout-&gt;getTotalPrice() . \' zl\';
echo \'&lt;br&gt;&lt;br&gt;\';

$checkout2 = new Checkout();
$checkout2-&gt;setPrice(120.9);
$checkout2-&gt;setPromotion(new SpanishKangaroosFestival());

echo $checkout2-&gt;getTotalPrice() . \' zl\';
</code></pre>','publishedAt' => '2019-07-16 07:41:14','updatedAt' => '2019-07-16 07:41:14','headtitle' => 'Wzorzec strategia','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '166','user_id' => '1','title' => 'Absolutny url w twigu','slug' => 'absolutny-url-w-twigu','content' => '<pre class="language-php"><code>&lt;a href="{{ absolute_url(path(\'route_name\', {\'param\' : value})) }}"&gt;A link&lt;/a&gt;

&lt;img src="{{ absolute_url(asset(\'bundle/myname/img/image.gif\')) }}" alt="Title"/&gt;

&lt;img src="{{ absolute_url(\'my/absolute/path\') }}" alt="Title"/&gt;

</code></pre>','publishedAt' => '2019-07-17 13:25:35','updatedAt' => '2019-07-17 13:25:35','headtitle' => 'Absolutny url w twigu','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '167','user_id' => '1','title' => 'Template w javascript','slug' => 'template-w-javascript','content' => '<pre class="language-javascript"><code>function commentsTemplate(commentId,commentAuthor,publishedAt,comment_content,comment_author_url,comment_author_email_md5){
    var img=Avatar(comment_author_email_md5);
    var url=\'\';
    comment_author_url != null &amp;&amp; comment_author_url.length &gt; 1 ? url =comment_author_url : url =\'\';
    var comments=`&lt;article id=${commentId}&gt;
        &lt;div class="col-lg-12 text-left pd-zero"&gt;
            &lt;div class="panel panel-white post panel-shadow"&gt;
                &lt;header&gt;
                    &lt;div class="post-heading"&gt;
                        &lt;div class="pull-left image"&gt;
                            &lt;img src="${img}" class="img-circle avatar" alt="user profile image"&gt;
                        &lt;/div&gt;
                        &lt;div class="pull-left meta"&gt;
                            &lt;div class="title h5"&gt;
                                &lt;a class="text-capitalize" href="${url}" target="_blank"&gt;&lt;b&gt;${commentAuthor}&lt;/b&gt;&lt;/a&gt;
                            &lt;/div&gt;
                            &lt;h6 class="text-muted time"&gt;${publishedAt}&lt;/h6&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/header&gt;
                &lt;div class="post-description"&gt;
                    &lt;p style=""&gt;${comment_content}&lt;/p&gt;
                    &lt;button class="test btn btn-primary" name="subject" type="submit"  onclick="postReply(${commentId})" data-toggle="modal" data-target=".myModalHorizontal"&gt;ODPOWIEDZ&lt;/button&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/article&gt;`;
    return comments;
}</code></pre>','publishedAt' => '2019-07-17 14:12:29','updatedAt' => '2019-07-17 14:12:29','headtitle' => 'Template w javascript','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '168','user_id' => '1','title' => 'asdsad','slug' => 'asdsad','content' => '<p><a href="https://stackoverflow.com/questions/6268679/best-way-to-get-the-key-of-a-key-value-javascript-object">https://stackoverflow.com/questions/6268679/best-way-to-get-the-key-of-a-key-value-javascript-object</a></p>','publishedAt' => '2019-07-18 15:06:09','updatedAt' => '2019-07-18 15:06:09','headtitle' => 'asdsad','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '169','user_id' => '1','title' => 'Wspólne klucze dwóch tablic','slug' => 'wspolne-klucze-dwoch-tablic','content' => '<pre class="language-javascript"><code>statValueValue = [1,2,3];

valuesFromInput = [1,3];

let intersection = statValueValue.filter(function(i) {return valuesFromInput.indexOf(i) &lt; 0;});

//lub
Array.prototype.diff = function(a) {
     return this.filter(function(i) {return a.indexOf(i) &lt; 0;});
};

let intersection = statValueValue.diff( valuesFromInput );  
</code></pre>','publishedAt' => '2019-07-18 15:13:08','updatedAt' => '2019-07-18 15:13:08','headtitle' => 'Wspólne klucze dwóch tablic','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '170','user_id' => '1','title' => 'Symfony join','slug' => 'symfony-join','content' => '<pre class="language-php"><code>public function test() {
        $query = $this-&gt;getEntityManager()-&gt;createQueryBuilder()
                -&gt;select(\'s\',\'su\',\'rv\')
                -&gt;from(Project::class, \'s\')
                -&gt;leftJoin(\'s.sub_projects\', \'su\')
                -&gt;leftJoin(\'su.reports_validation\', \'rv\')

                -&gt;getQuery()
        ;
        
        return $query-&gt;getResult();
    }</code></pre>
<p>2/</p>
<pre class="language-php"><code>/**
     * get sub projects
     * 
     * @param Project $project
     * 
     * @return array
     */
    public function getSubProjects(Project $project): array {
        $query = $this-&gt;getEntityManager()-&gt;createQueryBuilder()
                -&gt;select(\'s.id\', \'s.name\')
                -&gt;from(SubProject::class, \'s\')
                -&gt;where(\'s.project = :project\')-&gt;setParameter(\'project\', $project)
                -&gt;getQuery()
        ;
        try {
            return $query-&gt;getResult();
        } catch (\\Doctrine\\ORM\\NoResultException $e) {
            return [];
        }
    }
</code></pre>','publishedAt' => '2019-07-19 07:36:01','updatedAt' => '2019-07-19 07:36:01','headtitle' => 'Symfony join','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '171','user_id' => '1','title' => 'zamiana na kropki wszystkich / z pominięciem pierwszego','slug' => 'zamiana-na-kropki-wszystkich-z-pominieciem-pierwszego','content' => '<pre class="language-php"><code>&lt;?php

$str     = \'6/3/2\';
$res_str = array_chunk(explode("/",$str),2);
foreach( $res_str as &amp;$val){
   $val  = implode("/",$val);
}
echo implode(".",$res_str);

//6/3.2</code></pre>','publishedAt' => '2019-07-22 07:07:59','updatedAt' => '2019-07-22 07:07:59','headtitle' => 'zamiana na kropki wszystkich / z pominięciem pierwszego','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '172','user_id' => '1','title' => 'Przykład test z mocowaniem metod','slug' => 'przyklad-test-z-mocowaniem-metod','content' => '<p>//metoda</p>
<pre class="language-php"><code>public function updateStepNumberForTransaction(?string $transactionId = null, ?string $httpReferer = null) : void {
        if (!$transactionId || !$httpReferer) {
            return ;
        }
        
        // get step nr from httpreferer
        $httpRefererStepNr = null;
        // parse url
        $parseUrl = parse_url($httpReferer);
        $query = [];
        if (isset($parseUrl[\'query\'])) {
            parse_str($parseUrl[\'query\'], $query);
        }
        if (isset($query[\'sn\'])) {
            $httpRefererStepNr = (int)$query[\'sn\'];
        }
        if (!$httpRefererStepNr) {
            return ;
        }
        
        // find transaction
        /* @var $transaction TransactionEntity */
        $transaction = $this-&gt;transactionRepository-&gt;findOneBy([\'transaction_id\' =&gt; $transactionId]);
        if (!$transaction) {
            return ;
        }
        
        // get step number from transaction
        $transactionStepNr = null;
        // parse url
        $parseUrl = parse_url($transaction-&gt;getHttpReferer());
        $query = [];
        if (isset($parseUrl[\'query\'])) {
            parse_str($parseUrl[\'query\'], $query);
        }
        if (isset($query[\'sn\'])) {
            $transactionStepNr = (int)$query[\'sn\'];
        }
        
        // check if update
        if (!$transactionStepNr || $transactionStepNr &lt; $httpRefererStepNr) {
            $query[\'sn\'] = $httpRefererStepNr;
            
            // update query values
            $parseUrl[\'query\'] = http_build_query($query);
            //joining a new address
            $newUrl = ($parseUrl[\'scheme\'] ?? \'\') . "://" . ($parseUrl[\'host\'] ?? \'\') . ($parseUrl[\'path\'] ?? \'\') . \'?\' . ($parseUrl[\'query\'] ?? \'\');

            // if correct new url - update it
            if (filter_var($newUrl, FILTER_VALIDATE_URL)) {
                $transaction-&gt;setHttpReferer($newUrl);
                $this-&gt;transactionRepository-&gt;flush();
            }
        }
    }</code></pre>
<p>//test</p>
<pre class="language-php"><code>&lt;?php

namespace Tests\\DM\\MultiStepFormBundle\\Service\\Transaction;

use Tests\\ModelTestCase;
use DM\\MultiStepFormBundle\\Service\\Transaction\\TransactionHelper;
use DM\\MultiStepFormBundle\\Repository\\Transaction\\TransactionRepository;
use DM\\MultiStepFormBundle\\Entity\\Transaction;

class TransactionHelperClassTest extends ModelTestCase {

    /**
     * @dataProvider updateStepNumberForTransactionProvider
     */
    public function testUpdateStepNumberForTransaction(?string $transactionId, ?string $httpRefer, $transactionHttpRefer, $expected) {
        $transaction = new Transaction();
        $transaction-&gt;setTransactionId($transactionId);
        $transaction-&gt;setHttpReferer($transactionHttpRefer);

        $transactionRepositoryMock = $this-&gt;createMock(TransactionRepository::class);
        $transactionRepositoryMock-&gt;expects($this-&gt;any())-&gt;method(\'findOneBy\')-&gt;willReturn($transaction);
        $transactionRepositoryMock-&gt;expects($this-&gt;any())-&gt;method(\'flush\')-&gt;willReturn(null);

        $service = new TransactionHelper($transactionRepositoryMock);
        $serviceResult = $service-&gt;updateStepNumberForTransaction($transactionId, $httpRefer);

        return $this-&gt;assertEquals($expected, $transaction-&gt;getHttpReferer());
    }

    public function updateStepNumberForTransactionProvider() {
        return [
            [
                null,
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=3\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=3\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=3\',
            ],
            [
                \'797d3d68-fac6-48d2-b716-a6d9636476e8\',
                null,
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=3\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=3\',
            ],
            [
                \'797d3d68-fac6-48d2-b716-a6d9636476e8\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=10\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=10\',
            ],
            [
                \'797d3d68-fac6-48d2-b716-a6d9636476e8\',
                \'https://test.pl/\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8\',
            ],
            [
                \'797d3d68-fac6-48d2-b716-a6d9636476e8\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=8\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=4\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=8\',
            ],
            [
                \'797d3d68-fac6-48d2-b716-a6d9636476e8\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=4\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=8\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=8\',
            ],
            [
                \'797d3d68-fac6-48d2-b716-a6d9636476e8\',
                \'incorrectUrl?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=4\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=8\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=8\',
            ],
            [
                \'797d3d68-fac6-48d2-b716-a6d9636476e8\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=8\',
                \'incorrectUrlfs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=4\',
                \'https://test.pl/?fs=ttl-wniosek&amp;tId=797d3d68-fac6-48d2-b716-a6d9636476e8&amp;sn=8\',
            ],
        ];
    }

}
</code></pre>','publishedAt' => '2019-07-26 13:16:45','updatedAt' => '2019-07-26 13:16:45','headtitle' => 'Przykład test z mocowaniem metod','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '173','user_id' => '1','title' => 'Zamiana klucza w json','slug' => 'zamiana-klucza-w-json','content' => '<pre class="language-php"><code>/**
     * replace user agent in text to user agent from actual environment
     * 
     * @param string $text
     * 
     * @return string
     */
    private function replaceUserAgent(string $text): string {
        $client = new Client();
        $userAgent = str_replace(\'/\', \'\\/\', ($client-&gt;getConfig(\'headers\')[\'User-Agent\']));

        $result = preg_replace(\'/"user_agent":"(Guzzle[A-Za-z0-9 . \\/\\\\\\]*)"/\', \'"user_agent":"\' . $userAgent . \'"\', $text);

//or $result = preg_replace(\'/"user_agent":"GuzzleHttp(.*)"/\', \'"user_agent":"\' . $userAgent . \'"\', $text);

//. kropka - dowolny znak * oznacza dowolną ilosc razy bez gwiazdki brało by pod uwagę tylko pierwszy znak.  

        return $result;
    }</code></pre>','publishedAt' => '2019-07-26 13:19:35','updatedAt' => '2019-12-14 12:25:32','headtitle' => 'Zamiana klucza w json','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '174','user_id' => '1','title' => 'opcja curla','slug' => 'opcja-curla','content' => '<pre class="language-php"><code>curl_setopt($ch, CURLOPT_HEADER, false);</code></pre>
<p>//pokazuje czy w zwrotce dodawać nagłowki&nbsp; true/false</p>','publishedAt' => '2019-07-31 13:16:57','updatedAt' => '2019-07-31 13:16:57','headtitle' => 'opcja curla','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '175','user_id' => '1','title' => 'strpos problem','slug' => 'strpos-problem','content' => '<pre class="language-php"><code>if (isset($resposne[\'message\']) &amp;&amp; (strpos($resposne[\'message\'], \'wystepuje blokada ponownego dodania\')!== false)) {
   $this-&gt;normalizeStatus = self::STATUS_DUPLICATE;
}</code></pre>
<p>Trzeba dodać !== false bo ineczej nie działa to poprawnie</p>','publishedAt' => '2019-08-02 08:57:20','updatedAt' => '2019-12-14 12:25:20','headtitle' => 'strpos problem','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '176','user_id' => '1','title' => 'Wzorzec facade(fasada)','slug' => 'wzorzec-facadefasada','content' => '<p>Wydzielanie złożonych funkcji&nbsp; np do jednej klasy co ukrywa szczeg&oacute;ły i pozwala w łatwy spos&oacute;b uruchamiać złozone funkcjonalności.&nbsp;</p>
<p>Wyobraźmy sobie, że struktura i treść kodu są jeszcze bardziej złożone i że przez to nie ma szans na ich przepisanie na własną rękę &mdash; pozostaje nam jedynie korzystanie z rozwiązania kalekiego, ale gotowego. Aby skonwertować plik o zawartości: 234-bluza_damska 55 532-kapelusz_męski 55 na postać tablicy obiekt&oacute;w, musimy wywołać wszystkie z prezentowanych funkcji (dla uproszczenia pomijamy przy tym wyodrębnianie z pliku liczby kończącej wiersz, reprezentującej cenę artykułu):</p>
<pre class="language-php"><code>&lt;?php
$lines = getProductFileLines(\'test.txt\');
$objects = array();
foreach ($lines as $line)
{
    $id = getIDFromLine($line);
    $name = getNameFromLine($line);
    $objects[$id] = getProductObjectFromID($id, $name);
}

</code></pre>
<p>Jeśli będziemy w swoim projekcie stosować powyższą procedurę odczytu plik&oacute;w, zwiążemy ściśle kod projektu z kodem podsystemu wczytywania plik&oacute;w. Jeśli potem &oacute;w podsystem ulegnie zmianom albo zdecydujemy się na skorzystanie z innego podsystemu w jego miejsce, staniemy w obliczu problemu rozległych modyfikacji kodu projektu. Trzeba by więc powołać do życia bramkę pomiędzy tym podsystemem a resztą projektu.</p>
<p>Implementacja Oto prosta klasa udostępniająca interfejs do kodu proceduralnego, z kt&oacute;rym borykaliśmy się w poprzednim punkcie:</p>
<pre class="language-php"><code>&lt;?php
class ProductFacade
{
    private $products = array();
    function __construct($file)
    {
        $this-&gt;file = $file;
        $this-&gt;compile();
    }
    private function compile()
    {
        $lines = getProductFileLines($this-&gt;file);
        foreach ($lines as $line)
        {
            $id = getIDFromLine($line);
            $name = getNameFromLine($line);
            $this-&gt;products[$id] = getProductObjectFromID($id, $name);
        }
    }
    function getProducts()
    {
        return $this-&gt;products;
    }
    function getProduct($id)
    {
        if (isset($this-&gt;products[$id]))
        {
            return $this-&gt;products[$id];
        }
        return null;
    }
}

</code></pre>
<p>Z punktu widzenia użytkownika tego kodu dostęp do obiekt&oacute;w Product generowanych na podstawie pliku rejestru produkt&oacute;w jest znacznie uproszczony:</p>
<pre class="language-php"><code>$facade = new ProductFacade(\'test.txt\');
$facade-&gt;getProduct(234);</code></pre>','publishedAt' => '2019-08-02 10:07:01','updatedAt' => '2020-05-07 13:35:31','headtitle' => 'Wzorzec facade(fasada)','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '177','user_id' => '1','title' => 'sortowanie asocjacyjnej tablicy po 2 kluczach','slug' => 'sortowanie-asocjacyjnej-tablicy-po-2-kluczach','content' => '<pre class="language-php"><code>
  foreach ($normalLinks as $linkId =&gt; $data) {
            // prepare additional parameters from link
            // find link
            /* @var $link Linki */
            $link = $this-&gt;linksRepository-&gt;find($linkId);
            // norlam link CPL
            if (isset($data[\'linkUrl\']) &amp;&amp; $data[\'linkUrl\'] != \'\') {
                // save to result
                $result[$linkId] = [
                    \'partnerName\' =&gt; $transaction-&gt;getPartnerNameLinkId($linkId),
                    \'redirectUrl\' =&gt; $data[\'linkUrl\'],
                    \'logoSrc\' =&gt; $link ? $link-&gt;getLogoSrc() : null,
                    \'description\' =&gt; $link ? $link-&gt;getDescription() : null,
                    \'customOrderNr\' =&gt;$link ? $link-&gt;getCustomOrderNr():null,
                    \'orderNr\' =&gt; isset($data[\'orderNr\']) ? $data[\'orderNr\'] : 99999,
                ];
            }
        }
        
        //sort by customOrderNr and orderNr
        uasort($result, function ($a, $b) {
            $customOrderNrDiff = $a[\'customOrderNr\'] - $b[\'customOrderNr\'];
            return $customOrderNrDiff ?  $customOrderNrDiff: $a[\'orderNr\'] - $b[\'orderNr\'];
        });

        return $result;
</code></pre>
<p>Sortowanie malejąco:</p>
<pre class="language-php"><code>uasort($parseLogs[\'fields\'], function ($a, $b) {
    if ($a[\'numberOfErrors\'] == $b[\'numberOfErrors\']) {
       return 0;
    }

    return $a[\'numberOfErrors\'] &lt; $b[\'numberOfErrors\'] ? 1 : -1;
});</code></pre>','publishedAt' => '2019-08-06 13:08:23','updatedAt' => '2019-11-27 08:09:31','headtitle' => 'sortowanie asocjacyjnej tablicy po 2 kluczach','keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '178','user_id' => '1','title' => 'Iterator w php','slug' => 'iterator-w-php','content' => '<p><a href="https://www.php.net/manual/en/class.iteratoraggregate.php">https://www.php.net/manual/en/class.iteratoraggregate.php</a></p>
<p><a href="https://www.php.net/manual/en/class.arrayiterator.php">https://www.php.net/manual/en/class.arrayiterator.php</a></p>
<pre class="language-php"><code>class CarsIterator implements Iterator {
	private $position = 0;
	private $cars = [\'BMW\', \'Audi\', \'KIA\', \'Opel\', \'Ford\'];

	public function current() {
		return $this-&gt;cars[$this-&gt;position];
	}

	public function key() {
		return $this-&gt;position;
	}

	public function next() {
		$this-&gt;position++;
	}

	public function rewind() {
		$this-&gt;position = 0;
	}

	public function valid() {
		if (isset($this-&gt;cars[$this-&gt;position])) {
			return true;
		}

		return false;
	}
	public function reverse() {
		$this-&gt;cars = array_reverse($this-&gt;cars, true);
	}
}

$tests = new CarsIterator();

var_dump($tests-&gt;current());

$tests-&gt;next();
$tests-&gt;next();
$tests-&gt;next();
$tests-&gt;next();
$tests-&gt;next();

var_dump($tests-&gt;current());</code></pre>','publishedAt' => '2019-08-09 11:18:21','updatedAt' => '2019-08-09 11:18:21','headtitle' => 'Iterator w php','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '179','user_id' => '1','title' => 'Symfony walidacja w osobnym obiekcie','slug' => 'symfony-walidacja-w-osobnym-obiekcie','content' => '<p><a href="https://sarvendev.com/2018/01/encja-byc-zawsze-poprawnym-obiektem/">https://sarvendev.com/2018/01/encja-byc-zawsze-poprawnym-obiektem/</a></p>','publishedAt' => '2019-08-09 11:45:02','updatedAt' => '2019-08-09 11:45:02','headtitle' => 'Symfony walidacja w osobnym obiekcie','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '180','user_id' => '1','title' => 'yield w php','slug' => 'yield-w-php','content' => '<pre class="language-php"><code>&lt;?php

// Starting clock time in seconds
$start_time = microtime(true);
$a = 1;

function convert($size) {
	$unit = array(\'b\', \'kb\', \'mb\', \'gb\', \'tb\', \'pb\');
	return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . \' \' . $unit[$i];
}

function getLines($file) {
	while ($line = fgets($file)) {
		yield $line;
	}
}

$file = fopen(\'a.csv\', \'rb\');
$lines = getLines($file);

foreach ($lines as $line) {
	//echo $line;
}
fclose($file);

echo convert(memory_get_usage(true)); // 123 kb

echo \'&lt;br&gt;\';
// End clock time in seconds
$end_time = microtime(true);

// Calculate script execution time
$execution_time = ($end_time - $start_time);

echo " Execution time of script = " . $execution_time . " sec";</code></pre>
<pre class="language-php"><code>&lt;?php
// Starting clock time in seconds
$start_time = microtime(true);
$a = 1;

function convert($size) {
	$unit = array(\'b\', \'kb\', \'mb\', \'gb\', \'tb\', \'pb\');
	return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . \' \' . $unit[$i];
}

function getLines($file) {
	$lines = [];
	while ($line = fgets($file)) {
		$lines[] = $line;
	}
	return $lines;
}

$file = fopen(\'a.csv\', \'rb\');
$lines = getLines($file);
fclose($file);

foreach ($lines as $line) {

}

echo convert(memory_get_usage(true)); // 123 kb

echo \'&lt;br&gt;\';
// End clock time in seconds
$end_time = microtime(true);

// Calculate script execution time
$execution_time = ($end_time - $start_time);

echo " Execution time of script = " . $execution_time . " sec";</code></pre>','publishedAt' => '2019-08-09 13:53:52','updatedAt' => '2019-08-09 13:53:52','headtitle' => 'yield w php','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '181','user_id' => '1','title' => 'Losowanie lotto','slug' => 'losowanie-lotto','content' => '<pre class="language-php"><code>&lt;?php

class Lotto {

    /**
     * @var int 
     */
    private $numbersQuantity;

    /**
     * @var int
     */
    private $maxNumber;

    /**
     * @param int $numbersQuantity
     * @param int $maxNumber
     */
    public function __construct(int $numbersQuantity, int $maxNumber) {
        if ($numbersQuantity &lt;= 0 || $maxNumber &lt;= 0) {
            throw new \\Exception(\'Parameters musts been biggers than zero\');
        }

        $this-&gt;numbersQuantity = $numbersQuantity;
        $this-&gt;maxNumber = $maxNumber;
    }

    /**
     * generate lotto numbers
     * 
     * @return array
     */
    public function generateLottoNumbers(): array {
        $numbers = range(1, $this-&gt;maxNumber);
        shuffle($numbers);

        $lottoNumbers = array_slice($numbers, - $this-&gt;numbersQuantity);

        sort($lottoNumbers);

        return $lottoNumbers;
    }

}

/**
 * @param array $data
 * @return boolean
 */
function consecutiveElementsInArray(array $data): bool {
    $count = count($data);
    foreach ($data as $key =&gt; $value) {
        if ($key == $count - 1) {
            break;
        }

        if (($data[$key + 1]) != ($value + 1)) {
            return false;
        }
    }

    return true;
}

$lottoNumbers = (new Lotto(6, 49))-&gt;generateLottoNumbers();

$interest = array_intersect($lottoNumbers, [3, 7, 9, 12, 27, 45]);

echo \'&lt;h1&gt;trafiles: \' . count($interest) . \' liczb\';
echo \'&lt;br&gt;\';

foreach ($lottoNumbers as $number) {
    echo \'&lt;b&gt;\' . $number . \'&lt;/b&gt;  \';
}

die;

$count = 0;

for ($i = 1; $i &lt; 139; $i++) {
    $lottoNumbers = (new Lotto(6, 49))-&gt;generateLottoNumbers();
    if (consecutiveElementsInArray($lottoNumbers)) {
        $count++;
    }
}

echo \'&lt;h1&gt;tyle ma liczby po kolei: \' . $count . \'&lt;/h1&gt;\';
die;
​</code></pre>','publishedAt' => '2019-08-15 12:28:02','updatedAt' => '2019-08-15 12:28:02','headtitle' => 'Losowanie lotto','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '182','user_id' => '1','title' => 'Wyszukiwanie po jsonie','slug' => 'wyszukiwanie-po-jsonie','content' => '<pre class="language-markup"><code>SELECT * FROM `multi_step_form_client_data` WHERE JSON_CONTAINS(serializedData, \'{"pesel" : "86042287797"}\')
</code></pre>','publishedAt' => '2019-08-21 11:37:19','updatedAt' => '2019-08-21 11:37:19','headtitle' => 'Wyszukiwanie po jsonie','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '183','user_id' => '1','title' => 'Pojedynczy rezultat zapytania','slug' => 'pojedynczy-rezultat-zapytania','content' => '<pre class="language-php"><code>$createdAtCondition = new \\DateTime(\'now -30 days\');
        $queryBuilder = $this-&gt;createQueryBuilder(\'cd\')
        -&gt;where(\'cd.createdAt &gt; :createdAt\')
        -&gt;andWhere(\'cd.stepNr = :stepNr\')
        -&gt;andWhere("JSON_CONTAINS(cd.serializedData, :pesel, \'$.pesel\') = 1");

        //$queryBuilder-&gt;setParameter(\'pesel\', ".$keyValue.");
        $queryBuilder-&gt;setParameter(\'createdAt\', $createdAtCondition);
        $queryBuilder-&gt;setParameter(\'stepNr\', $lastStepNumber);
        $queryBuilder-&gt;setParameter(\'pesel\', \'"\'.$keyValue.\'"\');

        $queryBuilder-&gt;orderBy(\'cd.createdAt\', \'DESC\')-&gt;setMaxResults(1);
        
        return $queryBuilder-&gt;getQuery()-&gt;getSingleResult();</code></pre>','publishedAt' => '2019-08-21 13:43:01','updatedAt' => '2019-08-21 13:43:01','headtitle' => 'Pojedynczy rezultat zapytania','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '184','user_id' => '1','title' => 'Czyste zapytanie','slug' => 'czyste-zapytanie','content' => '<pre class="language-php"><code>  public function test2(string $keyName, string $keyValue, int $lastStepNumber) {
        $status = ClientStatuses::TYPE_COMPLETED;

        $key = \'{"\' . $keyName . \'" : "\' . $keyValue . \'"}\';

        $createdAtCondition = (new \\DateTime(\'now -30 days\'))-&gt;format(\'Y-m-d\');
        $sqlQuery = \'SELECT * FROM `multi_step_form_client_data` WHERE createdAt &gt; :createdAt and stepNr = :stepNr and status = :status and JSON_CONTAINS(serializedData, :jsonKey)  order BY createdAt desc limit 1\';
        $connection = $this-&gt;getEntityManager()-&gt;getConnection()-&gt;prepare($sqlQuery);
        $connection-&gt;bindParam(\':stepNr\', $lastStepNumber);
        $connection-&gt;bindParam(\':status\', $status);
        $connection-&gt;bindParam(\':createdAt\', $createdAtCondition);
        $connection-&gt;bindParam(\':jsonKey\', $key);
        $connection-&gt;execute();

        return $connection-&gt;fetch();
    }</code></pre>','publishedAt' => '2019-08-21 13:56:52','updatedAt' => '2019-08-21 13:56:52','headtitle' => 'Czyste zapytanie','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '185','user_id' => '1','title' => 'symfony JSON_CONTAINS','slug' => 'symfony-json-contains','content' => '<pre class="language-php"><code>  public function getClientDataBySerializedDataJsonKey(string $keyName, string $keyValue, int $penultimateStep) :?MultiStepFormClientData {
        $createdAtCondition = new \\DateTime(\'now -30 days\');
        
        $query = $this-&gt;createQueryBuilder(\'cd\')
            -&gt;where(\'cd.createdAt &gt; :createdAt\')
            -&gt;andWhere(\'cd.stepNr = :stepNr\')
            -&gt;andWhere(\'cd.status = :status\')    
            -&gt;andWhere("JSON_CONTAINS(cd.serializedData, :keyName, \'$.".$keyName."\') = 1");

        $query-&gt;setParameter(\'createdAt\', $createdAtCondition);
        $query-&gt;setParameter(\'stepNr\', $penultimateStep);
        $query-&gt;setParameter(\'status\', ClientStatuses::TYPE_COMPLETED);
        $query-&gt;setParameter(\'keyName\', \'"\'.$keyValue.\'"\');
        $query-&gt;orderBy(\'cd.createdAt\', \'DESC\')-&gt;setMaxResults(1);
        
        return $query-&gt;getQuery()-&gt;getOneOrNullResult();
    }</code></pre>
<p>//wymaga&nbsp;</p>
<p><a href="https://packagist.org/packages/syslogic/doctrine-json-functions">link</a></p>','publishedAt' => '2019-08-22 08:38:09','updatedAt' => '2019-08-22 08:38:09','headtitle' => 'symfony JSON_CONTAINS','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '186','user_id' => '1','title' => 'Update pola w tabeli z jsona z tej samej tabeli w zakresie czasowym','slug' => 'update-pola-w-tabeli-z-jsona-z-tej-samej-tabeli-w-zakresie-czasowym','content' => '<pre class="language-markup"><code>UPDATE multi_step_form_client_data SET pesel = REPLACE(json_extract(serializedData,\'$.pesel\'),\'"\',\'\') where createdAt &gt;= (now() - interval 30 day)</code></pre>','publishedAt' => '2019-08-22 12:32:23','updatedAt' => '2019-08-22 12:32:23','headtitle' => 'Update pola w tabeli z jsona z tej samej tabeli w zakresie czasowym','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '187','user_id' => '1','title' => 'Fabryka abstrakcyjna','slug' => 'fabryka-abstrakcyjna','content' => '<pre class="language-php"><code>&lt;?php

interface CarType {

    CONST POTROL = \'petrol\';
    CONST DIESEL = \'desiel\';

}

class CarsFactory {

    public function createDesiel() {
        return new Car(CarType::DIESEL, \'1.7\', false);
    }

    public function createFastBigCar() {
        return new Car(CarType::POTROL, \'3.7\', false);
    }

    public function createFastSmallCar() {
        return new Car(CarType::POTROL, \'1.0\', true);
    }

}

Class Car {

    private $engine;
    private $capacity;
    private $turbo;

    public function __construct(string $engine, string $capacity, bool $turbo) {
        $this-&gt;engine = $engine;
        $this-&gt;capacity = $capacity;
        $this-&gt;turbo = $turbo;
    }

}

$factory = new CarsFactory();

$diesel = $factory-&gt;createDesiel();
$fastBigCar = $factory-&gt;createFastBigCar();
$fastSmallCar = $factory-&gt;createFastSmallCar();

var_dump($diesel, $fastBigCar, $fastSmallCar);
</code></pre>','publishedAt' => '2019-08-23 11:59:27','updatedAt' => '2019-08-23 11:59:27','headtitle' => 'Fabryka abstrakcyjna','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '188','user_id' => '1','title' => 'Wiele parametrów przekazanych nie jako tablica','slug' => 'wiele-parametrow-przekazanych-nie-jako-tablica','content' => '<pre class="language-php"><code>function sum(...$numbers) {
    $acc = 0;
    foreach ($numbers as $n) {
        $acc += $n;
    }
    return $acc;
}
echo sum(1, 2, 3, 4);//10</code></pre>','publishedAt' => '2019-08-23 12:21:05','updatedAt' => '2019-08-23 12:21:05','headtitle' => 'Wiele parametrów przekazanych nie jako tablica','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '189','user_id' => '1','title' => 'Ustawianie prywatne wartości w testach','slug' => 'ustawianie-prywatne-wartosci-w-testach','content' => '<pre class="language-php"><code>&lt;?php

namespace Tests\\MultiStepFormBundle\\Domain\\Flow\\Manager;

use MultiStepFormBundle\\Domain\\Flow\\Manager\\FlowManager;
use Tests\\ModelTestCase;

class FlowManagerTest extends ModelTestCase {

    /**
     * @dataProvider mergeClientDataWithDataFromSidByMinModifyDateProvider
     */
    public function testMergeClientDataWithDataFromSidByMinModifyDate(array $clientData, array $clientDataFromSid, array $expected) {
        $transactionModelFactoryMock = $this-&gt;createMock(\\MultiStepFormBundle\\Domain\\Transaction\\Factory\\TransactionModelFactory::class);
        $clientDataModelFactoryMock = $this-&gt;createMock(\\MultiStepFormBundle\\Domain\\ClientData\\Factory\\ClientDataModelFactory::class);
        $formModelFactoryMock = $this-&gt;createMock(\\MultiStepFormBundle\\Domain\\Form\\Factory\\FormModelFactory::class);
        $containerMock = $this-&gt;createMock(\\Symfony\\Component\\DependencyInjection\\Container::class);

        $service = new FlowManager($transactionModelFactoryMock, $clientDataModelFactoryMock, $formModelFactoryMock, $containerMock);

        $this-&gt;setPrivateVariable($service, \'copyClientData\', $clientData);

        $method = $this-&gt;getMethod(FlowManager::class,
                \'mergeClientDataWithDataFromSidByMinModifyDate\');
        $result = $method-&gt;invokeArgs($service, [$clientDataFromSid]);

        return $this-&gt;assertEquals($expected, $result);
    }

    public function mergeClientDataWithDataFromSidByMinModifyDateProvider() {
        return [
            [
                [
                    \'imie\' =&gt; \'Jan\',
                ],
                [
                    \'imie\' =&gt; [
                        \'value\' =&gt; \'Jan\',
                        \'modify\' =&gt; (new \\DateTime(\'now + 5minutes\'))-&gt;format(\'Y-m-d H:i:s\')
                    ],
                ],
                [
                    \'imie\' =&gt; \'Jan\',
                ],
            ],
            [
                [
                    \'imie\' =&gt; \'Jan\',
                ],
                [
                    \'imie\' =&gt; [
                        \'value\' =&gt; \'Stefan\',
                        \'modify\' =&gt; (new \\DateTime(\'now + 5minutes\'))-&gt;format(\'Y-m-d H:i:s\')
                    ],
                ],
                [
                    \'imie\' =&gt; \'Jan\',
                ],
            ],
            [
                [
                    \'imie\' =&gt; \'\',
                ],
                [
                    \'imie\' =&gt; [
                        \'value\' =&gt; \'Stefan\',
                        \'modify\' =&gt; (new \\DateTime(\'now + 5minutes\'))-&gt;format(\'Y-m-d H:i:s\')
                    ],
                ],
                [
                    \'imie\' =&gt; \'Stefan\',
                ],
            ],
            [
                [
                    \'imie\' =&gt; \'Dupa\',
                ],
                [
                    \'imie\' =&gt; [
                        \'value\' =&gt; \'Stefan\',
                        \'modify\' =&gt; (new \\DateTime(\'now - 5 days\'))-&gt;format(\'Y-m-d H:i:s\')
                    ],
                ],
                [
                    \'imie\' =&gt; \'Dupa\',
                ],
            ],
            [
                [
                    \'imie\' =&gt; \'\',
                ],
                [
                    \'imie\' =&gt; [
                        \'value\' =&gt; \'Stefan\',
                        \'modify\' =&gt; (new \\DateTime(\'now - 5 days\'))-&gt;format(\'Y-m-d H:i:s\')
                    ],
                ],
                [
                    \'imie\' =&gt; \'\',
                ],
            ],
            [
                [
                    \'imie\' =&gt; \'Jan\',
                ],
                [
                    \'imie\' =&gt; [
                        \'value\' =&gt; \'Stefan\',
                        \'modify\' =&gt; null
                    ],
                ],
                [
                    \'imie\' =&gt; \'Jan\',
                ],
            ],
            [
                [
                    \'imie\' =&gt; \'Jan\',
                ],
                [
                    \'imie\' =&gt; [
                        \'value\' =&gt; \'Stefan\',
                        \'modify\' =&gt; \'\'
                    ],
                ],
                [
                    \'imie\' =&gt; \'Jan\',
                ],
            ],
            [
                [
                    \'imie\' =&gt; \'Jan\',
                ],
                [],
                [
                    \'imie\' =&gt; \'Jan\',
                ],
            ],
            [
                [
                ],
                [
                    \'imie\' =&gt; [
                        \'value\' =&gt; \'Stefan\',
                        \'modify\' =&gt; (new \\DateTime(\'now + 5 days\'))-&gt;format(\'Y-m-d H:i:s\')
                    ],
                ],
                [
                    \'imie\' =&gt; \'Stefan\',
                ],
            ],
            [
                [],
                [
                    \'imie\' =&gt; [
                        \'value\' =&gt; \'Stefan\'
                    ],
                ],
                [],
            ],
            [
                [
                    \'imie\' =&gt; \'janusz\'
                ],
                [
                    \'imie\' =&gt; [
                        \'value\' =&gt; \'Stefan\'
                    ],
                ],
                [
                    \'imie\' =&gt; \'janusz\'
                ],
            ],
        ];
    }

    /**
     * get acces to private method
     * @param string $className
     * @param string $methodName
     * @return \\ReflectionMethod
     */
    protected static function getMethod(string $className, string $methodName) {
        $class = new \\ReflectionClass($className);
        $method = $class-&gt;getMethod($methodName);
        $method-&gt;setAccessible(true);
        return $method;
    }

    protected static function setPrivateVariable($object, string $objectParameter, $objectValue) {
        $reflection = new \\ReflectionClass($object);
        $property = $reflection-&gt;getProperty($objectParameter);
        $property-&gt;setAccessible(true);
        $property-&gt;setValue($object, $objectValue);
    }

}
</code></pre>','publishedAt' => '2019-08-28 09:34:54','updatedAt' => '2019-08-28 09:34:54','headtitle' => 'Ustawianie prywatne wartości w testach','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '190','user_id' => '1','title' => 'Petla do dniach miedzy dwoma datami','slug' => 'petla-do-dniach-miedzy-dwoma-datami','content' => '<pre class="language-php"><code>$dateTemp = clone $dateFrom;

//loop literate single days between two dates ($dateFrom, $dateTo)
while ($dateTemp &lt;= ($dateTo-&gt;setTime(23, 59, 59, 999999))) {

   $dateTemp-&gt;modify(\'+1 days\');
}</code></pre>
<p>lub</p>
<pre class="language-php"><code>&lt;?php
$begin = new DateTime(\'2019-09-01\');
$end = new DateTime(\'2019-09-10\');

$interval = DateInterval::createFromDateString(\'1 day\');
$period = new DatePeriod($begin, $interval, $end);

foreach ($period as $dt) {
    echo $dt-&gt;format("l Y-m-d H:i:s\\n");
}
</code></pre>','publishedAt' => '2019-09-03 13:17:31','updatedAt' => '2019-09-03 13:17:31','headtitle' => 'Petla do dniach miedzy dwoma datami','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '191','user_id' => '1','title' => 'Wstrzykniecie parametru bezposrednio z parametrów','slug' => 'wstrzykniecie-parametru-bezposrednio-z-parametrow','content' => '<pre class="language-php"><code>dm_report.cost_sms:
        class: DM\\RaportyBundle\\Service\\ReportValidation\\Cost\\CostSmsService
        arguments: 
            - "@dm_report.report_validation.cost_repository"
            - "@dm_sms.group.repository"
            - "@dm_sms.storage.repository"
            - "@dm_report.report_validation.cost_value_repository"
            - "%logs_dir%"</code></pre>','publishedAt' => '2019-09-05 07:37:14','updatedAt' => '2019-09-05 07:37:14','headtitle' => 'Wstrzykniecie parametru bezposrednio z parametrów','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '192','user_id' => '1','title' => 'Offset i limit w tablicy','slug' => 'offset-i-limit-w-tablicy','content' => '<pre class="language-php"><code>$getDataToProcess = array_slice($allDataFromItem, $offset, $limit, true);
</code></pre>','publishedAt' => '2019-09-05 12:37:49','updatedAt' => '2019-09-05 12:37:49','headtitle' => 'Offset i limit w tablicy','keyworks' => '','description' => '','img' => '','type' => '2','number_of_comments' => '0'),
  array('id' => '193','user_id' => '1','title' => 'Jak stworzyć swój własny helper w laravelu (5.5)','slug' => 'jak-stworzyc-swoj-wlasny-helper-w-laravelu-55','content' => '<div class="container-fluid pd-top-bt-20 text-left bg-white">
<div class="row">
<div class="container">
<p>W wielu projektach nie jednokrotnie przychodzi moment w kt&oacute;rym potrzebujesz stworzyć szybką funkcje/helpera przyspieszającą twoją pracę. Dziś zajmiemy się implementacją takiej funkcji w popularnej "larvie". Na początek w folderze app stw&oacute;rzmy nowy folder do przechowywania naszych helper&oacute;w o nazwie helpers a w nim zapiszmy plik custom_helpers.php.&nbsp; Kolejną rzeczą jaką musimy wykonać jest edycja pliku composer.json. Po otwarciu pliku fragment:</p>
<pre class="language-javascript"><code>    "autoload": {"classmap": ["database/seeds","database/factories"],"psr-4": {"App\\\\": "app/"}},</code></pre>
<p>Zamieniamy na:</p>
<pre class="language-javascript"><code>    "autoload": {"classmap": ["database/seeds","database/factories"],"psr-4": {"App\\\\": "app/"},"files": ["app/Helpers/custom_helpers.php"]},</code></pre>
<p>Dzięki temu zabiegowi ładujemy plik do frameworka i możemy korzystać z niego wszędzie. Tworzymy teraz w <strong>folderze app nowy folder Helpers a w nim plik custom_helpers.php</strong>. Na samym końcu instalujemy nowe zależności comoserem.</p>
<pre class="language-php"><code>composer install</code></pre>
<p>Od teraz w pliku&nbsp;custom_helpers.php możemy tworzyć własne helpery. Do zobaczenia wkr&oacute;tce :]</p>
</div>
</div>
</div>','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2020-06-09 13:25:01','headtitle' => 'Jak stworzyć swój własny helper w laravelu (5.5)','keyworks' => 'laravel, helpery, php','description' => 'W wielu projektach nie jednokrotnie przychodzi moment w którym potrzebujesz stworzyć szybką funkcje/helpera przyspieszającą twoją pracę. Dziś zajmiemy się implementacją takiej funkcji w popularnej "larvie"','img' => 'img/upload/3/titleimg/neo-urban-1734495_1920.jpg','type' => '1','number_of_comments' => '74'),
  array('id' => '194','user_id' => '1','title' => 'Przeprowadzanie obliczeń matematycznych w sql','slug' => 'przeprowadzanie-obliczen-matematycznych-w-sql','content' => '<article class="container-fluid pd-top-bt-20 bg-white">
<div class="container pd-zero">
<div class="row">
<div class="col-lg-12">
<p style="text-align: left;">Przeprowadzanie obliczeń matematycznych w aplikacji możliwe a nawet konieczne na samym poziomie bazy danych. Przyspiesza to działanie naszej aplikacjii w znaczący spos&oacute;b. W dzisiejszym wpisie zajmiemy się przykładem takiej operacji. Mamy przykładową tabelę:</p>
<h3 style="text-align: left;">Przykładowa tabela Zamowienia:</h3>
<div class="table-responsive">
<table class="table table-bordered" style="margin-left: auto; margin-right: auto; width: 100%;">
<tbody>
<tr>
<td style="width: 23.7612%;"><strong>prod_id</strong></td>
<td style="width: 27.2987%;"><strong>ilosc</strong></td>
<td style="width: 48.7013%;"><strong>cena_elem</strong></td>
</tr>
<tr>
<td style="width: 23.7612%;">RGAN01</td>
<td style="width: 27.2987%;">5</td>
<td style="width: 48.7013%;">12.9900</td>
</tr>
<tr>
<td style="width: 23.7612%;">BR03</td>
<td style="width: 27.2987%;">5</td>
<td style="width: 48.7013%;">51.9900</td>
</tr>
<tr>
<td style="width: 23.7612%;">BNBG01</td>
<td style="width: 27.2987%;">10</td>
<td style="width: 48.7013%;">9.9900</td>
</tr>
<tr>
<td style="width: 23.7612%;">BNBG02</td>
<td style="width: 27.2987%;">10</td>
<td style="width: 48.7013%;">9.9900</td>
</tr>
<tr>
<td style="width: 23.7612%;">BNBG03</td>
<td style="width: 27.2987%;">10</td>
<td style="width: 48.7013%;">9.9900</td>
</tr>
</tbody>
</table>
</div>
<pre class="language-php"><code>SELECT prod_id,ilosc,cena_elem,ilosc*cena_elem AS wartoscFROM Zamowienia</code></pre>
<div class="table-responsive">
<table class="table table-bordered" style="margin-left: auto; margin-right: auto; width: 100%;">
<tbody>
<tr style="height: 47.9141px;">
<td style="height: 47.9141px;"><strong>prod_id</strong></td>
<td style="height: 47.9141px;"><strong>ilosc</strong></td>
<td style="height: 47.9141px;"><strong>cena_elem</strong></td>
<td style="height: 47.9141px;"><strong>wartosc</strong></td>
</tr>
<tr style="height: 50px;">
<td style="height: 50px;">RGAN01</td>
<td style="height: 50px;">5</td>
<td style="height: 50px;">12.9900</td>
<td style="height: 50px;">64.9500</td>
</tr>
<tr style="height: 50px;">
<td style="height: 50px;">BR03</td>
<td style="height: 50px;">5</td>
<td style="height: 50px;">51.9900</td>
<td style="height: 50px;">259.9500</td>
</tr>
<tr style="height: 50px;">
<td style="height: 50px;">BNBG01</td>
<td style="height: 50px;">10</td>
<td style="height: 50px;">9.9900</td>
<td style="height: 50px;">99.9000</td>
</tr>
<tr style="height: 50px;">
<td style="height: 50px;">BNBG02</td>
<td style="height: 50px;">10</td>
<td style="height: 50px;">9.9900</td>
<td style="height: 50px;">99.9000</td>
</tr>
<tr style="height: 50px;">
<td style="height: 50px;">BNBG03</td>
<td style="height: 50px;">10</td>
<td style="height: 50px;">9.9900</td>
<td style="height: 50px;">99.9000</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</article>','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2020-09-11 08:10:24','headtitle' => 'Przeprowadzanie obliczeń matematycznych w sql','keyworks' => 'sql, operacje matematyczne','description' => 'Przeprowadzanie obliczeń matematycznych w aplikacji możliwe a nawet konieczne na samym poziomie bazy danych. Przyspiesza to działanie naszej aplikacjii w znaczący sposób. W dzisiejszym wpisie zajmiemy się przykładem takiej operacji','img' => 'img/upload/5/titleimg/book-1853677_1920.jpg','type' => '1','number_of_comments' => '92'),
  array('id' => '195','user_id' => '1','title' => 'Jak wyłaczyć ekran logowania w systemie windows','slug' => 'jak-wylaczyc-ekran-logowania-w-systemie-windows','content' => '<article class="container-fluid pd-top-bt-20 bg-white"><div class="container pd-zero"><div class="row"><div class="col-lg-8 col-lg-offset-2 text-left"><p>W przypadku użycia komputera w domu przez zaufanych użytkowników panel logowania do systemu windows może być tylko nie potrzebną stratą czasu. Dziś zajmiemy się wyłączeniem takiego panelu dzięki czemu windows po uruchomieniu komputera włączy się bezpośrednio i zaoszczędzi nam to często trochę czasu, a nie od dziś wiadomo że czas to pieniądz :]. <b>Jak więc to zrobić ?</b> </p><p>Naciskamy kombinacje klawiszy na klawiaturze: <b>win+r</b>. Pojawia nam sie okienko dialogowe w którym wpisujemy "<b>netplwiz</b>". Resztę procedury przedstawiłem na poniższym gifie:</p><figure><a href="img/upload/4/post/ezgif.com-video-to-gif.gif" data-lightbox="image-1"><img src="img/upload/4/post/ezgif.com-video-to-gif.gif" class="my-img" alt="Instrukcja do posta"></a></figure><p>Po zatwierdzeniu zmian wystarczy zresetować nasz komputer i możemy się cieszyć bezpośrednim przejściem do pulpitu. Do zobaczenia  wkrótce :)</p></div></div></div></article>','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'Jak wyłaczyć ekran logowania w systemie windows','keyworks' => 'windows, wyłącznie logowania','description' => 'W przypadku użycia komputera w domu przez zaufanych użytkowników panel logowania do systemu windows może być tylko nie potrzebną stratą czasu. Dziś zajmiemy się wyłączeniem takiego panelu dzięki czemu windows po uruchomieniu komputera włączy się bezpośrednio i zaoszczędzi nam','img' => 'img/upload/4/titleimg/login-1203603_1280.jpg','type' => '1','number_of_comments' => '95'),
  array('id' => '196','user_id' => '1','title' => 'cypher1','slug' => 'cypher1','content' => 'Qui consequatur molestias similique. Et dolore laboriosam omnis qui. Autem dolore tenetur nemo. In eum nobis quae. Et laboriosam voluptate sed odit. Enim non nulla amet eum velit culpa ratione. Cumque officiis autem qui vel. Adipisci in repellat est reprehenderit itaque. Est voluptas quasi autem quas ut rerum nulla. Perferendis et aliquid ut est dignissimos eveniet itaque. Ab nihil velit ratione eligendi et. Consequatur iste et ipsa dolore vel non. Et qui totam et enim quae. Voluptates sed sequi possimus a dolorem eius autem. Labore minus neque enim aut perspiciatis. Quia iure est qui quos laboriosam aut corporis. Ad in facere ea aut eum vero. Dolorem ipsum modi quasi est. Cum rem dolorem numquam vitae voluptas et quaerat. Dolorem quia debitis earum aut perferendis ipsam numquam nulla. Dolorum et eaque neque tempore et magni. Dolorem qui illum sit molestiae voluptatem. Quis harum sequi deserunt quia similique illum sint iusto. Doloribus voluptates explicabo consequatur id. Veritatis aut pariatur non. Modi rerum ut saepe corrupti recusandae eaque. Quam ut modi facilis et qui voluptas. Dolores quos facere incidunt aspernatur qui voluptate. Est excepturi consectetur libero voluptas voluptas nesciunt. Ut est ducimus esse ex. Veniam ducimus voluptas omnis. Distinctio deleniti neque consectetur et similique. Neque distinctio dolores qui facilis et. Enim aut consectetur iusto voluptatem. Nemo et in aspernatur eligendi consequatur aut. Dolorem ipsa nihil unde quis repellat aut. Iste qui in voluptas quo aut maxime et. Sint eveniet est quia animi natus. Doloremque minus voluptatem placeat corporis. Repudiandae consequuntur vel dolor molestiae totam velit nihil. Dolor quos et est pariatur. Voluptas vel ut quae asperiores rem dolor consectetur. Accusamus aut ex id facilis omnis eos aut. Reiciendis et et blanditiis illum mollitia corrupti. Optio voluptatem incidunt eius. Consequuntur sunt a doloremque culpa provident iure. Et iure reprehenderit ut libero aut perspiciatis. Quibusdam voluptatem qui voluptatem error. Est illo incidunt optio est. Autem expedita eum provident aut id. Nihil provident soluta eos alias quisquam ut. Eum eveniet odit quod ducimus culpa. Voluptatum eveniet doloribus ut. Quos dolorem ut reiciendis iure eum. Repellat laboriosam quisquam consequuntur. Quos rerum sit aliquam. Dolorum cumque minima asperiores ut. Blanditiis enim sed enim sit dignissimos hic quaerat rerum. Beatae maiores ea aut iusto in. Sunt dolor et quae atque nam veritatis. Explicabo ut cupiditate commodi voluptatem repellat enim tenetur. Quam aut repellendus similique molestiae numquam aut et sequi. Deleniti aliquid facilis accusantium deserunt distinctio cum ipsam. Non sit ullam accusamus praesentium ut consequatur. Aut commodi quia quis nesciunt qui reprehenderit aliquid. Fuga aut voluptas magnam non. Laborum ad consequuntur assumenda eos et cupiditate ad. In doloremque molestiae cupiditate nostrum. Debitis laboriosam omnis eius et explicabo dolor. Inventore voluptatum sequi consectetur ea et vel. A ipsum animi officia sit quasi vel labore. Quia et earum odit assumenda impedit sunt. Numquam assumenda perspiciatis aut doloribus. Voluptatem quia ducimus iusto harum et tenetur. Corrupti iure nihil iure tempore reprehenderit. Qui in iste distinctio modi. Accusamus et voluptate fugiat laboriosam sunt quaerat. Accusantium architecto veniam ea quibusdam tempore animi. Excepturi voluptatem deserunt voluptatem molestiae quis. Itaque facere enim velit non ab. Ullam minima pariatur molestiae similique nisi qui. Non itaque eos esse id et velit omnis. Ducimus excepturi autem eligendi repudiandae. Ut labore debitis sunt rerum. Nisi rerum quia quos optio ut id. Cumque vero fuga totam nemo. Rerum enim sed quis ut praesentium sapiente ut. Neque magnam rem commodi omnis earum voluptas. Voluptas eum quae sint blanditiis et et necessitatibus. Qui nisi et eum possimus optio enim. Illo beatae recusandae aut molestiae hic vero. Similique doloremque ratione quia cupiditate nostrum. Earum labore in et ipsa sed enim sed. Eius voluptate ipsam velit molestiae repellat qui harum. Voluptatum nihil culpa harum harum quam quam quibusdam. Architecto qui fugit beatae excepturi alias ut delectus. Aperiam quaerat laudantium cumque qui. Repellat quia optio quia unde velit consequatur. Non delectus eius ex vel. Minus numquam architecto tempora qui velit quae nisi saepe. Ratione porro reprehenderit voluptas repellat eligendi nihil iusto. Odio aperiam minus odit qui maxime non saepe. Dolor eaque magni ut officia aperiam asperiores enim. Veritatis animi eligendi deleniti porro non. Et eos a iure voluptas. Sunt similique odio qui quaerat. Praesentium unde eum et minus rerum. Voluptates odio cupiditate et atque. Et quam eos dicta ullam est. Sunt autem aut a eum voluptatem ratione voluptatum. Est quas excepturi tempora molestiae quisquam perspiciatis. Incidunt aut quis illum dolores.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle1','keyworks' => 'key works test asd1','description' => 'Unde ipsam voluptatum dicta. Ad quisquam voluptatum eveniet et.','img' => NULL,'type' => '1','number_of_comments' => '80'),
  array('id' => '197','user_id' => '3','title' => 'cypher2','slug' => 'cypher2','content' => 'Temporibus laboriosam placeat asperiores eum reprehenderit perferendis. Accusantium laudantium recusandae itaque qui ut deleniti. Minima occaecati perspiciatis ut ex. A et qui libero sint. Praesentium nemo dolorem enim vitae nisi voluptatem. Magni ea consectetur aperiam autem rem quasi veritatis. Voluptatem minima est quia nisi. Voluptatem fuga sit quia expedita harum vero et. Quaerat eum possimus neque est. Dolorum quibusdam est voluptate. Eveniet qui libero repellendus quia quam. Ex perferendis exercitationem reprehenderit. Dolor sequi libero similique. Dolor officiis error qui voluptatibus. Distinctio et perferendis itaque maxime animi. Consequatur expedita alias soluta nam libero amet. Officiis enim dolores aperiam tenetur et rerum molestiae. Non adipisci suscipit voluptate quod possimus quia. Dolore deleniti corporis quo harum et quasi deleniti. Perferendis enim consequatur perspiciatis odit. Eum ex asperiores eos. Et est sit velit veniam pariatur sint nesciunt amet. Nihil corporis eveniet ullam et quis. Consequatur deserunt eaque nemo reiciendis ea. Autem incidunt rerum temporibus et. Labore maxime qui quia totam. Quia quia quod provident asperiores temporibus provident. Voluptate fuga quos recusandae aut et sequi nam. Repellat quibusdam doloribus et et. Harum eveniet sint aliquam ut. Porro cumque quo facere natus ad. Deleniti quam error ex aliquid accusamus. Facere ipsum consequatur architecto voluptates at. Non dolor id corporis iusto occaecati occaecati. Explicabo eum in rerum assumenda quas nobis. Saepe blanditiis nobis sed ut autem. Occaecati illum adipisci quidem aspernatur totam. Sed aliquam mollitia et omnis ad officiis non. Nam et culpa unde fugiat quod modi. Vitae velit modi facilis autem. Ex occaecati omnis temporibus aut necessitatibus inventore quaerat earum. Tempore voluptates optio enim qui recusandae quasi qui. Eos minima et perspiciatis nam est ratione est. Corporis et commodi consequatur dolorum eligendi. Possimus magnam nam quisquam dolorem. Distinctio sunt sint est omnis in a. Odit aut aperiam cum aliquam fuga. Dolorem et tempore aut unde consequatur. Maiores occaecati quam expedita enim. Ut qui omnis veniam asperiores. Exercitationem distinctio dolore eaque ut quia. Odit quaerat eaque odio non sint voluptates et asperiores. Quidem odit reprehenderit aspernatur et inventore. Corporis est aliquid dignissimos et ipsam laboriosam suscipit. Qui dolores aut id ipsum sed voluptas eum. Blanditiis repellendus et non incidunt. Earum unde eos quia eos laboriosam veritatis. Rem quis atque quas qui aut earum. Praesentium molestias fugiat voluptatem corporis nostrum eos tempore. Qui vel repellendus eveniet corporis. Magni ipsum et repellendus aut ipsum excepturi. Incidunt ut inventore quae nostrum impedit vero aut quisquam. Laboriosam ea aut mollitia id expedita numquam laboriosam. Non aut aut laboriosam suscipit excepturi omnis. Non alias eos sint consequatur. Qui ea non a. Maiores blanditiis vero vero et et. Quis nesciunt vitae similique voluptas placeat sunt. Natus architecto numquam dolor suscipit non ut. Accusamus velit beatae dolore laborum repellat ab molestiae. Laudantium maiores vitae dolorem ut. Aperiam et eum velit alias ut esse earum facere. Reiciendis illo qui modi enim non. Quis praesentium dolores ullam doloremque et. Aut voluptatem dolorem illum quia. Iste asperiores in consequatur eius necessitatibus et consectetur. Harum voluptas sint et animi eum voluptas. Alias cumque est non repellendus est quidem. Ut ipsa aut minima et facilis. Esse porro rerum tempore ut soluta reiciendis. Est doloribus voluptas dolorem debitis cupiditate. Saepe velit sunt rerum eaque quo quam facilis. Voluptas sint facere doloribus. Molestias enim dolores incidunt suscipit. Magnam eaque sapiente minima saepe. Ut natus reprehenderit voluptatem autem dolores omnis. Explicabo non minus nihil facere vel nisi. Blanditiis et molestiae ut asperiores. Impedit impedit quibusdam nihil ea magnam. Maxime et minima qui dicta culpa mollitia. Repudiandae nostrum quia non minus et vel tenetur. Placeat rerum accusamus nobis molestias ea. Omnis perferendis autem exercitationem architecto ipsam error veniam. Natus sed et magnam explicabo nostrum illo. Voluptas vitae at corrupti iste similique. Aut reiciendis dolores consequatur in. Ut tempore vel praesentium. Voluptatem aut unde cum aperiam praesentium quas odit. Qui illo aut quasi non. Rerum natus corporis nostrum ut labore. Dicta esse porro dolorem iure est. Et corporis fugit et et. Est harum sapiente sunt ut non qui. Qui qui excepturi perspiciatis nemo quos minima minima. Nihil ducimus optio iusto tempora similique et odit nihil. Maxime rerum est iste eos nostrum voluptatem corporis. Ex quis cupiditate mollitia quam incidunt vel et. Sapiente laborum molestias possimus ipsam ut ipsum. Culpa soluta sit nihil cum est. Qui inventore repudiandae laborum harum consequatur. Et est esse atque et minus facilis ducimus. Ratione earum ad quia.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle2','keyworks' => 'key works test asd2','description' => 'Quis quia quae fugit repellendus. Unde fugit laborum repellendus aut.','img' => NULL,'type' => '1','number_of_comments' => '89'),
  array('id' => '198','user_id' => '2','title' => 'cypher3','slug' => 'cypher3','content' => 'Commodi totam ad officia culpa quibusdam enim id. Sed optio voluptas consequatur laboriosam quisquam eius consectetur. Dolorum quis libero temporibus qui provident corporis itaque. Esse aliquam architecto voluptas porro reiciendis eveniet. Aut eos voluptas et cumque ex. Totam laudantium voluptatem quam consequuntur dolorem facilis est. Fuga possimus mollitia voluptatem et et et enim. Voluptatibus odio distinctio inventore vel nihil impedit. Ea dolorum reprehenderit cumque in debitis earum incidunt. Repellendus praesentium dolorem voluptatum vel consequatur eius. Magni voluptatem enim ad enim rerum aliquam. Enim aliquam esse consequatur rerum similique consectetur. Eveniet atque est magni ratione sint eos. Nostrum consequatur ipsum voluptatem rem. Eos omnis dolor et. Inventore quaerat hic ut dolores nostrum. Voluptas veniam maxime debitis dolor reiciendis. Voluptas voluptatem dignissimos ea velit ipsa in totam. Eum facere unde fugit voluptatem sunt quia omnis. Cum rerum magni error aut eaque odio quaerat. Soluta nam delectus est sed. Voluptas eligendi similique placeat facere. Eveniet voluptas magni voluptatem voluptatem quis. Et modi atque architecto occaecati. Mollitia rerum rem quos eaque sequi. Minima pariatur dolorem quibusdam qui eos magnam expedita autem. Mollitia soluta rerum rerum rerum aut. Rerum et hic modi repellat veniam. Magnam quo quas aut aspernatur fugit. Qui voluptatibus voluptas fuga voluptate id. Hic nisi et temporibus optio minus. Velit consectetur laboriosam laboriosam assumenda id. Quas autem repudiandae necessitatibus similique tempora laudantium. Esse enim mollitia adipisci laborum accusamus. Modi tenetur est aperiam fuga dolorem odio accusamus. Rerum non aliquam aut in maxime rerum quisquam. Repudiandae iusto debitis voluptatum ipsum quaerat magnam. Ab eum perspiciatis aut commodi. Sit aut reiciendis dolorum. Porro voluptate odio et. Ut corporis est dicta aut. Ea quis pariatur consectetur est laborum velit. Ex nam laboriosam dolorem ut aliquid. Sed sed dolorem fugit et. Corrupti quis velit sequi inventore debitis. Amet ut est iste saepe neque cupiditate. Sunt laudantium odio voluptas eos unde nesciunt. Et expedita alias rerum atque. Blanditiis quibusdam suscipit architecto. Sapiente doloribus repellendus odio quo eaque. Sit blanditiis rem quae est. Quia qui reiciendis nulla dignissimos expedita architecto ut. Magnam reprehenderit enim nostrum aperiam ullam. Harum voluptatibus quis nihil sequi labore expedita ea non. Voluptas sint minus et quibusdam tenetur. Autem eaque nemo et excepturi architecto. Dicta aperiam et quia ut corporis. Voluptas unde ut numquam laboriosam nisi facere illo delectus. Sunt nisi sit quia minima cupiditate atque. Nemo neque deleniti minus consequuntur ut molestiae enim nihil. Qui earum voluptatem distinctio iusto eligendi nihil. Consequatur aut delectus dolor vel officia commodi modi. Voluptatem suscipit nemo et cupiditate doloribus. Tempore facilis laborum assumenda accusamus fugit rerum quasi. Recusandae vel autem delectus voluptate. Repellendus aperiam laboriosam soluta itaque. Dolorum voluptates laborum autem ut. Quia omnis sapiente qui vel explicabo at dignissimos nam. Dolor placeat eaque accusantium. Perferendis occaecati velit rerum autem non. Maxime ut et repellendus. Eligendi omnis facilis ea. Ut id velit fuga et tempora velit explicabo. Soluta quod labore placeat. Rem voluptas qui doloribus. Dolor voluptatem quo minima ut nemo. Illum adipisci blanditiis rerum culpa quam eos autem. Illo in saepe repellendus excepturi qui voluptas ut numquam. Sunt expedita maiores officia labore doloremque aut. Iste iste est perferendis ipsam magnam. Quibusdam non quo quos. Velit in ipsa sed maxime unde. Consequuntur nobis quia eum. Consequuntur eos est amet quia quas. Est et quasi sed adipisci distinctio sequi. Sit eligendi facilis rem eos. Nesciunt nulla officia quae ullam modi. Quia qui quasi tenetur sed. Nulla consequatur expedita deserunt nisi sed autem. Et ullam minima magni molestiae sit eligendi. Tempore culpa ut ut dicta ipsam cumque rerum. Ad ex qui atque. Et ut molestiae totam rerum et perferendis. Occaecati amet quis qui et. Quidem vitae ipsam excepturi labore dignissimos. Molestiae at omnis consequatur. Aliquam eius aliquam adipisci dolor. Dolorem laborum sint necessitatibus qui minima. Repellendus iusto quia soluta ut temporibus tempore. Magni dolorem sunt quia explicabo ut hic harum. Blanditiis dolorum soluta praesentium ipsum qui. Eaque et nihil ut doloremque odio ullam hic enim. Illo omnis dolor architecto modi ea magnam. Magnam fugit sit harum iste incidunt praesentium. Necessitatibus voluptates ab temporibus nesciunt sed enim. Tenetur incidunt excepturi dolorem veniam temporibus. Doloremque architecto aut voluptas et temporibus. Perferendis est enim eos temporibus vitae eum quibusdam. Consequatur qui dolorem non et est illo. Ducimus rem eius rem quidem similique. Sit aut aut repellat et cum quaerat. Dolore consequuntur in quo maiores quo rerum.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle3','keyworks' => 'key works test asd3','description' => 'Sit sit dolor rem eaque porro. A praesentium qui quis quasi.','img' => NULL,'type' => '1','number_of_comments' => '90'),
  array('id' => '199','user_id' => '3','title' => 'cypher4','slug' => 'cypher4','content' => 'Eos dolores assumenda doloremque consequatur incidunt dicta. A quis aut eos illum et. Magnam quaerat maiores ut rerum quia iste omnis. Iusto perferendis qui rerum mollitia velit omnis consequatur voluptatem. Et aut repellendus alias aperiam ut sit. Autem fugit voluptas quisquam dicta est inventore nesciunt. Odio deleniti dolorum eos provident alias quod repellendus. Corrupti ipsa aperiam rerum ut officia. Dolor nostrum possimus odio ipsa. Eveniet sunt ab mollitia cumque porro rem. Aut repellat voluptate veritatis qui a itaque itaque. Maiores ut quia dolor et qui hic ducimus. Quia eos hic sit. Sapiente veritatis provident magnam aut. Sequi sint ea molestiae rerum quo. Voluptatem impedit magnam deleniti aut. Non nemo a ipsum. Rerum animi accusamus et pariatur. Consectetur illum repellendus sint et ex rerum laudantium. Illo animi commodi autem consequuntur maxime atque recusandae. Et occaecati ratione voluptas sed. Porro labore consequatur eos ipsum. Commodi accusamus perferendis eum est minus blanditiis facilis. Voluptas cum asperiores velit odio. Iste nostrum aliquid perspiciatis quae ut voluptates. Quasi eos soluta corrupti. Qui mollitia sed libero non vel. Et voluptatem quia quibusdam perferendis et quia minima. Assumenda repellendus rerum expedita enim. Tenetur sapiente et nobis et. At sit ut rerum id. Sed debitis esse minus dicta rerum ut reprehenderit et. Adipisci qui quis rerum. Maxime et delectus aperiam ut laborum possimus dolor. Qui neque consequatur dolores odio sed. Voluptates dignissimos aperiam quo quod. Architecto consequatur dolorem vero eius at optio. Natus aut facere ipsa qui molestias provident. Quasi quod porro vel cum dolorem sunt repellat. Distinctio itaque id numquam esse eum veritatis ea. Velit et fugit hic modi minus voluptatem. Qui qui sit commodi veniam iste est. Nihil et eum hic nihil voluptatem cumque dolores. Sit nisi adipisci quo error fugiat ut quaerat. Quasi illo exercitationem sequi excepturi qui tempora reiciendis. Possimus quam aut non. Corporis vel enim doloribus dolore corporis earum. Asperiores quod explicabo repudiandae aut aut. Cupiditate doloremque labore iusto fugit. Voluptas eum dolores molestiae ut consequatur architecto aliquid ut. Voluptatem consequuntur consectetur asperiores commodi quam quos. Maiores quia velit aspernatur. Sit voluptas aspernatur dolorem aperiam. Laborum maxime eaque nemo delectus ut autem nemo. Omnis molestias mollitia nihil numquam. Architecto natus cupiditate non quia in. Animi aliquid praesentium odit et. Nemo deserunt voluptatem illum veniam. Quo veritatis temporibus ut soluta quod. Ducimus earum nam atque officia nulla omnis repudiandae. Molestiae sunt et aperiam asperiores architecto. Quod molestias adipisci qui quis quam nisi. Ut praesentium earum nemo ipsum laborum laudantium facere. Veritatis quia omnis itaque rerum aut debitis. Quo velit possimus quaerat quidem aut libero qui. Doloremque enim animi similique. Id iure non ut est voluptate dolor. Cupiditate omnis sit natus et quod. Quo fuga eum sed velit dolorum consequuntur. Facere voluptas quia assumenda nam. Est qui autem id et. Doloremque sint modi sed illo praesentium officia molestiae. Dolore voluptas aut placeat fugit officia. Mollitia excepturi deserunt maxime fugit deleniti dolorum. Nihil vero distinctio minus explicabo asperiores dolorem blanditiis. Nihil accusamus minus et ut. Dolorem occaecati consequatur et explicabo dolores qui quam. Tenetur est aliquid maxime in expedita similique. Aut incidunt odit qui atque aliquid. Saepe qui incidunt fugit quia corporis. Excepturi enim iusto quia qui. Nam id qui id sit maiores et nemo eos. Sunt repellendus repudiandae dolorem fugiat natus officiis cum. Corporis soluta deleniti ea. Sint enim distinctio repudiandae laboriosam. Molestias dolores et porro et non. Dolorem nesciunt minus autem eaque dignissimos rerum sequi recusandae. Eligendi sit ut reprehenderit eveniet non. Ut dolores laudantium quos velit est voluptates. Ad nihil ex doloremque corrupti. Autem omnis quo sed inventore impedit. Et occaecati harum ratione consequatur error sed. Non et dolor sed asperiores deleniti. Vero error nisi provident modi reiciendis voluptas autem molestiae. Voluptates voluptatem hic quidem inventore eveniet pariatur voluptatem modi. Voluptatibus quos molestiae necessitatibus nisi. Ad voluptatum maxime ipsa. Numquam magnam deserunt pariatur et odio est iste ullam. Voluptatem eius eos velit et assumenda nesciunt. Accusamus reiciendis esse ipsum. Et saepe quia perferendis ad quia expedita. Commodi dolor id recusandae velit occaecati cum mollitia dolore. Rerum veritatis animi illum tempore itaque et exercitationem dolore. Deserunt rerum aspernatur minima incidunt quas ut reiciendis. Voluptatem temporibus dignissimos iste et nulla voluptatem deserunt voluptates. Ut temporibus cumque consequuntur odit similique maiores.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle4','keyworks' => 'key works test asd4','description' => 'Quidem soluta omnis neque tempore distinctio perferendis qui debitis. Odio sed rerum quo libero.','img' => NULL,'type' => '1','number_of_comments' => '81'),
  array('id' => '200','user_id' => '2','title' => 'cypher5','slug' => 'cypher5','content' => 'Iusto earum asperiores nisi commodi recusandae iste aliquam. Harum quis iure velit beatae accusamus quo. Repellendus suscipit et voluptatem dignissimos velit reprehenderit ut ducimus. Possimus incidunt non molestiae aut consequuntur. Amet repellat facere mollitia nulla impedit. Ut dolor labore ea voluptates qui. Laborum consequatur fuga temporibus voluptatem dignissimos. Porro rerum culpa omnis sequi animi dicta odio. Provident iste temporibus inventore quisquam. Consequatur ut veniam cumque a quas dolore perspiciatis. Molestiae sit rerum saepe quisquam aut qui. Placeat corrupti omnis sequi unde dignissimos quasi tempora. Reprehenderit minima pariatur odit placeat corporis sint. Qui et id ut et vel molestiae. Nihil facere laboriosam consequuntur et quod. Unde autem aperiam nihil fugit facere et libero. Modi aut sed est ex. Soluta in dolor ut aut tempora. Distinctio et enim qui. Harum ea nam et quae velit nobis. Quos laborum voluptatem quod asperiores maiores quis. Est molestiae ut esse qui placeat autem ea culpa. Delectus sapiente et et voluptatem porro quibusdam repellendus blanditiis. Aut ratione modi aliquid saepe. Dolor velit natus illum maiores. Esse provident temporibus soluta voluptas. Fugit commodi et doloremque. Molestiae reprehenderit eos qui dolorem. Incidunt deleniti inventore nihil corporis sit cupiditate debitis. Aliquam aut cumque nobis praesentium. Hic quas temporibus illum sapiente at cum iure. Quia dicta quisquam voluptate sed quia. Natus in facilis illum in. Quo vel voluptatibus sit quos. Nihil similique vel consequatur eius. Optio aut voluptates error sit. Sed voluptatem ea sit amet molestiae velit reiciendis. Et officia reprehenderit quis non quas ut cumque. Labore ullam aut et unde quo deserunt fugiat. Quo dolorem provident ut nihil culpa. Et aut rerum magni. Qui ab et dolore fuga dolores voluptatem. Eos quos rerum id sequi. Doloremque sed rerum deleniti autem maxime. Doloremque maxime ipsa perferendis nulla facere repellendus in. Voluptatem esse est animi iusto natus fugit sunt. Cum aut illum est ex fugiat accusantium dolor. Sit voluptas iusto qui nulla rerum. Sit labore non quasi sapiente commodi. Minima qui unde molestiae modi. Sit qui omnis nam. Et molestiae vitae unde nihil. Asperiores illo aut voluptas culpa aut sunt. Reprehenderit quibusdam dolor sunt facere. Omnis id vel iste minus. Sint velit et qui iure. Saepe eligendi distinctio asperiores sint repellat iusto qui delectus. Eligendi harum dolore est nisi atque explicabo quasi. Quis excepturi assumenda laborum dolorem sit facere doloribus. Sed est dignissimos cupiditate nesciunt. Dolores veritatis sunt enim. Debitis iste molestiae ea deleniti. Error ipsum quo qui autem sapiente totam corporis. Quia in et repellat tenetur magni. Necessitatibus esse culpa qui provident vel autem. Numquam at maxime illum voluptatem sequi. Dolores voluptatem eos aut consequatur. Ipsum consequatur laudantium nobis reiciendis dolor. Asperiores non libero voluptate dolorem. Velit aliquid ipsam inventore deleniti in cumque. Voluptatem quaerat optio quo animi ut error tenetur. Vero repellendus rerum consequatur et. Sit vel quos velit autem libero. Rerum exercitationem assumenda numquam maxime officiis et. Similique enim vel delectus et ullam. Placeat culpa et placeat modi pariatur voluptatibus veritatis. Aliquam aut voluptatibus id deleniti illo saepe. Atque et magnam quia odio ipsa quis qui possimus. Magni voluptates nisi ex est in. Magnam suscipit esse quod blanditiis. Numquam quam saepe et quidem ut aut hic. Aut qui architecto provident maxime voluptates voluptatum. Vel ut soluta consequuntur sint voluptatem vitae nostrum maiores. Provident velit maxime culpa sunt ut rerum officiis. Modi sed deleniti rem. Aut qui ratione placeat ut molestiae possimus. Eveniet dolorem non eos rerum. Voluptatem ut perspiciatis exercitationem velit cumque inventore repellat. Qui accusantium officiis quia hic sed saepe. Delectus asperiores quisquam illum fuga. Quasi sint reiciendis suscipit architecto at et ullam. Consequatur corporis minima ducimus vel enim sapiente consequatur. Ad non id in. Autem consequatur id cupiditate eaque cum. Aliquam voluptatibus neque maxime doloribus nesciunt aspernatur quasi. Velit quasi dolore consequuntur praesentium autem reiciendis libero. Porro dolore quo ut rem pariatur libero quia. Dignissimos nobis eum autem voluptatem. Odit consequatur minima quas nesciunt. Et quod magni a sequi delectus. Quia at aspernatur tenetur est. Dolores expedita ducimus quia pariatur aut. Deserunt ab nam dolores quo sunt aperiam. A ea voluptatem fugit blanditiis numquam. Neque maiores dicta sint. Exercitationem labore praesentium excepturi rerum ipsam. Omnis porro quidem nihil beatae saepe. Placeat qui consectetur omnis velit sapiente maiores. Earum sint beatae quae odio quaerat et similique. Tempora rerum est sed aut voluptas vero. Et molestiae tenetur non dignissimos officia rerum.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle5','keyworks' => 'key works test asd5','description' => 'Tenetur fuga voluptas et minus. Ipsa voluptas ea rem eos. Et veniam tempore distinctio reiciendis.','img' => NULL,'type' => '1','number_of_comments' => '77'),
  array('id' => '201','user_id' => '3','title' => 'cypher6','slug' => 'cypher6','content' => 'Fuga molestias et ab porro qui omnis. Dicta dolor rem enim est cumque. At voluptatem porro ex nobis facere. Facilis officiis pariatur quia et. Minima quasi ullam consequatur est. Est sit necessitatibus qui qui. Repellat est dolor rerum quam distinctio cum possimus autem. Iusto quibusdam sint illo odio consequuntur omnis sunt. Consequuntur consectetur quos tenetur perspiciatis. Sequi explicabo consequuntur dignissimos reiciendis. Molestias libero accusamus perferendis quibusdam assumenda sapiente odit. Exercitationem expedita qui rem ex facilis aut nesciunt. Fugit molestias autem sed quis. Dolores voluptatum quibusdam omnis assumenda. Eligendi possimus velit tempora repellat ab sint. Et ipsam ratione sed minima et. Alias assumenda laboriosam placeat laboriosam laudantium. Quae a est non perferendis ut nam et. Et sint perspiciatis qui sapiente quam quis. Modi tenetur similique quasi doloribus repellendus officiis. Soluta exercitationem repellendus laboriosam minus aliquam autem perspiciatis. Tempora soluta sit numquam nemo quisquam qui explicabo molestias. Distinctio ea ullam aut veritatis. Rerum fugiat illum et blanditiis quia cum. Fugit voluptatibus accusamus iusto doloribus eum. Non ut doloribus tempore ipsum. Sit distinctio quia sequi deserunt enim reprehenderit. In voluptas ducimus quo aut autem non et. Iste modi explicabo fugit eius aperiam commodi. Amet corrupti quaerat doloremque iste error. Accusantium est quis iste sed optio enim praesentium accusantium. Accusantium est ipsam esse autem omnis ut. Blanditiis alias incidunt asperiores natus magni officiis saepe. Suscipit molestiae et aperiam sunt vero ducimus et. Porro quas perferendis amet quas veritatis recusandae. Pariatur et quisquam non. Qui est veritatis et perferendis voluptatum perspiciatis totam. Quia facere molestiae similique tempore corporis id. Voluptate dicta quis iste odit maiores. Ut velit perspiciatis pariatur sed. Consequatur qui eum repellendus qui magnam ut. Nihil ex nihil repellat ut. Quia minus quibusdam hic dolor occaecati dolorum qui eum. Hic non odio facilis animi nisi. Consequatur laboriosam eveniet laboriosam eos sunt qui. Modi sint ipsa quae reprehenderit ut. Quo quibusdam error nam porro consequatur eaque ut. Odit sapiente eius accusamus voluptates quidem accusamus. Unde suscipit harum veritatis aut. Voluptatem quia odit at pariatur. Reprehenderit est repudiandae omnis harum similique. Officia voluptas sit vel voluptate et velit tempore. Autem delectus magni eum sint. Doloribus beatae molestiae et. Aut ipsum quae magni. Voluptatem doloremque voluptas sunt ut tempora blanditiis laudantium saepe. Et doloremque explicabo voluptatem ipsum sed eveniet qui. Consequatur qui nihil qui. Consequatur distinctio possimus ex voluptatem omnis reiciendis. Maiores accusamus non animi pariatur quos pariatur et soluta. Qui autem eos sint est quia deserunt commodi. Sit ut molestias libero eius totam. Modi eos qui iste et et. Et ea hic dicta officiis. Qui unde qui voluptates. Facere consequuntur eum consectetur rerum. Facere exercitationem quod libero dignissimos totam facilis. Quo quo ad dignissimos ipsam consequatur et. Enim consequatur veniam sed. Qui provident labore et soluta quo fuga. In iste voluptatem alias sit. Totam quam aperiam unde quia dolorem distinctio. Consequatur ad voluptatem sequi repellat aliquam. Nulla enim rem ut eos voluptas. Dolorem qui nesciunt nostrum quae voluptatibus. Facere ut quia ullam voluptates nemo et ut. Nihil id laboriosam et ducimus dignissimos. Quidem ea adipisci dolorem corrupti magnam. Sed non quia cupiditate atque. Soluta aliquam quisquam perferendis laudantium est. Incidunt molestiae molestias dolor facilis. Quam eius consequatur sit maiores. Molestiae aut aut sed ea molestiae neque odio reprehenderit. Quam omnis molestiae officiis doloremque. Expedita recusandae animi et qui. Voluptas non consectetur voluptas vero dolore. Dignissimos expedita occaecati deserunt consequuntur molestias voluptatibus sunt. Modi sed et labore sint laboriosam. Harum vel provident vel magnam. Architecto omnis corporis ducimus distinctio ipsum est enim. Et ut fugit numquam molestiae sit deserunt. Cum est sed et qui. Assumenda dolore quia aut fuga quibusdam debitis doloremque. Tempore alias doloremque ut libero quo tempore ut. Mollitia sit omnis molestias nihil facilis. Animi ut voluptate ut qui. Ut possimus sit aspernatur ratione. Nulla est ad enim distinctio. Iure officiis minus est. Eos aut excepturi et. Voluptatem odit nihil cumque dolores. Amet quia porro consequatur soluta. Voluptates fugiat voluptatum voluptates dignissimos quis molestias sequi. Cupiditate minima eveniet qui non velit id. Debitis aspernatur quae repudiandae laboriosam. Ut qui ut eos itaque. Atque provident voluptas molestiae alias eaque ipsum aut. Id saepe eos iste. Repellendus repellat consequatur veniam voluptas odio iste.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle6','keyworks' => 'key works test asd6','description' => 'Quisquam molestiae eos quia. Accusantium omnis iure perferendis cupiditate.','img' => NULL,'type' => '1','number_of_comments' => '94'),
  array('id' => '202','user_id' => '1','title' => 'cypher7','slug' => 'cypher7','content' => 'Deserunt occaecati libero consequatur quis velit consequatur ratione aut. In illum nemo eum magnam praesentium excepturi veniam. Magni dolor omnis quas totam. Odit alias saepe consequuntur sint. Voluptatem ipsam voluptatibus eos est ipsa. Fugit maiores fuga unde adipisci. Quibusdam provident consequatur quis quisquam eveniet magnam. Harum ea eaque consequatur quasi itaque. Error sint quod quo suscipit et rerum. Aut quis itaque repellat quas. Quibusdam non molestiae dolor voluptatem. Itaque enim aliquam iste quas fugiat. Dolorum sit cumque suscipit facere aliquam nobis voluptatum vel. Fuga dolorem numquam quisquam impedit aut enim sapiente. Minus blanditiis est recusandae veniam saepe aut et. Et vitae rerum natus rerum. Vero minus rem harum non hic. Excepturi iure vero iusto nobis sed quia omnis. Illo praesentium qui eligendi voluptatem enim culpa. Dolores quia perferendis officiis rem. Modi magni ipsa quia non delectus natus quos et. Pariatur rem nihil ipsum cupiditate non. Non dolores fugit quam debitis consectetur. Tenetur consequuntur nulla excepturi quae sunt nostrum. Eos odio sit non eveniet ea nulla est. Veritatis deserunt libero quaerat veniam voluptatem. Distinctio sint delectus nisi quo. Qui eos et fugit consequuntur. Quod aut hic autem sunt. Natus qui ut possimus voluptas illum aut nihil. Ex asperiores nisi voluptas doloribus dicta blanditiis minima. Beatae possimus distinctio dolorem dolore ut eos et. Et libero et qui nostrum quo. Provident corporis ea quas sunt asperiores illum culpa. Occaecati incidunt quae aliquam consequatur. Iste id ducimus tempora provident nobis temporibus et. Ad non sunt eum nihil. Laudantium beatae nobis necessitatibus est. Velit autem sint nulla asperiores non. Et aperiam quaerat nulla ullam. A animi eum natus ullam consequuntur est consequatur. Vel minus id sed quo. Aut harum enim reprehenderit doloremque beatae voluptatem at molestias. Ratione laudantium aut labore aspernatur repellendus tempore. Et quia assumenda in eos asperiores. Aut voluptates dolorum accusantium voluptas sit impedit quo. Quam accusantium facilis excepturi ut aut voluptas ipsa et. Enim quo iste iusto vero animi. Omnis repudiandae quis qui neque. Ducimus sit nulla eius labore quia odit. Accusamus vero veritatis autem occaecati saepe aut quia. Quo unde consequatur consequatur praesentium minima cum libero nihil. Nisi natus reiciendis qui unde. Veniam aperiam dolores minima. Nihil autem et cumque vel praesentium et illum. Necessitatibus dignissimos odio et atque autem perferendis distinctio excepturi. Aut tempora sit ea et. Aliquid provident repellat magnam repellendus. Sit enim debitis enim aut culpa aliquam labore aut. Tempora et rerum accusantium sed et. Aut excepturi commodi quis ut. Maiores repellat voluptatem odit dolore exercitationem. Nesciunt est ea maxime ut quo. Quidem perspiciatis iusto modi sunt. Et est quis ducimus. Et ipsum quo magnam consequatur molestiae beatae eum. Dolores corporis et dolor corrupti quisquam voluptas. Odit consectetur ipsum ea voluptates qui amet repellendus. Dicta sunt et quod id tempora. Rerum delectus asperiores autem est nobis rem. Incidunt maxime sit voluptatibus maxime nam rerum adipisci architecto. Nisi repudiandae quaerat sit voluptatibus. Voluptatem omnis pariatur vel voluptatem ut sint rerum. Dicta quasi sunt nulla rerum hic ad. Ut ea sit est aliquam voluptatibus expedita ratione. Nam est odio nostrum voluptatum ea. Est dolorem ea rem qui eveniet ipsum eum neque. Est natus voluptate eius omnis a repellat. Deserunt provident enim eligendi voluptates et. Doloremque provident totam voluptas autem dolores temporibus. Cumque quia incidunt tempora sapiente architecto dolorem. Velit debitis autem sit aut voluptas autem dicta eligendi. Quis repellendus et corporis consequatur optio ducimus eaque. Et atque laudantium voluptatem quis praesentium vel. Et ea tenetur deleniti aliquid. Et quisquam qui nisi esse sequi laborum dolore. Ratione suscipit quibusdam esse quam. Dignissimos sint amet vel dolores tempora repudiandae. Quas qui nemo maiores sed iste repellat. Unde nostrum et rem necessitatibus. Voluptatem est ipsa veritatis sed deserunt eligendi nihil. Rerum quasi eum alias sequi dolores. Ut similique error recusandae dolorum quae necessitatibus. Soluta esse labore ab et est. Et et iusto ea ipsa enim ut quae autem. Dolorem praesentium similique voluptas vel recusandae et. Perspiciatis laudantium nemo quos velit. Unde rerum et et repellat. Assumenda quasi ea corporis consequatur fugiat sed magnam. Dolorem dolor fugiat unde hic veniam necessitatibus nisi et. Unde consequatur et est rem consequuntur. A voluptatem cupiditate qui sed debitis iusto et. Error qui sint quo odio aut eius perferendis. Maiores dolor dolorem in consectetur. Consequuntur laborum quae voluptatem dignissimos corrupti vel. Tempora debitis ut illo ducimus ea deserunt. Repellendus est et aut in placeat quis. Ut eligendi nostrum eaque quia est eos.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle7','keyworks' => 'key works test asd7','description' => 'Aut voluptas sequi aut eos. Veniam et mollitia laborum. Doloribus vitae ducimus optio cumque.','img' => NULL,'type' => '1','number_of_comments' => '85'),
  array('id' => '203','user_id' => '2','title' => 'cypher8','slug' => 'cypher8','content' => 'Earum est unde ut nobis quae. Error fugit qui quidem doloribus velit. Minima alias aspernatur est similique. Recusandae ut dignissimos ut qui. Eius aut velit aut quisquam nostrum asperiores. Possimus quis tempora alias totam. Laboriosam laudantium minima aut magni eos sit. Vitae sint nesciunt eos quisquam quam similique est blanditiis. Qui dolorum est possimus. Expedita est qui est laborum rerum in quo. Unde occaecati quasi dignissimos mollitia eaque. Eligendi occaecati recusandae nesciunt voluptates in. Voluptatem a architecto quos quo. Illum deleniti consectetur et et iure quia ratione. Qui temporibus ut incidunt. Occaecati similique excepturi itaque nihil sint suscipit. Sed dolores error et quos aut sint. Adipisci dolores sint non quo. Sed praesentium maxime id sit cupiditate voluptas rerum. Iste delectus adipisci dolores adipisci. Tempora error quasi ut eius aspernatur. Quia earum aspernatur corporis. Velit temporibus excepturi quam perferendis quae. Explicabo molestiae rerum enim vitae enim placeat. Beatae ea sint illum enim omnis hic corrupti dignissimos. Dolor quia nulla quia. Veniam totam ut ut vel accusantium eos. Necessitatibus ratione laudantium consequuntur consequatur unde. Dolores dicta doloribus assumenda quis expedita. Reprehenderit et aut incidunt aut laudantium asperiores aut. Autem dolorem quaerat omnis quis vero. Sed aut et voluptatem ut accusamus perferendis. Dolorem ut cumque debitis reiciendis fugit cum adipisci. Corporis amet est maiores dolore soluta laudantium. Modi voluptas provident qui enim consequatur rerum. Eum quos tempore officia. Est sequi sit libero sint ipsam dolores. Qui minima excepturi ipsum voluptates. Et odio hic similique delectus deleniti ut. Voluptatem odit velit debitis ipsum quibusdam atque omnis. Consequuntur tempora provident et ut ut. Vel provident quis impedit at enim alias. Aut sapiente et officiis sequi. Eveniet iure debitis et explicabo accusantium hic placeat est. Cumque inventore iusto sed exercitationem. Temporibus laudantium deserunt aut est magnam. Quia tenetur maiores quia. Sunt tenetur voluptatibus ut iusto nesciunt non. Sunt voluptatibus aliquid ratione occaecati est cumque ut. Qui sint nulla reiciendis magni qui nesciunt. Et delectus eum doloribus consequatur exercitationem in officia. Repellendus reprehenderit at accusamus qui dolorem maxime maxime ut. Repellat error ut accusamus et odio error illo placeat. Quas a et laudantium enim quasi nulla iure enim. Cumque a voluptates est consequatur cupiditate dolor. Eligendi aut odio eos debitis nulla aliquid qui. Fugiat repudiandae voluptatum velit. Numquam vel dolorem vel id eos. Laborum aperiam occaecati laudantium sunt. Et quia est sit aliquam dicta aut laborum. Similique necessitatibus ipsa rem officiis. Distinctio optio quibusdam commodi quod autem. Ullam accusantium nemo ut laudantium. Minus modi quod enim sed officia in nobis. Incidunt cupiditate quasi est deleniti veritatis minima placeat. Iusto vel provident consequatur facilis. Et ut voluptas fugiat quo reiciendis necessitatibus. Explicabo et est quidem aliquam pariatur. Animi enim vero perspiciatis aliquam quia. Maxime et est suscipit quae. Ipsam veniam atque sapiente minima voluptates ut nesciunt eum. Qui laudantium illo praesentium et. Cum eum dicta dicta est rerum dicta velit. Vel ea consectetur alias non sunt ducimus consequatur. Veritatis error doloremque doloribus. Numquam alias nobis nam fugiat praesentium quod architecto. Ratione ea delectus veniam in hic eum praesentium. Adipisci eum corrupti recusandae expedita unde vero. Velit quibusdam voluptatem cumque. Quo cumque delectus explicabo delectus consequatur officia molestias. Molestias a ratione sit illum blanditiis culpa. Quo autem autem modi nam ut accusamus ex. Aperiam velit est in ut temporibus id. Tenetur qui eum incidunt quisquam sequi labore. Ea quisquam vel sapiente ut commodi quia. Voluptatem aliquid in nisi sint natus esse nulla. Qui eveniet est cumque repudiandae sit ut. Quaerat laborum maxime omnis ipsam expedita voluptatem. Quasi voluptas aut tempora molestiae. Eveniet temporibus eligendi modi laborum iste quis enim. Omnis ex sint sapiente. Nisi id itaque porro cumque excepturi. Et omnis corporis sed culpa consequuntur. Provident qui voluptas quibusdam. Qui animi nesciunt aut qui aut perspiciatis dolor. Non nulla aut voluptatem ipsum delectus. Sint tempore mollitia sed eveniet recusandae quo qui. Incidunt sequi ab ut repellat molestiae voluptatem nihil. Et quis ab expedita commodi rerum. Et repellendus sed eum beatae voluptatem aut omnis. Voluptas nesciunt nihil et et. Laborum voluptate in ducimus vitae sunt. Eaque tempore quia minima occaecati consectetur rerum. Cumque porro facere voluptate quia et et sint. Unde in quisquam maxime molestiae voluptatibus officia. Blanditiis iste iure perspiciatis.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle8','keyworks' => 'key works test asd8','description' => 'Omnis laboriosam non saepe optio autem. At aliquam et quas ratione alias quos.','img' => NULL,'type' => '1','number_of_comments' => '96'),
  array('id' => '204','user_id' => '1','title' => 'cypher9','slug' => 'cypher9','content' => 'Ipsa ipsam in omnis fuga vitae recusandae quis. Adipisci voluptas quis eum facilis nisi laudantium quia. Qui ut voluptatibus voluptate eveniet non. Ea impedit facere rerum. Facilis exercitationem temporibus quibusdam. Maxime sit omnis et libero eos. Perspiciatis accusantium laboriosam et similique quasi repellat iusto. Consectetur similique perferendis atque voluptate. Fugit possimus et tenetur ipsum enim. Inventore nisi molestiae ipsa alias porro. Sunt necessitatibus reprehenderit quasi qui odit exercitationem. Sint vero non numquam consequatur. At adipisci perspiciatis animi inventore. Similique assumenda recusandae veniam explicabo. Ab pariatur animi dolores eaque. Culpa et totam in dicta. Maiores et aliquid ut sit doloremque mollitia. Quos officiis beatae eos temporibus. Qui fugit dolores ipsum architecto nesciunt. Voluptate id omnis recusandae non. Adipisci veniam dicta necessitatibus et fugiat ut. Enim quis quia aut nam est maxime. Nihil quia qui ut sapiente recusandae. Corporis quis dolore dolorem ut vel. Architecto voluptatem voluptate officia nobis. Non est corrupti blanditiis doloribus quod voluptatem. Placeat laboriosam placeat repudiandae est suscipit laudantium harum maxime. Molestiae ratione eum aut ut nostrum. Aut aut et ducimus debitis aut. Rerum minus voluptas et perferendis assumenda. Vitae autem quis earum et quod. Quia doloremque dolorem illum. Eos et aut laborum sunt et. Ut et quas praesentium delectus. Consequatur error omnis deleniti veniam. Qui placeat dolore saepe quia reprehenderit suscipit est et. Quia blanditiis enim fugit dicta dolorum omnis sequi. Vitae magnam temporibus non similique. Porro ad harum et. Voluptatem non iure cumque nemo quo nobis recusandae quia. Eveniet quae quasi aut ut vero consectetur. Dolore eos minus et ut sit aliquam eaque. Et id voluptas voluptas consequatur tempore facilis. Vel laborum asperiores aliquam minima assumenda aut eaque. Et aliquid adipisci deserunt magnam velit nisi temporibus. Earum amet ad sunt asperiores maxime illum rerum. Voluptatum quia qui necessitatibus qui est qui ut laborum. Delectus consequuntur ut itaque harum earum. Excepturi soluta vitae quam ut nulla a saepe. Est minus impedit sit necessitatibus facere consectetur sint voluptatibus. Cumque est repudiandae magnam perferendis enim et nihil numquam. Omnis odit cum nemo ipsa sunt et accusantium. Sit corporis veritatis in aut et incidunt quo. Maiores est dicta suscipit illo sit. Vel sint aut reiciendis. Qui quasi repellendus autem et consequatur labore velit. Autem dolorum autem dolores iusto. Vitae harum ut accusamus exercitationem velit omnis sit eius. Quis occaecati aperiam assumenda quia dolores quae. Amet odio quod necessitatibus et et omnis. Modi laudantium tenetur sunt ea. Commodi placeat qui est explicabo explicabo numquam sapiente veniam. Mollitia magnam eos nobis qui nesciunt. Reprehenderit assumenda molestiae eligendi. Minima temporibus molestiae magni. Laborum saepe inventore soluta alias voluptate. Cupiditate qui qui aperiam sunt. Vitae molestias repellat magnam enim et. Velit aliquam fugiat ipsa. Et inventore qui commodi. Totam voluptatem aut accusantium perspiciatis reprehenderit aliquid reprehenderit. Blanditiis vel iste voluptate magnam dolorem. Quo odit id voluptas voluptas. Quis facere nostrum et ex accusantium quia. Molestiae ad distinctio voluptas hic ipsum officia. Maiores soluta repellendus ut quo et nihil enim a. Qui ab error temporibus necessitatibus. Vitae quaerat nesciunt ad reiciendis et velit voluptatum nisi. Totam velit architecto necessitatibus eius. Animi officiis in itaque doloribus et error non. Est quia deserunt incidunt vel. Voluptatem quia dolor autem quis. Aut dolorem distinctio velit dicta aut et. Enim quis dolorem voluptates iste aut similique ut est. Sint dolor nam fugiat. Voluptatem aperiam nihil voluptatem et quo rerum. Doloribus dolore maxime molestiae dolorem nam similique quis. Sit repudiandae cupiditate ipsam at. Officiis vitae adipisci nihil eos est pariatur autem. Id facilis molestias eveniet. Nam eum et et voluptatibus dolore et itaque. Cumque voluptatem ipsa sequi quaerat ipsum ea. Natus alias id deserunt et voluptates. Architecto id officia vel molestiae voluptatum. Modi voluptas illum doloremque itaque fugiat sit. Laudantium dolorum facere quas non. Cupiditate fuga sint soluta voluptatem voluptates dolorem. Id itaque nihil delectus nisi. Aut rem labore consequatur earum pariatur earum dolor ipsa. Possimus laudantium vitae debitis atque voluptatibus. Accusamus et placeat et aut. Dicta voluptatem numquam assumenda aut magnam sed sed. Quia magnam voluptas est quam. Perspiciatis ipsum in recusandae dolor et. Magnam sapiente quo non eligendi rerum voluptas id. Cumque architecto soluta qui modi aut dolorum cum. Fugiat mollitia laboriosam sed inventore omnis officia placeat. Itaque ut culpa et unde sit. Itaque aliquam voluptatem dolorum quas laborum.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle9','keyworks' => 'key works test asd9','description' => 'Est vel culpa temporibus enim doloribus et distinctio. Quos nam saepe cum veniam est alias.','img' => NULL,'type' => '1','number_of_comments' => '80'),
  array('id' => '205','user_id' => '2','title' => 'cypher10','slug' => 'cypher10','content' => 'Odit odit porro numquam ut aliquid a laboriosam. Laborum quaerat minima totam molestiae. Libero voluptas magnam voluptas placeat fugiat. Architecto et ut ut exercitationem vel recusandae. Veniam eos quis non. Et quia consequuntur non nobis. Ipsam consectetur qui quia consequatur quia. Voluptatem voluptates quo rem et et minus unde. Totam reiciendis placeat vitae. Necessitatibus quo aspernatur tempora maxime repellendus. Quisquam unde et voluptatem ratione labore officia. Qui saepe expedita aut commodi hic. Cum qui ex quis ipsa corporis doloremque. Quidem eum libero pariatur est labore. Pariatur ut est asperiores assumenda. Reiciendis doloribus in culpa vitae. Iure distinctio iusto debitis iusto quidem ab facilis. Cum accusantium voluptatem exercitationem blanditiis illum perferendis eaque. Voluptatem consequuntur quibusdam magnam numquam eius excepturi. Non aut voluptatem molestiae eaque explicabo ea odio nihil. Explicabo qui est qui magnam non placeat. Deleniti aut est et assumenda consequatur delectus. Eaque perferendis pariatur ut optio autem voluptatem. Qui quia sed natus dolorem tempore minus impedit. Aut facere repellat eos necessitatibus. Perspiciatis aut natus molestias perspiciatis repellendus eum iusto excepturi. Est omnis totam ducimus earum et aut ullam. Quas illo porro quo officiis dolorem. Est non minima ut et. Incidunt illum et exercitationem voluptatem ratione ipsa quo dolorum. Sed nulla possimus unde pariatur dicta vero. Maiores similique neque itaque dicta inventore fugiat. Aut sint velit quam labore incidunt quia. Consequuntur qui aut quo minus explicabo dolor magnam. Quo optio qui ipsam ut voluptas voluptatem saepe. Fugiat aut sapiente sint. Maxime eaque minima ut suscipit corrupti ut. Voluptatum et deserunt ab totam qui sit excepturi. Ducimus et fugiat nemo expedita. Rem suscipit incidunt incidunt dolorem aut nostrum. Eos recusandae repellat consequatur. Perferendis voluptatem vitae sed eum officiis aut optio eos. Architecto iure eius numquam provident accusamus. Aut qui explicabo non non sit enim. Voluptas autem magni accusamus nam. A ut labore qui atque modi facilis. Est minus qui distinctio qui sunt fugiat nihil distinctio. Quos qui vero labore maiores quaerat aut rerum fugit. Labore cumque officia sit eum est repellendus molestiae. Debitis cum inventore impedit reprehenderit corrupti debitis. Quia repellat ducimus ad vel ut magni vitae laborum. Accusamus iusto ipsam voluptates dolorem praesentium. Fuga ut neque necessitatibus eveniet. Quo voluptas molestiae adipisci nobis atque et. Quis molestias vel architecto eaque voluptatibus labore. Consequatur assumenda eius numquam labore ut esse iste voluptate. Nesciunt odio rerum quis molestiae accusantium harum et. Voluptatem adipisci architecto sint tempore rerum placeat. Qui voluptatem adipisci quis sint. Velit recusandae ab sunt voluptate. Magni quia omnis repudiandae. Provident accusamus sit possimus voluptatibus eius est. Consequatur dicta ullam officiis aliquam ea sint repellat. Qui expedita quia est dolorem pariatur culpa nostrum hic. Numquam voluptas voluptatem rerum qui. Eos tempora provident laudantium enim optio magni. Error distinctio quibusdam exercitationem accusamus rem. Quo nobis velit error ut. Cupiditate natus ullam numquam ipsam impedit aut. Commodi ad expedita labore impedit et temporibus. Modi odio et totam repellat temporibus. Laudantium ut autem et molestias vero omnis eum. Ea quod in cupiditate est fuga odio itaque. Accusamus repellat quo dolor. Ipsam aliquam aspernatur facere dolores sequi amet deleniti. Sint nisi perspiciatis eveniet unde. Ullam itaque non non eum atque. Dolores ad dignissimos quaerat non omnis. Ullam qui culpa quia delectus minima et dolor. Eligendi sed quas sed. Qui rem fugiat odit totam quisquam animi sed. Doloribus quo vel tenetur distinctio ut unde similique. Sit odio nihil error officia accusamus non quis. Odio iusto ut in rerum incidunt. At nemo consequatur vel voluptatum qui dignissimos. Saepe neque natus non et distinctio laudantium repudiandae. Quam est aliquid qui non corporis. Et reiciendis asperiores nobis sint. Nesciunt necessitatibus quidem dolor asperiores. Itaque quae incidunt ut ea explicabo in facere natus. Deserunt corporis quisquam rerum sit fugiat dolorum. Placeat culpa rem voluptatum vitae distinctio. Aut quam autem enim officia voluptatem optio. Illo at id ipsum. Ut nam ut consectetur molestias. Autem minus qui eaque aut voluptas expedita non. Pariatur architecto asperiores similique rerum ipsa. Libero aut molestiae repellat maiores. Deserunt quisquam suscipit nemo. Quisquam tempora vel vel ratione aspernatur et. Repellat quos eius dolorum officiis ex id. Earum delectus qui velit totam sequi. Voluptate natus aspernatur deleniti est. Modi laboriosam aliquam in esse vero veniam. Qui illum modi ipsum et voluptatem exercitationem. Corrupti quasi ipsa eos.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle10','keyworks' => 'key works test asd10','description' => 'Consequuntur consectetur aut dolor tempore ducimus. Magnam adipisci id nisi quibusdam.','img' => NULL,'type' => '1','number_of_comments' => '86'),
  array('id' => '206','user_id' => '1','title' => 'cypher11','slug' => 'cypher11','content' => 'Perspiciatis quo dolores ex quos quas dicta dolorem ullam. Nihil sequi culpa consectetur dolore repudiandae excepturi quia odio. Dolorem deleniti praesentium sint rerum nemo. Vero rerum ad temporibus eveniet. Aut harum placeat laudantium sint magni. Sit dolorum aliquam nobis magni quam maiores dolor aut. Deserunt ad sunt rerum aut doloremque iure et. Eius sit molestias facere non. Qui atque sint ut veritatis ipsa a. Et consequatur fugit ex magnam et temporibus. Non accusantium minus non iure sint voluptates. Et est incidunt qui quaerat. Esse qui sit maiores et itaque aut est odio. Debitis vitae dolorem eius ab quod. Asperiores nulla voluptatem quaerat. Fugiat eius sed eos vel odio dolore culpa dolor. Dolor rerum sequi totam. Facere nihil sequi quos hic. Assumenda qui magnam nesciunt dolores. At voluptas velit consequuntur eos voluptatem rem. Nam ratione nulla in. Dolore enim laboriosam est ducimus illo in sed. Non at enim et dignissimos provident omnis quos. Vel totam consequatur molestiae beatae voluptas voluptatem reiciendis. Labore voluptatum error alias reiciendis sunt dolorem. In amet voluptatem est nam explicabo. Vitae incidunt esse est occaecati voluptate. Aliquid facilis unde omnis voluptatem neque fuga. Ipsam sunt similique optio earum magni necessitatibus et. Sit expedita animi magnam aut. Fuga aspernatur aut non ut inventore placeat. Mollitia iusto quasi libero cumque. Cum harum tempora unde ipsa magnam architecto. Nobis odit ad ut enim officiis. Ipsa vel quasi natus magni debitis. Quis autem rerum ad consequuntur. Quam ipsam cupiditate id ex voluptatem pariatur. Voluptas ad reiciendis expedita aut eaque quibusdam aut tempore. Impedit consectetur consequatur vel perspiciatis suscipit rerum debitis rerum. Porro necessitatibus voluptas dolores itaque aut et illum. Nostrum et ullam in nobis voluptatum et ullam. Aut officia temporibus quis nihil repudiandae recusandae distinctio nesciunt. Rerum fugit consequatur voluptates voluptas quo. Qui cumque sunt pariatur voluptas nesciunt quam. Voluptatum vero tempore maxime earum accusantium natus. Suscipit eveniet voluptatem voluptatum. Voluptatum iusto hic nesciunt vel ut voluptatem veritatis. Omnis exercitationem pariatur dolor quidem aut rerum deleniti. Facilis delectus eius aut qui. Excepturi magni dolorem animi dolor perspiciatis temporibus. Explicabo totam illo eos et aut veritatis eos beatae. Dolorum reprehenderit et aut est culpa animi. Asperiores vitae quis dolorum. Ab quam accusamus omnis neque. Numquam modi voluptas aut saepe necessitatibus sit aut aperiam. Vitae non quaerat est sapiente. Omnis fuga et impedit ipsam iste recusandae assumenda in. Id excepturi quo qui quibusdam est. Ea commodi molestiae quia velit recusandae voluptatem corrupti. Nulla quia provident beatae ut. Dolorum sint veniam eos labore cupiditate aut dolore. Quia dolor dolorem tempora impedit. Sequi et quis temporibus sed quam quod provident. Quia animi porro reiciendis saepe qui. Iusto quos explicabo harum voluptatibus harum. Ducimus non commodi laborum quasi et quas corrupti. Voluptatem vel voluptas vero eveniet enim autem sint eveniet. Molestiae dolorum voluptas et quod optio id iste. Alias enim odio est. Quia est fuga quia eum possimus non. Accusamus voluptatem qui qui autem et quo dolores. At labore fugiat tempora non architecto quo velit. Delectus modi quod aut aut et corporis animi. Enim esse quasi quo quis dolor saepe. Et corrupti ut occaecati officiis maiores. Sed modi qui doloremque omnis dolor quasi. Blanditiis id aut voluptatum consequatur mollitia voluptas eveniet. Quaerat veniam aperiam corporis asperiores quo. Consectetur quo blanditiis tempore suscipit dolore id. Et aut sapiente animi ea adipisci repellendus quia. Occaecati rerum cumque autem earum quis enim aut autem. Id velit et consequatur sit eligendi qui ut in. Expedita molestias delectus accusantium aspernatur aut corrupti ipsam. Quo quia nostrum ipsam vero possimus culpa quasi facilis. Rerum et rerum quo blanditiis odio. Maiores eum porro aspernatur mollitia. Doloremque delectus repellat reiciendis est. Recusandae excepturi iusto dolores et et libero voluptas. Repudiandae nihil repellendus qui quos facilis doloremque suscipit temporibus. Ex autem voluptatem animi quidem. Commodi facere quidem officia atque distinctio. Saepe nam adipisci quia dolores omnis ad. Ducimus inventore delectus doloribus iusto nihil. Itaque explicabo culpa vitae voluptates pariatur dolor odit. Odio illum repellendus ut. Et quasi consectetur quis quam. Aut in quod ea adipisci et aut. Quaerat soluta eveniet ratione labore possimus quisquam harum. Tempore saepe sunt eius numquam est modi. Ducimus et sed quod ad officia eum. Et non alias optio sint perspiciatis. Ullam et nihil magni voluptatibus dolor. Excepturi nisi consequuntur quis aut in. Omnis cum sunt vel aut ratione vel et.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle11','keyworks' => 'key works test asd11','description' => 'Est est eum facere sed aut aut animi. Neque in et sit atque odit nobis.','img' => NULL,'type' => '1','number_of_comments' => '99'),
  array('id' => '207','user_id' => '2','title' => 'cypher12','slug' => 'cypher12','content' => 'Labore quis numquam veniam eum. Quae vel officiis sed quos iste voluptas. Rerum molestias sunt corrupti. Laborum esse repudiandae atque molestiae dolor. Ipsam enim eos aliquam. Consequatur ipsam iste animi possimus dolores magnam sed quia. Dignissimos harum dolor repudiandae rerum. Nulla qui sed voluptatem veniam est sed. Animi assumenda ipsam aliquam ut ducimus enim reprehenderit. Voluptas quia rerum aut eum et cumque. Mollitia molestiae quo cumque magnam qui. Ut qui id enim et omnis. Voluptatum iure quaerat quia corporis cupiditate quasi in ipsum. Id distinctio non id eveniet autem dolor tempora. Eum consequuntur eos natus delectus. Perspiciatis exercitationem explicabo fuga neque quasi omnis officia. Perspiciatis possimus facilis est aut qui. Dolorem est harum voluptatem mollitia. Qui voluptatum velit doloremque nihil sit quos. Nostrum repudiandae quis aut rem consequatur autem labore. Et repellendus illum aliquid non sequi est aut. Soluta dicta consectetur voluptatum autem. Voluptate dolorum non voluptas in tempora. Quam dignissimos qui quia velit deserunt. Corporis eaque perspiciatis omnis inventore et aut. Nihil rem numquam voluptas aliquam quam. Aspernatur dolores nostrum ut corporis deserunt nihil. Ut tempore maiores ut dolorem. Ea repellendus minus nemo aliquam est perspiciatis. Nisi beatae magnam ut assumenda ipsum. Consequatur necessitatibus qui omnis qui omnis dolorum aut. Asperiores earum facilis accusantium et. Qui voluptatem atque dolor ipsam. Et in repudiandae eum et omnis maxime. Reiciendis soluta cumque molestiae quia dolor dolores maxime cupiditate. Cum ex ipsam odit. Et et deserunt aliquam sunt similique. Unde ipsam dolorum dolores. Blanditiis eveniet sint non ipsam ducimus aperiam esse. Magnam sunt quam totam autem excepturi ducimus. Labore consequatur quas qui ea nobis. Temporibus iure ut molestias dolor aperiam eligendi. Et earum nihil vitae aut qui a maxime doloribus. Facere praesentium porro earum impedit. Occaecati neque quis distinctio est. Dolor id a illo eum iusto autem. Dicta itaque vitae vel quasi labore repellat nobis voluptatem. Ab ducimus et quod repellendus. Officia est nam inventore earum. At facilis sint ut saepe reprehenderit recusandae. Quam accusamus quod sint qui esse doloribus id. Eos voluptas modi reiciendis dolorem. Sunt magnam quidem ad aut. Placeat voluptas ipsam sed recusandae. Est quaerat enim quia. Sint dolorum consequatur nisi odio in est sit vero. Cum quam voluptates ipsum alias. Iure magni sed consequatur ipsa. Vel dolores reiciendis enim deleniti nobis asperiores. Est assumenda molestiae enim ipsum. Sed laborum iste magnam cupiditate. Adipisci nihil sed delectus. Quod dolor veritatis ut omnis nemo eos quis. Error ipsum ratione tenetur odit cupiditate temporibus. Sint aut voluptatum rerum velit delectus. Aut voluptatum et aperiam aliquid. Consectetur voluptate amet alias. Pariatur vitae nesciunt dolorum sit. Fugit numquam consequatur reprehenderit necessitatibus. Quibusdam modi ducimus qui nihil ratione. Dolor est quis necessitatibus vitae porro. Dicta excepturi nulla eveniet hic id autem. Voluptate consequatur voluptas adipisci quia. Optio facilis eum dolorem laudantium doloribus cum est. Sit itaque recusandae deserunt expedita nisi sapiente ad. Eos voluptates aperiam harum ut voluptatibus. Veniam dolor ullam exercitationem. Dicta reiciendis veritatis alias non non aperiam autem. Similique at reprehenderit dicta blanditiis culpa. Veritatis neque vero porro quisquam explicabo. Rerum dolor pariatur pariatur at. Ut impedit deserunt consequatur explicabo ipsa officiis. Occaecati dolorem doloremque inventore et quia est in tenetur. Qui quo excepturi incidunt error facilis. Ut quia voluptas ducimus. Tempora assumenda ipsum sunt. Quidem rerum rem et dolorem provident aliquid et. Quasi voluptas repellendus nemo et laudantium inventore ut. Quae commodi unde distinctio repudiandae. Tempora voluptas et tempore enim sed illum et dolorum. Ex sit consequatur atque est voluptatem modi. Quia labore vero esse ut. Dolores natus dolorem id aliquam culpa eos neque. Ab deleniti facere qui. Sequi molestiae rerum modi eum. Eum enim accusamus vel veritatis. Occaecati dolores architecto dolorum est eos excepturi. Molestias rerum et aut non et quae. Molestias exercitationem deserunt aut explicabo. Ut quaerat occaecati eos possimus eaque aspernatur. Velit fuga aut ut non pariatur et quam. Omnis enim rem corporis amet id quisquam fugit. Natus quia nemo nihil voluptas nam ipsa eligendi. Voluptas qui sit minima voluptatibus. Laboriosam consequuntur placeat vel. Ut et qui aut quod. Id ut optio et pariatur. Sunt illo illo nemo id in pariatur quos asperiores. Est soluta quia sed. Harum velit pariatur et facere expedita architecto. Consequatur qui et eveniet quidem. Laborum et consectetur excepturi qui. Placeat impedit est velit et et quos. Et omnis dignissimos unde iure et nulla. Eveniet ut dolorem delectus sint voluptas vitae.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle12','keyworks' => 'key works test asd12','description' => 'Omnis soluta maiores aut molestiae. Qui ut itaque est eos qui.','img' => NULL,'type' => '1','number_of_comments' => '90'),
  array('id' => '208','user_id' => '3','title' => 'cypher13','slug' => 'cypher13','content' => 'Optio enim ut dicta iure. Omnis a repellat velit. Aut sequi quia aliquam quo voluptatum. Alias ipsam qui magnam repellat odio quis qui. Voluptatum ut earum architecto placeat in qui. Quasi et dignissimos quam aliquam. Eligendi rerum aliquam atque voluptatum voluptatibus. Beatae alias quia dolores inventore corporis id dolores ea. Libero rerum est quia. Ea rerum corrupti consequuntur natus. Quas voluptas nihil et possimus eaque quam deleniti. Aut est a omnis eius ut. Cupiditate distinctio nam consequatur dolorem. Eveniet reprehenderit sit nisi laudantium. Incidunt sit et incidunt ipsam dolorem eos non. Laudantium voluptatem dolorem nihil ut ut quidem praesentium. Deserunt consectetur molestiae omnis est omnis impedit. Enim ut aliquam eaque. Corporis assumenda dolorem nam vel. Unde est reprehenderit officia. Porro totam et eaque veniam modi magnam. Cupiditate quas facere adipisci adipisci iste dolorem. Delectus a quo consequatur recusandae quasi non. Eum autem nihil non aut ab reprehenderit facere harum. Sed accusamus officiis ea. Ex distinctio et enim et doloremque odio animi quidem. Labore sit debitis voluptatem nostrum ex quidem. Cumque nulla occaecati et reprehenderit nam nihil hic. Aliquam commodi facilis laborum illum sit officiis. Culpa dolore nesciunt et tempora et magni quidem assumenda. Voluptatibus eum voluptatem illo doloribus unde consequatur est ducimus. Sed vel tempora et eum mollitia iusto delectus. Rerum quia debitis id aut dignissimos eius dolor. Qui ratione minima culpa ut. Accusamus nihil ducimus consequatur aperiam. Rerum aut consequatur debitis necessitatibus nulla nam dolore. Dignissimos provident qui dolorem voluptatem eveniet quisquam aut. Corrupti blanditiis amet quos. Adipisci officia voluptas dolorem et est natus iure. Et iste voluptatem harum non. Provident voluptatibus fuga officia libero harum dolores. Mollitia esse autem numquam vel quia. Eligendi veritatis fuga voluptatem iste. Numquam explicabo maxime sed libero aut. Voluptas iste deleniti voluptas mollitia delectus molestiae voluptate. Eveniet mollitia harum et nihil. Modi est repellendus voluptates assumenda et. Omnis optio voluptatem qui quis. Ut laboriosam velit deleniti blanditiis labore quis tempore. Cumque dolore aspernatur corporis nulla vel accusamus. Dolores est esse magni ratione. Et error velit qui tempore. Ullam qui perferendis odio dolores porro. Atque fugiat delectus voluptatem dolores qui. Et omnis repellendus et est. Assumenda ut aut qui ex ullam. Nihil illo natus a officia possimus natus. Animi quia sed aliquam veritatis non illo sapiente. Illum dolorem enim cum error. Cum itaque iure aut ipsa harum corrupti libero. Aspernatur et maiores et saepe. Perferendis fugit quis natus cumque error error accusantium. Tempore eos deleniti soluta earum consectetur. Magnam voluptatem magni doloribus deleniti repudiandae debitis. Et quia ipsum nemo eum. Provident eos id recusandae fuga repudiandae. Voluptates alias nam facilis pariatur asperiores aut. Consequatur quam labore porro sed et. Velit amet sit tenetur rerum officiis nobis mollitia. Corrupti labore laudantium ipsam iure. Aut adipisci nulla fugiat sit exercitationem asperiores. Sint sint qui deserunt magni ipsum ratione. Tempora rerum natus harum dolores fugit eveniet ipsam quod. Reprehenderit alias laudantium unde cupiditate quam et. Minus cum magnam neque eius nihil. Non sit quo ipsa veritatis. Placeat dolor occaecati quae dignissimos laborum. Architecto similique assumenda ut qui vero. Architecto dolorem et adipisci aspernatur ut. Aut aut quo sit at sint. Expedita hic autem dolorem. Et doloremque sed praesentium voluptate. Deleniti facilis voluptatem non voluptatem aliquam harum. Et aspernatur voluptas molestiae sapiente quia. Et unde ullam fugiat laudantium quia quia qui. Ut vitae consectetur illo doloribus molestias. Officiis molestiae impedit eum consequuntur maiores qui. Quam id cum inventore. Molestiae laboriosam quia est mollitia nobis vel. Harum odio optio error. Facilis non a ut voluptas accusantium qui et. Consequuntur rem accusamus fuga sunt ut. Voluptatem enim et assumenda cumque minus nisi repudiandae. Voluptatem quae possimus repellat. Eum nam at veritatis molestiae sapiente perspiciatis velit. Excepturi aliquid sit ab est veniam ipsam. Voluptatibus accusantium molestias facilis excepturi eius sed. Repudiandae aliquam fugit aut. Quia beatae possimus alias quis velit dignissimos tempore voluptatem. Consectetur minima quod quam quod. Et perspiciatis sint labore blanditiis. Non et iste natus voluptas. Occaecati earum consequuntur sit et corrupti voluptates accusantium. Officia similique quis labore ipsam quod beatae omnis. Mollitia ducimus facilis quia quos aliquam. Nobis sapiente ea amet. Vel eligendi fugiat consequatur tenetur fugiat omnis. Saepe eum est necessitatibus ullam qui. Officia soluta rerum officia labore et eius. Est sapiente atque est id tempora. Consequatur eligendi eveniet neque tempore dolorum occaecati.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle13','keyworks' => 'key works test asd13','description' => 'Praesentium dolor rerum fugiat. Impedit ipsa similique ipsa nesciunt.','img' => NULL,'type' => '1','number_of_comments' => '86'),
  array('id' => '209','user_id' => '1','title' => 'cypher14','slug' => 'cypher14','content' => 'Et quis vel ut voluptatem velit debitis. Ut sit id sint quaerat aut natus. Rem dolores sed molestias. Ut sint totam dolorem voluptate. Esse enim voluptatem earum. Omnis reprehenderit dolor et iure ea. Nam quo veritatis sapiente est et ad. Et molestias veritatis eum neque qui. Ex debitis architecto similique provident id. Facilis consequatur quibusdam et quia quis laborum at. Libero in nesciunt est sit nobis voluptas dolores in. Molestiae enim quod incidunt et ipsam. Aut animi voluptates deserunt cumque culpa. Beatae at quis temporibus ipsum quia vero saepe. Ut sapiente et qui. Ab similique quis repudiandae magnam corrupti sit et. Qui possimus molestiae laudantium rerum quia possimus hic. Officiis molestias nam facere totam. Labore quia nostrum sint voluptas quae qui earum. Laudantium ut exercitationem illo. Aliquam enim aperiam qui eum. Omnis quia dolor magni ut quia. Ratione laborum quo et et consequuntur facere sit. Qui laboriosam molestiae qui ipsa. Ut id molestiae nemo corporis ut veritatis. Minus explicabo facilis aut aut consequatur itaque doloremque. Et deserunt laborum culpa. Recusandae occaecati neque distinctio exercitationem dolores et adipisci. Incidunt nesciunt magni omnis saepe et et. Asperiores totam animi aspernatur hic. Soluta rerum facilis quam deleniti aut nam. Praesentium ut nulla nihil velit et iusto. Voluptate vel eum consequatur ad maiores deleniti. Voluptatem nihil et et. Quidem et quasi velit et perferendis minus ut. Vero amet sit tempore aut consequatur amet. Et quibusdam qui aspernatur corporis ad commodi dolores cupiditate. Distinctio est suscipit id quia vitae. Nam provident perferendis non perferendis. Quo voluptatem voluptatem labore unde dolorem sed dolorum. Iure doloribus qui consequatur aspernatur. Eius odio occaecati ipsam exercitationem culpa occaecati aut corrupti. Est voluptate tempora doloribus officiis. Facilis fuga omnis dolores tempora ratione ratione eum. In praesentium nihil dolorum. Molestiae earum vel temporibus autem qui. Sed expedita sit nihil porro expedita ipsum. Quia ad et qui deserunt est esse ea. Laboriosam magni quis architecto ex dignissimos nisi id. Dignissimos ut animi et nesciunt aut autem consequuntur. Doloribus et consequatur inventore porro ex quia dolorem. Esse voluptatum corrupti sed aut necessitatibus. Ut nihil porro odio rerum. Voluptatem deleniti est consectetur inventore rerum dolor facilis. Enim nulla ad aut dolorem. Atque qui est sunt vero. Optio iusto in error omnis aliquid minima harum aliquid. Quod recusandae ut ducimus quia necessitatibus voluptas ad minima. Voluptatem tenetur velit ex est voluptatum fugiat. Eos repellendus voluptas nihil tenetur mollitia ex facere. Laboriosam ea excepturi consequatur eos quibusdam et. Dolor eos ipsum necessitatibus aliquid molestias tempora ex ullam. Quaerat molestiae impedit et rerum vitae. Aut blanditiis omnis est nulla sit. Eius eius dolorem et dolorum animi pariatur. Voluptas et placeat ipsum quidem dolores. Quia voluptatem quia aperiam dolor. Dignissimos sed suscipit error et. Cum recusandae ea iste eos officia. Accusamus omnis ducimus laudantium amet ut quia. Aliquid doloribus omnis libero impedit inventore officia sed. Facilis reiciendis ullam et omnis rerum ut adipisci reprehenderit. Nemo architecto aliquam et. Veniam illo hic soluta aut. Quisquam numquam soluta sed ut. Natus minus cumque nobis et. Error dolorem non quia assumenda. Est occaecati odio aut harum. A ut eos recusandae sit praesentium. Quo est odit ut qui nisi. Voluptatum ut animi aut quasi molestiae. Blanditiis hic natus iusto tempora distinctio. Commodi dolor et voluptas. Dolore fugit vel commodi unde modi ipsam laboriosam dolores. Dolorum velit quas eaque rerum nostrum molestias. Enim et ea repellendus. Saepe fuga ut et sit. Qui quo eum nemo doloribus corrupti velit. Consequatur debitis accusamus vero dolorum odit sed aut. Omnis fuga velit quia iure ad aut reprehenderit. Eos sunt illum vitae eos vitae accusantium facere. Molestiae cum dicta nobis quos aut dolor. Ut repellat omnis occaecati. Quae quis assumenda quibusdam. Ut asperiores natus nobis esse. Perferendis explicabo iusto officia facere nobis dolores. Velit aliquid officia omnis nostrum excepturi soluta. Facilis deserunt nihil non consequatur. Et voluptatem cumque in autem inventore molestiae iusto aut. Sed eveniet occaecati quia et ut. In tempore ipsa consequatur in maiores iure voluptates. Sit soluta praesentium quam distinctio repellendus. Facilis minima sit quo est placeat maiores. Et sint est maiores perferendis autem. Saepe ad tempora doloribus molestiae eos. Aut enim sit beatae aliquam aut perspiciatis maxime. Libero consequatur illum at ut. Deserunt odio enim nesciunt facilis ut similique. Ea natus omnis tenetur nesciunt doloribus blanditiis provident. Cumque rerum incidunt ipsum voluptatem rerum. Autem ratione dolorem reiciendis eum est quia similique odit. Harum similique dicta tempora nemo et cum. Vitae et illo non esse magni et.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle14','keyworks' => 'key works test asd14','description' => 'Eveniet et dolorum dolores magni ut. Omnis harum quia enim sed omnis et non.','img' => NULL,'type' => '1','number_of_comments' => '72'),
  array('id' => '210','user_id' => '3','title' => 'cypher15','slug' => 'cypher15','content' => 'Et voluptatem sed nisi aut facere ut nesciunt. Dolor magnam doloremque deleniti est eius et. Nihil nihil esse eum repudiandae quia animi. Voluptas ipsum corporis animi voluptatem. Unde vel molestias minus alias. Quia dolorem sit quis enim accusantium beatae voluptatem. Error occaecati at corporis. Quae illo numquam inventore. Sed fugit inventore temporibus quaerat eum. Vel nesciunt voluptatum assumenda illo. Quasi omnis quis nihil et omnis. Perspiciatis vitae sint sunt aut et. Qui veritatis occaecati officiis sint. Reprehenderit mollitia excepturi nesciunt qui. Alias fugit ipsam quam autem velit. Molestiae et voluptas a et dolorum. Aut suscipit hic facere est impedit perferendis voluptatum. Blanditiis maxime quo ipsum quisquam accusantium rerum nam dolore. Voluptatem iste illum veritatis ut qui tempora. Voluptatem culpa ratione voluptates facilis tenetur quidem eveniet et. Vero facilis id assumenda ab nam id. Architecto quis aut mollitia reiciendis tenetur molestiae. Magnam accusantium modi consequatur minus voluptatum tempora. Aut eaque sequi sint modi et fuga distinctio. Asperiores ea quas consequatur deserunt occaecati. Velit sit voluptate ducimus voluptatem ad nesciunt eum blanditiis. Facere cupiditate illo officia sunt est totam consectetur. Architecto totam ex totam blanditiis earum. Inventore itaque possimus quaerat et a provident sequi quae. Earum alias vero quisquam natus excepturi. Dolores et iure maxime voluptatum. Dolores culpa recusandae inventore est autem temporibus explicabo. Veritatis sunt placeat consequuntur animi qui recusandae eligendi minima. Voluptate quod explicabo tenetur ipsam qui. Sed sit velit facere autem possimus qui hic delectus. Quas accusantium excepturi et culpa quos. Totam qui harum quia ullam asperiores. Totam dolor maiores aut esse est quos voluptas. Doloremque autem ex beatae et perspiciatis dolor et. Quo vel modi aut voluptate voluptatem consequatur culpa. Voluptatem harum hic atque. Consequatur omnis dolore nisi temporibus qui. Odit culpa necessitatibus nemo quam aliquam. Et dolor est laboriosam et aut voluptatem qui. Pariatur et earum et molestiae quia quo exercitationem. Consequatur debitis quia possimus iusto. Minima voluptatem repudiandae exercitationem doloribus consequatur consequatur itaque. Aut voluptas et qui laborum labore non molestiae. Facilis praesentium deserunt molestias molestias et sint. Similique dolorem doloremque eaque nemo eaque. Nisi enim quam et quae ut voluptatem temporibus. Quia quidem aut magni quaerat tempore. Explicabo natus sint fuga non. Quia commodi aliquam reiciendis et ipsum itaque. Magnam hic ut nesciunt dolores odio tenetur aliquid harum. Hic blanditiis natus sit cumque tenetur. Minus aut corrupti reiciendis vel. Aut quisquam libero non non eum et aut. Nisi quia voluptate repudiandae omnis dolor doloribus est laudantium. Laboriosam dolores natus cumque unde officia. Illum vel recusandae laboriosam molestias quidem qui possimus. Quis repellat id quisquam animi. A atque ex corrupti quia eos ipsam nihil. Nam ducimus doloremque sunt molestiae sit. Fugiat corrupti a fuga eos nisi incidunt repellendus et. Repudiandae corporis dolorem nulla perferendis. Illo molestias minima quis nam eos fugiat repudiandae. Optio rerum nemo illum necessitatibus. Deserunt earum consequuntur aut perferendis culpa officiis ut. Dolor recusandae repellat reprehenderit soluta quis porro. Ut sunt aspernatur est dignissimos ducimus ducimus. Illo voluptas qui ut qui. Dolor ea adipisci nisi porro fuga illum. Numquam quod minima maxime quia illo. Eos eos impedit totam occaecati sunt. Dolor aut doloremque blanditiis neque. Quis et velit eum commodi perspiciatis natus optio. Sunt autem ut nulla aut sed dolores. Aut et nesciunt sunt deleniti corporis possimus. Eos officiis nam non ut dignissimos. Ipsum alias velit quod reprehenderit. Dolorum omnis et eveniet est magnam similique fuga. Eaque ut quam dolores tempora minima eos minus. Incidunt ut maxime voluptatem illum facilis soluta vitae. Sit voluptatem maxime qui voluptatem voluptas laboriosam amet qui. Iusto aut vitae nobis distinctio adipisci repellendus. Est veritatis voluptatem sit cupiditate. Mollitia eos eos doloribus omnis. Dolor eum voluptas incidunt facilis rem. Consequatur ab neque qui ea veritatis enim. Et expedita sunt nulla alias. Voluptas laudantium nostrum accusantium mollitia nihil dolorem in. Expedita est totam voluptas id qui et eius necessitatibus. Magni laborum explicabo velit iusto magnam. Modi mollitia pariatur nobis fugit. Est eveniet ipsa nostrum recusandae non sed. Libero iste laboriosam doloribus officiis quia. Molestiae commodi porro recusandae doloribus. Occaecati sit aut facilis corporis velit dolorem. Et quia dolorum inventore omnis beatae dolor. Vero asperiores fugiat molestiae. Error quis sint iure. Repellendus labore aliquam totam libero vel minus sed. Ullam odit consequatur aut autem. Necessitatibus quia sit odio quos ducimus.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle15','keyworks' => 'key works test asd15','description' => 'Aliquam a non eum adipisci incidunt et. Eum ea modi eos.','img' => NULL,'type' => '1','number_of_comments' => '103'),
  array('id' => '211','user_id' => '2','title' => 'cypher16','slug' => 'cypher16','content' => 'Nam eum inventore quaerat repudiandae nihil. Reprehenderit dolor mollitia voluptatem nemo eligendi non. Voluptas explicabo quibusdam et. At ratione aliquid quisquam recusandae corporis nesciunt. Quis dolor repellat qui est amet. Omnis necessitatibus quis inventore recusandae et consequatur et non. Qui laboriosam eveniet aliquam deserunt hic optio omnis. Ea aliquam saepe dolore libero. Voluptatem totam ut qui qui. Voluptatem rem qui sit dicta quia quam aliquid maxime. Voluptates tempore aliquid laudantium labore ea. Quisquam delectus tenetur est non rerum perferendis. Esse dolor voluptas iure et est voluptas aspernatur. Id voluptatem voluptatem deserunt neque. Enim alias fuga laborum reprehenderit. In quas beatae magnam nulla. Et perspiciatis eos nisi nulla et recusandae sunt. Fugit aut sit quidem et tenetur rerum voluptas nam. Cupiditate sed voluptatum labore cum eius. Quas et incidunt optio sed id dolor. Beatae facere consequatur natus eligendi quia. Accusamus ad et sunt. Velit quaerat repellendus nemo rem alias quam nulla. Doloribus dolorem voluptatibus exercitationem fuga optio vitae. Dolorem blanditiis inventore quis quasi exercitationem sunt. Et esse pariatur quis quas. Sit exercitationem qui in aut perferendis similique a error. Consectetur asperiores architecto velit veniam. Rerum non ut qui aut praesentium tenetur. Aspernatur cumque magni eius dolor sunt. Ut a repellendus tenetur perspiciatis beatae temporibus exercitationem. Enim nobis veritatis repellat. Ut quis dolores consequatur corrupti aut vitae animi dicta. Et et placeat possimus aut. Sit impedit sed et eos consequuntur voluptates et. Voluptatem labore nisi incidunt quidem dolores autem. Et omnis fuga temporibus velit ab. Rem dolorem rerum nostrum accusamus ea nesciunt. Neque et iste sapiente quis natus. Fuga itaque voluptatem neque voluptatum optio. In perferendis aperiam deserunt voluptatem doloribus. Quia et et non dicta quas eveniet. Esse inventore necessitatibus iure et non nihil exercitationem. Aliquid non laudantium sed tempora nobis. Officia excepturi occaecati tempora nulla explicabo ipsa. Quis tempore debitis ipsam voluptatem iusto rerum. Sed dolor est a iusto amet at nihil. Accusamus sed ex quidem officiis delectus. Debitis consequuntur tenetur facilis consequatur adipisci dicta quia ea. Nihil hic rerum culpa voluptas culpa dolorem maiores. Aut ullam numquam tenetur iste aperiam. Quisquam beatae sint et. Est dolor aliquid assumenda recusandae repellat commodi. Odit et dolores dolore eaque. Quisquam ratione sed ut voluptatum. Impedit adipisci illo maxime exercitationem consequatur quibusdam voluptate nam. Blanditiis quia nostrum et odio laboriosam enim expedita. Rem placeat natus tempora fugiat quia sunt qui. Accusamus rerum quis ab aut. Cumque voluptatibus nostrum natus. Sunt non est magnam pariatur doloribus voluptatum laborum. Quo sunt quas excepturi eum sit. Aspernatur minus quibusdam neque non. Iusto aut quis consequatur commodi praesentium sed et voluptatibus. Libero eius beatae et corrupti. Enim ut nihil ipsa qui. Accusamus beatae aut repellat dolor. Nihil incidunt consequatur officia nihil non aliquid reiciendis temporibus. Id dolorem odio animi debitis qui cum sint expedita. Quas et eos cupiditate perspiciatis est. Id voluptas et sunt consequuntur laborum et. Libero dolorem placeat sed nam eaque necessitatibus dolorem. Non voluptatibus vel officiis officia. Nisi et laboriosam voluptatem ut sint accusamus qui. Ipsam explicabo quibusdam blanditiis dolorum dolores ullam quibusdam. Amet accusantium id rerum dolore. Voluptate vero facilis accusamus voluptatem. Pariatur magnam numquam quas vel iure atque asperiores. Qui asperiores soluta illo. Id consectetur reprehenderit iure provident. Voluptate reiciendis magni libero maiores odio ea impedit autem. Inventore vero quas aut ut suscipit assumenda quaerat. Fuga perferendis dolorem eius suscipit nesciunt rerum. Dolores ab ducimus voluptas facere quasi. Consequatur omnis necessitatibus eum natus autem aut voluptatem. Aut fuga ullam dolorem delectus est. Qui fugit quia voluptas odio et. Itaque enim maiores nihil omnis alias enim. Assumenda velit maiores minus. Est molestiae ut animi animi omnis. Dolores labore quasi fugiat ut dolorum modi rerum. Qui cumque sit ea autem necessitatibus eos. Et iure nemo omnis vero. Occaecati placeat soluta alias autem. Recusandae beatae ut qui sed voluptatem dolores ratione. Totam accusamus nobis minus aliquam qui et laudantium. Possimus voluptatem nisi temporibus esse eum sequi eveniet. Voluptatibus accusantium sunt ducimus pariatur doloribus recusandae. Non quae accusamus totam maiores dolorem. Sint qui quibusdam odit consequatur. Neque ipsum et quia sint quis. Tempore sint nemo qui nostrum. Iste ratione debitis perferendis voluptates hic beatae voluptatem maxime. Temporibus deleniti ab quo corrupti non et. Maxime error facere laboriosam esse.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle16','keyworks' => 'key works test asd16','description' => 'Laboriosam asperiores optio molestias. Facilis a ratione quasi.','img' => NULL,'type' => '1','number_of_comments' => '103'),
  array('id' => '212','user_id' => '2','title' => 'cypher17','slug' => 'cypher17','content' => 'Accusantium esse accusamus quia quam. Dolores quia velit distinctio deserunt tempora magni. Tenetur maiores sed delectus totam distinctio placeat repudiandae sed. Dolor eligendi dolorum deleniti consectetur voluptas officiis ipsa. Aperiam repellat dolorem consequatur. Sed fuga possimus ratione in consequatur. Voluptas quos dolorem commodi. Reiciendis fugiat atque itaque officiis eius. Voluptate quaerat sit tempora ut. Est molestias voluptatem dolorem unde. Aut tempora aut qui quisquam tenetur quaerat eos. Temporibus delectus nulla laudantium ratione. Ut incidunt non quia ratione. Similique numquam quia voluptatem molestias debitis nostrum. Et inventore velit explicabo cum sit. Error a corporis non nam. Asperiores magni aut dolores nesciunt fuga consequatur. Qui sit velit rerum delectus. Doloribus architecto alias non libero iusto culpa. Nam molestiae est eaque adipisci dolorum veniam est. Repellat quod sed aperiam earum magnam nemo natus. Ut eum odit odit consectetur aut. Cumque aut et magnam. Delectus consequuntur architecto voluptatem officiis minus enim ad quia. Vitae adipisci corporis et possimus quis id. Numquam et tempore quidem impedit iusto quia. Autem quos et deserunt rerum sed. Aperiam sequi eveniet voluptate non nesciunt dolorem est. Et consequatur ipsa dicta molestias tempore. Qui vitae rerum corrupti repellendus iste impedit ipsum ad. In rerum praesentium itaque magni consectetur animi. Adipisci enim nam ex sed suscipit iste. Magni ipsum quis ad voluptatem nobis ut a. Nemo dicta cum unde. Atque repellendus quia ullam sapiente impedit et qui. Exercitationem excepturi corporis dolor qui est aut debitis rerum. Aut dolor sed debitis nobis non qui. Ut expedita eum earum velit debitis dolorum id. Consequatur temporibus eaque quos repellendus odio natus. Et eaque a nisi ipsam fuga quos. Voluptatem ex incidunt voluptas repudiandae. Reiciendis sunt perspiciatis quibusdam fuga libero. Temporibus perferendis qui qui est saepe qui et. Quia officia eos ab enim dolor. Officiis sint eligendi aperiam nihil adipisci soluta aut. Non quis ut qui sint eligendi qui. Illo nostrum nobis et officia officiis. Rem et repudiandae aut nihil. Odit nihil aliquid nulla alias facilis atque. Et et est ut vitae doloremque. Inventore cumque blanditiis dolores consequatur est corporis. Et eos optio veniam labore voluptas. Voluptatem consectetur quia exercitationem at libero culpa sunt. Et voluptates ea nulla placeat molestias laboriosam. Eum modi eveniet error id. Magni omnis perferendis numquam numquam possimus. Quod eveniet qui nemo quas in quos fuga. Accusantium rem velit nam temporibus. Perferendis facilis enim in similique exercitationem natus. Non quia labore quod ea. Omnis nam enim voluptatibus quis. Necessitatibus quo modi doloribus in quam omnis eos. Corrupti eum voluptatem eveniet pariatur. Et qui blanditiis praesentium aliquam et. Aut sed est exercitationem exercitationem non consequatur debitis et. Cumque eum eos autem labore deleniti. Amet voluptatibus nemo cum saepe laudantium excepturi veritatis. Enim reiciendis ab dolor odio. Aut omnis provident non odit. Nesciunt consectetur consequuntur ut quasi dignissimos aut suscipit qui. Non libero similique nihil incidunt saepe quasi. Omnis dolor tempore harum fuga quod. Vitae maxime autem a rerum labore error. Quia delectus laboriosam non culpa. Quasi corrupti labore id inventore hic aut adipisci voluptate. Qui ipsam veritatis qui. Quo voluptas voluptatem est quia. Soluta rerum rem est non delectus ratione incidunt atque. Inventore animi quidem voluptatem. Dolor doloribus aut quia. Ut qui eius voluptates nihil occaecati sit. Sed minima ipsam sit omnis et. Ullam consequatur repellat quod sit rerum. Excepturi adipisci laudantium accusamus autem. Nulla quisquam culpa ab fuga. Sunt et dolores tempora sint sit tempora. Voluptatem delectus quis nostrum quam. Illo velit illo veniam deleniti quaerat dignissimos. Pariatur non vel quidem aliquid magnam repudiandae ut ipsum. Et laboriosam eos nisi quisquam. Molestias sed nihil laboriosam dicta perferendis aut ratione. Repellat voluptas inventore ipsam. Repudiandae labore dolores eaque consectetur repudiandae iste et. Voluptatem et pariatur laborum consequatur iusto maiores. Sint quis dolores odit qui quisquam laboriosam. Facilis esse omnis molestias. Repudiandae perferendis facere in aspernatur quod numquam tenetur. Molestiae aut earum architecto est omnis. Aspernatur voluptatem ipsa et. Asperiores distinctio ea eum et consequuntur qui quia reprehenderit. Laboriosam atque voluptatem nihil deserunt sit. Deserunt sit libero autem aperiam iure. Quia vel rerum quod repellat et similique. Et id id autem doloribus sapiente dolorem voluptas. Animi esse eos ut ex qui est. Eos voluptatem harum quia. Voluptas culpa non deserunt officia. Doloremque sapiente rerum molestiae ut est laboriosam dolore. Sed alias libero modi quisquam earum itaque aspernatur.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle17','keyworks' => 'key works test asd17','description' => 'Nobis atque minus sed voluptatem. Enim corrupti modi sed dolor repellendus quo tempora.','img' => NULL,'type' => '1','number_of_comments' => '94'),
  array('id' => '213','user_id' => '1','title' => 'cypher18','slug' => 'cypher18','content' => 'Eos et esse consequatur voluptatem unde. Ut mollitia culpa eum veniam voluptas voluptate. Et ullam nihil at omnis quod. Perspiciatis error omnis non voluptatem. Quia harum voluptatibus vitae dolore repellat amet asperiores. Accusamus mollitia cum recusandae ut numquam voluptas. Nam debitis cumque est voluptas ut deleniti. Optio est ducimus et sint corporis voluptatem nulla soluta. Animi tenetur ipsum alias veritatis. Perspiciatis nesciunt est quis et nihil est quam. Incidunt reprehenderit fugiat sed eum maiores dicta nobis ea. Veritatis accusantium omnis harum culpa molestias aut autem. Dolor minus in architecto. Sed rerum voluptatum ut est debitis reprehenderit qui provident. Dolorum dicta expedita tempora. Dolorum fugiat incidunt aut rerum sunt. Non ipsam voluptatem aliquid iusto illo. Fugiat molestiae ratione vero. Tenetur in nostrum amet. Voluptatem laboriosam saepe dolor quod est vitae. Atque natus nulla et labore non ipsum labore. Ut odio quia quae sunt consequatur dolores. Eos ea mollitia libero voluptatibus id aut dolorem tempore. Magnam sint voluptatem et reprehenderit earum odit. Aut tenetur quis dolorem. Provident sed ad cum officia. Corporis tenetur qui laudantium aliquid magni. Vero sapiente odit nobis voluptas mollitia tempora placeat. Quisquam id omnis dolorem quo illo. Non consequuntur eum voluptas enim. Voluptatem perferendis saepe est deleniti maxime. Voluptas explicabo repellendus sed qui nostrum laborum. Quia illo veritatis quae. Assumenda quia excepturi tempora sed qui. Corporis quidem illum recusandae non et quis quis maxime. Ratione est ab voluptates error esse dolores quo consequatur. Voluptatem ea perferendis modi est adipisci non. Quisquam voluptas provident amet impedit. Adipisci rerum eveniet placeat est aut aliquam. Dolor accusamus quidem quasi tempora similique. Totam vel ipsam mollitia illo ut provident rem ut. Odio quod autem distinctio autem autem velit. Et nihil vel quia doloremque est at perferendis amet. Quis cumque temporibus occaecati saepe. Perferendis cupiditate eius sequi asperiores minus et error mollitia. Animi quam ducimus modi nesciunt ut dolorum veniam. Aut sunt expedita accusantium omnis. In earum fugit omnis dignissimos et alias. Autem quisquam numquam dolorum id fuga in. Et quasi voluptatum nisi nisi eaque autem molestias. Similique voluptatem fugiat similique culpa qui ex. Temporibus officia fuga porro incidunt. Quis et recusandae rerum esse architecto magni vel. Odio mollitia nisi aut earum fugit. Voluptatibus soluta vel similique aliquam. Et ipsam nemo sit qui assumenda. Id saepe iusto aliquid error fugit. Sed ratione voluptatem suscipit et. Vel expedita et voluptatem odio. Ducimus qui culpa placeat voluptas consequuntur molestias voluptatum. Recusandae at ut sint. Expedita quod qui neque velit. Expedita voluptatem quia dolores iusto qui reprehenderit adipisci velit. Blanditiis sed quaerat consequatur est unde earum. Odit nam quo in impedit et nam alias. Molestiae temporibus laborum molestiae. Voluptate maxime illum assumenda omnis fugiat qui error. Officia saepe non sed aut autem voluptas. Dolorum debitis voluptatem et voluptatem dolores aut. Occaecati qui facilis omnis quis aut. Cupiditate quibusdam amet provident ex aliquid nesciunt. Natus harum quibusdam voluptatibus aut aut. Facere quia veniam commodi debitis praesentium eum. Omnis omnis omnis autem earum perferendis minus repellendus. Ex quaerat distinctio commodi quos error quo. Eos qui id omnis quidem. Provident id sit adipisci. Eum quo laborum facilis molestias nihil alias qui. Sed sunt ut eveniet ea ut. Placeat sint sit et sed fugit corporis accusamus. Ut molestias quisquam atque dignissimos possimus. Sunt magnam in quis. Corrupti deserunt nihil nesciunt. Sed omnis est sint assumenda atque. Beatae libero error aut. Enim rerum numquam sunt eos aut. Exercitationem animi autem deleniti nemo aliquam quasi temporibus. Quia repudiandae et corporis perspiciatis dolor. Illo non neque enim mollitia eos eius fugiat. Deserunt eos illum quia quos rerum soluta ea. Aspernatur aliquam aspernatur molestias cumque eaque autem. Inventore nam eum quisquam deleniti non numquam. Aliquid repellendus ab modi. Architecto quae doloremque molestiae magnam autem consequatur amet. Eos fugiat excepturi architecto deserunt voluptatem facere et. Fugit animi at dignissimos labore beatae et. Cum voluptas esse quis. Consequuntur et ratione error dolor. Sunt accusantium sequi debitis. Quis error corporis rerum earum est aspernatur. Porro et aliquam autem odio laborum sint velit. Omnis aut voluptatem ipsum quis. In qui sint qui qui sint voluptatem. Aliquam nemo molestiae non sunt labore sint molestiae. Est est voluptatibus dolore. Non sunt praesentium maxime aspernatur nisi et sed. Hic ut voluptatum harum dolorum. Sequi quidem in voluptatibus consequatur voluptas et id. Qui quam odit fugiat optio. Voluptatum placeat ratione explicabo alias voluptatum animi aliquid totam. Ea et dignissimos et et.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle18','keyworks' => 'key works test asd18','description' => 'Facilis error debitis et sequi debitis quos. Omnis sed necessitatibus labore magnam veniam.','img' => NULL,'type' => '1','number_of_comments' => '89'),
  array('id' => '214','user_id' => '3','title' => 'cypher19','slug' => 'cypher19','content' => 'Non ullam autem amet harum. Dolore expedita reiciendis at accusamus. Dolorem sed provident assumenda autem consequuntur dolor neque. Earum totam et inventore molestiae natus labore sint. Mollitia nesciunt labore tempore ipsum. Maxime tempora ut magnam id aut voluptates. Odio eius qui beatae nisi dolor. Consequatur nobis quos deserunt. Rerum unde culpa optio pariatur non. Voluptatem ut iste fuga. Rerum rerum molestiae est illum quia. Perferendis eveniet animi molestiae optio incidunt voluptas minima. Qui ut quam eaque ipsum aliquam. Dolores sit voluptas veniam mollitia excepturi et voluptatem ullam. Numquam dicta perspiciatis aperiam impedit eos. In beatae voluptatem suscipit quo exercitationem. Perferendis ab sint perferendis saepe dolorum. Officia a ab a at reprehenderit ea debitis. Saepe veniam similique corporis ea omnis. Cum dolorem consequatur voluptates qui minima pariatur aut placeat. Molestiae facere voluptatem qui quidem. Harum similique rerum voluptatem vero consequuntur unde sed similique. Aut nostrum et sed itaque. Commodi dolores omnis enim qui ut sit laboriosam. Corporis quod quod aliquid est. Laboriosam sint repellendus expedita fuga aliquid est quaerat. Delectus odio laudantium ut ea. Quis beatae minima delectus sed inventore sunt. Similique itaque aliquid molestiae et reprehenderit. Molestiae est et atque officia tenetur tempora. Fugiat est tempora ut earum laborum. Sunt praesentium maxime atque et. Dolor veritatis fugit ullam temporibus omnis. Atque aut quaerat ut vitae accusamus qui. Repellat dolores voluptatem est eligendi et aspernatur. Expedita et illo harum soluta quae sunt rerum. Nobis quaerat sit beatae ab. Deleniti accusantium consequatur eos. A commodi ipsum dolorem aperiam repudiandae at. Vel reiciendis id quam repellat tempore consequatur laudantium. Ipsum sunt ab mollitia perferendis. Sit eius quae fuga. Sed cumque eaque velit qui. Culpa in nobis maxime at est unde. Quas magni et eos est. Non debitis deserunt aliquid qui perspiciatis. Velit debitis similique illo necessitatibus. Adipisci iure occaecati dolor harum. Et unde eveniet iusto sunt facilis dolores nulla. Ipsum accusamus laborum fugit tempore architecto natus ratione. Odit voluptate doloribus vitae ab in. Eveniet odit rerum voluptatum. Vero cum et quaerat mollitia. Consectetur et quasi corporis atque officia aut. Unde magnam eos porro ipsum eius quis soluta accusantium. Voluptate praesentium consequatur non id error vel. Temporibus voluptatum minima dolores commodi aut quibusdam. Neque quod repudiandae voluptatum quidem qui. Aperiam est numquam est aut. Sit sit quia eos. Quod quos harum voluptatibus esse sunt cumque omnis. Quia modi ratione labore ea numquam. Vero reiciendis suscipit quia voluptas. Nobis laboriosam autem soluta ratione perspiciatis occaecati doloribus error. Numquam quis beatae nihil nam esse aut. Deleniti et qui sed officia repellendus et. Consequatur est dignissimos voluptates voluptates ullam. Officiis cupiditate odio dicta voluptate id rerum. Dolor eos omnis et accusamus eveniet laboriosam. Dolores aut et aut enim. Esse molestias eos harum quia quo quia. Provident officia harum quis repellat repellendus ut error. Nostrum voluptatem praesentium maxime. Nisi sapiente quo ut qui est dolorem. Quis cumque dolor labore. Voluptate ex dolores illo minus mollitia. Omnis nihil accusantium esse. Veniam consequuntur deleniti sunt illo. Laudantium aliquam eos necessitatibus alias. Placeat tempore excepturi nihil dolorem. Vitae placeat quo corporis eos inventore est expedita odio. Fugit hic et dignissimos repellat. Exercitationem voluptatem qui et. Magnam laudantium blanditiis et magni occaecati porro. Sint sunt laudantium sed non. Sit iure perspiciatis voluptatem voluptatum ipsum laudantium voluptatem sed. Eligendi ea qui dolorum. Quis sint voluptates id. Autem non odio qui et ipsam temporibus. Sequi vel consequatur id impedit dicta. Id mollitia veniam asperiores id omnis ut ea. Corporis vero placeat voluptas eos consequatur. Non quod laborum sed reprehenderit natus qui et. Dolorem neque ad libero. Quia et occaecati excepturi unde. Placeat quis nulla dolorem molestiae dolore. Excepturi autem dignissimos consequatur error non aperiam sunt. Neque qui ipsum ut ex soluta molestiae. Dolore id non autem vel. Natus in ab ducimus ut voluptatem et. Ut doloribus et dolorem reiciendis suscipit libero deserunt. Tenetur ullam consectetur rerum. Officiis perspiciatis explicabo aut perferendis deleniti labore. Quam ratione dignissimos ut facere. Quam eius debitis animi incidunt ut voluptates in et. Est sapiente et occaecati cum necessitatibus animi. Illo ex odit reiciendis illo. Animi libero omnis reprehenderit laboriosam quia sed est fuga. Deserunt sit iste sunt quia corporis itaque nobis. Omnis quos voluptates est magni magnam. Omnis ea voluptatem aperiam laboriosam nemo at quasi. Dolores aut doloribus distinctio sequi. Animi minus omnis error sequi fuga ipsum quidem. Aut et fugit accusantium aspernatur.','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2019-09-07 14:53:51','headtitle' => 'headtitle19','keyworks' => 'key works test asd19','description' => 'Et ad non cumque qui inventore. Est laudantium saepe at impedit. Est commodi ullam et debitis.','img' => NULL,'type' => '1','number_of_comments' => '74'),
  array('id' => '215','user_id' => '3','title' => 'cypher20','slug' => 'cypher20','content' => '<h2 style="text-align: center;">Quake Live</h2>
<p><iframe title="YouTube video player" src="https://www.youtube.com/embed/TKkzofCzj4g" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
<p>Atque doloremque provident velit optio cumque voluptatem. Beatae officia tempore qui. Ut fuga laboriosam nostrum vel neque ducimus. Minima aut fugiat voluptatem hic. Enim reiciendis veniam tenetur laboriosam nisi est doloremque. Dolore suscipit sunt alias voluptatem nisi iste harum. Corporis natus tenetur odit nihil. Et et consequatur possimus repudiandae consequatur. Quia eligendi nemo aliquam libero accusantium. Sit non error ab voluptas explicabo. Numquam ducimus amet repellat dignissimos molestiae est vel dolor. Ab reiciendis aut totam totam natus perspiciatis. Excepturi nesciunt molestias perspiciatis rerum et earum ipsum debitis. Ut accusamus aliquid aut impedit. Est iusto sint placeat molestiae repudiandae fugiat. Voluptates ab qui in eius rerum qui eum. Vel similique ut quibusdam temporibus voluptatibus voluptate. Non amet non ducimus vero corporis. Rem iste voluptates totam ut fuga voluptas ut modi. Vel natus dolorum velit amet. Quasi sit sed consequatur quia. Qui eius expedita cum quidem debitis nisi. Et voluptas impedit et dolorem suscipit. Maxime magni voluptatum quo itaque enim ut. Velit esse doloremque laboriosam veritatis aliquid. In hic recusandae quam. Est qui sed dolor odio ea. Doloremque corporis similique ab quibusdam qui sed labore similique. Nihil iste consectetur ut et in. Voluptas et neque qui consequatur architecto accusantium. Accusamus facere distinctio quibusdam. Esse rerum culpa tempore est explicabo recusandae quia. Iure inventore omnis corporis labore voluptas molestias reiciendis soluta. Facilis placeat dicta enim reiciendis. Odit ut consequatur qui porro officiis ea qui. Labore voluptatem autem temporibus nemo excepturi qui quia libero. Nobis officiis minus soluta aut quis sed molestias. Voluptatem assumenda animi quisquam laudantium iste earum. Et facilis eos animi recusandae veniam. Repellat sit dolorem ut. Ut eos laboriosam animi quia. Nostrum voluptas et eligendi iste rerum velit. Et pariatur laborum eius ut consectetur temporibus. Quo nostrum explicabo ab nulla dolorem. Maxime sit fugit a voluptas voluptas iusto. Eius ut voluptas vitae qui voluptatibus explicabo beatae. Labore consequatur explicabo sit. Voluptatem distinctio molestias esse et voluptate quidem expedita. Veritatis officiis id natus consequatur aut repellendus. Tenetur quis culpa cum fuga necessitatibus. Harum aliquid unde quis dolorem temporibus ex. Voluptas id enim delectus illum qui. Quam sunt earum quidem non qui voluptates officia. Blanditiis quae error sunt. Dignissimos aperiam velit et dolorum ut pariatur. Maiores non officiis consequatur. Voluptas culpa at veniam praesentium et. Laborum magni quia fugit in accusamus. Eum blanditiis itaque sit eos. Aliquam voluptatem dignissimos magni ut aut quidem. Nam temporibus corrupti tempora consequatur voluptatum cumque quis. Est similique aut omnis voluptatum quis. A dicta velit soluta voluptatum non necessitatibus quia esse. Dolor sit error quisquam tempora. Temporibus nemo id vitae aut. Quod fugit sit aliquid. Quia dolor nisi nostrum quod nobis quo necessitatibus voluptas. Quas aut deserunt aut aut perferendis nihil. Atque aut vel quidem harum. Aut maiores modi dolorem accusantium voluptatem molestiae ea. Dignissimos debitis quis et velit voluptas repellat. Iure minima at in atque non non expedita. Omnis officia enim aut non et quis necessitatibus. Fugiat aspernatur nobis maiores ex et temporibus facere. Deleniti exercitationem tempora aspernatur expedita consequatur voluptas. Maxime dolores possimus quia odit officia culpa aspernatur labore. Aut illum incidunt beatae eius. Est doloribus sit nulla. Pariatur quisquam hic amet fuga quidem autem quo. Odio quo error sed est ipsum eaque impedit. Fugit ut consequuntur non explicabo voluptas est excepturi amet. Repellendus id recusandae a repellendus enim ut. Quia sint cumque odio rem impedit. Nisi asperiores provident sed rerum doloremque tempore. Provident sed sed assumenda quibusdam ut aut. At dolor velit rem qui voluptatem aperiam quo et. Eius exercitationem aut dignissimos tempore. Non iste rerum aut sint sed. Perspiciatis autem quo amet esse. Ab minus est accusamus quae. Saepe blanditiis atque voluptatem error ipsam eum sunt. Et ipsam quos cupiditate. Veniam qui quam expedita ut. Voluptas qui aspernatur incidunt ut aut blanditiis at. Maxime aut quia veniam cum. Quis magnam voluptatem laboriosam molestiae iste porro quas. Ducimus ab qui quisquam facere accusamus et. Ratione dolores dignissimos corrupti dolorum aspernatur quis. Molestias quos assumenda error ad vero. Quia quibusdam accusantium et porro cumque quibusdam. Modi voluptatem odio autem minima accusantium itaque. Sapiente sed optio molestias est expedita natus. Et necessitatibus alias excepturi sequi ipsum. Odio eos aut hic labore aut nihil. Aliquam rerum qui occaecati voluptas. Consectetur aliquam explicabo voluptas molestiae cum. Eaque quod assumenda blanditiis sunt. Eos possimus architecto eos. Repudiandae consequatur ad aut deserunt cupiditate.</p>','publishedAt' => '2019-09-07 14:53:51','updatedAt' => '2021-04-28 22:12:40','headtitle' => 'headtitle20','keyworks' => 'key works test asd20','description' => 'Quibusdam minima ut eos. Odio impedit dolorum quia suscipit.','img' => NULL,'type' => '1','number_of_comments' => '81'),
  array('id' => '216','user_id' => '1','title' => 'Grzegorz Tarka - kim jestem i moja historia','slug' => 'o-mnie','content' => '<article class="container-fluid pd-tp-bt-0">
    <div class="row ligh-bg blog">
        <div class="col-lg-12 white-font">
            <h1>O MNIE</h1>
        </div>
    </div>
</article>
<article>
    <section class="container-fluid black-font grey-cont pd-lf-rg-0">
        <div class="container kolor">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <figure>
                        <img src="{{asset(\'img/ja/20141101_173642.jpg\')}}" class="img-circle" width="204" height="254"
                             alt="Grzegorz avatar">
                    </figure>
                </div>
                <div class="col-lg-6 col-lg-offset-3 text-left">
                    <p>
                        <strong>Witam nazywam się Grzegorz,</strong> programowaniem zajmuje się od około 5 lat, jednak trudno powiedzieć mi kiedy to wszystko się zaczęło bo w zasadzie od zawsze interesowałem sie informatyką i programowaniem. Z czasem zainteresowanie to przełożyło poprostu na prace w tym zakresie. Oprócz programowania zajmuje się naprawą szeroko pojętej elektroniki. 
                    </p>
                    <p><strong>System szkolnictwa</strong> nie jest w Polsce idealny a kiedyś przez moje nie
                        zdecydowanie co chcę w życiu robić skończyłem studia całkowicie w innym kierunku niż informatyka
                        i programowanie jako takie. Kierunek jaki ukończyłem był dla mnie za nudny więc w między czasie
                        przewinąłem się przez kilka różnych zawodów :). Postanowiłem wrócić jednak do informatyki, którą
                        zawsze lubiłem. Stosunkowo szybko nadrobiłem "zaległości" i gdy poczułem się już pewnie w
                        Fron/Back End postanowiłem uderzyć na głęboką wodę :). </p>
                    <p><strong>Jestem samoukiem</strong> stale dążącym do celu. Programowanie w różnych językach stało
                        się od jakiegoś już czasu takim celem. Chętnie uczę się nowych technologii i szybko
                        przyzwyczajam sie do wszelkich nowości. </p>
                    <p><strong>Poza programowaniem</strong> 
                    interesuje sie motoryzacją podróżami i historią. Lubię również jeździć na rowerze, siatkówkę, góry i czasem obejrzeć jakiś dobry film. Jestem też wielkim fanem starych samochodów i klimatu lat 80 i 90. Należe do otwartych osób  i łatwo nawiązuje znajomości. 
                    </p>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4 col-md-offset-2">
                    <a href="https://github.com/GrzesiekTa" target="_blank">
                        <i class="fab fa-7x fa-github-square"></i>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="https://www.linkedin.com/in/grzegorz-tarka-547444148/" target="_blank">
                        <i class="fab fa-7x fa-linkedin"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="quotation white-font container-fluid dark-bg">
        <div class="col-lg-6 col-lg-offset-3">
            <h2 class="text-center">—</h2>
            <h1 class="text-center">“<strong>Wyobraźnia bez wiedzy może stworzyć rzeczy piękne. Wiedza bez wyobraźni
                    najwyżej doskonałe.”&nbsp;</strong></h1>
            <h3>—Albert Einstein—</h3>
        </div>
    </section>
</article>
<article class="container-fluid  grey-cont text-center">
    <div class="container">
        <header><h2 class="elegantshadow"><b>Umiejętności</b></h2></header>
        <br>
        <div class="row skils-container">
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>HTML</b></h2>
                <i class="fab fa-html5"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>CSS3</b></h2>
                <i class="fab fa-css3-alt"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>BOOTSTRAP</b></h2>
                <img src="{{asset(\'img/skils/Boostrap_logo.png\')}}" alt="placeholder+image">
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>PHP</b></h2>
                <i class="fab fa-php"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>LARAVEL</b></h2>
                <i class="fab fa-laravel"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>SYMFONY</b></h2>
                <img src="{{asset(\'img/skils/img_437055.png\')}}" alt="placeholder+image">
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>JAVASCRIPT</b></h2>
                <i class="fab fa-js"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>JQUERY</b></h2>
                <img src="{{asset(\'img/skils/jquery.png\')}}" alt="placeholder+image">
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>VUE</b></h2>
                <i class="fab fa-vuejs"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>GIT</b></h2>
                <i class="fab fa-git-square"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>SQL</b></h2>
                <i class="fas fa-database"></i>
            </section>
            <section class="col-lg-3 col-md-4 col-sm-6 skil-item">
                <h2><b>SEO</b></h2>
                <i class="fas fa-search"></i>
            </section>
        </div>
    </div>
</article>','publishedAt' => '2019-09-07 14:53:52','updatedAt' => '2021-01-14 23:08:35','headtitle' => 'Grzegorz Tarka - kim jestem i moja historia','keyworks' => NULL,'description' => 'Witam nazywam się Grzegorz, programowaniem zajmuje się od nieco ponad roku, jednak trudno powiedzieć mi kiedy to wszystko się zaczęło bo w zasadzie od zawsze interesowałem sie informatyką i programowaniem.','img' => NULL,'type' => '3','number_of_comments' => '0'),
  array('id' => '217','user_id' => '1','title' => 'Grzesiek Tarka-programista webowy','slug' => 'home','content' => '<article class="vertical dark-container">
    <div class="crossfade">
        <figure></figure>
        <figure></figure>
        <figure></figure>
        <figure></figure>
        <figure></figure>
    </div>
    <div class="container-fluid table-cell text-center" id="welcome">
        <div class="container write-text">
        <h1 class="hellow-world wow bounceInDown hidden-xs" data-wow-delay="0.3s">&#60;&#8725;Hellow World&gt</h1>
        <h3 class="main-title tp-splitted" id="demo">Nazywam sie Grzegorz Tarka. Witam cie na mojej stronie</h3>
        </div>
        <a href="#oferta" class="sendmessage_button line-height-50 smoothScroll wow bounceInUp no-ajax-load" data-wow-delay="9s" style="display: inline-block">OFERTA</a>
        <div class="arrow animated infinite bounce no-ajax-load">
            <a class="smoothScroll" style="display: block" href="#projects">
                <img src="{{asset(\'img/down-arrow.svg\')}}" class="ikonaproject" alt="Strzałka">
            </a>
        </div>
    </div>
</article>
<article class="container-fluid bg-white pd black-font background_cutout" id="projects">
    <div class="row">
        <div class="container vertical_align">
            <div class="row">
                <div class="col-lg-5 text-right text-center-md wow zoomInLeft" data-wow-delay="0s"> <img src="{{asset(\'img/home/39fae7201b008036d5d46fde08364d63.jpg\')}}" alt="Kompas"> </div>
                <div class="col-lg-7 text-left wow zoomInRight" data-wow-delay="0s">
                    <h2>Potrzebujesz strony internetowej ?</h2>
                    <h4>Przenieś swój bizes na niespotykany dotąd poziom!</h4>
                    <p>Internet w oczach twoich klientów to gigantyczna porównywarka cen usług i produktów ... tysiące podobnych ofert i wśród nich Ty - zastanów się, czy jesteś dostatecznie widoczny na tle konkurencji ? Profesjonalnie stworzona strona to najlepszy sposób aby twoja marka wyróżniała sie w internecie oraz gwarancja tego, że produkty i usługi oferowane przez ciebie znacznie lepiej będą się prezentować. Czas to pieniądz i ja szanuje twój dlatego proponuje ci w tym miejscu wszystko to czego potrzebujesz! Profesjonalnie stworzona strona na zawsze odmieni twój wizerunek w sieci ! </p>
                </div>
            </div>
        </div>
    </div>
</article>
<article class="container-fluid gradient_bg">
    <div class="container pd-lf-rg-0">
        <header class="row">
            <div class="col-lg-12">
                <h1>Zalety nowoczesnej strony WWW</h1>
            </div>
        </header>
        <div class="row">
            <section class="col-lg-4 wow zoomIn" data-wow-delay="0.1s">
                <h3>Intuicyjna i wygodna</h3>
                <p class="text-left">Zaprojektowana jako podróż dla Klienta po Twojej ofercie, nie wymaga od Klienta szukania i dokonywania niezrozumiałych wyborów - intuicyjnie przeprowadza go krok po kroku, od najważniejszych zalet po szczegółową ofertę, wraz z najbardziej oczywistą czynnością na stronie WWW - przewijaniem.</p>
            </section>
            <section class="col-lg-4 wow zoomIn" data-wow-delay="0.1s">
                <h3>Przejrzysta i czytelna</h3>
                <p class="text-left">Dzięki liniowej organizacji informacji oraz subtelnym animacjom, przewijana strona pomaga Klientowi zapoznać się z produktem. Konieczność zwięzłej prezentacji cech produktu sprawia, że komunikacja z Klientem staje się łatwiejsza.</p>
            </section>
            <section class="col-lg-4 wow zoomIn" data-wow-delay="0.1s">
                <h3>Przyjazna i mobilna</h3>
                <p class="text-left">W erze technologii Twoja oferta powinna być łatwo dostępna z każdego urządzenia - komputera, telewizora, tabletu oraz telefonu. Tworzone przeze mnie strony współpracują z każdym urządzeniem !</p>
            </section>
        </div>
    </div>
</article>
<article>
    <header class="container-fluid offer_conatiner white-font over-hiden" id="oferta">
        <div class="container">
            <div class="row text-left">
                <div class="col-lg-12">
                    <h3 class=" wow fadeInLeft" data-wow-delay="0.5s">Spójrz jakie rzeczy mogę dla ciebie zrobić</h3>
                    <h1 class="bigfont wow fadeInRight" data-wow-delay="0.5s">OFERTA</h1>
                    <hr class="my-hr wow fadeInUp" data-wow-delay="0.8s">
                </div>
            </div>
        </div>
    </header>
    <article class="container-fluid pd-tp-bt-0 bg-white offer offer pd-zero">
        <section class="col-lg-3 col-md-6 col-sm-12 col-xs-12 hover-services" data-mh="my-group">
            <a href="#1" class="scroll_to_content pd-top-bt-50px deskop-padding" data-toggle="tab">
                <i class="fas fa-tv fa-4x" aria-hidden="true"></i>
                <h3>Strony Internetowe</h3>
                <hr class="my-hr-2">
                <p class="spcial-p">Strony internetowe dostosowane do Twoich potrzeb.</p>
            </a>
        </section>
        <section class="col-lg-3 col-md-6 col-sm-12 col-xs-12 hover-services" data-mh="my-group">
            <a href="#2" class="scroll_to_content pd-top-bt-50px deskop-padding" data-toggle="tab">
                <i class="far fa-gem fa-4x" aria-hidden="true"></i>
                <h3>Aplikacje Internetowe</h3>
                <hr class="my-hr-2">
                <p class="spcial-p">Skrypty i Aplikacje bazodanowe.</p>
            </a>
        </section>
        <section class="col-lg-3 col-md-6 col-sm-12 col-xs-12 hover-services" data-mh="my-group">
            <a href="#3" class="scroll_to_content pd-top-bt-50px deskop-padding" data-toggle="tab">
                <i class="fab fa-wordpress fa-4x" aria-hidden="true"></i>
                <h3>Wordpress development</h3>
                <hr class="my-hr-2">
                <p class="spcial-p">Strony oparte na najpopularniejszym na świecie systemie CMS.</p>
            </a>
        </section>
        <section class="col-lg-3 col-md-6 col-sm-12 col-xs-12 hover-services" data-mh="my-group">
            <a href="#4" class="scroll_to_content pd-top-bt-50px deskop-padding" data-toggle="tab">
                <i class="fab fa-free-code-camp fa-4x" aria-hidden="true"></i>
                <h3>Optymalizacja stron i aplikacji</h3>
                <hr class="my-hr-2">
                <p class="spcial-p">Optymalizacja i dodawanie nowych funkcji do stron i istniejących aplikacji.</p>
            </a>
        </section>
    </article>
    <div class="clearfix"></div>
    <article class="container-fluid pd-zero black-font" id="offer_container" style="background-color: #fff">
        <div id="exTab2">
            <div class="tab-content">
                <div class="tab-pane fade in active" id="1">
                    <section class="vertical_align">
                        <div class="col-lg-6 pd-zero text-left padding-left-s">
                            <h1 class="bigfont">STRONY INTERNETOWE</h1>
                            <hr class="my-hr-3">
                            <div class="clearfix"></div>
                            <p class="spcial-p">W dzisiejszym czasie biznes bez strony internetowej skazany jest na porażkę. Każde nowoczesne przedsiębiorstwo stawiające na rozwój buduje swój wizerunek w internecie. Strona internetowa to nie tylko zbiór informacji o firmie. Jest to również potężne narzędzie do budowania długotrwałych relacji firmy z klientem.</p>
                            <p class="spcial-p">Mogę zaprojektować od podstaw uszyta na miarę stronę dostosowaną do twoich potrzeb. Wszystkie projekty są w pełni responsywne (RWD) i dobrze wyglądają na każdym urządzeniu.</p>
                        </div>
                        <div class="col-lg-6 pd-zero">
                            <figure><img src="{{asset(\'img/home/oferta/1/ipad-820272_1920.jpg\')}}" class="img-responsive my-img"></figure>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="2">
                    <section class="vertical_align">
                        <div class="col-lg-6 pd-zero text-left padding-left-s">
                            <h1 class="bigfont">APLIKACJE INTERNETOWE</h1>
                            <hr class="my-hr-3">
                            <div class="clearfix"></div>
                            <p class="spcial-p ">Oferuję wykonanie autorskich, dostosowanych do indywidualnych potrzeb aplikacji internetowych. Dopasowuje funkcjonalność oprogramowania do indywidualnego charakteru firmy, stosując przy tym frameworki PHP, Laravel Framework bądź Codeigniter. Dedykowana aplikacja to duży krok w usprawnianiu działania twojej firmy! </p>
                        </div>
                        <div class="col-lg-6 pd-zero">
                            <figure><img src="{{asset(\'img/home/oferta/2/blur-1853262_1920.jpg\')}}" class="img-responsive my-img"></figure>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="3">
                    <section class="vertical_align">
                        <div class="col-lg-6 pd-zero text-left padding-left-s">
                            <h1 class="bigfont">WORDPRESS</h1>
                            <hr class="my-hr-3">
                            <div class="clearfix"></div>
                            <p class="spcial-p ">WordPress miał swój początek w 2003 roku i posiadał wówczas zaledwie kilku użytkowników. Dziś to najpopularniejszy system CMS na świecie i zasila ponad 27% wszystkich stron internetowych!. Ze względu na na jego funkcjonalność, ogromną liczbę wtyczek i szablonów z powodzeniem możemy użyć go do zarządzania treścią twojej strony, dzięki czemu dodawanie treści na stonie stanie się dziecinne proste!.</p>
                            <p class="spcial-p ">Z moją pomocą uruchomisz własny serwis oparty na tym narzędziu. Mogę stworzyć dla ciebie szablon całkowicie od podstaw lub wykorzystać już gotowy darmowy lub komercyjny szablon.</p>
                        </div>
                        <div class="col-lg-6 pd-zero">
                            <figure><img src="{{asset(\'img/home/oferta/3/wordpress-923188_1920.jpg\')}}" class="img-responsive my-img"></figure>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="4">
                    <section class="vertical_align">
                        <div class="col-lg-6 pd-zero text-left padding-left-s">
                            <h1 class="bigfont">OPTYMALIZACJA</h1>
                            <hr class="my-hr-3">
                            <div class="clearfix"></div>
                            <p class="spcial-p ">Ile razy wchodząc na stronę internetową z powodu długiego wczytywania po kilku chwilach frustracji przechodzisz do innej strony w wyszukiwarce. Mimo tego że strona może zawierać potrzebne rzeczy czy informacje pomijasz ją z powodu długiego czasu ładowania. </p>
                            <p class="spcial-p">Zadbaj o to aby twoja strona była należycie zoptymalizowana i sytuacje tego typu nie dotyczyły twoich wizytówek w sieci.</p>
                        </div>
                        <div class="col-lg-6 pd-zero">
                            <figure><img src="{{asset(\'img/home/oferta/4/military-jet-1053394_1280.jpg\')}}" class="img-responsive my-img"></figure>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </article>
</article>
<article class="container-fluid home_contact">
    <div class="row vertical_align">
        <div class="col-lg-6">
            <h4 class="white-font">Masz pytania?</h4>
        </div>
        <div class="col-lg-6"> <a class="home-link ajax" href="{{ route(\'contact.index\') }}">Skontaktuj się</a> </div>
    </div>
</article>
<article class="container-fluid bg-white hidden-sm hidden-xs">
       <i class="fab fa-html5 custom-icon wow slideInLeft" data-wow-delay="0.2s" aria-hidden="true"></i> <i class="fab fa-css3-alt custom-icon wow slideInLeft" data-wow-delay="0.3s" aria-hidden="true"></i> <i class="fas fa-database custom-icon wow slideInLeft" data-wow-delay="0.4s" aria-hidden="true"></i> <i class="fab fa-wordpress custom-icon wow slideInLeft" data-wow-delay="0.5s" aria-hidden="true"></i> <i class="fab fa-facebook custom-icon  wow slideInLeft" data-wow-delay="0.6s" aria-hidden="true"></i> <i class="fab fa-google custom-icon  wow slideInLeft" data-wow-delay="0.7s" aria-hidden="true"></i> <i class="fab fa-firefox custom-icon wow slideInLeft" data-wow-delay="0.8s" aria-hidden="true"></i> <i class="fab fa-chrome custom-icon wow slideInLeft" data-wow-delay="0.9s" aria-hidden="true"></i> <i class="fab fa-opera custom-icon wow slideInLeft" data-wow-delay="1s" aria-hidden="true"></i> <i class="fab fa-internet-explorer custom-icon wow slideInLeft" data-wow-delay="1.1s" aria-hidden="true"></i> <i class="fab fa-safari custom-icon wow slideInLeft" data-wow-delay="1.2s" aria-hidden="true"></i> <i class="fab fa-github custom-icon wow slideInLeft" data-wow-delay="1.3s" aria-hidden="true"></i> <i class="fab fa-windows custom-icon wow slideInLeft" data-wow-delay="1.4s" aria-hidden="true"></i> <i class="fab fa-linux custom-icon wow slideInLeft" data-wow-delay="1.5s" aria-hidden="true"></i>
</article>','publishedAt' => '2019-09-07 14:53:52','updatedAt' => '2019-09-07 14:53:52','headtitle' => 'Grzesiek Tarka-programista webowy','keyworks' => '','description' => 'Tworzenie wyjątkowych stron internetowych. Freelancer/ web developer. Aplikacje webowe frontend i backend. Projekty w PHP,Javascript, CSS, HTML.','img' => NULL,'type' => '3','number_of_comments' => '0'),
  array('id' => '218','user_id' => '1','title' => 'Grzegorz Tarka - kim jestem i moja historia2','slug' => 'portfolio','content' => '<article class="container-fluid pd-tp-bt-0">
    <div class="row ligh-bg blog">
        <div class="col-lg-12 white-font">
            <h1>Szablony stron</h1>
        </div>
    </div>
</article>
<article class="container-fluid pd-top-bt-50px black-font grey-cont">
    <header>
        <nav class="nav_portfolio row">
            <ul class="simplefilter">
                <li class="active" data-filter="all">WSZYSTKIE</li>
                <li data-filter="1">PHOTO</li>
                <li data-filter="2">RESTAURANT</li>
                <li data-filter="3">SPORT</li>
                <li data-filter="4">MUSIC</li>
                <li data-filter="5">WIZYTÓWkI</li>
                <li data-filter="7">INNE</li>
            </ul>
        </nav>
    </header>
    <section class="container">
        <!-- Shuffle & Sort Controls -->
        <div class="row mg-bt-top-10px">
            <ul class="sortandshuffle pd-zero">
                <li class="shuffle-btn" data-shuffle>Shuffle</li>
            </ul>
        </div>
        <!-- Search control -->
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <div class="form-group">
                    <div class="controls"> 
                        <input type="text" placeholder="Szukaj po tytule..." class="form-control form-control-special" name="filtr-search" data-search> 
                    </div>
                </div>
            </div>
        </div>
        <div class="row filtr-container">
            <a href="portfolio-strony/szablony/29" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme29 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Photo#1</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/28" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Ecology#1">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme28 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Ecology#1</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/27" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#1">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme27 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#1</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/26" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme26 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#2</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/25" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="3" data-sort="Sport#1">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme25 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Sport#1</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/24" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme24 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Photo#2</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/23" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo#3">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme23 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Photo#3</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/22" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Dreamhouse">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme22 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">dreamhouse</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/21" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Restless machines">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme21 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Wizytówki#1</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/20" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Restless machines">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme20 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">restless machines</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/19" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo#4">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme19 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Photo#4</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/18" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#3">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme18 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#3</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/17" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="7" data-sort="Inne#1">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme17 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Inne#1</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/16" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Wizytówki#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme16 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Wizytówki#2</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/15" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#4">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme15 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#4</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/14" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#5">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme14 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#5</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/13" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="7" data-sort="Inne#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme13 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Inne#2</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/12" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo#5">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme12 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Photo#5</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/11" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="3" data-sort="Sport#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme11 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Sport#2</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/10" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="7" data-sort="Inne#3">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme10 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Inne#3</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/9" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="5" data-sort="Wizytówki#3">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme9 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Wizytówki#3</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/8" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="1" data-sort="Photo#6">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme8 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Photo#6</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/6" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="4" data-sort="Music#1">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme6 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Music#1</span> 
                </div>
            </a>
            <a href="portfolio-strony/szablony/5" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="4" data-sort="Music#2">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme5 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Music#2</span> 
                </div>
            </a>
            <a href="" target="_blank" class="portfolio-link">
                <div class="col-xs-12 col-sm-6 col-md-3 filtr-item" data-category="2" data-sort="Restaurant#6">
                    <div class="over-hiden"> <img class="img-responsive sort-img" src="{{asset(\'img/szablony/theme4 - kopia.jpg\')}}" alt="sample image"> </div> <span class="item-desc">Restaurant#6</span> 
                </div>
            </a>
        </div>
    </section>
</article>
<script type="text/javascript">
    function initSort() {
        $.getScript("js/sort/jquery.filterizr.js");
        //Simple filter controls
        $(\'.simplefilter li\').click(function () {
            $(\'.simplefilter li\').removeClass(\'active\');
            $(this).addClass(\'active\');
        });
        //Multifilter controls
        $(\'.multifilter li\').click(function () {
            $(this).toggleClass(\'active\');
        });
        //Shuffle control
        $(\'.shuffle-btn\').click(function () {
            $(\'.sort-btn\').removeClass(\'active\');
        });
        //Sort controls
        $(\'.sort-btn\').click(function () {
            $(\'.sort-btn\').removeClass(\'active\');
            $(this).addClass(\'active\');
        });
        setTimeout(function () {
            $(\'.filtr-container\').filterizr();
        }, 1000);
    }

    window.onload = function () {
        initSort();
    };

    if (window.jQuery) {
        initSort();
    }
</script>','publishedAt' => '2019-09-07 14:53:52','updatedAt' => '2019-09-07 14:53:52','headtitle' => 'Grzegorz Tarka - kim jestem i moja historia2','keyworks' => '','description' => 'Witam nazywam się Grzegorz, programowaniem zajmuje się od nieco ponad roku, jednak trudno powiedzieć mi kiedy to wszystko się zaczęło bo w zasadzie od zawsze interesowałem sie informatyką i programowaniem.','img' => NULL,'type' => '3','number_of_comments' => '0'),
  array('id' => '219','user_id' => '1','title' => 'Ct8 zmiana wersji php','slug' => 'ct8-zmiana-wersji-php','content' => '<pre class="language-php"><code>ln -sf /usr/local/bin/php73 ~/bin/php</code></pre>','publishedAt' => '2019-09-07 00:00:00','updatedAt' => '2019-09-07 00:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '220','user_id' => '1','title' => 'Wzorzec adapter','slug' => 'wzorzec-adapter','content' => '<pre class="language-php"><code>&lt;?php

ini_set(\'display_errors\', 1);
ini_set(\'display_startup_errors\', 1);
error_reporting(E_ALL);

interface ImageResizeInterface {

    public function setImage(string $fileName);

    public function resizeTo(string $width, string $height);

    public function resizeHeightByWidth(string $width);

    public function save();
}

class ResizeImage implements ImageResizeInterface {

    public function setImage(string $fileName) {
        //code
    }

    public function resizeTo(string $width, string $height) {
        //code
    }

    public function resizeHeightByWidth(string $width) {
        //code
    }

    public function save() {
        //code
    }

}

interface ImageInterface {

    public function setImageToResize(string $fileName);

    public function resizeImageTo(string $imageWidth, string $imageHeight);

    public function resizeImageHeightByWidth(string $width);

    public function saveImage();
}

class ImageResize implements ImageInterface {

    public function setImageToResize(string $fileName) {
        //code
    }

    public function resizeImageTo(string $imageWidth, string $imageHeight) {
        //code
    }

    public function resizeImageHeightByWidth(string $width) {
        //code
    }

    public function saveImage() {
        //code
    }

}

class ImageAdapter implements ImageResizeInterface {

    private $image;

    public function __construct(ImageResize $image) {
        $this-&gt;image = $image;
    }

    public function setImage(string $fileName) {
        $this-&gt;image-&gt;setImageToResize($fileName);
    }

    public function resizeTo(string $width, string $height) {
        $this-&gt;image-&gt;resizeImageTo($width, $height);
    }

    public function resizeHeightByWidth(string $width) {
        $this-&gt;image-&gt;resizeImageHeightByWidth($width);
    }

    public function save() {
        $this-&gt;image = saveImage();
    }

}

$imageAdapter = new ImageAdapter(new ImageResize());


var_dump($imageAdapter);
</code></pre>
<p>2//</p>
<pre class="language-php"><code>&lt;?php

//Book
class Book {

    private $author;
    private $title;

    function __construct(string $author, string $title) {
        $this-&gt;author = $author;
        $this-&gt;title = $title;
    }

    function getAuthor(): string {
        return $this-&gt;author;
    }

    function getTitle(): string {
        return $this-&gt;title;
    }

}

//book adapter
class BookAdapter {

    private $book;

    function __construct(Book $book) {
        $this-&gt;book = $book;
    }

    function getAuthorAndTitle(): string {
        return $this-&gt;book-&gt;getTitle() . \' by \' . $this-&gt;book-&gt;getAuthor();
    }

}

// client
$book = new Book("J.R.R. Tolkien", "Władca Pierścieni:");
$bookAdapter = new BookAdapter($book);

echo (\'Author and Title: \' . $bookAdapter-&gt;getAuthorAndTitle());
</code></pre>','publishedAt' => '2019-12-10 00:00:00','updatedAt' => '2019-12-10 00:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '221','user_id' => '1','title' => 'Regex z forum','slug' => 'regex-z-forum','content' => '<pre class="language-php"><code>&lt;?php
 
$regex = \'/\\d(\\.\\d{1,2})+/\';
 
$inputs = [
    \'1.1\',
    \'1.11\',
    \'1.11.1\'
];
 
foreach( $inputs as $input ) {
    $matches = [];
    preg_match( $regex, $input, $matches );
    var_dump( $matches );
}</code></pre>','publishedAt' => '2019-09-17 00:00:00','updatedAt' => '2019-09-17 00:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '222','user_id' => '1','title' => 'Php obcinanie stringa','slug' => 'php-obcinanie-stringa','content' => '<pre class="language-php"><code>&lt;?php
$test = \'ęą\';

$res1 = substr($test, 0, 11));
$res2 = mb_substr($test, 0, 11));

//substr sie wykrzczy na polskich znakach!!!! </code></pre>','publishedAt' => '2019-09-26 12:00:00','updatedAt' => '2019-12-14 12:24:33','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '223','user_id' => '1','title' => 'Zapytanie zliczające w relacyjnej bazie danych','slug' => 'zapytanie-zliczajace-w-relacyjnej-bazie-danych','content' => '<pre class="language-php"><code>Mamy tabelę \'users\':
------------------------------
id, name, surname
------------------------------

i tabelę \'trade\' gdzie wystawiają uzytkownicy przedmioty
--------------------------------------------------
id, item, id_user (relacja do users.id)
--------------------------------------------------

Jak wyświetlić użytkownik&oacute;w kt&oacute;rzy mają wystawione co najmniej powiedzmy 5 przedmiot&oacute;w w tabeli trade?
</code></pre>
<pre class="language-php"><code>SELECT users.name FROM users 
JOIN trade ON trade.id_user = users.id
GROUP BY users.id
HAVING COUNT(trade.id) &gt;= 5</code></pre>','publishedAt' => '2019-09-26 12:00:00','updatedAt' => '2019-09-26 12:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '224','user_id' => '1','title' => 'Wzorzec dekorator','slug' => 'wzorzec-dekorator','content' => '<pre class="language-php"><code>&lt;?php
interface CarInterface {
    public function calcualtePrice(): int;
}

abstract class CarDecorator implements CarInterface {
    protected $car;
    public function __construct(CarInterface $car) {
        $this-&gt;car = $car;
    }
}

Class Car implements CarInterface {
    public function calcualtePrice(): int {
        return 5000;
    }
}

Class CarWithAirConditioning extends CarDecorator {
    public function calcualtePrice(): int {
        return $this-&gt;car-&gt;calcualtePrice() + 101;
    }
}

Class CarWithSunRoof extends CarDecorator {
    public function calcualtePrice(): int {
        return $this-&gt;car-&gt;calcualtePrice() + 70;
    }
}

Class CarWith5Skin extends CarDecorator {
    public function calcualtePrice(): int {
        return $this-&gt;car-&gt;calcualtePrice() + 111;
    }
}

Class CarWithCamera extends CarDecorator {
    public function calcualtePrice(): int {
        return $this-&gt;car-&gt;calcualtePrice() + 52;
    }
}

$car1 = new CarWithCamera(new CarWithAirConditioning(new Car()));
var_dump($car1-&gt;calcualtePrice());

$car2 = new CarWithAirConditioning(new CarWith5Skin(new Car()));
var_dump($car2-&gt;calcualtePrice());

$car3 = new Car();
$car3 = new CarWithAirConditioning($car3);
$car3 = new CarWithSunRoof($car3);
$car3 = new CarWith5Skin($car3);
$car3 = new CarWithCamera($car3);
var_dump($car3-&gt;calcualtePrice());</code></pre>','publishedAt' => '2019-09-27 12:00:00','updatedAt' => '2019-09-27 12:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '225','user_id' => '1','title' => 'Event delegation','slug' => 'event-delegation','content' => '<pre class="language-php"><code>document.addEventListener(\'DOMContentLoaded\', function(){
    body.addEventListener(\'click\', function (e) {
        if (e.target.classList.contains("ajax") || e.target.classList.contains("page-link")) {
            e.preventDefault();
            const url=e.target.getAttribute(\'href\');
            getPageWithAjax(url);
        }

        if (e.target.id==\'searchButton\') {
            e.preventDefault();
            const searchWords=this.querySelector(\'#title\').value;
            const action=this.querySelector(\'#searchForm\').action;
            const url =action+\'?title=\'+searchWords;
            getPageWithAjax(url);
        }
    });

   if (window.history &amp;&amp; window.history.pushState) {
        //window.history.pushState(\'forward\', null, \'./#forward\');
        $(window).on(\'popstate\', function() {
            getPageWithAjax(window.location.href);
        });
    }
});</code></pre>','publishedAt' => '2019-10-02 13:00:00','updatedAt' => '2019-10-02 13:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '226','user_id' => '1','title' => 'Php ważne','slug' => 'php-wazne','content' => '<pre class="source">Funkcje substr i strlen krzaczą się z polskimi znakami. Zamiast nich używać :</pre>
<pre class="language-php"><code>&lt;?php

$string = \'ć&oacute;w123214\';

$lenght = mb_strlen($string, \'UTF-8\');
$cut = mb_substr($string, 0, 2, \'UTF-8\');

var_dump($lenght);
var_dump($cut);</code></pre>','publishedAt' => '2019-10-04 12:00:00','updatedAt' => '2019-12-14 12:22:29','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '227','user_id' => '1','title' => 'symfony zapytanie nie puste i nie null','slug' => 'symfony-zapytanie-nie-puste-i-nie-null','content' => '<pre class="language-php"><code>public function getReportsValidationCostsHavingGoogleAccount(): array {
        return $this-&gt;createQueryBuilder(\'c\')
                        -&gt;andWhere(\'c.google_account != \\\'\\\'\')
                        -&gt;getQuery()
                        -&gt;getResult()
        ;
}</code></pre>','publishedAt' => '2019-10-09 12:00:00','updatedAt' => '2019-10-09 12:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '228','user_id' => '1','title' => 'Kierowanie forma na na konkretny adres','slug' => 'kierowanie-forma-na-na-konkretny-adres','content' => '<pre class="language-php"><code>$formSendSms = $this-&gt;createForm(SmsType::class, [], [\'action\' =&gt; $this-&gt;generateUrl(\'user_two_step_auth_notification_option\'), \'method\' =&gt; \'POST\'])-&gt;handleRequest($request);
$formSendEmail = $this-&gt;createForm(EmailType::class, [], [\'action\' =&gt; $this-&gt;generateUrl(\'user_two_step_auth_notification_option\'), \'method\' =&gt; \'POST\'])-&gt;handleRequest($request);
</code></pre>','publishedAt' => '2019-10-10 12:00:00','updatedAt' => '2019-10-10 12:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '229','user_id' => '1','title' => 'Konfiguracja swiftmailera w symfony','slug' => 'konfiguracja-swiftmailera-w-symfony','content' => '<pre class="language-markup"><code>MAILER_URL=smtp://odrserwer.home.pl:465?encryption=ssl&amp;auth_mode=login&amp;username=noreplay%40confronter.pl&amp;password=xxx_your_encoder_password_xxx
</code></pre>
<p>Aby wysyłka emaila działała należy ustawić zaencodeowane hasło @ np w tym przypadku ma postać %40</p>','publishedAt' => '2019-10-11 12:00:00','updatedAt' => '2019-10-11 12:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '230','user_id' => '1','title' => 'Mysql najlepsze kodowanie znaków','slug' => 'mysql-najlepsze-kodowanie-znakow','content' => '<p><strong>It is best to use character set&nbsp;<code>utf8mb4</code>&nbsp;with the collation&nbsp;<code>utf8mb4_unicode_ci</code>.</strong></p>','publishedAt' => '2019-10-14 12:00:00','updatedAt' => '2019-10-14 12:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '231','user_id' => '1','title' => 'największa ilość wpisów z danej kategori','slug' => 'najwieksza-ilosc-wpisow-z-danej-kategori','content' => '<pre class="language-php"><code>SELECT cat_id,COUNT(cat_id) FROM auctionsEngineTest GROUP BY cat_id ORDER BY COUNT(cat_id) DESC LIMIT 1
</code></pre>','publishedAt' => '2019-10-14 12:12:12','updatedAt' => '2019-10-14 12:12:12','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '232','user_id' => '1','title' => 'złe praktyki w sql','slug' => 'zle-praktyki-w-sql','content' => '<p><a href="https://pl.seequality.net/10-zlych-praktyk-niebezpiecznych-nawykow-w-sql-server/">link</a></p>
<p><a href="https://www.eversql.com/faster-pagination-in-mysql-why-order-by-with-limit-and-offset-is-slow/" target="_blank" rel="noopener">paginacja dlaczego ejst wolna</a></p>','publishedAt' => '2019-10-14 12:12:12','updatedAt' => '2019-10-14 12:12:12','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '233','user_id' => '1','title' => 'Siatka grid w css','slug' => 'siatka-grid-w-css','content' => '<pre class="language-markup"><code>&lt;div class="container"&gt;
  &lt;div class="col-1"&gt;col-1&lt;/div&gt;
  &lt;div class="col-2"&gt;col-2&lt;/div&gt;
  &lt;div class="col-3"&gt;col-3&lt;/div&gt;
  &lt;div&gt;col-4&lt;/div&gt;
  &lt;div&gt;col-4&lt;/div&gt;
  &lt;div&gt;col-4&lt;/div&gt;
  &lt;div&gt;col-4&lt;/div&gt;
&lt;/div&gt;</code></pre>
<pre class="language-css"><code>.container {
  width: 1000px;
  height: 500px;
  margin: 30px auto;
  
  display: grid;
  grid-template-rows: repeat(4, 1fr);
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 20px;
  
  &amp; &gt; * {
    border: 5px solid black;
    display: flex;
    align-items: center;
    justify-content: center;
  }
 
  .col-1 {
    grid-column: 1 / 3;
    grid-row: 1 / 3;
  }
  .col-2, .col-3 {
    grid-column: 3 / 5;
  }
}</code></pre>','publishedAt' => '2019-10-15 12:00:00','updatedAt' => '2019-10-15 12:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '234','user_id' => '1','title' => 'Siatka flex w css','slug' => 'siatka-flex-w-css','content' => '<pre class="language-markup"><code>&lt;div class="container"&gt;
  &lt;div class="row"&gt;
    &lt;div class="col"&gt;&lt;/div&gt;
    &lt;div class="col"&gt;
      &lt;div class="col"&gt;&lt;/div&gt;
      &lt;div class="col"&gt;&lt;/div&gt;  
    &lt;/div&gt;
  &lt;/div&gt;
  &lt;div class="row"&gt;
      &lt;div class="col"&gt;&lt;/div&gt;
      &lt;div class="col"&gt;&lt;/div&gt;  
      &lt;div class="col"&gt;&lt;/div&gt;
      &lt;div class="col"&gt;&lt;/div&gt;  
  &lt;/div&gt;
&lt;/div&gt;</code></pre>
<pre class="language-css"><code>* { box-sizing: border-box; }
.container {
  width: 80%;
  margin: 0 auto;
}
.row {
  display: flex;
  width: 100%;
}
.col {
  flex: 1;
  border: 5px solid black;
  min-height: 150px;
  margin: 5px;
}</code></pre>','publishedAt' => '2019-10-15 13:00:00','updatedAt' => '2019-10-15 13:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '235','user_id' => '1','title' => 'array map php funkcja anonimowa','slug' => 'array-map-php-funkcja-anonimowa','content' => '<pre class="language-php"><code>return array_map(function($value) {
   return $value[\'1\'];
}, $result);</code></pre>','publishedAt' => '2019-10-15 12:00:00','updatedAt' => '2019-10-15 12:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '236','user_id' => '1','title' => 'wybranie pola z relacji w symfony','slug' => 'wybranie-pola-z-relacji-w-symfony','content' => '<pre class="language-php"><code>public function getRejectedInstitutionsIdsForClientByInstitutionsIdsAndDate(Client $client, array $institutionsIds, \\DateTime $dateTime): array {
        $query = $this-&gt;createQueryBuilder(\'cis\')
                        -&gt;select(\'IDENTITY(cis.institution)\')
                        -&gt;andWhere(\'cis.client = :client\')-&gt;setParameter(\'client\', $client)
                        -&gt;andWhere(\'cis.institution IN (:institutions)\')-&gt;setParameter(\'institutions\', $institutionsIds)
                        -&gt;andWhere(\'cis.date_of_possible_next_serve &gt;= :date_of_possible_next_serve\')-&gt;setParameter(\'date_of_possible_next_serve\', $dateTime);

        $result = $query-&gt;getQuery()-&gt;getResult();

        return array_map(function($value) {
            return $value[\'1\'];
        }, $result);
}</code></pre>','publishedAt' => '2019-10-15 12:00:00','updatedAt' => '2020-04-02 09:42:26','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '237','user_id' => '1','title' => 'ustawienie ut8-8 w pdo','slug' => 'ustawienie-ut8-8-w-pdo','content' => '<pre class="language-php"><code>$connect = new PDO(
  "mysql:host=$host;dbname=$db", 
  $user, 
  $pass, 
  array(
    PDO::ATTR_ERRMODE =&gt; PDO::ERRMODE_EXCEPTION,
    PDO::MYSQL_ATTR_INIT_COMMAND =&gt; "SET NAMES utf8"
  )
);</code></pre>','publishedAt' => '2019-10-17 00:00:00','updatedAt' => '2019-10-17 00:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '238','user_id' => '1','title' => 'sql pokazywanie statusu cache','slug' => 'sql-pokazywanie-statusu-cache','content' => '<pre class="language-php"><code>SHOW VARIABLES LIKE \'%query_cache_type%\'</code></pre>','publishedAt' => '2019-10-17 00:00:00','updatedAt' => '2019-10-17 00:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '239','user_id' => '1','title' => 'Łapanie elmentów o podobnej nazwie','slug' => 'lapanie-elmentow-o-podobnej-nazwie','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
    &lt;head&gt;
        &lt;meta charset="UTF-8"&gt;
        &lt;title&gt;Document&lt;/title&gt;
    &lt;/head&gt;
    &lt;body&gt;
        &lt;div class="sp-module-content"&gt;
            &lt;ul id="middle menu" class="nav menu"&gt;
                &lt;li class="item-118"&gt;
                    &lt;a href="/www orla/index.php/kontakt-menu-middle"&gt;
                        Zadzwoń: 61 666 666
                    &lt;/a&gt;
                &lt;/li&gt;
                &lt;li class="item-119"&gt;...&lt;/li&gt;
                &lt;li class="item-120"&gt;...&lt;/li&gt;
            &lt;/ul&gt;
        &lt;/div&gt;
        &lt;style type="text/css"&gt;
            .sp-module-content .nav [class*=\'item-\'] a  {
                color:pink;
            }
        &lt;/style&gt;
    &lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2019-10-17 00:00:00','updatedAt' => '2019-10-17 00:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '240','user_id' => '1','title' => 'Normlaize polskich znaków','slug' => 'normlaize-polskich-znakow','content' => '<pre class="language-php"><code>/**
 * Description of NormalizeNoneLatinCharacters
 *
 * @author Grzegorz Tarka
 */
class NormalizeNotPolishLetters {

    /**
     * normalize
     * 
     * @param string $string
     * 
     * @return string|null
     */
    public static function normalize(?string $string): ?string {

        $table = [
            \'&Scaron;\' =&gt; \'S\', \'&scaron;\' =&gt; \'s\', \'Đ\' =&gt; \'Dj\', \'đ\' =&gt; \'dj\', \'Ž\' =&gt; \'Z\', \'ž\' =&gt; \'z\', \'Č\' =&gt; \'C\', \'č\' =&gt; \'c\',
            \'&Agrave;\' =&gt; \'A\', \'&Aacute;\' =&gt; \'A\', \'&Acirc;\' =&gt; \'A\', \'&Atilde;\' =&gt; \'A\', \'&Auml;\' =&gt; \'A\', \'&Aring;\' =&gt; \'A\', \'&AElig;\' =&gt; \'A\', \'&Ccedil;\' =&gt; \'C\', \'&Egrave;\' =&gt; \'E\', \'&Eacute;\' =&gt; \'E\',
            \'&Ecirc;\' =&gt; \'E\', \'&Euml;\' =&gt; \'E\', \'&Igrave;\' =&gt; \'I\', \'&Iacute;\' =&gt; \'I\', \'&Icirc;\' =&gt; \'I\', \'&Iuml;\' =&gt; \'I\', \'&Ntilde;\' =&gt; \'N\', \'&Ocirc;\' =&gt; \'O\',
            \'&Otilde;\' =&gt; \'O\', \'&Ouml;\' =&gt; \'O\', \'&Oslash;\' =&gt; \'O\', \'&Ugrave;\' =&gt; \'U\', \'&Uacute;\' =&gt; \'U\', \'&Ucirc;\' =&gt; \'U\', \'&Uuml;\' =&gt; \'U\', \'&Yacute;\' =&gt; \'Y\', \'&THORN;\' =&gt; \'B\', \'&szlig;\' =&gt; \'Ss\',
            \'&agrave;\' =&gt; \'a\', \'&aacute;\' =&gt; \'a\', \'&acirc;\' =&gt; \'a\', \'&atilde;\' =&gt; \'a\', \'&auml;\' =&gt; \'a\', \'&aring;\' =&gt; \'a\', \'&aelig;\' =&gt; \'a\', \'&ccedil;\' =&gt; \'c\', \'&egrave;\' =&gt; \'e\', \'&eacute;\' =&gt; \'e\',
            \'&ecirc;\' =&gt; \'e\', \'&euml;\' =&gt; \'e\', \'&igrave;\' =&gt; \'i\', \'&iacute;\' =&gt; \'i\', \'&icirc;\' =&gt; \'i\', \'&iuml;\' =&gt; \'i\', \'&eth;\' =&gt; \'o\', \'&ntilde;\' =&gt; \'n\',
            \'&ocirc;\' =&gt; \'o\', \'&otilde;\' =&gt; \'o\', \'&ouml;\' =&gt; \'o\', \'&oslash;\' =&gt; \'o\', \'&ugrave;\' =&gt; \'u\', \'&uacute;\' =&gt; \'u\', \'&ucirc;\' =&gt; \'u\', \'&yacute;\' =&gt; \'y\', \'&yacute;\' =&gt; \'y\', \'&thorn;\' =&gt; \'b\',
            \'&yuml;\' =&gt; \'y\', \'Ŕ\' =&gt; \'R\', \'ŕ\' =&gt; \'r\', "&ograve;" =&gt; \'&oacute;\', \'&Ograve;\' =&gt; \'&Oacute;\', \'ė\' =&gt; \'e\', \'Ė\' =&gt; \'E\'
        ];

        return strtr($string, $table);
    }

}</code></pre>','publishedAt' => '2019-10-18 12:00:00','updatedAt' => '2019-10-18 12:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '241','user_id' => '1','title' => 'mysql slow logi','slug' => 'mysql-slow-logi','content' => '<p>W tym miejscu można przytoczyć następujący przykład: powtarzane tysiąc razy na sekundę zapytanie wykonywane w ciągu dziesięciu milisekund stanowi większe<br />obciążenie dla serwera niż zapytanie wykonywane raz na sekundę i trwające dziesięć sekund.</p>
<p>Przedstawione poniżej opcje konfiguracyjne powodują włączenie dziennika zdarzeń, przechwycenie wszystkich zapytań wykonywanych dłużej niż dwie sekundy oraz zarejestrowanie zapytań, kt&oacute;re nie używają indeks&oacute;w. Dziennik będzie r&oacute;wnież zapisywał powolne polecenia<br />administracyjne, takie jak OPTIMIZE TABLE:</p>
<p><strong>log-slow-queries = &lt;nazwa_pliku&gt;</strong><br /><strong>long_query_time = 2</strong><br /><strong>log-queries-not-using-indexes</strong><br /><strong>log-slow-admin-statements</strong></p>
<p style="text-align: left;">Profilowanie | 83<br />Powyższe opcje konfiguracyjne należy dostosować do własnych potrzeb, a następnie umieścić w pliku konfiguracyjnym serwera o nazwie my.cnf.&nbsp;</p>
<p style="text-align: left;">Wartość domyślna opcji long_query_time wynosi dziesięć sekund. W większości zastosowań to zbyt długi okres czasu, autorzy zazwyczaj ustawiają tutaj dwie sekundy. Jednak w określonych sytuacjach nawet jedna sekunda będzie zbyt długim okresem czasu. Dokładne dostrojenie procesu rejestrowania zdarzeń zostało przedstawione w kolejnym podrozdziale.</p>
<p style="text-align: left;">W MySQL 5.1 zmienne systemowe slow_query_log i slow_query_log_file zapewniają w trakcie działania kontrolę nad dziennikiem powolnych zdarzeń, ale w MySQL 5.0 nie można<br />włączyć lub wyłączyć powolnych zdarzeń bez ponownego uruchomienia serwera MySQL. W MySQL 5.0 obejściem problemu zwykle jest zmienna long_query_time, kt&oacute;rej wartość<br />można dynamicznie zmieniać. Przedstawione poniżej polecenie faktycznie nie wyłącza dziennika powolnych zdarzeń, ale praktycznie daje taki efekt. (Jeżeli jakiekolwiek zapytanie w aplikacji jest wykonywane w czasie dłuższym niż 10000 sekund, to i tak należy je zoptymalizować!).</p>','publishedAt' => '2019-10-21 00:00:00','updatedAt' => '2019-10-21 00:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '242','user_id' => '1','title' => 'fulltext mysql','slug' => 'fulltext-mysql','content' => '<pre class="language-php"><code>SELECT uniqueIDS FROM
(
    SELECT * FROM search_V2 WHERE siteID=1 AND status=1
) A
WHERE MATCH(data) AGAINST (\'scale\' IN BOOLEAN MODE);</code></pre>
<p><a href="https://dba.stackexchange.com/questions/15214/why-is-like-more-than-4x-faster-than-match-against-on-a-fulltext-index-in-mysq/15218#15218?newreg=36837d8616984e289377475b27ee0758" target="_blank" rel="noopener">link</a></p>
<p>&nbsp;</p>','publishedAt' => '2019-10-22 00:00:00','updatedAt' => '2019-10-22 00:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '243','user_id' => '1','title' => 'Zmiana limitu pamięci w phpstan','slug' => 'zmiana-limitu-pamieci-w-phpstan','content' => '<pre class="language-php"><code>php vendor/bin/phpstan analyse --memory-limit=-1
</code></pre>','publishedAt' => '2019-10-23 00:00:00','updatedAt' => '2019-10-23 00:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '244','user_id' => '1','title' => 'Uuid w mysql','slug' => 'uuid-w-mysql','content' => '<p>Jeżeli przechowywane są wartości UUID, należy usunąć myślniki lub najlepiej skonwertować za pomocą funkcji UNHEX() wartości UUID na 16-bajtowe liczby, kt&oacute;re następnie będą przechowywane w kolumnie BINARY(16). Pobranie wartości w formacie szesnastkowym odbywa się za pomocą funkcji HEX().<br />Wartości wygenerowane przez UUID() mają charakterystykę inną od wartości wygenerowanych przez funkcję kryptograficzną, np. SHA1(); wartości UUID są nier&oacute;wnomiernie rozproszone i w pewien spos&oacute;b sekwencyjne. Jednak nadal nie jest to tak dobre rozwiązanie jak monotonie zwiększana o jednostkę liczba całkowita.</p>','publishedAt' => '2019-10-27 00:00:00','updatedAt' => '2019-10-27 00:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '245','user_id' => '1','title' => 'Klasa do wysyłki smsow','slug' => 'klasa-do-wysylki-smsow','content' => '<pre class="language-php"><code>&lt;?php

namespace App\\Utils\\ExternalApi\\SmsApiAdapter;

use Symfony\\Component\\DependencyInjection\\ContainerInterface as Container;

class SmsApiAdapter {

    /**
     * @var Container 
     */
    private $container = null;

    /**
     * @var string 
     */
    private $token = null;

    /**
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this-&gt;container = $container;
    }

    /**
     * authorization
     * 
     * @return void
     */
    public function authorization(): void {
        $this-&gt;token = $this-&gt;container-&gt;getParameter(\'sms_smsapi_token\');
    }

    /**
     * send sms
     * 
     * @param string $phoneNumber - phone number
     * @param string $senderName - sender name
     * @param string $content - message content
     * 
     * @return mixed
     */
    public function sendSms(string $phoneNumber, string $senderName, string $content) {
        $params = array(
            \'to\' =&gt; $phoneNumber,
            \'from\' =&gt; $senderName,
            \'message\' =&gt; "" . $content . "",
        );

        return $this-&gt;sms_send($params, $this-&gt;token);
    }

    /**
     * send sms 
     * 
     * @param array $params
     * @param string $token
     * @param bool $backup
     * @return mixes
     * 
     * @throws \\Exception
     */
    private function sms_send(array $params, string $token, bool $backup = false) {
        $result = null;

        if ($backup == true) {
            $url = \'https://api2.smsapi.pl/sms.do\';
        } else {
            $url = \'https://api.smsapi.pl/sms.do\';
        }

        $c = curl_init();
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_POST, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, $params);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_HTTPHEADER, array(
            "Authorization: Bearer $token"
        ));

        $result = curl_exec($c);
        $http_status = curl_getinfo($c, CURLINFO_HTTP_CODE);

        if ($http_status != 200 &amp;&amp; $backup == false) {
            $backup = true;
            $this-&gt;sms_send($params, $token, $backup);
        }

        if (!(mb_strlen($result, \'utf-8\') &gt;= 2 &amp;&amp; $result[0] == \'O\' &amp;&amp; $result[1] == \'K\')) {
            throw new \\Exception(\'SmsAPI - \' . $result);
        }

        curl_close($c);

        return $result;
    }

}
</code></pre>','publishedAt' => '2019-10-28 00:00:00','updatedAt' => '2020-07-01 17:10:11','headtitle' => NULL,'keyworks' => NULL,'description' => 'test','img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '246','user_id' => '1','title' => 'Sms hosted api klasa','slug' => 'sms-hosted-api-klasa','content' => '<pre class="language-php"><code>&lt;?php

class SmsHostedApiAdapter extends ExternalApi {

    /**
     * send sms
     * 
     * @param string $phoneNumber
     * @param string $senderName - in this provider senderName is permanent
     * @param string $content - message content
     * 
     * @return string
     */
    public function sendSms(string $phoneNumber, string $senderName, string $content) {
        $transactionId = RandomCodeGenerator::generate(64, true);

        $parameters = [
            \'Phone\' =&gt; [$phoneNumber],
            \'Message\' =&gt; $content,
            \'Sender\' =&gt; $this-&gt;key,
            \'TransactionId\' =&gt; $transactionId,
            \'Priority\' =&gt; 1,
            \'FlashSms\' =&gt; false,
        ];

        return $this-&gt;sms_send($parameters);
    }

    /**
     * real method for send sms via api
     * 
     * @param array $params
     * @param string $token
     * 
     * @return string
     */
    private function sms_send(array $parameters) {
        return $this-&gt;call(\'Smses\', $parameters, true);
    }

    /**
     * get delivery reports
     * 
     * @param array $parameters
     * 
     * @return type
     */
    public function getDeliveryReports(array $parameters) {
        return $this-&gt;call(\'DeliveryReports\', $parameters, false);
    }

    /**
     * get inputs sms
     * 
     * @param array $parameters
     * 
     * @return type
     */
    public function getInputSms(array $parameters) {
        return $this-&gt;call(\'InputSmses\', $parameters, false);
    }

    /**
     * call method 
     * 
     * @param type $method
     * @param type $parameters
     * @param bool $post - flag post or get method
     * @return array
     * 
     * @throws \\Exception
     */
    public function call($method, $parameters = [], bool $post = true): array {
        $response = [];
        $url = $this-&gt;host . $method;

        if (!$post) {
            $parameters = http_build_query($parameters);
            $url .= \'?\' . $parameters;
        }

        $parameters = json_encode($parameters);

        $headers = [
            \'Content-Type: application/json; charset=utf-8\',
            \'Accept: application/json\',
            \'Authorization: Basic \' . base64_encode("$this-&gt;login:$this-&gt;pass")
        ];

        try {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            if ($post) {
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
            }

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $responseJson = curl_exec($ch);
            $response = json_decode($responseJson, true);

            $this-&gt;lastHttpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close($ch);
        } catch (\\Exception $ex) {
            throw new \\Exception(\'Curl call failed! HttpCode: \' . $this-&gt;lastHttpCode . \' (Ex: \' . $ex-&gt;getMessage() . \')\');
        }

        return $response;
    }

}
</code></pre>','publishedAt' => '2019-10-28 00:00:00','updatedAt' => '2019-10-28 00:00:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '247','user_id' => '1','title' => 'Izolacja kolumny w mysql','slug' => 'izolacja-kolumny-w-mysql','content' => '<p>Jeżeli w zapytaniu nie zostały wyizolowanie zindeksowane kolumny, w&oacute;wczas baza danych MySQL w zasadzie nie może używać indeks&oacute;w na kolumnach, przynajmniej do chwili ich<br />wyizolowania w zapytaniu. Pojęcie &bdquo;izolacji&rdquo; kolumny oznacza, że nie powinna być częścią wyrażenia, ani znajdować się wewnątrz funkcji używanej w zapytaniu. Przykładowo poniższe zapytanie nie będzie używało indeksu na kolumnie actor_id:<br /><strong>mysql&gt; SELECT actor_id FROM sakila.actor WHERE actor_id + 1 = 5;</strong><br />Człowiek może z łatwością dostrzec, że klauzula WHERE odpowiada wyrażeniu actor_id = 4, ale baza danych MySQL nie będzie mogła rozwiązać r&oacute;wnania dla kolumny actor_id. To jest zadanie dla człowieka. Należy nabrać nawyku upraszczania kryteri&oacute;w klauzuli WHERE, tak aby zindeksowana kolumna była jedyną znajdującą się po jej stronie operatora por&oacute;wnania.Poniżej przedstawiono inny przykład często popełnianego błędu:<br /><strong>mysql&gt; SELECT ... WHERE TO_DAYS(CURRENT_DATE) - TO_DAYS(date_col) &lt;= 10;</strong><br />Powyższe zapytanie odszuka wszystkie rekordy, dla kt&oacute;rych wartość date_col będzie mniejsza niż dziesięć dni, ale nie będzie używać indeks&oacute;w z powodu zastosowania funkcji TO_DAYS().Lepszym sposobem zapisania tego zapytania może być:<br /><strong>mysql&gt; SELECT ... WHERE date_col &gt;= DATE_SUB(CURRENT_DATE, INTERVAL 10 DAY);</strong><br />Powyższe zapytanie nie będzie miało problem&oacute;w z wykorzystaniem indeks&oacute;w, ale wciąż można je usprawnić na pewien spos&oacute;b. Odwołanie do CURRENT_DATE uniemożliwia buforowi zapytania buforowanie jego wynik&oacute;w. Rozwiązaniem jest zastąpienie odwołania do CURRENT_DATE dosłowną wartością daty:<br /><strong>mysql&gt; SELECT ... WHERE date_col &gt;= DATE_SUB(\'2008-01-17\', INTERVAL 10 DAY);</strong></p>','publishedAt' => '2019-10-28 12:12:12','updatedAt' => '2019-10-28 12:12:12','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '248','user_id' => '1','title' => 'like w php','slug' => 'like-w-php','content' => '<pre class="language-php"><code>&lt;?php
$expectedDateString = \'to słowo ma now w sobie\';

if (mb_strpos($expectedDateString, \'now\', null, \'utf-8\') !== false) {
   echo \'jest\';
}</code></pre>','publishedAt' => '2019-10-30 00:00:00','updatedAt' => '2019-12-13 12:45:56','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '250','user_id' => '1','title' => 'Dodanie klasy do labela','slug' => 'dodanie-klasy-do-labela','content' => '<pre class="language-php"><code>-&gt;add(\'name\', TextType::class, [
    \'label\' =&gt; \'Nazwa grupy:\',
    \'required\' =&gt; true,
    \'attr\' =&gt; [\'placeholder\' =&gt; \'Wpisz nazwę grupy...\', \'class\' =&gt; \'form-control\'],
    \'label_attr\' =&gt; [\'class\' =&gt; \'panel-title\']
])</code></pre>','publishedAt' => '2019-11-06 11:52:51','updatedAt' => '2019-11-06 11:52:51','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '251','user_id' => '1','title' => 'Doctrine hydracja','slug' => 'doctrine-hydracja','content' => '<div class="lead">
<p><span style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">W takim razie problemem raczej jest spos&oacute;b hydrowania danych, można to rozwiązać na dwa sposoby:</span><br style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">1. Użyć odpowiedniej adnotacji (LAZY, EAGER lub EXTRA LAZY), więcej np. tutaj:&nbsp;</span><a style="background: #fff6e9; color: #890000 !important; text-decoration: none; border-bottom: 1px solid #890000 !important; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;" href="http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/tutorials/extra-lazy-associations.html" target="_blank" rel="noopener">http://docs.doctrine-project.org/projects/...sociations.html</a><span style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;i to się sprawdzi jeśli wiemy, że np. wraz z obiektem User zawsze będziemy potrzebowali kolekcji emaili (wtedy ustawiamy na EAGER)</span><br style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">2. Jeśli spos&oacute;b hydrowania jest zależny od kontekstu, to nie ma innego (wygodnego) wyjścia jak użyć query buildera. Jednak nie wystarczy:</span></p>
<div class="head" style="background: #4f4f4f; color: #f0f0f0; font-family: \'Lucida Sans Unicode\', Arial; padding: 3px 5px 3px 7px; border-top-left-radius: 2px; border-top-right-radius: 2px;">[PHP] <a style="background: transparent; color: #e6d2a8 !important; text-decoration: none; border-bottom: none !important;" href="http://forum.php.pl/Pobierz-Plik-480992.html" target="_blank" rel="noopener">pobierz</a>, <a style="background: transparent; color: #e6d2a8 !important; text-decoration: none; border-bottom: none !important;" href="http://forum.php.pl/Plaintext-480992.html" target="_blank" rel="noopener">plaintext</a></div>
<ol style="margin: 0px; background: #ffffff; color: #2d2d2d; font-family: Inconsolata, Consolas, Courier, monospace, \'Lucida Sans Unicode\'; border-width: 1px 1px 2px; border-style: solid; border-color: #4f4f4f; border-image: initial;">
<li class="li1" style="background: #ffffff; padding: 1px 1px 1px 5px; border-left: 1px solid #b0afaf;">
<div class="de1" style="margin: 0px; padding: 0px; vertical-align: top; white-space: pre-wrap;"><span class="re0" style="color: #000088;">$qb</span><span class="sy0" style="color: #339933;">-&gt;</span><span class="me1" style="color: #004000;">join</span><span class="br0" style="color: #009900;">(</span><span class="st0" style="color: #0000ff;">\'u.emails\'</span><span class="sy0" style="color: #339933;">,</span> <span class="st0" style="color: #0000ff;">\'e\'</span><span class="br0" style="color: #009900;">)</span><span class="sy0" style="color: #339933;">;</span></div>
</li>
</ol>
<p><span style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">- trzeba jeszcze wymusić pobieranie poprzez dodanie select:</span><br style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial;" /></p>
<div class="head" style="background: #4f4f4f; color: #f0f0f0; font-family: \'Lucida Sans Unicode\', Arial; padding: 3px 5px 3px 7px; border-top-left-radius: 2px; border-top-right-radius: 2px;">[PHP] <a style="background: transparent; color: #e6d2a8 !important; text-decoration: none; border-bottom: none !important;" href="http://forum.php.pl/Pobierz-Plik-480993.html" target="_blank" rel="noopener">pobierz</a>, <a style="background: transparent; color: #e6d2a8 !important; text-decoration: none; border-bottom: none !important;" href="http://forum.php.pl/Plaintext-480993.html" target="_blank" rel="noopener">plaintext</a></div>
<ol style="margin: 0px; background: #ffffff; color: #2d2d2d; font-family: Inconsolata, Consolas, Courier, monospace, \'Lucida Sans Unicode\'; border-width: 1px 1px 2px; border-style: solid; border-color: #4f4f4f; border-image: initial;">
<li class="li1" style="background: #ffffff; padding: 1px 1px 1px 5px; border-left: 1px solid #b0afaf;">
<div class="de1" style="margin: 0px; padding: 0px; vertical-align: top; white-space: pre-wrap;"><span class="re0" style="color: #000088;">$qb</span><span class="sy0" style="color: #339933;">-&gt;</span><span class="me1" style="color: #004000;">select</span><span class="br0" style="color: #009900;">(</span><span class="st0" style="color: #0000ff;">\'u\'</span><span class="sy0" style="color: #339933;">,</span> <span class="st0" style="color: #0000ff;">\'e\'</span><span class="br0" style="color: #009900;">)</span><span class="sy0" style="color: #339933;">;</span></div>
</li>
</ol>
<p><br style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">W ten spos&oacute;b emaile zawsze będą w user niezależnie od tego, czy są potrzebne czy nie.</span><br style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial;" /><br style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial;" /><span style="color: #222222; font-family: Verdana, Tahoma, Arial, \'Trebuchet MS\', sans-serif, Georgia, Courier, \'Times New Roman\', serif; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #fff6e9; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Więcej nie da się powiedzieć bo nie znam Twojego kodu, r&oacute;wnie dobrze problemem mogą być źle zaprojektowane encje.</span></p>
</div>','publishedAt' => '2019-11-06 11:55:30','updatedAt' => '2019-11-06 11:55:30','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '252','user_id' => '1','title' => 'Powrót wstecz back','slug' => 'powrot-wstecz-back','content' => '<pre class="language-php"><code>return $this-&gt;redirect($request-&gt;headers-&gt;get(\'referer\'));</code></pre>','publishedAt' => '2019-11-08 08:15:07','updatedAt' => '2019-11-08 08:15:07','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '253','user_id' => '1','title' => 'Print memory php pamiec','slug' => 'print-memory-php-pamiec','content' => '<pre class="language-php"><code>function printMemory() {
    $mem_usage = memory_get_usage();
    $mem_peak = memory_get_peak_usage();
    echo \'The script is now using: &lt;strong&gt;\' . round($mem_usage / 1024) . \'KB&lt;/strong&gt; of memory.&lt;br&gt;\';
    echo \'Peak usage: &lt;strong&gt;\' . round($mem_peak / 1024) . \'KB&lt;/strong&gt; of memory.&lt;br&gt;&lt;br&gt;\';
}</code></pre>','publishedAt' => '2019-11-08 14:30:23','updatedAt' => '2019-11-08 14:30:23','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '254','user_id' => '1','title' => 'Czas wykonywania skryptu','slug' => 'czas-wykonywania-skryptu','content' => '<pre class="language-php"><code>&lt;?php
$timeStart = microtime(true);
//code.....................

$timeEnd = microtime(true);
$executionTime = ($timeEnd - $timeStart);

echo \'&lt;h3&gt;&lt;b&gt;czas&lt;/b&gt; \' . round($executionTime * 1000) . \' milliseconds&lt;/h3&gt;\';
echo \'&lt;h3&gt;&lt;b&gt;czas&lt;/b&gt; \' . $executionTime . \' mikro&lt;/h3&gt;\';
echo \'&lt;h3&gt;&lt;b&gt;czas&lt;/b&gt; \' . $executionTime/60 . \' sec&lt;/h3&gt;\';</code></pre>
<pre class="language-php"><code>&lt;?php
$timeStart = microtime(true);
        
//code

$duration = $endtime - $timeStart;
$hours = (int)($duration/60/60);
$minutes = (int)($duration/60)-$hours*60;
$seconds = $duration-$hours*60*60-$minutes*60;
        
$milliseconds = round($duration * 1000);

echo \'&lt;h3&gt;&lt;b&gt;czas&lt;/b&gt; \' . $milliseconds . \' milliseconds&lt;/h3&gt;\';
echo \'&lt;h3&gt;&lt;b&gt;czas&lt;/b&gt; \' . number_format((float)$seconds, 2, \'.\', \'\') . \' seconds&lt;/h3&gt;\';
</code></pre>
<pre class="language-php"><code>    protected function microtimeFormat($startTime, $format = null, $lng = null)
    {
        $seconds = microtime(true) - $startTime;
        $hours = ($seconds / 3600);
        $minutes = $seconds / 60;

        return [
            \'hours\' =&gt;  number_format($hours, 4, \'.\', \'\'),
            \'minutes\' =&gt; number_format($minutes, 4, \'.\', \'\'),
            \'seconds\' =&gt; number_format($seconds, 4, \'.\', \'\'),
            \'milliseconds\' =&gt; $seconds * 1000
        ];
    }</code></pre>','publishedAt' => '2019-11-08 14:40:55','updatedAt' => '2020-11-09 19:36:34','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '255','user_id' => '1','title' => 'Wzorzec prototype','slug' => 'wzorzec-prototype','content' => '<pre class="language-php"><code>&lt;?php
$timeStart = microtime(true);

function randomDateTime() {
    $timestamp = mt_rand(1, time());
    $randomDate = date("d M Y", $timestamp);
    return new DateTime($randomDate);
}

abstract class Solder {
    protected $name;
    protected $secondName;
    protected $rank = \'szeregowy\';
    protected $uniformColor;
    protected $force;
    protected $flair;
    protected $courage;
    protected $life;
    protected $dateOfBirth;

//    public function __construct(string $name, string $secondName, string $uniformColor, int $force, int $flair, int $courage, int $life, DateTime $dateOfBirth) {
//        $this-&gt;name = $name;
//        $this-&gt;secondName = $secondName;
//        $this-&gt;uniformColor = $uniformColor;
//        $this-&gt;force = $force;
//        $this-&gt;flair = $flair;
//        $this-&gt;courage = $courage;
//        $this-&gt;life = $life;
//        $this-&gt;dateOfBirth = $dateOfBirth;
//    }

    public function getName() {
        return $this-&gt;name;
    }

    public function getSecondName() {
        return $this-&gt;secondName;
    }

    public function getRank() {
        return $this-&gt;rank;
    }

    public function getUniformColor() {
        return $this-&gt;uniformColor;
    }

    public function getForce() {
        return $this-&gt;force;
    }

    public function getFlair() {
        return $this-&gt;flair;
    }

    public function getCourage() {
        return $this-&gt;courage;
    }

    public function getLife() {
        return $this-&gt;life;
    }

    public function getDateOfBirth() {
        return $this-&gt;dateOfBirth;
    }

    public function setName($name) {
        $this-&gt;name = $name;
        return $this;
    }

    public function setSecondName($secondName) {
        $this-&gt;secondName = $secondName;
        return $this;
    }

    public function setRank($rank) {
        $this-&gt;rank = $rank;
        return $this;
    }

    public function setUniformColor($uniformColor) {
        $this-&gt;uniformColor = $uniformColor;
        return $this;
    }

    public function setForce($force) {
        $this-&gt;force = $force;
        return $this;
    }

    public function setFlair($flair) {
        $this-&gt;flair = $flair;
        return $this;
    }

    public function setCourage($courage) {
        $this-&gt;courage = $courage;
        return $this;
    }

    public function setLife($life) {
        $this-&gt;life = $life;
        return $this;
    }

    public function setDateOfBirth($dateOfBirth) {
        $this-&gt;dateOfBirth = $dateOfBirth;
        return $this;
    }

}

class StrongSolder extends Solder {
    protected $rank = \'general\';
}

class CleverSolder extends Solder {
    
}

$strongSolder = (new StrongSolder())-&gt;setName(\'new janusz\')-&gt;setSecondName(\'kowalsi\')-&gt;setUniformColor(\'blul\')-&gt;setForce(90)-&gt;setFlair(80)-&gt;setCourage(70)-&gt;setLife(49)-&gt;setDateOfBirth(randomDateTime());
$cleverSolder = (new CleverSolder())-&gt;setName(\'new jan\')-&gt;setSecondName(\'kowalsi\')-&gt;setUniformColor(\'blul\')-&gt;setForce(90)-&gt;setFlair(80)-&gt;setCourage(70)-&gt;setLife(49)-&gt;setDateOfBirth(randomDateTime());

$strongSolders = [];
$cleversSolders = [];

for ($i = 0; $i &lt; 10000; $i++) {
    $strongSolders[] = (new StrongSolder())
            -&gt;setName(\'new janusz\' . $i)
            -&gt;setSecondName(\'kowalsi\' . $i)
            -&gt;setUniformColor(\'blul\')
            -&gt;setForce(90)
            -&gt;setFlair(80)
            -&gt;setCourage(70)
            -&gt;setLife(49)
            -&gt;setDateOfBirth(randomDateTime());

    $cleversSolders[] = (new CleverSolder())
            -&gt;setName(\'new jan\' . $i)
            -&gt;setSecondName(\'kowalsi\' . $i)
            -&gt;setUniformColor(\'blul\')
            -&gt;setForce(90)
            -&gt;setFlair(80)
            -&gt;setCourage(70)
            -&gt;setLife(49)
            -&gt;setDateOfBirth(randomDateTime());
}

$timeEnd = microtime(true);
$executionTime = ($timeEnd - $timeStart);

echo \'&lt;h3&gt;&lt;b&gt;czas normalne tworzenie obiekto&lt;/b&gt; \' . round($executionTime * 1000) . \' milliseconds&lt;/h3&gt;\';
echo \'&lt;h3&gt;&lt;b&gt;czas normalne tworzenie obiektow&lt;/b&gt; \' . $executionTime . \' mikro&lt;/h3&gt;\';
echo \'&lt;h3&gt;&lt;b&gt;czas normalne tworzenie obiektow&lt;/b&gt; \' . $executionTime/60 . \' sec&lt;/h3&gt;\';

$timeStart = microtime(true);

for ($i = 0; $i &lt; 10000; $i++) {
    $strongSolders[] = clone $strongSolder
                    -&gt;setName(\'new janusz\' . $i)
                    -&gt;setSecondName(\'kowalsi\' . $i)
                    -&gt;setUniformColor(\'blul\')
                    -&gt;setForce(90)
                    -&gt;setFlair(80)
                    -&gt;setCourage(70)
                    -&gt;setLife(49)
                    -&gt;setDateOfBirth(randomDateTime())
    ;
    $cleversSolders[] = clone $cleverSolder
                    -&gt;setName(\'new jan\' . $i)
                    -&gt;setSecondName(\'kowalsi\' . $i)
                    -&gt;setUniformColor(\'blul\')
                    -&gt;setForce(90)
                    -&gt;setFlair(80)
                    -&gt;setCourage(70)
                    -&gt;setLife(49)
                    -&gt;setDateOfBirth(randomDateTime())
    ;
}
//printMemory();
//
//function printMemory() {
//    $mem_usage = memory_get_usage();
//    $mem_peak = memory_get_peak_usage();
//    echo \'The script is now using: &lt;strong&gt;\' . round($mem_usage / 1024) . \'KB&lt;/strong&gt; of memory.&lt;br&gt;\';
//    echo \'Peak usage: &lt;strong&gt;\' . round($mem_peak / 1024) . \'KB&lt;/strong&gt; of memory.&lt;br&gt;&lt;br&gt;\';
//}
echo \'&lt;br&gt;&lt;br&gt;\';
$timeEnd = microtime(true);
$executionTime = ($timeEnd - $timeStart);

echo \'&lt;h3&gt;&lt;b&gt;czas klonowanie&lt;/b&gt; \' . round($executionTime * 1000) . \' milliseconds&lt;/h3&gt;\';
echo \'&lt;h3&gt;&lt;b&gt;czas klonowanie&lt;/b&gt; \' . $executionTime . \' mikro&lt;/h3&gt;\';
echo \'&lt;h3&gt;&lt;b&gt;czas klonowanie&lt;/b&gt; \' . $executionTime/60 . \' sec&lt;/h3&gt;\';
</code></pre>','publishedAt' => '2019-11-08 14:42:26','updatedAt' => '2019-11-08 14:44:16','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '256','user_id' => '1','title' => 'Usunięcie limitu pamięci w comoser','slug' => 'usuniecie-limitu-pamieci-w-comoser','content' => '<pre class="language-php"><code>COMPOSER_MEMORY_LIMIT=-1 composer update
</code></pre>','publishedAt' => '2019-11-15 11:03:26','updatedAt' => '2019-11-15 11:03:26','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '258','user_id' => '1','title' => 'Stała dynamicznie pobrana z stringa','slug' => 'stala-dynamicznie-pobrana-z-stringa','content' => '<pre class="language-php"><code>$test = constant(PhoneCheckNumber::class . "::" . \'STATUS_\' . $stat3);</code></pre>','publishedAt' => '2019-11-21 12:03:12','updatedAt' => '2019-11-21 12:03:37','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '259','user_id' => '1','title' => 'Wzorzec mediator','slug' => 'wzorzec-mediator','content' => '<pre class="language-php"><code>&lt;?php

ini_set(\'display_errors\', 1);
ini_set(\'display_startup_errors\', 1);
error_reporting(E_ALL);

interface Mediator {

    public function send(string $name, string $message): void;
}

class ConcreteMediator implements Mediator {

    private $colleagues = [];

    public function colleagueRegister(Colleague $colleague): void {
        $colleague-&gt;registerMediator($this);
        $this-&gt;colleagues[$colleague-&gt;getName()] = $colleague;
    }

    public function send(string $name, string $message): void {
        $this-&gt;colleagues[$name]-&gt;getMessage($message);
    }

}

class Colleague {

    private $mediator;
    private $name;

    public function __construct(string $name) {
        $this-&gt;name = $name;
    }

    public function registerMediator(Mediator $mediator): void {
        $this-&gt;mediator = $mediator;
    }

    public function getName() {
        return $this-&gt;name;
    }

    public function send(string $name, string $message): void {
        echo "Przesyłanie wiadomości od " . $this-&gt;name . " do " . $name . ": " . $message . \'&lt;br&gt;\';

        $this-&gt;mediator-&gt;send($name, $message); /// Rzeczywista komunikacja odbywa się za pośrednictwem mediatora!!!
    }

    public function getMessage(string $message): void {
        echo "Wiadomość odebrana przez " . $this-&gt;name . ": " . $message . \'&lt;br&gt;\';
    }

}

$michael = new Colleague("michael");
$john = new Colleague("john");
$lucy = new Colleague("lucy");


$mediator = new ConcreteMediator();
$mediator-&gt;colleagueRegister($michael);
$mediator-&gt;colleagueRegister($john);
$mediator-&gt;colleagueRegister($lucy);

$lucy-&gt;send(\'john\', "Hello world.");

echo \'&lt;br&gt;\';

$michael-&gt;send(\'lucy\', "Witaj!");
echo \'&lt;br&gt;\';

</code></pre>','publishedAt' => '2019-11-21 22:34:06','updatedAt' => '2019-11-21 22:34:06','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '260','user_id' => '1','title' => 'Ukryte pola bez jsa','slug' => 'ukryte-pola-bez-jsa','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
&lt;head&gt;
  &lt;meta charset="UTF-8" /&gt;
  &lt;meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /&gt;
  &lt;title&gt;CSS3 Accordion &amp;mdash; Paul Hayes&lt;/title&gt;
  &lt;meta name="author" content="Paul Hayes" /&gt;
  &lt;link rel="canonical" href="https://paulrhayes.com/experiments/accordion/" /&gt;
&lt;/head&gt;
&lt;body class="experiment"&gt;
  &lt;div class="wrapper"&gt;
    &lt;p class="learn"&gt;
      This is a demo, &lt;a href="https://paulrhayes.com/2009-06/accordion-using-only-css/"&gt;learn how it works&lt;/a&gt;.
    &lt;/p&gt;
    &lt;div id="experiment"&gt;
      &lt;div class="accordion"&gt;
        &lt;h2&gt;Accordion Demo&lt;/h2&gt;
        &lt;div id="one" class="section"&gt;
          &lt;h3&gt;
            &lt;a href="#one"&gt;Heading 1&lt;/a&gt;
          &lt;/h3&gt;
          &lt;div&gt;
            &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
              ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
              fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
              mollit anim id est laborum.&lt;/p&gt;
          &lt;/div&gt;
        &lt;/div&gt;
        &lt;div id="two" class="section"&gt;
          &lt;h3&gt;
            &lt;a href="#two"&gt;Heading 2&lt;/a&gt;
          &lt;/h3&gt;
          &lt;div&gt;
            &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua.&lt;/p&gt;
          &lt;/div&gt;
        &lt;/div&gt;
        &lt;div id="three" class="section"&gt;
          &lt;h3&gt;
            &lt;a href="#three"&gt;Heading 3&lt;/a&gt;
          &lt;/h3&gt;
          &lt;div&gt;
            &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
              ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.&lt;/p&gt;
          &lt;/div&gt;
        &lt;/div&gt;
        &lt;div id="four" class="section large"&gt;
          &lt;h3&gt;
            &lt;a href="#four"&gt;Heading 4&lt;/a&gt;
          &lt;/h3&gt;
          &lt;div&gt;
            &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
              ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.&lt;/p&gt;
            &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
              ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
              fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
              mollit anim id est laborum.&lt;/p&gt;
            &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
              ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
              fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
              mollit anim id est laborum.&lt;/p&gt;
          &lt;/div&gt;
        &lt;/div&gt;
        &lt;div id="five" class="section"&gt;
          &lt;h3&gt;
            &lt;a href="#five"&gt;Heading 5&lt;/a&gt;
          &lt;/h3&gt;
          &lt;div&gt;
            &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua.&lt;/p&gt;
          &lt;/div&gt;
        &lt;/div&gt;
        &lt;div id="six" class="section"&gt;
          &lt;h3&gt;
            &lt;a href="#six"&gt;Heading 6&lt;/a&gt;
          &lt;/h3&gt;
          &lt;div&gt;
            &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua.&lt;/p&gt;
          &lt;/div&gt;
        &lt;/div&gt;
      &lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
  &lt;style&gt;
    .accordion {
      background: #eee;
      border: 1px solid #999;
      padding: 0 1em 24px;
      width: 500px;
      margin: 2em auto;
    }

    .accordion h2 {
      margin: 12px 0;
    }

    .accordion .section {
      border-bottom: 1px solid #ccc;
      padding: 0 1em;
      background: #fff;
    }

    .accordion h3 a {
      display: block;
      font-weight: normal;
      padding: 1em 0;
    }

    .accordion h3 a:hover {
      text-decoration: none;
    }

    .accordion h3+div {
      height: 0;
      overflow: hidden;
      -webkit-transition: height 0.3s ease-in;
      -moz-transition: height 0.3s ease-in;
      -o-transition: height 0.3s ease-in;
      -ms-transition: height 0.3s ease-in;
      transition: height 0.3s ease-in;
    }

    .accordion :target h3 a {
      text-decoration: none;
      font-weight: bold;
    }

    .accordion :target h3+div {
      height: 100px;
    }

    .accordion .section.large:target h3+div {
      overflow: auto;
    }
  &lt;/style&gt;
&lt;/body&gt;

&lt;/html&gt;</code></pre>','publishedAt' => '2019-11-23 13:29:40','updatedAt' => '2019-11-23 13:29:40','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '261','user_id' => '1','title' => 'Spradzanie ajaxa w blade','slug' => 'spradzanie-ajaxa-w-blade','content' => '<pre class="language-markup"><code>@if(Request::ajax())
//code
@endif</code></pre>','publishedAt' => '2019-11-23 15:17:05','updatedAt' => '2019-11-23 15:17:05','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '262','user_id' => '1','title' => 'Doctrine2 typy pól','slug' => 'doctrine2-typy-pol','content' => '<pre class="language-php"><code>const TARRAY = \'array\';
const SIMPLE_ARRAY = \'simple_array\';
const JSON_ARRAY = \'json_array\';
const BIGINT = \'bigint\';
const BOOLEAN = \'boolean\';
const DATETIME = \'datetime\';
const DATETIMETZ = \'datetimetz\';
const DATE = \'date\';
const TIME = \'time\';
const DECIMAL = \'decimal\';
const INTEGER = \'integer\';
const OBJECT = \'object\';
const SMALLINT = \'smallint\';
const STRING = \'string\';
const TEXT = \'text\';
const BINARY = \'binary\';
const BLOB = \'blob\';
const FLOAT = \'float\';
const GUID = \'guid\';</code></pre>','publishedAt' => '2019-11-26 08:39:50','updatedAt' => '2019-11-26 08:39:50','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '263','user_id' => '1','title' => 'Form w twigu','slug' => 'form-w-twigu','content' => '<pre class="language-markup"><code>{{ form_start(form) }}
    {{ form_errors(form.postCode) }}
    {{ form_label(form.postCode,\'\', { \'label_attr\': {\'class\': \'panel-title\'} }) }}
    {{ form_widget(form.postCode, {\'attr\':{\'class\':\'form-control\'}}) }}
{{ form_end(form) }}</code></pre>','publishedAt' => '2019-11-26 10:02:51','updatedAt' => '2019-11-26 10:02:51','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '264','user_id' => '1','title' => 'Data od do w doctrine','slug' => 'data-od-do-w-doctrine','content' => '<pre class="language-php"><code>public function getUniqueTransactionCountByStatusInDateRange(\\DateTime $dateFrom, \\DateTime $dateTo, $transactionStatus) {
        $query = $this-&gt;getEntityManager()-&gt;createQueryBuilder()
                        -&gt;from(Transaction::class, \'t\')
                        -&gt;select(\'COUNT(DISTINCT(t.transaction_id))\')
                        -&gt;where(\'t.created_at &gt;= :DateTimeBegin\')-&gt;setParameter(\'DateTimeBegin\', $dateFrom-&gt;format(\'Y-m-d H:i:s\'))
                        -&gt;andWhere(\'t.created_at &lt;= :DateTimeEnd\')-&gt;setParameter(\'DateTimeEnd\', $dateTo-&gt;format(\'Y-m-d H:i:s\'))
                        -&gt;andWhere(\'t.status = :Status\')-&gt;setParameter(\'Status\', $transactionStatus)
        ;

        try {
            return (int) $query-&gt;getQuery()-&gt;getSingleScalarResult();
        } catch (\\Doctrine\\ORM\\NoResultException $e) {
            return 0;
        }
    }</code></pre>','publishedAt' => '2019-11-26 10:47:21','updatedAt' => '2019-11-26 10:47:21','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '265','user_id' => '1','title' => 'Zaokrąglanie w twigu','slug' => 'zaokraglanie-w-twigu','content' => '<pre class="language-markup"><code>{{ ((single.errors/parseLogs.numberOfLogs)*100)|number_format(2,\',\', \'\') }}%
</code></pre>','publishedAt' => '2019-11-26 12:09:53','updatedAt' => '2019-11-26 12:09:53','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '266','user_id' => '1','title' => 'Customowe pole w doctrine','slug' => 'customowe-pole-w-doctrine','content' => '<pre class="language-php"><code>stepNumber:
   columnDefinition : \'TINYINT NOT NULL\'
   nullable: false</code></pre>','publishedAt' => '2019-12-02 12:16:24','updatedAt' => '2019-12-02 12:16:24','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '267','user_id' => '1','title' => 'Intersection Observer','slug' => 'intersection-observer','content' => '<pre class="language-markup"><code>IntersectionObserver</code></pre>','publishedAt' => '2019-12-04 13:06:59','updatedAt' => '2019-12-04 13:06:59','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '268','user_id' => '1','title' => 'soap php xml przykład','slug' => 'soap-php-xml-przyklad','content' => '<pre class="language-php"><code>&lt;?php

namespace DM\\ApiBundle\\Utils\\ExternalApi\\SmsTideSoftware;

use DM\\ApiBundle\\Utils\\ExternalApi\\ExternalApi;
use Symfony\\Component\\DependencyInjection\\Container;
use SimpleXMLElement;
use DM\\DashboardBundle\\Utils\\RandomCodeGenerator;

class SmsTideSoftwareApiAdapter extends ExternalApi {

    /**
     * max number of attempts
     */
    CONST MAX_NUMBER_OF_ATTEMPTS = 30;

    /**
     * delay check staus in seconds
     */
    CONST DELAY_CHECK_STATUS_IN_SECONDS = 1;

    /**
     * @var Container 
     */
    private $container;

    /**
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this-&gt;normalizeInfo = \'\';
        $this-&gt;container = $container;
        $this-&gt;autorization();

        parent::__construct();
    }

    /**
     * autorization
     */
    private function autorization(): void {
        $host = $this-&gt;container-&gt;hasParameter(\'api_smsTideSoftwareConfronter_host\') ? $this-&gt;container-&gt;getParameter(\'api_smsTideSoftwareConfronter_host\') : \'\';
        $login = $this-&gt;container-&gt;hasParameter(\'api_smsTideSoftwareConfronter_login\') ? $this-&gt;container-&gt;getParameter(\'api_smsTideSoftwareConfronter_login\') : \'\';
        $pass = $this-&gt;container-&gt;hasParameter(\'api_smsTideSoftwareConfronter_pass\') ? $this-&gt;container-&gt;getParameter(\'api_smsTideSoftwareConfronter_pass\') : \'\';
        $key = $this-&gt;container-&gt;hasParameter(\'api_smsTideSoftwareConfronter_key\') ? $this-&gt;container-&gt;getParameter(\'api_smsTideSoftwareConfronter_key\') : \'\';
        $affiliateId = $this-&gt;container-&gt;hasParameter(\'api_smsTideSoftwareConfronter_affiliate_id\') ? $this-&gt;container-&gt;getParameter(\'api_smsTideSoftwareConfronter_affiliate_id\') : \'\';

        $this-&gt;setConnectionParameters($host, $login, $pass, $key, $affiliateId);
    }

    /**
     * call method 
     * 
     * @param type $method
     * @param type $parameters
     * @return array
     * 
     * @throws \\Exception
     */
    public function call($method, $parameters = []): array {
        
    }

    /**
     * set normalize info
     * 
     * @param array $resposne
     * 
     * @return void
     */
    public function setNormalizeInfo($resposne): void {
        $arrayResult = json_decode(json_encode((array) $resposne), TRUE);

        if (isset($arrayResult[\'success\'])) {
            $this-&gt;normalizeInfo .= $arrayResult[\'success\'];
        }

        if (isset($arrayResult[\'error\'])) {
            $this-&gt;normalizeInfo .= $arrayResult[\'error\'];
        }
    }

    /**
     * set normalize status
     * 
     * @param type $resposne
     * 
     * @return void
     */
    public function setNormalizeStatus($resposne): void {
        $arrayResult = json_decode(json_encode((array) $resposne), TRUE);

        if (isset($arrayResult[\'success\']) || isset($arrayResult[\'TideEnvelopeHeader\'][\'TideCustomersID\'])) {
            $this-&gt;normalizeStatus = self::STATUS_SUCCESS;
        }

        if (isset($arrayResult[\'error\'])) {
            $this-&gt;normalizeStatus = self::STATUS_ERROR;
        }
    }

    /**
     * get sms report by external id or sms id 
     * 
     * @param int|null $externalID
     * @param int|null $smsId
     * 
     * @return array
     */
    public function getSmsReportByExternalIdOrSmsId(?int $externalID = null, ?int $smsId = null): array {
        $host = \'https://tideplatformgate.com/tidexmlreceiver/TideGate.asmx?WSDL\';

        $content = \'&lt;?xml version="1.0" encoding="utf-8"?&gt;
        &lt;TideEnvelope:Envelope xmlns:Tide="http://www.tideplatformgate.com/TideProtocol.xsd" xmlns:TideEnvelope="http://www.w3.org/2003/05/soap-envelope"&gt;
            &lt;TideEnvelope:Header&gt;
                &lt;Tide:CustomersID value="\' . $this-&gt;login . \'" /&gt;
                &lt;Tide:UsersID value="\' . $this-&gt;key . \'" /&gt;
                &lt;Tide:Password value="\' . $this-&gt;pass . \'" /&gt;
                &lt;Tide:Generate Date="2019-11-26" Time="15:40:55.160" /&gt;
                &lt;Tide:Receipt Date="2019-11-26" Time="15:40:55.160" /&gt;
            &lt;/TideEnvelope:Header&gt;
            &lt;TideEnvelope:Body&gt;
                &lt;Tide:Process id="1" actualnode="1"&gt;
                    &lt;Tide:Node id="1" NodesID="23"&gt;
                        &lt;Tide:Datagroup id="1" /&gt;
                        &lt;Tide:Parameters&gt;
                            &lt;Tide:Parameter id="1" name="SMSStatusesDataGroup" value="1" /&gt;
                        &lt;/Tide:Parameters&gt;
                    &lt;/Tide:Node&gt;
                &lt;/Tide:Process&gt;
                &lt;Tide:Data&gt;
                    &lt;Tide:Group id="1"&gt;
                        &lt;Tide:Items&gt;\';
        if ($externalID) {
            $content .= \'&lt;Tide:Item id="1" name="ExternalID"&gt;\' . $externalID . \'&lt;/Tide:Item&gt;\';
        }

        if ($smsId) {
            $content .= \'&lt;Tide:Item id="1" name="SMSID"&gt;\' . $smsId . \'&lt;/Tide:Item&gt;\';
        }

        $content .= \'&lt;/Tide:Items&gt;
                    &lt;/Tide:Group&gt;
                &lt;/Tide:Data&gt;
            &lt;/TideEnvelope:Body&gt;
        &lt;/TideEnvelope:Envelope&gt;\';

        try {
            $resultXml = $this-&gt;callSoap($host, $content);
            $body = $resultXml-&gt;xpath(\'//TideEnvelopeEnvelope/TideEnvelopeBody/TideData/TideGroup/TideItems/TideItem/TideAttributes/TideAttribute\');
            $arrayResult = json_decode(json_encode((array) $body), TRUE);

            return $arrayResult;
        } catch (\\Exception $ex) {
            $this-&gt;normalizeStatus = self::STATUS_ERROR;
            $this-&gt;normalizeInfo .= \'error: \' . $ex-&gt;getMessage() . \' line: \' . $ex-&gt;getLine();
            return [];
        }
    }

    /**
     * sendSms
     * 
     * @param string $phoneNumber
     * @param string $senderName
     * @param string $content
     * 
     * @return void
     */
    public function sendSms(string $phoneNumber, string $senderName, string $content): void {
        $host = $this-&gt;affiliateId; //first free parametr from abstract !!! - because we have two hosts for this provider
        $randomNumber = time() . RandomCodeGenerator::generate(5);
        $content = \'&lt;?xml version="1.0" encoding="utf-8" ?&gt;
            &lt;TideEnvelope:Envelope xmlns:Tide="http://www.tideplatformgate.com/TideProtocol.xsd" xmlns:TideEnvelope="http://www.w3.org/2003/05/soap-envelope"&gt;
            &lt;TideEnvelope:Header&gt;
                &lt;Tide:CustomersID value="\' . $this-&gt;login . \'" /&gt;
                &lt;Tide:UsersID value="\' . $this-&gt;key . \'" /&gt;
                &lt;Tide:Password value="\' . $this-&gt;pass . \'" /&gt;
            &lt;/TideEnvelope:Header&gt;
            &lt;TideEnvelope:Body&gt;
                &lt;Tide:Process id="1" actualnode="1"&gt;
                    &lt;Tide:Node id="1" NodesID="23"&gt;
                        &lt;Tide:Datagroup id="1" /&gt;
                        &lt;Tide:Parameters&gt;
                            &lt;Tide:Parameter id="1" name="PhoneNumberAttributeClassID" value="1" /&gt;
                            &lt;Tide:Parameter id="2" name="AuthorizationAttributeClassID" value="2" /&gt;
                            &lt;Tide:Parameter id="3" name="ExternalIDAttributeClassID" value="3" /&gt;
                            &lt;Tide:Parameter id="4" name="SuspensionAttributeClassID" value="4" /&gt;
                            &lt;Tide:Parameter id="5" name="BrandingAttributeClassID" value="5" /&gt;
                            &lt;Tide:Parameter id="6" name="PriorityAttributeClassID" value="6" /&gt;
                            &lt;Tide:Parameter id="7" name="FlashAttributeClassID" value="7" /&gt;
                            &lt;Tide:Parameter id="8" name="WAPAttributeClassID" value="8" /&gt;
                            &lt;Tide:Parameter id="9" name="VoiceAttributeClassID" value="9" /&gt;
                            &lt;Tide:Parameter id="10" name="TTSVoicesIDAttributeClassID" value="10" /&gt;
                            &lt;Tide:Parameter id="11" name="SendDateTimeAttributeClassID" value="11" /&gt;
                        &lt;/Tide:Parameters&gt;
                    &lt;/Tide:Node&gt;
                &lt;/Tide:Process&gt;
                &lt;Tide:Data&gt;
                    &lt;Tide:Group id="1"&gt;
                        &lt;Tide:Items&gt;
                            &lt;Tide:Item id="1" name="SMSMessage"&gt;\' . $content . \'&lt;/Tide:Item&gt;
                            &lt;Tide:Item id="2" name="SMSAttributes"&gt;
                                &lt;Tide:Attributes save="1"&gt;
                                    &lt;Tide:Attribute ExternalID="1" value="\' . $phoneNumber . \'" /&gt;
                                    &lt;Tide:Attribute ExternalID="2" value="0" /&gt;
                                    &lt;Tide:Attribute ExternalID="3" value="\' . $randomNumber . \'" /&gt;
                                    &lt;Tide:Attribute ExternalID="5" value="\' . $senderName . \'"/&gt;
                                    &lt;Tide:Attribute ExternalID="6" value="0" /&gt;
                                    &lt;Tide:Attribute ExternalID="7" value="0" /&gt;
                                    &lt;Tide:Attribute ExternalID="8" value="0" /&gt;
                                    &lt;Tide:Attribute ExternalID="9" value="0" /&gt;
                                &lt;/Tide:Attributes&gt;
                            &lt;/Tide:Item&gt;
                        &lt;/Tide:Items&gt;
                    &lt;/Tide:Group&gt;
                &lt;/Tide:Data&gt;
            &lt;/TideEnvelope:Body&gt;
        &lt;/TideEnvelope:Envelope&gt;\';

        $this-&gt;callSoap($host, $content);

        if ($this-&gt;normalizeStatus === self::STATUS_SUCCESS) {
            for ($i = 0; $i &lt; self::MAX_NUMBER_OF_ATTEMPTS; $i++) {
                $smsData = $this-&gt;getSmsReportByExternalIdOrSmsId($randomNumber);
                if ($this-&gt;checkSmsStatus($smsData)) {
                    $this-&gt;normalizeStatus = self::STATUS_SUCCESS;
                    return;
                }

                sleep(self::DELAY_CHECK_STATUS_IN_SECONDS);
            }
        }

        $this-&gt;normalizeStatus = self::STATUS_ERROR;
        $this-&gt;normalizeInfo .= \' message could be not send\';
    }

    /**
     * get report by year month and day
     * 
     * @param int $year
     * @param int $month
     * @param int|null $day
     * 
     * @return SimpleXMLElement
     */
    public function getReportByYearMonthAndDay(int $year, int $month, ?int $day = null): SimpleXMLElement {
        $content = \'&lt;?xml version="1.0" encoding="utf-8"?&gt;
        &lt;TideEnvelope:Envelope xmlns:Tide="http://www.tideplatformgate.com/TideProtocol.xsd" xmlns:TideEnvelope="http://www.w3.org/2003/05/soap-envelope"&gt;
            &lt;TideEnvelope:Header&gt;
                &lt;Tide:CustomersID value="\' . $this-&gt;login . \'" /&gt;
                &lt;Tide:UsersID value="\' . $this-&gt;key . \'" /&gt;
                &lt;Tide:Password value="\' . $this-&gt;pass . \'" /&gt;
                &lt;Tide:Generate Date="2009-11-26" Time="01:40:55.160" /&gt;
                &lt;Tide:Receipt Date="2009-11-26" Time="01:40:55.160" /&gt;
            &lt;/TideEnvelope:Header&gt;
            &lt;TideEnvelope:Body&gt;
                &lt;Tide:Process id="1" actualnode="1"&gt;
                    &lt;Tide:Node id="1" NodesID="29"&gt;
                        &lt;Tide:Parameters&gt;
                            &lt;Tide:Parameter id="2" name="Year" value="\' . $year . \'" /&gt;
                            &lt;Tide:Parameter id="3" name="Month" value="\' . $month . \'" /&gt;\';
        if ($day) {
            $content .= \'&lt;Tide:Parameter id="4" name="Day" value="\' . $day . \'" /&gt;\';
        }

        $content .= \'&lt;/Tide:Parameters&gt;
                    &lt;/Tide:Node&gt;
                &lt;/Tide:Process&gt;
            &lt;/TideEnvelope:Body&gt;
        &lt;/TideEnvelope:Envelope&gt;\';

        return $this-&gt;callSoap($this-&gt;host, $content);
    }

    /**
     * check sms send status
     * 
     * @param array $result
     * 
     * @return bool
     */
    public function checkSmsStatus(array $result): bool {
        if (
                (isset($result[4][\'@attributes\'][\'name\']) &amp;&amp; $result[4][\'@attributes\'][\'name\'] === \'DeliveryStatus\') &amp;&amp;
                (isset($result[4][\'@attributes\'][\'value\']) &amp;&amp; $result[4][\'@attributes\'][\'value\'] === \'2\')
        ) {
            return true;
        }

        return false;
    }

    /**
     * connect to web service
     * 
     * @param string $host
     * @param string $content - xml
     * 
     * @return SimpleXMLElement
     */
    public function callSoap(string $host, string $content): SimpleXMLElement {
        $optionsConnect = [
            \'exceptions\' =&gt; true,
            \'trace\' =&gt; 1,
            \'cache_wsdl\' =&gt; WSDL_CACHE_NONE,
            \'connection_timeout\' =&gt; 60, // in seconds
            \'keep_alive\' =&gt; false,
        ];

        try {
            $client = new \\SoapClient($host, $optionsConnect);
            $contentXml = new SimpleXMLElement($content);
            $params = new \\stdClass();
            $params-&gt;xml = $contentXml-&gt;asXML();
            $result = $client-&gt;PostXML($params);
            $response = preg_replace("/(&lt;\\/?)(\\w+):([^&gt;]*&gt;)/", "$1$2$3", $result-&gt;PostXMLResult);
            if ($result-&gt;PostXMLResult === \'Authentication error\') {
                throw new \\Exception(\'Authentication error\');
            }

            if (mb_strpos($result-&gt;PostXMLResult, \'OK \', null, \'utf-8\') !== FALSE) {
                $response = \'&lt;?xml version="1.0" encoding="utf-8"?&gt;
                &lt;result&gt;
                    &lt;success&gt;\' . $response . \'&lt;/success&gt;
                &lt;/result&gt;\';
            }

            $result = new SimpleXMLElement($response);

            $this-&gt;setNormalizeInfo($result);
            $this-&gt;setNormalizeStatus($result);

            return $result;
        } catch (\\Exception $ex) {
            $result = new SimpleXMLElement(\'&lt;?xml version="1.0" encoding="utf-8"?&gt;
                &lt;result&gt;
                    &lt;error&gt;\' . $ex-&gt;getMessage() . \' line: \' . $ex-&gt;getLine() . \'&lt;/error&gt;
                &lt;/result&gt;\');
            $this-&gt;setNormalizeInfo($result);
            $this-&gt;setNormalizeStatus($result);

            return $result;
        }
    }

}
</code></pre>','publishedAt' => '2019-12-04 14:59:52','updatedAt' => '2019-12-04 15:00:40','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '269','user_id' => '1','title' => 'Usawienie hasła dla usera w mysql','slug' => 'usawienie-hasla-dla-usera-w-mysql','content' => '<pre class="language-php"><code>SET PASSWORD FOR \'root\'@\'localhost\' = PASSWORD(\'root\');</code></pre>','publishedAt' => '2019-12-05 10:23:12','updatedAt' => '2019-12-05 10:23:12','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '270','user_id' => '1','title' => 'Walidacja hasła','slug' => 'walidacja-hasla','content' => '<pre class="language-php"><code>namespace App\\Utils\\User;

class PasswordValidator {

    /**
     * function is checking that password have at least uppercase 8 chars, special symbols etc.
     *
     * @param string $password
     * 
     * @return boolean
     */
    public function validate(string $password): bool {
        $uppercase = preg_match(\'@[A-Z]@\', $password);
        $lowercase = preg_match(\'@[a-z]@\', $password);
        $number = preg_match(\'@[0-9]@\', $password);
        $specialChars = preg_match(\'@[^\\w]@\', $password);

        if (!$uppercase || !$lowercase || !$number || !$specialChars || mb_strlen($password, \'utf-8\') &lt; 8) {
            return false;
        }

        return true;
    }

}</code></pre>','publishedAt' => '2019-12-05 11:40:30','updatedAt' => '2019-12-05 11:40:30','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '271','user_id' => '1','title' => 'Duże wielkie litery w windowsie','slug' => 'duze-wielkie-litery-w-windowsie','content' => '<p>Na początku jako admin w poershellu:</p>
<pre class="language-markup"><code>Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
</code></pre>
<p>Nastpenie trzeba zainstalować dowolnego linuxa z strora. Potem zn&oacute;w w powershelu jako admin:</p>
<pre class="language-markup"><code>fsutil.exe file SetCaseSensitiveInfo C:\\wamp64\\www enable</code></pre>','publishedAt' => '2019-12-12 11:44:14','updatedAt' => '2019-12-12 11:44:14','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '272','user_id' => '1','title' => 'Instalacja starszej wersji chroma na linuxie','slug' => 'instalacja-starszej-wersji-chroma-na-linuxie','content' => '<p>Po pobraniu pakietu np (google-chrome-stable_71.0.3578.98-1_amd64.deb) z innego zr&oacute;dła. Aby go zainstalować trzeba użyć takiej komendy:</p>
<pre class="language-markup"><code>sudo dpkg -i google-chrome-stable_71.0.3578.98-1_amd64.deb</code></pre>
<p>dpkg -i - pozwala na instalację obcych pakiet&oacute;w.</p>','publishedAt' => '2019-12-12 12:17:22','updatedAt' => '2019-12-12 12:25:11','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '273','user_id' => '1','title' => 'Obciążenie w linuxie','slug' => 'obciazenie-w-linuxie','content' => '<p>Monitor obciążenia:</p>
<pre class="language-markup"><code>htop lub top</code></pre>
<p>w consoli.</p>','publishedAt' => '2019-12-13 08:28:34','updatedAt' => '2019-12-13 08:29:20','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '275','user_id' => '1','title' => 'Edycja pliku na linuxie','slug' => 'edycja-pliku-na-linuxie','content' => '<pre><code>sudo gedit helloWorld.txt
</code></pre>
<p><strong>OR</strong></p>
<pre><code>sudo nano helloWorld.txt</code></pre>','publishedAt' => '2019-12-29 02:55:20','updatedAt' => '2019-12-29 02:55:20','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '276','user_id' => '1','title' => 'Łącznie kluczy z dwóch tablic','slug' => 'lacznie-kluczy-z-dwoch-tablic','content' => '<pre class="language-php"><code>$array1 = [
  [
	  "ID" =&gt; "442505",
	  "Identyfikator użytkownika" =&gt; "748",
	  "Data utworzenia" =&gt; "10-01-2020 09:15:53",
	  "Data wysyłki" =&gt; "10-01-2020 09:15:53",
	  "Nadawca" =&gt; "Confronter",
	  "Treść" =&gt; "czesc",
	  "Wiadomości" =&gt; "1",
	  "Dostarczone" =&gt; "1",
	  "Koszt" =&gt; "1",
	  "Zwroty" =&gt; "0"
  ]
];

$array2 = [
	[
	  "ID wysyłki" =&gt; "442505",
	  "Numer telefonu" =&gt; "511152133",
	  "Dostarczono" =&gt; "TAK",
	  "Data dostarczenia" =&gt; "10-01-2020 09:15:59",
	  "Pow&oacute;d odrzucenia" =&gt; "",
	  "Pobrano opłatę" =&gt; "TAK"
	]
];

$newArray = return array_replace_recursive($array1, $array2);

//final
[
	[
	  "ID" =&gt; "442505"
	  "Identyfikator użytkownika" =&gt; "748"
	  "Data utworzenia" =&gt; "10-01-2020 09:15:53"
	  "Data wysyłki" =&gt; "10-01-2020 09:15:53"
	  "Nadawca" =&gt; "Confronter"
	  "Treść" =&gt; "czesc"
	  "Wiadomości" =&gt; "1"
	  "Dostarczone" =&gt; "1"
	  "Koszt" =&gt; "1"
	  "Zwroty" =&gt; "0"
	  "ID wysyłki" =&gt; "442505"
	  "Numer telefonu" =&gt; "511152133"
	  "Dostarczono" =&gt; "TAK"
	  "Data dostarczenia" =&gt; "10-01-2020 09:15:59"
	  "Pow&oacute;d odrzucenia" =&gt; ""
	  "Pobrano opłatę" =&gt; "TAK"
	]
]</code></pre>','publishedAt' => '2020-01-15 15:18:55','updatedAt' => '2020-01-15 15:22:55','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '277','user_id' => '1','title' => 'Symfony update czysty sql','slug' => 'symfony-update-czysty-sql','content' => '<pre class="language-php"><code> /**
     * restore suspended items
     * (with status in_progress and olther then 7 days)
     *
     * @return bool
     */
    public function restoreSuspendedItems(): bool
    {
        $maxDatetimeAdd = (new \\DateTime(\'now - 7 days\'))-&gt;format(\'Y-m-d H:i:s\');
        try {
            $sqlQuery = \'UPDATE rb_sms_storage SET status= "\' . SmsResultStatuses::WAITING . \'" WHERE status = "\' . SmsResultStatuses::IN_PROGRESS . \'" AND datetime_add &lt;= "\' . $maxDatetimeAdd . \'"\';
            $connection = $this-&gt;getEntityManager()-&gt;getConnection()-&gt;prepare($sqlQuery);

            $connection-&gt;execute();
            return true;
        } catch (\\Exception $ex) {
            return false;
        }
    }
</code></pre>','publishedAt' => '2020-01-17 09:38:17','updatedAt' => '2020-01-17 09:38:17','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '278','user_id' => '1','title' => 'Parsowanie stringa jak csv','slug' => 'parsowanie-stringa-jak-csv','content' => '<pre class="language-php"><code>$string = \'
"Numer telefonu";"Dostarczono";"Data dostarczenia";"Pow&oacute;d odrzucenia";"Pobrano opłatę"\\n
"500500500";"NIE";"";"";"TAK"\';

$explode = explode(\'\\n\', $string);

$data = array_combine(str_getcsv($explode[0], \';\'), str_getcsv($explode[1], \';\'));

var_dump($data);</code></pre>','publishedAt' => '2020-01-20 12:27:33','updatedAt' => '2020-01-20 12:27:33','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '279','user_id' => '1','title' => 'Wybrane pola mapowane na obiekt w doctrine','slug' => 'wybrane-pola-mapowane-na-obiekt-w-doctrine','content' => '<pre class="language-php"><code>public function findPosts() {
        $posts = $this-&gt;getEntityManager()-&gt;createQueryBuilder(\'p\');
        $posts
                -&gt;select(\'partial p.{id, title, slug,shortcontent,publishedAt}\', \'t\')
                -&gt;from(\'App:Post\', \'p\')
                -&gt;leftJoin(\'p.tags\', \'t\')
                -&gt;orderBy(\'p.id\', \'DESC\');

        return $posts;
    }</code></pre>','publishedAt' => '2020-01-22 09:36:27','updatedAt' => '2020-06-11 11:32:32','headtitle' => 'Wybrane pola mapowane na obiekt w doctrine','keyworks' => 'doctrine,wybrane, pola, mapowanie','description' => 'Wybrane pola mapowane na obiekt w doctrine','img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '280','user_id' => '1','title' => 'Temperatura na linuxie','slug' => 'temperatura-na-linuxie','content' => '<pre class="language-markup"><code>watch -n 2 sensors</code></pre>','publishedAt' => '2020-01-22 22:43:53','updatedAt' => '2020-01-22 22:43:53','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '281','user_id' => '1','title' => 'Zapis dubagera symfony do pliku','slug' => 'zapis-dubagera-symfony-do-pliku','content' => '<pre class="language-php"><code> php bin/console debug:container &gt; c:/as.txt</code></pre>','publishedAt' => '2020-01-27 13:16:09','updatedAt' => '2020-01-27 13:16:09','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '282','user_id' => '1','title' => 'timer na podstawie sekund','slug' => 'timer-na-podstawie-sekund','content' => '<pre class="language-javascript"><code>function timer(numberOfSeconds){
    var mins = Math.floor(numberOfSeconds / 60);
    var secs = Math.floor(numberOfSeconds % 60);
    if (secs &lt; 10) {
        secs = \'0\' + String(secs);
    }
    return  mins + \':\' + secs;
}

//3:20
console.log(timer(200));</code></pre>
<div id="gtx-trans" style="position: absolute; left: 212px; top: 5px;">
<div class="gtx-trans-icon">&nbsp;</div>
</div>','publishedAt' => '2020-01-31 23:02:10','updatedAt' => '2020-06-10 12:03:42','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '283','user_id' => '1','title' => 'Update BETWEEN','slug' => 'update-between','content' => '<pre class="language-php"><code>        return $this-&gt;createQueryBuilder(\'c\')
            -&gt;update()
            -&gt;set(\'c.modifiedDatetime\', \':modifiedDatetime\')-&gt;setParameter(\'modifiedDatetime\', $dateTimeNow)
            -&gt;andWhere(\'c.requestedDatetime BETWEEN :requestedDatetimeStart AND :requestedDatetimeEnd\')-&gt;setParameter(\'requestedDatetimeStart\', $dateFrom)-&gt;setParameter(\'requestedDatetimeEnd\', $dateTo)
            -&gt;andWhere(\'( (c.modifiedDatetime NOT BETWEEN :dateStart AND :dateEnd) or c.modifiedDatetime is null )\')-&gt;setParameter(\'dateStart\', $dateFrom)-&gt;setParameter(\'dateEnd\', $dateTo)
            -&gt;getQuery()
            -&gt;getSingleScalarResult();</code></pre>','publishedAt' => '2020-02-03 15:36:30','updatedAt' => '2020-02-03 15:36:30','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '284','user_id' => '1','title' => 'Funkcje php do parsowania xmla wydajność','slug' => 'funkcje-php-do-parsowania-xmla-wydajnosc','content' => '<pre class="language-php"><code>&lt;?php
//pamietaj o LIBXML_COMPACT | LIBXML_PARSEHUGE inaczej funkcje rzucają warringi przy dużych plikach

$xml = new \\DOMDocument(\'1.0\', \'utf-8\');
$xml-&gt;loadXML($xmlData-&gt;asXML(), LIBXML_COMPACT | LIBXML_PARSEHUGE);
$xml-&gt;save($this-&gt;fileLocationPath);


$xml = simplexml_load_string(file_get_contents($this-&gt;fileLocationPath), \'SimpleXMLElement\', LIBXML_COMPACT | LIBXML_PARSEHUGE);</code></pre>','publishedAt' => '2020-03-10 15:15:43','updatedAt' => '2020-03-10 15:15:43','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '285','user_id' => '1','title' => 'Flush persist w repozytorium','slug' => 'flush-persist-w-repozytorium','content' => '<pre class="language-php"><code>  /**
     * EM flush
     */
    public function flush()
    {
        $this-&gt;getEntityManager()-&gt;flush();
    }

    /**
     * EM persist
     * 
     * @param ClientStatData $entity
     */
    public function persist(ClientInstytutionStat $entity)
    {
        $this-&gt;getEntityManager()-&gt;persist($entity);
    }</code></pre>','publishedAt' => '2020-03-12 07:53:20','updatedAt' => '2020-03-12 07:53:20','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '286','user_id' => '1','title' => 'visual pluginy instalacja','slug' => 'visual-pluginy-instalacja','content' => '<p>to wklepić do konsoli:</p>
<pre class="language-markup"><code>code --install-extension bmewburn.vscode-intelephense-client
code --install-extension choppedcode.behat-complete
code --install-extension Cronos87.symfony-snippets
code --install-extension donjayamanne.githistory
code --install-extension ecmel.vscode-html-css
code --install-extension emallin.phpunit
code --install-extension felixfbecker.php-debug
code --install-extension felixfbecker.php-intellisense
code --install-extension hbenl.vscode-test-explorer
code --install-extension jbockle.jbockle-format-files
code --install-extension mblode.twig-language-2
code --install-extension MehediDracula.php-namespace-resolver
code --install-extension mikestead.dotenv
code --install-extension ms-vscode-remote.remote-wsl
code --install-extension necinc.elmmet
code --install-extension neilbrayfield.php-docblocker
code --install-extension phproberto.vscode-php-getters-setters
code --install-extension recca0120.vscode-phpunit
code --install-extension redhat.vscode-yaml
code --install-extension TheNouillet.symfony-vscode
code --install-extension usernamehw.errorlens
code --install-extension vincaslt.highlight-matching-tag
code --install-extension vscode-icons-team.vscode-icons
code --install-extension whatwedo.twig</code></pre>
<p>settings.json od andreja :</p>
<pre class="language-javascript"><code>{
    "python.jediEnabled": false,
    "python.dataScience.sendSelectionToInteractiveWindow": true,
    "python.pythonPath": "C:\\\\Users\\\\Andrzej Sokołowski\\\\AppData\\\\Local\\\\Programs\\\\Python\\\\Python37\\\\python.exe",
    "window.zoomLevel": -1,
    "[html]": {
        "editor.defaultFormatter": "lonefy.vscode-JS-CSS-HTML-formatter"
    },
    "[javascript]": {
        "editor.defaultFormatter": "lonefy.vscode-JS-CSS-HTML-formatter"
    },
    "editor.largeFileOptimizations": false,
    "phpstan.enabled": true,
    "phpstan.level": "max",
    "phpstan.memoryLimit": "2048M",
    "[php]": {
        "editor.defaultFormatter": "bmewburn.vscode-intelephense-client"
    },
    "phpformatter.composer": true,
    "emmet.triggerExpansionOnTab": true,
    "terminal.integrated.shell.windows": "C:\\\\Program Files\\\\Git\\\\bin\\\\bash.exe",
    "[json]": {
        "editor.defaultFormatter": "vscode.json-language-features"
    },
    "php-cs-fixer.executablePath": "${extensionPath}\\\\php-cs-fixer.phar",
    "php.suggest.basic": false,
    "php.validate.enable": true,
    "php.validate.run": "onType",
    "editor.quickSuggestions": {
        "comments": true
    },
    "phpunit.args": [
        "--configuration",
        "./phpunit.xml.dist"
    ],
    "phpunit.phpunit": "C:\\\\bin\\\\phpunit.bat",
    "explorer.confirmDragAndDrop": false,
    "symfony-vscode.phpExecutablePath": "C:\\\\wamp64\\\\bin\\\\php\\\\php7.3.1\\\\php.exe",
    "editor.wordSeparators": "`~!@#%^&amp;*()-=+[{]}\\\\|;:\'\\",.&lt;&gt;/?",
    "workbench.sideBar.location": "left",
    "editor.minimap.enabled": true,
    "workbench.tree.indent": 16,
    "workbench.tree.renderIndentGuides": "always",
    "workbench.list.openMode": "doubleClick",
    "editor.hover.delay": 200,
    "files.maxMemoryForLargeFilesMB": 6144,
    "files.autoSaveDelay": 1000,
    "files.autoSave": "afterDelay",
    "editor.formatOnSave": false,
    "diffEditor.ignoreTrimWhitespace": false,
    "formatFiles.extensionsToInclude": "php",
}
</code></pre>','publishedAt' => '2020-03-20 09:42:12','updatedAt' => '2020-03-20 10:18:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '287','user_id' => '1','title' => 'konta na ct8','slug' => 'konta-na-ct8','content' => '<p><strong>grzesiekneo2</strong></p>
<p><strong>mordimergt</strong></p>','publishedAt' => '2020-03-21 12:43:54','updatedAt' => '2020-03-21 12:43:54','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '288','user_id' => '1','title' => 'Sortowanie po wielu kluczach','slug' => 'sortowanie-po-wielu-kluczach','content' => '<pre class="language-php"><code>&lt;?php

namespace DM\\KategorieBundle\\Service;

use DM\\KategorieBundle\\Entity\\Kategorie;
use Doctrine\\ORM\\PersistentCollection;

class CategorySorter
{
    const TYPE_NORMAL = 1;
    const TYPE_TEMP = 2;
    const TYPE_WIN_BACK = 3;

    /**
     * sort categories by Type and name
     *
     * @param array $categories
     * 
     * @return array
     */
    public function sortCategoriesByTypeAndName(array $categories): array
    {
        $items = [];

        foreach ($categories as $singleCategory) {
            $type = self::TYPE_NORMAL;
            $nameWithHighlightedHtml = \'&lt;b&gt;\' . ($singleCategory[\'knad_nazwa\'] ? $singleCategory[\'knad_nazwa\'] . \' &gt; \' : null) . $singleCategory[\'nazwa\'] . \'&lt;/b&gt;\' . \' [\' . $singleCategory[\'id\'] . \']\';

            if ($singleCategory[\'temp\'] === true &amp;&amp; mb_strpos($singleCategory[\'nazwa\'], \'win back\', null, \'utf-8\') !== false) {
                $type = self::TYPE_WIN_BACK;
                $nameWithHighlightedHtml = ($singleCategory[\'knad_nazwa\'] ? $singleCategory[\'knad_nazwa\'] . \' &gt; \' : null) . $singleCategory[\'nazwa\'] . \' [\' . $singleCategory[\'id\'] . \']\' . \' &lt;span class="label label-warning"&gt;win back&lt;/span&gt; \';
            } else if ($singleCategory[\'temp\'] === true) {
                $type = self::TYPE_TEMP;
                $nameWithHighlightedHtml = ($singleCategory[\'knad_nazwa\'] ? $singleCategory[\'knad_nazwa\'] . \' &gt; \' : null) . $singleCategory[\'nazwa\'] . \' [\' . $singleCategory[\'id\'] . \']\' . \' &lt;span class="label label-info"&gt;tymczasowa&lt;/span&gt; \';
            }

            $name = ($singleCategory[\'knad_nazwa\'] ? $singleCategory[\'knad_nazwa\'] . \' &gt; \' : null) . $singleCategory[\'nazwa\'] . \' [\' . $singleCategory[\'id\'] . \']\';

            $items[$singleCategory[\'id\']] = [
                \'name\' =&gt; $name,
                \'nameWithHighlightedHtml\' =&gt; $nameWithHighlightedHtml . ($singleCategory[\'zarchiwizowany\'] ? \' &lt;span class="label label-danger"&gt;zarchiwizowana&lt;/span&gt; \' : \'\'),
                \'type\' =&gt; $type,
                \'id\' =&gt; $singleCategory[\'id\'],
                \'archived\' =&gt; $singleCategory[\'zarchiwizowany\'],
            ];
        }

        usort($items, function ($a, $b) {
            if ($a[\'archived\'] &gt; $b[\'archived\']) {
                return 1;
            } elseif ($a[\'archived\'] &lt; $b[\'archived\']) {
                return -1;
            } else {

                if ($a[\'type\'] &gt; $b[\'type\']) {
                    return 1;
                } elseif ($a[\'type\'] &lt; $b[\'type\']) {
                    return -1;
                } else {
                    return strcmp(mb_strtolower($a[\'name\'], \'UTF-8\'), mb_strtolower($b[\'name\'], \'UTF-8\'));
                }
            }
        });

        return $items;
    }
    /**
     * sort categories collection by type and name to array
     *
     * @param array $categories
     * 
     * @return array
     */
    public function sortCategoriesCollectionByTypeAndNameToArray(PersistentCollection $categories): array
    {
        $categoriesArray = [];

        /** @var Kategorie $category */
        foreach ($categories as $category) {
            $categoriesArray[] = [
                \'id\' =&gt; $category-&gt;getId(),
                \'nazwa\' =&gt; $category-&gt;getNazwa(),
                \'temp\' =&gt; $category-&gt;getTemp(),
                \'zarchiwizowany\' =&gt; $category-&gt;getZarchiwizowany(),
                \'knad_nazwa\' =&gt; $category-&gt;getKategoriaNadrzedna() ? $category-&gt;getKategoriaNadrzedna()-&gt;getNazwa() : null
            ];
        }

        return $this-&gt;sortCategoriesByTypeAndName($categoriesArray);
    }
}
</code></pre>','publishedAt' => '2020-03-26 11:31:10','updatedAt' => '2020-03-26 11:31:10','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '289','user_id' => '1','title' => 'Reinstalacja mysql na linuxie','slug' => 'reinstalacja-mysql-na-linuxie','content' => '<pre class="language-markup"><code>sudo apt-get remove --purge mysql*
sudo apt-get purge mysql* 
sudo apt-get autoremove 
sudo apt-get autoclean
sudo apt-get remove dbconfig-mysql
sudo apt-get dist-upgrade

//instalcja mysql sudo apt-get install mysql-server
//instalacja mariadb sudo apt-get install mariadb-server
</code></pre>','publishedAt' => '2020-04-05 16:01:34','updatedAt' => '2020-04-05 16:01:34','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '290','user_id' => '1','title' => 'Dodanie virtual hostu w wampie na windowsie','slug' => 'dodanie-virtual-hostu-w-wampie-na-windowsie','content' => '<p>W pliku:</p>
<p>C:\\Windows\\System32\\drivers\\etc\\hosts</p>
<p>Plik możesz edytować np notatnikiem uruchamiając go w trybie administatora</p>
<p>Należy dodać adres nowego wirtualnego hostu :&nbsp;</p>
<pre class="language-markup"><code>127.0.0.1 localhost
::1 localhost
127.0.0.1 laravel_7</code></pre>
<p>I w pliku:</p>
<p>C:\\wamp64\\bin\\apache\\apache2.4.39\\conf\\extra\\httpd-vhosts.conf</p>
<p>Dodać:</p>
<pre class="language-markup"><code>&lt;VirtualHost *:80&gt;
  ServerName laravel_7
  ServerAlias laravel_7
  DocumentRoot "${INSTALL_DIR}/www/laravel_7/public"
  &lt;Directory "${INSTALL_DIR}/www/laravel_7/public/"&gt;
    Options +Indexes +Includes +FollowSymLinks +MultiViews
    AllowOverride All
    Require local
  &lt;/Directory&gt;
&lt;/VirtualHost&gt;</code></pre>
<p>Po wszystkim restart wampa. Od teraz w przegladarce po adresem laravel_7 bedzie dostępny ten projekt.</p>','publishedAt' => '2020-04-07 15:21:36','updatedAt' => '2020-09-13 21:57:20','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '291','user_id' => '1','title' => 'pobranie parametru z get','slug' => 'pobranie-parametru-z-get','content' => '<pre class="language-markup"><code> let pageFromGetAdress = this.$route.query.page;</code></pre>','publishedAt' => '2020-04-14 14:47:52','updatedAt' => '2020-04-14 14:47:52','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '293','user_id' => '1','title' => 'Flex artykuł','slug' => 'flex-artykul','content' => '<p><a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">link</a></p>','publishedAt' => '2020-04-17 12:10:43','updatedAt' => '2020-04-17 12:11:11','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '294','user_id' => '1','title' => 'mysql zmiana timeout','slug' => 'mysql-zmiana-timeout','content' => '<p><a href="https://stackoverflow.com/questions/54745076/mariadb-shutdown-right-after-start" target="_blank" rel="noopener">link</a></p>
<pre class="language-markup"><code>Please note that since 10.1.10, MariaDB uses systemd to start the service. The /etc/init.d/mysql script is no longer used, so MYSQLD_STARTUP_TIMEOUT has no effect.

You need to find your mariadb.service file. In our case, it did not contain a timeout so the MariaDB default was being used. Just add /etc/systemd/system/mariadb.service.d/override.conf file, and put in it:

[Service]
TimeoutStartSec = 0
In the [Service] section, and it will never time out. After create the file:

# systemctl daemon-reload
# systemctl restart mysql.service</code></pre>','publishedAt' => '2020-04-26 13:48:41','updatedAt' => '2020-04-26 13:48:41','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '295','user_id' => '1','title' => 'Pokazywanie wartości zmiennych','slug' => 'pokazywanie-wartosci-zmiennych','content' => '<pre class="language-markup"><code>show variables like \'%slow%\';
</code></pre>','publishedAt' => '2020-04-28 11:05:42','updatedAt' => '2020-04-28 11:05:42','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '296','user_id' => '1','title' => 'cfg zmienne opis','slug' => 'cfg-zmienne-opis','content' => '<pre class="prettyprint-override prettyprinted"><code><span class="pun">[</span><span class="pln">mysqld</span><span class="pun">]</span><span class="pln">
innodb_read_io_threads </span><span class="pun">=</span> <span class="lit">64</span><span class="pln">
innodb_write_io_threads </span><span class="pun">=</span> <span class="lit">64</span><span class="pln">
innodb_io_capacity </span><span class="pun">=</span> <span class="lit">5000</span>
</code></pre>
<p>Co to jest?</p>
<ul>
<li><a href="https://dev.mysql.com/doc/refman/5.5/en/innodb-parameters.html%23sysvar_innodb_read_io_threads#sysvar_innodb_read_io_threads" target="_blank" rel="nofollow noopener">innodb_read_io_threads</a>&nbsp;- Liczba wątk&oacute;w We / Wy dla operacji odczytu w InnoDB.</li>
<li><a href="https://dev.mysql.com/doc/refman/5.5/en/innodb-parameters.html%23sysvar_innodb_write_io_threads#sysvar_innodb_write_io_threads" target="_blank" rel="nofollow noopener">innodb_write_io_threads</a>&nbsp;- Liczba wątk&oacute;w we / wy dla operacji zapisu w InnoDB.</li>
<li><a href="https://dev.mysql.com/doc/refman/5.5/en/innodb-parameters.html%23sysvar_innodb_io_capacity#sysvar_innodb_io_capacity" target="_blank" rel="nofollow noopener">innodb_io_capacity</a>&nbsp;- G&oacute;rny limit aktywności we / wy wykonywanej przez zadania w tle InnoDB, takie jak opr&oacute;żnianie stron z puli bufor&oacute;w i scalanie danych z bufora wstawiania.</li>
</ul>
<div id="gtx-trans" style="position: absolute; left: -12px; top: 86px;">
<div class="gtx-trans-icon">&nbsp;</div>
</div>','publishedAt' => '2020-04-28 11:41:44','updatedAt' => '2020-04-28 11:41:44','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '297','user_id' => '1','title' => 'Wzorzec observer','slug' => 'wzorzec-observer','content' => '<pre class="language-php"><code>&lt;?php

interface Subscriber
{
    public function send(): void;
}

class Client implements Subscriber
{
    private $name;

    public function __construct(string $name)
    {
        $this-&gt;name = $name;
    }

    public function send(): void
    {
        echo \'i got it: \' . $this-&gt;name . \'&lt;br&gt;\';
    }
}

class Subject
{
    private $subscribers = [];

    public function subscribe(Subscriber $subscriber): void
    {
        $this-&gt;subscribers[] = $subscriber;
    }

    public function startWork(): void
    {
        sleep(1);
        //code

        foreach ($this-&gt;subscribers as $subscriber) {
            $subscriber-&gt;send();
        }
    }
}

$client1 = new Client(\'first client\');
$client2 = new Client(\'second client\');
$client3 = new Client(\'third client\');

$subject = new Subject();

$subject-&gt;subscribe($client1);
$subject-&gt;subscribe($client2);
$subject-&gt;subscribe($client3);

$subject-&gt;startWork();
</code></pre>
<p>2)Implementacja Sedno wzorca Observer (obserwator) polega na rozdzieleniu element&oacute;w użytkujących (obserwator&oacute;w) od klasy centralnej (podmiotu obserwacji). Obserwatory muszą być informowane o zdarzeniach zachodzących w podmiocie obserwacji. R&oacute;wnocześnie nie chcemy wprowadzać trwałych i sztywnych zależności pomiędzy podmiotem obserwacji a klasami obserwator&oacute;w. Możemy więc umożliwić obserwatorom rejestrowanie się w klasie podmiotu. W tym celu powinniśmy uzupełnić klasę Login o trzy nowe metody: rejestracji (attach()), rezygnacji (detach()) i powiadomienia (notify()), przystosowując klasę do wymog&oacute;w wyr&oacute;żniających podmioty obserwacji interfejsu (tutaj ma on nazwę Observable):</p>
<pre class="language-php"><code>&lt;?php
interface Observable
{
    function attach(Observer $observer);
    function detach(Observer $observer);
    function notify();
}

</code></pre>
<pre class="language-php"><code>&lt;?php
// Klasa Login&hellip;
class Login implements Observable
{
    private $observers = array();
    private $storage;
    const LOGIN_USER_UNKNOWN = 1;
    const LOGIN_WRONG_PASS = 2;
    const LOGIN_ACCESS = 3;
    function attach(Observer $observer)
    {
        $this-&gt;observers[] = $observer;
    }
    function detach(Observer $observer)
    {
        $this-&gt;observers = array_filter($this-&gt;observers, function ($a) use ($observer)
        {
            return (!($a === $observer));
        });
    }
    function notify()
    {
        foreach ($this-&gt;observers as $obs)
        {
            $obs-&gt;update($this);
        }
    }
    
    //
    
}
</code></pre>
<p>Mamy więc klasę podmiotu utrzymującą listę obiekt&oacute;w obserwator&oacute;w. Obiekty te są dodawane do listy z zewnątrz poprzez wywołanie metody attach(). Rezygnacja z obserwacji i usunięcie z listy następuje w wyniku wywołania metody detach(). Z kolei wywołanie metody notify() służy jako powiadomienie obiekt&oacute;w obserwator&oacute;w o potencjalnie interesujących ich zdarzeniach. Implementacja tej metody sprowadza się do przejrzenia tablicy obiekt&oacute;w obserwator&oacute;w i wywołania na rzecz każdego z nich metody update(). Wywołanie metody rozsyłającej powiadomienia następuje we wnętrzu klasy Login, w ciele metody handleLogin():</p>
<pre class="language-php"><code>//
function handleLogin($user, $pass, $ip)
{
    switch (rand(1, 3))
    {
        case 1:
            $this-&gt;setStatus(self::LOGIN_ACCESS, $user, $ip);
            $isvalid = true;
        break;
        case 2:
            $this-&gt;setStatus(self::LOGIN_WRONG_PASS, $user, $ip);
            $isvalid = false;
        break;
        case 3:
            $this-&gt;setStatus(self::LOGIN_USER_UNKNOWN, $user, $ip);
            $isvalid = false;
        break;
    }
    $this-&gt;notify();
    return $isvalid;
}

</code></pre>
<p>Zdefiniujmy interfejs klas obserwator&oacute;w:</p>
<pre class="language-php"><code>interface Observer {
 function update(Observable $observable);
}</code></pre>
<p>Do listy obserwator&oacute;w można dodawać (za pośrednictwem metody attach() klasy podmiotu obserwacji) dowolne obiekty, kt&oacute;re implementują interfejs Observable. Tak wygląda tworzenie konkretnego egzemplarza:</p>
<pre class="language-php"><code>&lt;?php
class SecurityMonitor implements Observer
{
    function update(Observable $observable)
    {
        $status = $observable-&gt;getStatus();
        if ($status[0] == Login::LOGIN_WRONG_PASS)
        {
            // wyślij wiadomość do administratora&hellip;
            print __CLASS__ . "\\twysyłam wiadomość do administratora\\n";
        }
    }
}
$login = new Login();
$login-&gt;attach(new SecurityMonitor());
</code></pre>
<p>Zwr&oacute;ćmy uwagę, jak obiekt obserwatora odwołuje się do egzemplarza klasy Observable (podmiotu obserwacji) w celu pozyskania dodatkowych informacji o zdarzeniu. Metody, za pośrednictwem kt&oacute;rych obiekty obserwator&oacute;w mogłyby dowiadywać się o stanie, powinny zostać udostępnione właśnie w klasie podmiotu obserwacji. W tym przypadku klasa podmiotu ma zdefiniowaną metodę getStatus(), dzięki kt&oacute;rej obiekty obserwator&oacute;w mogą dowiadywać się o bieżącym stanie obiektu obserwowanego. Pojawia się tutaj pewien problem. Ot&oacute;ż w wywołaniu metody Login::getStatus() klasa SecurityMonitor bazuje na wiedzy o klasie Login, na kt&oacute;rej nie powinna polegać. Przecież w wywołaniu otrzymuje obiekt Observable, ale nie ma żadnej gwarancji, że będzie to właśnie obiekt Login. Mamy tu kilka możliwości: możemy rozszerzyć interfejs Observable tak, aby zawierał w sobie deklarację metody getStatus(), i możemy od razu przemianować interfejs na ObservableLogin, sygnalizując, że ma związek z klasami Login. Możemy też utrzymać og&oacute;lny interfejs Observable i obarczyć klasy Observable odpowiedzialnością za to, aby podmioty obserwacji były odpowiedniego typu. Możemy wtedy złożyć na nie r&oacute;wnież zadanie kojarzenia się z podmiotami obserwacji. Ponieważ będziemy mieć więcej niż jeden typ Observer, a zamierzamy zaimplementować przy okazji czynności porządkowe wsp&oacute;lne dla wszystkich podtyp&oacute;w, możemy od razu udostępnić abstrakcyjną klasę bazową:</p>
<pre class="language-php"><code>&lt;?php
abstract class LoginObserver implements Observer
{
    private $login;
    function __construct(Login $login)
    {
        $this-&gt;login = $login;
        $login-&gt;attach($this);
    }
    function update(Observable $observable)
    {
        if ($observable === $this-&gt;login)
        {
            $this-&gt;doUpdate($observable);
        }
    }
    abstract function doUpdate(Login $login);
}

</code></pre>
<p>Klasa LoginObserver wymaga do konstrukcji obiektu typu Login. W konstruktorze zachowuje sobie referencję obiektu i wywołuje własną metodę Login::attach(). W wywołaniu update() następuje sprawdzenie, czy przekazany obiekt Observable jest w istocie referencją obserwowanego podmiotu, po czym dochodzi do wywołania metody szablonowej doUpdate(). Teraz możemy utworzyć cały zestaw obiekt&oacute;w LoginObserver, z kt&oacute;rych każdy będzie operował na obiekcie Login, a nie na dowolnym obiekcie implementującym nasz stary interfejs Observable:</p>
<pre class="language-php"><code>&lt;?php
class SecurityMonitor extends LoginObserver
{
    function doUpdate(Login $login)
    {
        $status = $login-&gt;getStatus();
        if ($status[0] == Login::LOGIN_WRONG_PASS)
        {
            // wysłanie wiadomości do administratora
            print __CLASS__ . ":\\twysyłam wiadomość do administratora\\n";
        }
    }
}

class GeneralLogger extends LoginObserver
{
    function doUpdate(Login $login)
    {
        $status = $login-&gt;getStatus();
        // dodanie danych do rejestru
        print __CLASS__ . ":\\tdodaję dane logowania do rejestru\\n";
    }
}
class PartnershipTool extends LoginObserver
{
    function doUpdate(Login $login)
    {
        $status = $login-&gt;getStatus();
        // sprawdzenie adresu IP
        // ustawienie ciasteczka dla dopuszczonego IP
        print __CLASS__ . ":\\tustawiam ciasteczko dla dopuszczonego IP\\n";
    }
}
//Tworzenie i podłączanie obserwator&oacute;w LoginObserver jest teraz wykonywane w czasie konkretyzacji obiekt&oacute;w:
$login = new Login();
new SecurityMonitor($login);
new GeneralLogger($login);
new PartnershipTool($login);

</code></pre>
<p>&nbsp;</p>','publishedAt' => '2020-04-28 14:56:39','updatedAt' => '2020-05-07 14:42:31','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '298','user_id' => '1','title' => 'aktualne taktowanie rdzeni','slug' => 'aktualne-taktowanie-rdzeni','content' => '<p>watch "grep \'cpu MHz\' /proc/cpuinfo"</p>','publishedAt' => '2020-04-29 11:59:27','updatedAt' => '2020-04-29 11:59:27','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '299','user_id' => '1','title' => 'vistual host na linuxie','slug' => 'vistual-host-na-linuxie','content' => '<p>1) skopiowanie domyslango pliku</p>
<pre class="language-markup"><code>sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/laravel_czat.conf
</code></pre>
<p>2) W pliku dodać</p>
<pre class="language-markup"><code>&lt;VirtualHost *:80&gt;
    ServerAdmin webmaster@myapp.com
    ServerName czat.com
    DocumentRoot /var/www/html/laravel_vue_chat/public/

    &lt;Directory /var/www/html/laravel_vue_chat/public/&gt;
        Options -Indexes +FollowSymLinks +MultiViews
        AllowOverride All
        Require all granted
        &lt;FilesMatch \\.php$&gt;
            # Change this "proxy:unix:/path/to/fpm.socket"
            # if using a Unix socket
            #SetHandler "proxy:fcgi://127.0.0.1:9000"
        &lt;/FilesMatch&gt;
    &lt;/Directory&gt;

    ErrorLog ${APACHE_LOG_DIR}/myapp.com-error.log

    # Possible values include: debug, info, notice, warn, error, crit,
    # alert, emerg.
    LogLevel warn
    CustomLog ${APACHE_LOG_DIR}/myapp.com-access.log combined
&lt;/VirtualHost&gt;</code></pre>
<p>3) W pliku :</p>
<pre class="language-markup"><code>etc/hosts</code></pre>
<p>dodać czat.com</p>
<pre class="language-markup"><code>127.0.0.1	localhost
127.0.1.1	mord-Inspiron-7520


# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

127.0.0.1 czat.com</code></pre>
<p>4)Odpalić komendy</p>
<pre class="language-markup"><code>sudo a2ensite laravel_czat
</code></pre>
<pre class="language-markup"><code>sudo systemctl reload apache2</code></pre>
<p>&nbsp;</p>','publishedAt' => '2020-05-04 10:29:01','updatedAt' => '2020-09-13 21:57:04','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '300','user_id' => '1','title' => 'przykład','slug' => 'przyklad','content' => '<pre class="language-php"><code>abstract class ParamHandler {
 protected $source;
 protected $params = array();

 function __construct($source) {
 $this-&gt;source = $source;
 }
 function addParam($key, $val) {
 $this-&gt;params[$key] = $val;
 }
 function getAllParams() {
 return $this-&gt;params;
 }
 static function getInstance($filename) {
 if ( preg_match("/\\.xml$/i", $filename))
 return new XmlParamHandler($filename);
 }
 return new TextParamHandler($filename);
 }
 abstract function write();
 abstract function read();
}


class XmlParamHandler extends ParamHandler {
 function write() {
 // zapis tablicy parametr&oacute;w $this-&gt;params w pliku XML
 }
 function read() {
 // odczyt pliku XML i wypełnienie tablicy parametr&oacute;w $this-&gt;params
 }
}
class TextParamHandler extends ParamHandler {
 function write() {
 // zapis tablicy parametr&oacute;w $this-&gt;params w pliku tekstowym
 }
 function read() {
 // odczyt pliku tekstowego i wypełnienie tablicy parametr&oacute;w $this-&gt;params
 }
}</code></pre>
<pre class="language-php"><code>abstract class Lesson {
 private $duration;
 private $costStrategy;
 function __construct($duration, CostStrategy $strategy) {
 $this-&gt;duration = $duration;
 $this-&gt;costStrategy = $strategy;
 }
 function cost() {
 return $this-&gt;costStrategy-&gt;cost($this);
 }
 function chargeType() {
 return $this-&gt;costStrategy-&gt;chargeType();
 }
 function getDuration() {
 return $this-&gt;duration;
 }
 // pozostałe metody klasy Lesson&hellip;
}
class Lecture extends Lesson {
 // implementacja właściwa dla wykład&oacute;w...
}
class Seminar extends Lesson {
 // implementacja odpowiednia dla seminari&oacute;w...
}



 Oto przebieg delegowania:
function cost() {
 return $this-&gt;costStrategy-&gt;cost($this);
}
A oto klasa CostStrategy wraz z jej klasami pochodnymi:
abstract class CostStrategy {
 abstract function cost(Lesson $lesson);
 abstract function chargeType();
}
class TimedCostStrategy extends CostStrategy {
 function cost(Lesson $lesson) {
 return ($lesson-&gt;getDuration() * 5);
 }
 function chargeType() {
 return "stawka godzinowa";
 }
}
class FixedCostStrategy extends CostStrategy {
 function cost(Lesson $lesson) {
 return 30;
 }
 function chargeType() {
 return "stawka stała";
 }
}</code></pre>
<p>&nbsp;</p>
<pre class="language-php"><code>class RegistrationMgr {
 function register(Lesson $lesson) {
 // jakieś operacje na obiekcie Lesson
 // i odpowiednie powiadomienie
 $notifier = Notifier::getNotifier();
 $notifier-&gt;inform(
 "nowe zajęcia: koszt ({$lesson-&gt;cost()})" );
 }
}
abstract class Notifier {
 static function getNotifier() {
 // pozyskanie konkretnej klasy odpowiedniej dla
 // konfiguracji bądź stanu logiki
 if ( rand(1,2) == 1 )
 else {
 return new TextNotifier();
 }
}
abstract function inform($message);
}
class MailNotifier extends Notifier {
 function inform($message) {
 print "powiadomienie w trybie MAIL: {$message}\\n";
 }
}
class TextNotifier extends Notifier {
 function inform($message) {
 print "powiadomienie w trybie TEXT: {$message}\\n";
 }
}</code></pre>','publishedAt' => '2020-05-05 15:27:19','updatedAt' => '2020-05-06 11:02:37','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '301','user_id' => '1','title' => 'złote myśli','slug' => 'zlote-mysli','content' => '<pre class="language-markup"><code>&bdquo;Programuj pod kątem interfejsu, nie implementacji&rdquo;.</code></pre>
<pre class="language-markup"><code>&bdquo;Faworyzuj kompozycję przed dziedziczeniem&rdquo;</code></pre>
<pre class="language-markup"><code>Kod ma używać interfejs&oacute;w, nie implementacji
Ta zasada to jeden z motyw&oacute;w przewodnich tej książki. </code></pre>
<pre class="language-markup"><code>Zadawanie pytań nie powinno zmienić odpowiedzi</code></pre>
<pre class="language-markup"><code>Co to za pamięc ?, kt&oacute;ra działa tylko wstecz.</code></pre>
<pre class="language-markup"><code>Jeśli się nie wie, do jakiego portu płynąć, nie należy liczyć, że wiatry będą sprzyjać.</code></pre>
<pre class="language-markup"><code>Nie pragnij być lepszym od innych, najpierw bądź samym sobą</code></pre>','publishedAt' => '2020-05-05 15:50:54','updatedAt' => '2020-10-05 20:16:25','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '302','user_id' => '1','title' => 'Wzorzec composite','slug' => 'wzorzec-composite','content' => '<p>Jak widać, wszystkie jednostki naszego modelu rozszerzają klasę Unit. Użytkownik może więc być pewien, że każdy z obiekt&oacute;w hierarchii Unit będzie obsługiwał metodę bombardStrength(). Klasa Unit może być traktowana identycznie jak Archer. Klasy Army i TroopCarrier są kompozytami (ang. composite) &mdash; obiektami składającymi się z innych obiekt&oacute;w. Klasy Archer i LaserCannonUnit to liście bądź końc&oacute;wki (ang. leaves), klasy reprezentujące końcowe węzły struktury drzewiastej przystosowane do obsługi podstawowych operacji hierarchii, ale nienadające się do przechowywania innych jej obiekt&oacute;w. Pojawia się wątpliwość, czy liście powinny implementować identyczny interfejs, co kompozyty (jak na rysunku 10.1). Na diagramie widać, że TroopCarrier i Army agregują inne jednostki, ale klasy liści r&oacute;wnież implementują wywołanie addUnit() &mdash; wr&oacute;cimy do tego wkr&oacute;tce. Na razie przyjrzyjmy się abstrakcyjnej klasie Unit:</p>
<pre class="language-php"><code>&lt;?php
abstract class Unit
{
    abstract function addUnit(Unit $unit);
    abstract function removeUnit(Unit $unit);
    abstract function bombardStrength();
}
//Mamy tu zarys podstawowej funkcjonalności wszystkich obiekt&oacute;w hierarchii Unit. Sp&oacute;jrzmy teraz,
//jak wymienione metody abstrakcyjne mogłyby być implementowane w obiektach-kompozytach:
class Army extends Unit
{
    private $units = array();
    function addUnit(Unit $unit)
    {
        if (in_array($unit, $this-&gt;units, true))
        {
            return;
        }
        $this-&gt;units[] = $unit;
    }
    function removeUnit(Unit $unit)
    {
        $this-&gt;units = array_udiff($this-&gt;units, array(
            $unit
        ) , function ($a, $b)
        {
            return ($a === $b) ? 0 : 1;
        });
    }
    function bombardStrength()
    {
        $ret = 0;
        foreach ($this-&gt;units as $unit)
        {
            $ret += $unit-&gt;bombardStrength();
        }
        return $ret;
    }
}
</code></pre>
<p>Metoda addUnit() klasy Army przed włączeniem do oddziału przekazywanej w wywołaniu jednostki sprawdza, czy nie posiada jej już w prywatnej tablicy jednostek. Metoda removeUnit() sprawdza w pętli (podobnie jak metoda addUnit()), czy usunąć dany obiekt Unit.</p>
<p>Obiekty klasy Army mogą przechowywać dowolnego rodzaju obiekty hierarchii Unit, w tym inne obiekty klasy Army lub końc&oacute;wki takie jak Archer czy LaserCannonUnit. Ponieważ wszystkie jednostki mają implementować metodę bombardStrength(), implementacja tej metody w klasie Army sprowadza się do przejrzenia wszystkich obiekt&oacute;w zawieranych, przechowywanych w składowej $units, i sumowania wartości zwracanych z inicjowanych na ich rzecz wywołań bombardStrength(). Problematycznym aspektem wzorca pozostaje implementacja operacji wcielania i usuwania jednostek. Klasyczny wzorzec zakłada definicję metod add...() i remove...() w abstrakcyjnej klasie bazowej. Dzięki temu wszystkie klasy objęte szablonem udostępniają wsp&oacute;lny interfejs. Ale przez to implementacje tych m</p>
<pre class="language-php"><code>&lt;?php
class UnitException extends Exception
{
}
class Archer extends Unit
{
    function addUnit(Unit $unit)
    {
        throw new UnitException(get_class($this) . " to liść");
    }
    function removeUnit(Unit $unit)
    {
        throw new UnitException(get_class($this) . " to liść");
    }
    function bombardStrength()
    {
        return 4;
    }
}

</code></pre>
<p>Z definicji klasa Archer nie jest przewidziana do przechowywania obiekt&oacute;w hierarchii Unit, więc na wywołanie na rzecz obiektu Archer metody addUnit() albo removeUnit() reagujemy zgłoszeniem wyjątku. Ponieważ musielibyśmy podobną implementację przewidzieć dla wszystkich klas końc&oacute;wek (liści), możemy zdecydować się na jej przeniesienie do abstrakcyjnej klasy bazowej</p>
<pre class="language-php"><code>&lt;?php
abstract class Unit
{
    abstract function bombardStrength();
    function addUnit(Unit $unit)
    {
        throw new UnitException(get_class($this) . " to liść");
    }
    function removeUnit(Unit $unit)
    {
        throw new UnitException(get_class($this) . " to liść");
    }
}
class Archer extends Unit
{
    function bombardStrength()
    {
        return 4;
    }
}

</code></pre>
<pre class="language-php"><code>// utworzenie armii
$main_army = new Army();
// włączenie do niej paru jednostek
$main_army-&gt;addUnit(new Archer());
$main_army-&gt;addUnit(new LaserCannonUnit() );
// utworzenie nowej armii
$sub_army = new Army();
// zaciąg do nowej armii
$sub_army-&gt;addUnit(new Archer());
$sub_army-&gt;addUnit(new Archer());
$sub_army-&gt;addUnit(new Archer());
// wcielenie drugiej armii do pierwszej
$main_army-&gt;addUnit($sub_army);
// obliczenia siły ataku wykonywane automatycznie w tle
print "Atak z siłą: {$main_army-&gt;bombardStrength()}\\n";</code></pre>
<p>Do utworzonego oddziału gł&oacute;wnego dodajemy kilka jednostek podstawowych. Proces ten powtarzamy dla drugiego utworzonego oddziału, kt&oacute;ry następnie wcielamy do pierwszego. Przy obliczaniu siły rażenia (Unit::bombardStrength()) wynikowego oddziału złożoność struktury hierarchii obiekt&oacute;w jest dla wywołującego zupełnie niewidoczna. Konsekwencje Jeśli Czytelnik myśli podobnie jak ja, powinien na widok kodu klasy Archer nabrać podejrzeń. Po co bowiem do klas końc&oacute;wek włączamy metody addUnit() i removeUnit(), jeśli nie ma potrzeby obsługiwania operacji wcielania i usuwania jednostek? Odpowiedź tkwi w przezroczystości typu Unit. Jeśli użytkownik otrzymuje obiekt typu Unit, ma pewność, że obiekt ten implementuje metody addUnit() i removeUnit(). Uwidacznia się tu przyjęta we wzorcu Composite zasada, że klasy obiekt&oacute;w niepodzielnych (liści) mają interfejs identyczny z klasami kompozyt&oacute;w. Taka odpowiedź jest jednak mało satysfakcjonująca, ponieważ honorowanie interfejsu nie oznacza w tym przypadku bezpieczeństwa wywołania metod addUnit() czy removeUnit() na rzecz każdego z obiekt&oacute;w hierarchii Unit. Gdybyśmy owe metody przesunęli tak, aby były dostępne jedynie dla klas kompozyt&oacute;w, wtedy z kolei powstałby problem niepewności co do tego, czy otrzymany obiekt hierarchii Unit obsługuje czy nie obsługuje daną metodę. Mimo wszystko pozostawienie metod-pułapek w klasach liści to dla mnie sytuacja mało komfortowa. Nie ma tu wartości dodanej, a jedynie zamieszanie w projekcie systemu, ponieważ interfejs w zasadzie okłamuje użytkownik&oacute;w co do swojej własnej funkcjonalności. Moglibyśmy w prosty spos&oacute;b wyeliminować tę niedogodność, wydzielając dla kompozyt&oacute;w ich własny podtyp CompositeUnit. Polegałoby to przede wszystkim na usunięciu metod addUnit() i removeUnit() z klasy Unit:</p>
<pre class="language-php"><code>&lt;?php
abstract class Unit
{
    function getComposite()
    {
        return null;
    }
    abstract function bombardStrength();
}

</code></pre>
<p>Zwr&oacute;ćmy uwagę na metodę getComposite(). Wr&oacute;cimy do niej za moment. Teraz potrzebujemy abstrakcyjnej klasy definiującej metody usunięte z klasy Unit. Możemy w niej nawet przewidzieć ich implementacje domyślne:</p>
<pre class="language-php"><code>abstract class CompositeUnit extends Unit
{
    private $units = array();
    function getComposite()
    {
        return $this;
    }
    protected function units()
    {
        return $this-&gt;units;
    }
    function removeUnit(Unit $unit)
    {
        $this-&gt;units = array_udiff($this-&gt;units, array(
            $unit
        ) , function ($a, $b)
        {
            return ($a === $b) ? 0 : 1;
        });
        function addUnit(Unit $unit)
        {
            if (in_array($unit, $this-&gt;units, true))
            {
                return;
            }
            $this-&gt;units[] = $unit;
        }
    }
}
</code></pre>
<p>Klasa CompositeUnit (kompozyt jednostek) choć sama w sobie nie zawiera żadnych metod abstrakcyjnych, jest deklarowana jako abstrakcyjna. R&oacute;wnocześnie rozszerza klasę Unit, nie definiując jej abstrakcyjnej metody bombardStrength(). Klasa Army (i wszystkie inne klasy kompozyt&oacute;w) może teraz rozszerzać klasę CompositeUnit. Organizację klas po tej modyfikacji ilustruje rysunek 10.2</p>
<p>&nbsp;</p>
<p>Wyeliminowaliśmy irytujące i bezużyteczne implementacje metod dodawania i usuwania jednostek z klas liści, ale teraz klient musi przed wywołaniem tych metod sprawdzać, czy obiekt, na rzecz kt&oacute;rego chce zainicjować wywołanie, jest obiektem klasy CompositeUnit. Tutaj do akcji wkracza metoda getComposite(). Domyślnie zwraca ona bowiem wartość pustą. Jedynie w klasach dziedziczących po CompositeUnit wartość zwracana to obiekt klasy CompositeUnit. Jeśli więc wywołanie tej metody zwr&oacute;ci obiekt, można na jego rzecz wywołać metodę addUnit(). Oto zastosowanie tej techniki z punktu widzenia użytkownika:&nbsp;</p>
<pre class="language-php"><code>&lt;?php
class UnitScript
{
    static function joinExisting(Unit $newUnit, Unit $occupyingUnit)
    {
        $comp;
        if (!isnull($comp = $occupyingUnit-&gt;getComposite()))
        {
            $comp-&gt;addUnit($newUnit);
        }
        else
        {
            $comp = new Army();
            $comp-&gt;addUnit($occupyingUnit);
            $comp-&gt;addUnit($newUnit);
        }
        return $comp;
    }
}

</code></pre>
<p>Metoda joinExisting() (połącz siły) przyjmuje dwa obiekty hierarchii Unit. Pierwszy z nich reprezentuje jednostkę nowo przybyłą na dane pole, drugi &mdash; jednostkę już na tym polu przebywającą (okupującą pole planszy). Jeśli druga z tych jednostek jest kompozytem (obiektem klasy CompositeUnit), wtedy pierwszy z obiekt&oacute;w jest do niej dodawany. W innym przypadku tworzony jest nowy obiekt klasy Army, do kt&oacute;rego wcielane są obie jednostki. Określanie przynależności do hierarchii klas kompozytowych odbywa się za pośrednictwem metody getComposite(). Jeśli zwr&oacute;ci ona obiekt, możemy wprost do niego dodawać nowe obiekty klasy Unit. Jeśli wynikiem wywołania getComposite() będzie wartość pusta, musimy utworzyć obiekt kompozytu na własną rękę, tworząc egzemplarz klasy Army i wcielając do niego obie jednostki. Model można uprościć jeszcze bardziej, wymuszając w metodzie Unit::getComposite() zwr&oacute;cenie obiektu Army wypełnionego początkowo bieżącą jednostką Unit. Moglibyśmy też wr&oacute;cić do poprzedniego modelu (w kt&oacute;rym nie rozr&oacute;żnialiśmy pomiędzy obiektami kompozyt&oacute;w a liśćmi) i zrealizować to samo w metodzie Unit::addUnit(): możemy tam utworzyć obiekt Army i dodać do niego oba obiekty Unit. To eleganckie rozwiązanie,Metoda joinExisting() (połącz siły) przyjmuje dwa obiekty hierarchii Unit. Pierwszy z nich reprezentuje jednostkę nowo przybyłą na dane pole, drugi &mdash; jednostkę już na tym polu przebywającą (okupującą pole planszy). Jeśli druga z tych jednostek jest kompozytem (obiektem klasy CompositeUnit), wtedy pierwszy z obiekt&oacute;w jest do niej dodawany. W innym przypadku tworzony jest nowy obiekt klasy Army, do kt&oacute;rego wcielane są obie jednostki. Określanie przynależności do hierarchii klas kompozytowych odbywa się za pośrednictwem metody getComposite(). Jeśli zwr&oacute;ci ona obiekt, możemy wprost do niego dodawać nowe obiekty klasy Unit. Jeśli wynikiem wywołania getComposite() będzie wartość pusta, musimy utworzyć obiekt kompozytu na własną rękę, tworząc egzemplarz klasy Army i wcielając do niego obie jednostki. Model można uprościć jeszcze bardziej, wymuszając w metodzie Unit::getComposite() zwr&oacute;cenie obiektu Army wypełnionego początkowo bieżącą jednostką Unit. Moglibyśmy też wr&oacute;cić do poprzedniego modelu (w kt&oacute;rym nie rozr&oacute;żnialiśmy pomiędzy obiektami kompozyt&oacute;w a liśćmi) i zrealizować to samo w metodzie Unit::addUnit(): możemy tam utworzyć obiekt Army i dodać do niego oba obiekty Unit. To eleganckie rozwiązanie,</p>
<pre class="language-php"><code>&lt;?php
class TroopCarrier
{
    function addUnit(Unit $unit)
    {
        if ($unit instanceof Cavalry)
        {
            throw new UnitException("Transporter nie może przewozić koni");
        }
        parent::addUnit($unit);
    }
    function bombardStrength()
    {
        return 0;
    }
}

</code></pre>
<p>Jesteśmy tu zmuszeni do testowania typu obiektu przekazanego w wywołaniu metody addUnit() za pośrednictwem operatora instanceof. Im więcej takich jak ten przypadk&oacute;w specjalnych, tym wady wzorca będą dokuczliwsze. Wzorzec Composite działa najlepiej wtedy, kiedy większość komponent&oacute;w to obiekty wymienialne, o zbliżonej semantyce. Kolejną kwestią jest koszt niekt&oacute;rych operacji w ramach wzorca. Typowym przykładem jest wywołanie Army::bombardStrength(), prowokujące kaskadę wywołań propagowanych w d&oacute;ł drzewa struktury jednostek zawieranych w oddziale. Przy mocno rozbudowanych drzewach z wieloma pododdziałami owo jedno wywołanie może sprowokować &bdquo;w tle&rdquo; istną lawinę wywołań. Co prawda koszt wykonania metody bombardStrength() nie jest obecnie wysoki, łatwo jednak sobie wyobrazić efekty skomplikowania obliczania siły ataku niekt&oacute;rych jednostek. Jednym ze sposob&oacute;w eliminacji nawału wywołań i delegowania jest buforowanie wynik&oacute;w poprzednich wywołań metod obiekt&oacute;w zawieranych w obiektach-kompozytach, tak aby w przyszłych odwołaniach do tej wartości można było pominąć narzut wywołań. Ale wtedy trzeba pilnować aktualizacji buforowanych wartości, wdrażając strategię opr&oacute;żniania bufor&oacute;w po operacjach na drzewie obiekt&oacute;w. Może to wymagać wyposażenia obiekt&oacute;w zawieranych w referencje do obiekt&oacute;w kompozyt&oacute;w. Wreszcie słowo o trwałości. Wzorzec Composite jest co prawda wyjątkowo elegancki, ale nie bardzo nadaje się do utrwalania zbudowanej struktury obiekt&oacute;w w bazie danych, a to dlatego, że całe struktury traktowane są jako pojedyncze obiekty. Aby więc skonstruować taką strukturę na podstawie informacji odczytywanych z bazy danych, trzeba posłużyć się serią kosztownych zapytań. Problem można wyeliminować, przypisując do całego drzewa identyfikator, tak aby można było jednym zapytaniem wyodrębnić z bazy danych wszystkie komponenty drzewa. Po wyodrębnieniu wszystkich obiekt&oacute;w trzeba będzie jednak i tak odtworzyć budowę drzewa, z zachowaniem układu obiekt&oacute;w podrzędnych i nadrzędnych, kt&oacute;ry r&oacute;wnież trzeba odzwierciedlić w schemacie bazy danych. Nie jest to zadanie bardzo trudne, ale mimo wszystko nieco skomplikowane. Przystosowanie wzorca Composite do baz danych jest wątpliwe, zupełnie inaczej ma się sprawa z językiem XML, a to dlatego, że w XML-u bardzo łatwo tworzyć drzewiaste struktury element&oacute;w</p>','publishedAt' => '2020-05-07 11:31:38','updatedAt' => '2020-05-07 13:01:48','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '303','user_id' => '1','title' => 'instalacja dockera','slug' => 'instalacja-dockera','content' => '<pre class="language-markup"><code>sudo apt install docker.io</code></pre>','publishedAt' => '2020-05-12 09:18:13','updatedAt' => '2020-05-12 09:18:13','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '304','user_id' => '1','title' => 'budowanie obrazu z pliku','slug' => 'budowanie-obrazu-z-pliku','content' => '<pre class="language-markup"><code>sudo docker build -t xxxxxx_name_xxxxxx .</code></pre>
<div id="gtx-trans" style="position: absolute; left: 215px; top: 5px;">
<div class="gtx-trans-icon">&nbsp;</div>
</div>','publishedAt' => '2020-05-12 09:51:05','updatedAt' => '2020-05-12 09:51:05','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '305','user_id' => '1','title' => 'rabbit przykład','slug' => 'rabbit-przyklad','content' => '<p>//composer.json</p>
<pre class="language-javascript"><code>{
    "require": {
        "php-amqplib/php-amqplib": "^2.11"
    }
}
</code></pre>
<p>//publisher.php</p>
<pre class="language-php"><code>&lt;?php
ini_set(\'display_errors\', 1);
ini_set(\'display_startup_errors\', 1);
error_reporting(E_ALL);

require_once(__DIR__ . \'/vendor/autoload.php\');

define("RABBITMQ_HOST", "localhost");
define("RABBITMQ_PORT", 5672);
define("RABBITMQ_USERNAME", "guest");
define("RABBITMQ_PASSWORD", "guest");
define("RABBITMQ_QUEUE_NAME", "task_queue");

$connection = new \\PhpAmqpLib\\Connection\\AMQPStreamConnection(
    RABBITMQ_HOST,
    RABBITMQ_PORT,
    RABBITMQ_USERNAME,
    RABBITMQ_PASSWORD
);

$channel = $connection-&gt;channel();

# Create the queue if it does not already exist.
$channel-&gt;queue_declare(
    $queue = RABBITMQ_QUEUE_NAME,
    $passive = false,
    $durable = true,
    $exclusive = false,
    $auto_delete = false,
    $nowait = false,
    $arguments = null,
    $ticket = null
);

$jobId = 0;

while (true) {
    $jobArray = [
        \'id\' =&gt; $jobId++,
        \'task\' =&gt; \'sleep\',
        \'sleep_period\' =&gt; rand(0, 3)
    ];

    $msg = new \\PhpAmqpLib\\Message\\AMQPMessage(
        json_encode($jobArray, JSON_UNESCAPED_SLASHES),
        [\'delivery_mode\' =&gt; 2] # Non-persistent (1) or persistent (2).
    );

    $channel-&gt;basic_publish($msg, \'\', RABBITMQ_QUEUE_NAME);
    print \'Job created\' . PHP_EOL;
    sleep(1);
}
</code></pre>
<p>//worker.php</p>
<pre class="language-php"><code>&lt;?php
ini_set(\'display_errors\', 1);
ini_set(\'display_startup_errors\', 1);
error_reporting(E_ALL);
require_once __DIR__ . \'/vendor/autoload.php\';

define("RABBITMQ_HOST", "localhost");
define("RABBITMQ_PORT", 5672);
define("RABBITMQ_USERNAME", "guest");
define("RABBITMQ_PASSWORD", "guest");
define("RABBITMQ_QUEUE_NAME", "task_queue");

$connection = new \\PhpAmqpLib\\Connection\\AMQPStreamConnection(
    RABBITMQ_HOST,
    RABBITMQ_PORT,
    RABBITMQ_USERNAME,
    RABBITMQ_PASSWORD
);

$channel = $connection-&gt;channel();

# Create the queue if it doesnt already exist.
$channel-&gt;queue_declare(
    $queue = RABBITMQ_QUEUE_NAME,
    $passive = false,
    $durable = true,
    $exclusive = false,
    $auto_delete = false,
    $nowait = false,
    $arguments = null,
    $ticket = null
);


echo \' [*] Waiting for messages. To exit press CTRL+C\', "\\n";

$callback = function ($msg) {
    echo " [x] Received ", $msg-&gt;body, "\\n";
    $job = json_decode($msg-&gt;body, $assocForm = true);
    sleep($job[\'sleep_period\']);
    echo " [x] Done", "\\n";
    $msg-&gt;delivery_info[\'channel\']-&gt;basic_ack($msg-&gt;delivery_info[\'delivery_tag\']);
};

$channel-&gt;basic_qos(null, 1, null);

$channel-&gt;basic_consume(
    $queue = RABBITMQ_QUEUE_NAME,
    $consumer_tag = \'\',
    $no_local = false,
    $no_ack = false,
    $exclusive = false,
    $nowait = false,
    $callback
);

while (count($channel-&gt;callbacks)) {
    $channel-&gt;wait();
}

$channel-&gt;close();
$connection-&gt;close();
</code></pre>','publishedAt' => '2020-06-09 13:28:59','updatedAt' => '2020-06-09 13:29:28','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '306','user_id' => '1','title' => 'Cqrs w praktyce','slug' => 'cqrs-w-praktyce','content' => '<p><a href="https://zawarstwaabstrakcji.pl/20170130-cqrs-w-praktyce-wprowadzenie-php/" target="_blank" rel="noopener">Link do artukułu </a></p>
<p>Zanim przejdziemy do kodu warto rozwinąć skr&oacute;t CQRS,&nbsp;<strong>Command Query Responsibility Segregation</strong>. Wzorzec opisał, przedstawił i prawdopodobnie wymyślił&nbsp;<a href="http://codebetter.com/gregyoung/">Greg Young</a>.<br />Kr&oacute;tko m&oacute;wiąc, celem tego wzorca jest rozdzielenie modelu służącego do zmiany stanu systemu od modelu służącego do odczytywania stanu systemu.<br />Wzorzec ten powstał prawdopodobnie na bazie pojęcia&nbsp;<strong>Command Query Separation</strong>. Pojęcie to z kolei wywodzi się z podstaw programowania obiektowego, m&oacute;wi ono że metody obiektu można podzielić na dwa typy.</p>
<ul>
<li><strong>Query</strong>&nbsp;- pozwalające uzyskać informację o obiekcie, odczytać go</li>
<li><strong>Commands</strong>&nbsp;- pozwalające wykonać operacje na obiekcie, zmodyfikować go</li>
</ul>
<p>Trzymając się tej reguły nigdy nie powinniśmy tworzyć czegoś na wz&oacute;r:</p>
<pre class="language-php"><code>&lt;?php

final class System
{
    private $status = 0;

    public function commandQuery() : int
    {
        $this-&gt;status = rand(0, 100);

        return $this-&gt;status;
    }
}</code></pre>
<p>Jak widać, metoda&nbsp;<code>commandQuery</code>&nbsp;służy zar&oacute;wno do zmiany stanu obiektu&nbsp;<code>System</code>&nbsp;jak i do jego pobrania. Odrobinę bardziej przewidywalna w skutkach implementacja tej samej klasy wyglądałaby tak:</p>
<pre class="language-php"><code>&lt;?php

final class System
{
    private $status = 0;

    public function command() : void
    {
        $this-&gt;status = rand(0, 100);
    }

    public function query() : int
    {
        return $this-&gt;status;
    }
}</code></pre>
<p>To przydługie wprowadzenie było niezbędne przed przejściem do tematu właściwego, CQRS. Myśląc o CQRS warto pamiętać o CQS tylko w skali bardziej globalnej, CQS odnosi się do obiekt&oacute;w, natomiast CQRS do systemu jako całości. CQS m&oacute;wi, że metody obiektu dzielimy na te kt&oacute;re odczytują i te kt&oacute;re modyfikują. CQRS natomiast wyznacza spos&oacute;b w jaki należy komunikować się z systemem, jak zmieniać jego stan oraz jak ten stan odczytywać.</p>
<p><strong>Czym jednak jest wspominany co kilka zdań&nbsp;<em>Stan Systemu</em>?</strong></p>
<p>Zał&oacute;żmy, że pracujemy z systemem służącym do zarządzania użytkownikami. Stanem systemu jest więc ilość użytkownik&oacute;w czy szczeg&oacute;łowe dane konkretnego użytkownika.</p>
<p><strong>W jaki spos&oacute;b można zmienić&nbsp;<em>Stan Systemu</em>?</strong></p>
<p>Zmiana stanu to na przykład utworzenie nowego użytkownika czy modyfikacja danych kt&oacute;regokolwiek z użytkownik&oacute;w.</p>
<p>W tym momencie powinniście się już domyślać, że CQRS to nic innego jak uporządkowany i jednolity spos&oacute;b pozwalający zmieniać i odczytywać stan systemu. Zacznijmy więc od zmian.</p>
<h1 id="write-model">Write Model</h1>
<p>Zmiana stanu systemu zgodnie z wzorcem CQRS powinna być powodowana jedynie po zmianach w&nbsp;<code>Write Modelu</code>. Postaram się wyjaśnić jak zbudować prosty system zbudowany z 3 prostych warstw wspartych przez implementację konkretnych narzędzi.</p>
<ul>
<li>Domain</li>
<li>Application</li>
<li>UserInterface</li>
</ul>
<p>W tej części skupie się jedynie na zmianie stanu systemu, odczyt zostawię na koniec. Trzeba w końcu mieć z czego czytać.</p>
<h3 id="user-interface">User Interface</h3>
<p>Każdy system posiada jakiś interfejs. Czasami jak w przypadku aplikacji desktopowych są to okienka, czasami może to być terminal a czasami protok&oacute;ł HTTP. Komendy mają sw&oacute;j początek właśnie w interfejsie użytkownika i reprezentują jego intencje względem systemu. System może posiadać wiele interfejs&oacute;w użytkownika. Przykładowo do obsługi systemu zarządzania użytkownikami można stworzyć API RESTowe lub umożliwić dostęp bezpośrednio poprzez terminal. Obydwa interfejsy mogą pozwalać zrobić dokładnie to samo tylko przy użyciu innych narzędzi. W jednym wypadku konieczne będzie wysłanie Requestu HTTP zawierającego email oraz nazwę użytkownika:</p>','publishedAt' => '2020-06-10 09:18:43','updatedAt' => '2020-06-11 08:58:08','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '307','user_id' => '1','title' => 'redis linux podstawy','slug' => 'redis-linux-podstawy','content' => '<p>Instalacja</p>
<pre class="language-markup"><code>sudo apt-get install redis-server</code></pre>
<p>Status</p>
<pre class="language-markup"><code>sudo systemctl status redis</code></pre>
<p>Cli na linuxie:</p>
<pre class="language-markup"><code>sudo redis-cli</code></pre>
<p>W cli:</p>
<pre class="language-markup"><code>SET country Poland</code></pre>
<p>EX - ustawi ana 3600 sekund</p>
<pre class="language-markup"><code>SET user_1234 "Marcin Lewandowski" EX 3600</code></pre>
<p>Pokazanie wartośći:</p>
<pre class="language-markup"><code>GET country</code></pre>
<p>Kasowanie:</p>
<pre class="language-markup"><code>DEL country</code></pre>
<p>Sprawdzanie klucza:</p>
<pre class="language-markup"><code>EXISTS country</code></pre>
<p>Ustwienie wygaśniecia dla klucza:</p>
<pre class="language-markup"><code>EXPIRE user_1234 60</code></pre>','publishedAt' => '2020-06-10 11:57:17','updatedAt' => '2020-06-10 11:57:17','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '308','user_id' => '1','title' => 'Nawigacja po tablicy','slug' => 'nawigacja-po-tablicy','content' => '<pre class="language-javascript"><code>function arrayNavigation(array, key){
    var len = array.length;
    var data = [];
    key = parseInt(key);
    data[\'current\'] = key;
    data[\'prev\'] = (key+len-1)%len;
    data[\'next\'] = (key+1)%len;
    return data;
}</code></pre>','publishedAt' => '2020-06-10 12:00:01','updatedAt' => '2020-06-10 12:00:01','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '309','user_id' => '1','title' => 'Zmiana szybkości filmu w html','slug' => 'zmiana-szybkosci-filmu-w-html','content' => '<pre class="language-javascript"><code>document.querySelector(\'video\').playbackRate = 1.5
</code></pre>','publishedAt' => '2020-06-10 12:06:22','updatedAt' => '2020-06-10 12:06:37','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '310','user_id' => '1','title' => 'wzorzec pyłek','slug' => 'wzorzec-pylek','content' => '<div class="figure-in-article-content">
<p>Wzorca projektowego pyłek (ang. flyweight) można użyć, jeśli mamy naprawdę dużo małych obiekt&oacute;w (małych jak muchy), kt&oacute;re r&oacute;żnią się tylko podanym stanem lub wieloma danymi, kt&oacute;re działają na podobnych danych wejściowych (lub część tego stanu jest powtarzalna).</p>
<p>Wyobraź sobie, że importujesz ogromny plik CSV z kilkoma tysiącami wierszy informacji o telewizorach rozmieszczonych po całej Europie. Twoim zadaniem jest sprawdzanie kondycji każdego z nich za pomocą pojedynczego połączenia z jego adresem IP i zalogowanie informacji o tym urządzeniu.</p>
<p>Kilka tysięcy nowych obiekt&oacute;w. To naprawdę sporo danych do przechowywania w pamięci RAM. Na szczęście duża część danych w wierszach jest powtarzalna. Możemy ponownie wykorzystywać obiekty, kt&oacute;re już stworzyliśmy i pracować na nich. To esencja wzorca pyłek.</p>
<p>Zacznijmy od utworzenia przykładowego raportu. Mamy wiele r&oacute;żnych telewizor&oacute;w umieszczonych w wielu lokalizacjach. Każdy z nich ma unikalny identyfikator użytkownika i kilka innych informacji. W tym przykładzie utworzymy 5 tys. wierszy danych.</p>
<pre class="language-php"><code>&lt;?php
class CSV {
    const COLUMNS = ["id", "uuid", "location", "resolution", "producer", "operating_system", "ip"];
    const LOCATIONS = ["Warsaw", "Berlin", "Amsterdam", "Paris"];
    const RESOLUTIONS = ["Full HD", "4K"];
    const PRODUCERS = ["LG", "Samsung", "Philips", "Sencor"];
    const OPERATING_SYSTEMS = ["Linux", "Android", "Ubuntu"];
    private $numItems;
    private $fileName;
    private $file;
    public function __construct (string $fileName, int $numItems) {
        $this-&gt;numItems = $numItems;
        $this-&gt;fileName = $fileName;
    }
    protected function createHeader (): void {
        fputcsv($this-&gt;file, self::COLUMNS);
    }
    protected function generateRandomString (int $length): string {
        return substr(str_shuffle(str_repeat($x = \'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\', ceil($length / strlen($x)))), 1, $length);
    }
    protected function getRand (array $arr): string {
        $key = array_rand($arr);
        return $arr[$key];
    }
    protected function getRandIp () {
        return mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255);
    }
    protected function generateData (): void {
        for ($idx = 0; $idx &lt; $this-&gt;numItems; $idx++) {
            fputcsv($this-&gt;file, [
                $idx + 1,
                $this-&gt;generateRandomString(rand(5, 10)),
                $this-&gt;getRand(self::LOCATIONS),
                $this-&gt;getRand(self::RESOLUTIONS),
                $this-&gt;getRand(self::PRODUCERS),
                $this-&gt;getRand(self::OPERATING_SYSTEMS),
                $this-&gt;getRandIp()
            ]);
        }
    }
    public function create (): void {
        $this-&gt;file = fopen($this-&gt;fileName, \'w\');
        $this-&gt;createHeader();
        $this-&gt;generateData();
        fclose($this-&gt;file);
    }
}
$csv = new CSV("demo.csv", 5000);
$csv-&gt;create();</code></pre>
<p><br />Nasz raport jest już gotowy. Teraz musimy dowiedzieć się, jak stworzyć aplikację, kt&oacute;ra będzie odpytywać każde urządzenie. Zauważyliśmy, że każdy ma unikalny identyfikator użytkownika i adres IP. Inne pola są dość powtarzalne i możemy je przechowywać w osobnym obiekcie. Nazwijmy to&nbsp;<code>DeviceType</code>.</p>
<pre class="language-php"><code>&lt;?php
namespace structural\\flyweight;
class DeviceType {
    protected $location;
    protected $resolution;
    protected $producer;
    protected $operatingSystem;
    public function __construct (
        string $location,
        string $resolution,
        string $producer,
        string $operatingSystem
    ) {
        $this-&gt;location = $location;
        $this-&gt;resolution = $resolution;
        $this-&gt;producer = $producer;
        $this-&gt;operatingSystem = $operatingSystem;
    }
    public function reportType () {
        return "Working on device in {$this-&gt;location} with resolution {$this-&gt;resolution} crated by {$this-&gt;producer} and running {$this-&gt;operatingSystem}";
    }
}</code></pre>
<p><br />Ponieważ możemy ponownie wykorzystywać obiekty, stw&oacute;rzmy fabrykę, kt&oacute;ra będzie odpowiedzialna za cache\'owanie ich i tworzenie nowych w razie potrzeby.</p>
<pre class="language-php"><code>&lt;?php
namespace structural\\flyweight;
class DeviceTypeFactory {
    protected $deviceTypes = [];
    public function getType (
        string $location,
        string $resolution,
        string $producer,
        string $operatingSystem): DeviceType {
        $key = $this-&gt;getKey(
            $location,
            $resolution,
            $producer,
            $operatingSystem);
        if (!array_key_exists($key, $this-&gt;deviceTypes)) {
            $this-&gt;deviceTypes[$key] = new DeviceType(
                $location,
                $resolution,
                $producer,
                $operatingSystem
            );
        }
        return $this-&gt;deviceTypes[$key];
    }
    protected function getKey (
        string $location,
        string $resolution,
        string $producer,
        string $operatingSystem) {
        return md5(implode("_", func_get_args()));
    }
}</code></pre>
<p>Rozpoznajemy każdy typ urządzenia, tworząc hash z wszystkich jego parametr&oacute;w. Teraz stw&oacute;rzmy nasze urządzenie.</p>
<pre class="language-php"><code>&lt;?php
namespace structural\\flyweight;
class Device {
    protected $uid;
    protected $ip;
    protected $type;
    public function __construct (
        string $uid,
        string $ip,
        DeviceType $type
    ) {
        $this-&gt;uid = $uid;
        $this-&gt;ip = $ip;
        $this-&gt;type = $type;
    }
    public function ping () {
        echo "Checking if device {$this-&gt;uid} is active" . PHP_EOL;
        $this-&gt;type-&gt;reportType() . PHP_EOL;
        echo "Calling it on ip {$this-&gt;ip}" . PHP_EOL;
    }
}</code></pre>
<p><br />Ten kod odpowiada za sprawdzenie stanu każdego urządzenia. Urządzenia te tworzone są za pomocą DeviceStorage, kt&oacute;ry r&oacute;wnież śledzi wszystkie utworzone obiekty.</p>
<pre class="language-php"><code>&lt;?php
namespace structural\\flyweight;
class DeviceStorage {
    public $devices = [];
    public $deviceFactory;
    public function __construct () {
        $this-&gt;deviceFactory = new DeviceTypeFactory();
    }
    public function addDevice (
        string $uuid,
        string $location,
        string $resolution,
        string $producer,
        string $operatingSystem,
        string $ip
    ) {
        $type = $this-&gt;deviceFactory-&gt;getType(
            $location,
            $resolution,
            $producer,
            $operatingSystem
        );
        $this-&gt;devices[] = new Device($uuid, $ip, $type);
    }
    public function checkDevicesHealth () {
        foreach ($this-&gt;devices as $device) {
            $device-&gt;ping();
        }
    }
}</code></pre>
<p>Zł&oacute;żmy to w całość.</p>
<pre class="language-php"><code>&lt;?php
namespace structural\\flyweight;
//require ...
$file = fopen(\'demo.csv\', \'r\');
$devicesDB = new DeviceStorage();

for ($i = 0; $row = fgetcsv($file); ++$i) {
    // Omitting file headers
    if ($i) {
        $devicesDB-&gt;addDevice(
            $row[1],
            $row[2],
            $row[3],
            $row[4],
            $row[5],
            $row[6]
        );
    }
}

fclose($file);
$devicesDB-&gt;checkDevicesHealth();
echo memory_get_usage() / 1024 / 1024 . " RAM USED";</code></pre>
<p>Aplikacja z wykorzystaniem Flyweight zużyła około 2,1 MB na zestawie danych 5 tys. I 13,6 MB na zestawie danych 50 tys.<br /><br />Bez użycia wsp&oacute;łużytkowanego DeviceType było to 3,3 MB dla 5k i 25,7 MB dla 50k. To ogromna oszczędność.</p>
<p>Pyłek jest rzadko używany w aplikacjach webowych PHP, ponieważ każde żądanie w PHP jest całkowicie niezależne. Często nie przechowujemy danych bezpośrednio w pamięci RAM, a raczej w niekt&oacute;rych trwałych bazach danych lub pamięci podręcznej. Niemniej jednak ten wzorzec może być całkiem przydatny w konkretnych przypadkach użycia lub aplikacjach wiersza poleceń.<br /><br /><br />Oryginał tekstu w języku angielskim przeczytasz&nbsp;<a href="https://medium.com/swlh/flyweight-design-pattern-in-php-edcda0486fb0" target="_blank" rel="nofollow noopener">tutaj</a>.</p>
</div>','publishedAt' => '2020-06-11 10:31:17','updatedAt' => '2020-06-11 10:43:26','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '311','user_id' => '1','title' => 'wzorzec most','slug' => 'wzorzec-most','content' => '<h2>Przeznaczenie</h2>
<p>Pozwala oddzielić abstrakcję obiektu od jego implementacji.</p>
<p>Formatter.php</p>
<pre class="language-php"><code>&lt;?php declare(strict_types=1);

namespace DesignPatterns\\Structural\\Bridge;

interface Formatter
{
    public function format(string $text): string;
}</code></pre>
<p>PlainTextFormatter.php</p>
<pre class="language-php"><code>&lt;?php declare(strict_types=1);

namespace DesignPatterns\\Structural\\Bridge;

class PlainTextFormatter implements Formatter
{
    public function format(string $text): string
    {
        return $text;
    }
}</code></pre>
<p>HtmlFormatter.php</p>
<pre class="language-php"><code>&lt;?php declare(strict_types=1);

namespace DesignPatterns\\Structural\\Bridge;

class HtmlFormatter implements Formatter
{
    public function format(string $text): string
    {
        return sprintf(\'&lt;p&gt;%s&lt;/p&gt;\', $text);
    }
}</code></pre>
<p>Service.php</p>
<pre class="language-php"><code>&lt;?php declare(strict_types=1);

namespace DesignPatterns\\Structural\\Bridge;

abstract class Service
{
    protected Formatter $implementation;

    public function __construct(Formatter $printer)
    {
        $this-&gt;implementation = $printer;
    }

    public function setImplementation(Formatter $printer)
    {
        $this-&gt;implementation = $printer;
    }

    abstract public function get(): string;
}</code></pre>
<p>HelloWorldService.php</p>
<pre class="language-php"><code>&lt;?php declare(strict_types=1);

namespace DesignPatterns\\Structural\\Bridge;

class HelloWorldService extends Service
{
    public function get(): string
    {
        return $this-&gt;implementation-&gt;format(\'Hello World\');
    }
}</code></pre>
<p>PingService.php</p>
<pre class="language-php"><code>&lt;?php declare(strict_types=1);

namespace DesignPatterns\\Structural\\Bridge;

class PingService extends Service
{
    public function get(): string
    {
        return $this-&gt;implementation-&gt;format(\'pong\');
    }
}</code></pre>
<h2>Testy</h2>
<p>Tests/BridgeTest.php</p>
<pre class="language-php"><code>&lt;?php declare(strict_types=1);

namespace DesignPatterns\\Structural\\Bridge\\Tests;

use DesignPatterns\\Structural\\Bridge\\HelloWorldService;
use DesignPatterns\\Structural\\Bridge\\HtmlFormatter;
use DesignPatterns\\Structural\\Bridge\\PlainTextFormatter;
use PHPUnit\\Framework\\TestCase;

class BridgeTest extends TestCase
{
    public function testCanPrintUsingThePlainTextFormatter()
    {
        $service = new HelloWorldService(new PlainTextFormatter());

        $this-&gt;assertSame(\'Hello World\', $service-&gt;get());
    }

    public function testCanPrintUsingTheHtmlFormatter()
    {
        $service = new HelloWorldService(new HtmlFormatter());

        $this-&gt;assertSame(\'&lt;p&gt;Hello World&lt;/p&gt;\', $service-&gt;get());
    }
}</code></pre>','publishedAt' => '2020-06-11 12:26:34','updatedAt' => '2020-06-11 12:27:41','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '312','user_id' => '1','title' => 'footer  enp','slug' => 'footer-enp','content' => '<div>
<table style="border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 65.8pt;">
<td style="width: 446.55pt; height: 65.8pt; padding: 0 5.4pt; border-style: none none solid none; border-bottom-width: 1pt; border-bottom-color: #CCCCCC;" valign="top">
<p style="font-size: 11pt; font-family: Calibri,sans-serif; background-color: white; margin: 0;"><span style="color: #1c202a; font-size: 10pt; font-family: Helvetica, sans-serif, serif, EmojiFont;">&nbsp;</span></p>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; background-color: white; margin: 0;"><span style="color: #1c202a; font-size: 10pt; font-family: Helvetica, sans-serif, serif, EmojiFont;">Pozdrawiam,</span><span style="color: black;"><br /><br /></span></p>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; background-color: white; margin: 0;"><strong><span style="color: #1c202a; font-size: 10pt; font-family: Helvetica, sans-serif, serif, EmojiFont;">Grzegorz Tarka<br />Backend developer</span></strong></p>
</td>
</tr>
</tbody>
</table>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; background-color: white; margin: 0;"><span style="color: black; font-size: 10pt; font-family: Tahoma, sans-serif, serif, EmojiFont;">&nbsp;</span></p>
<table style="border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 78.75pt;">
<td style="width: 58.5pt; height: 78.75pt; padding: 3pt;">
<p style="font-size: 11pt; font-family: Calibri,sans-serif; margin: 0;"><span style="font-size: 9pt; font-family: Helvetica, sans-serif, serif, EmojiFont;"><img id="x_Obraz_x0020_2" style="width: 54.74pt; height: 73.49pt; cursor: pointer;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEkAAABiCAYAAAAcGP4pAAAGw0lEQVR4nO2dzZGjOhCAOwRn8BwCtahV1J4IYS5GmpkLITgEh+AMvBk4BELwacfMXAjBIfgdQLbAgFugn5laukqn9aDWp1bTajVaAKJEab5ClHmM4sB4dmJMXBiKK0NxZUxcYhRFjOKAKPMozVfU506VL3iLSsguZ8iuJWSXEjbFJ4hDCeK9Avf9tyRJ5DpGcWhBIbQYxSFJ5NqFTjqgvlZCdvkEcTiDm/5vEqX5KkaxNwHT1ziXO5t6PQPU06z2f5MkkWvkspoLSDXksrJhVRMANZYlKqtWhfgWmS4tH6CmArIOyrYF2QI1F5A1UFGar1wCujWenUzefrYA3UFtismQbDhp287cNiDV/k5x5kki174AMRRXxsTl2bJzBUiFCMaxVIzi4BUSimuMYh8C0CRr8m5FmjX1+SYfgJQ1kSEhyjwIJBRXxmQaAtC9tfsflBBLrW/J+QeUXT9gM7jkO5YkTgEhFaEA1UtOHEmQXETX1IZcVqEANZAqGqRQ/qixpFCAfgakJvIOBcgMUojlpm1NwkLKTiRI3h13Z+9WbzpDQSLu47yGAD2b2zNkf0JBMggBPAWTA7v/EsQ23JIjBpNRmq+c+6WR9EgFYZw32WkrcZomIeSPShBH35A+QRyMIDnb5BITbF/wFvm2okkZSs7lLgQgJR+w2XsEtTMGBKAOHy2FA4aAAGrf5CMcmJW+BbB0EDABkJIzyLXLbcr3ODGZAUhJveG1b1HWz94mgbIASEltUfZAlbApnB15k525RUC6nCHbzYOTXUoQW9t6PcjTgglHgJScQa7PkP0x8VXNb3feq0sAAGKevcQo9jGKArmsYhSFjzIbgNvb7/0DNvsSNoVajo21VCVsijqMkGkQOH3SzSiWkJ1KEMcSxLuL9d9A2jZ9PPgr1/0bCyXlaqs+6AwyrR2vmT/yUp80JKY56Q/Y7KeYfwX5qh7o7LfbzgGGYfFVH2Q7VrIeI40rPq8+6AveItf9BANlrz4ou4wp+mO2Ja4A6Yr2+SjbUfbIRNGS/6EAqdaXW7bkpEltUn2ST0D3djf7Jqr2AqixJvP6JP+A2tbk04qsWJPP+qD6EMCvFc22Jt/FCyWI93ob4R9S3YhHSqEAnaFeciFOSvT+vzWg2pKyUwnZKRQkcn1S6PqgUIBU/yRIIeuDQjcypNCK/ghIoSvNwvZP3KKEdZz3NGyo/kmQPB8vt1rTd7D6JHLUHTKY+zHBZJ1gD1JDXX2H/skSYsnp9UHnMEuOttTuSrrNCD7OYjtD6T9VIqpJGcq/M4+U586iT2uedfTtIxwYik381ScR92tD4jrP/MzMQ/cfHBRVwdD9G4Kyt/RM64Nc1Cc5K6I4B64PCt2/gaKqPog+szbrg0L3P0Xh9F4flJ3qXfytHVV9UKj+a8tz1/8iiyyyyCKLLLLIIossssh3kyjNVzHPXjiXO9Vinr3Y+JaEMZky9rpVz2XsdYv4vHyZojOizF3o3JLbV0kjn25NvbK1BjL8eT1yWSHK3PS5jMk0RlG40Lmns9et0R0B7JWUq0kSuWY8I3/fG6MoqAMy+cAauaxinr1MBvTQWX0z8gFR5ogyZ+x122dhz65D7PsKM0ZR1Eusfjbnctf9DXJZPVsmAzrvWzozcezq/Csxt9baR+iDYOI4pGDvcmTDORx98MhlxZLh3/5KZK5bsrqpq1/n120H/P6JzoUO09gH6gOhXpSp30qBXPYeGeszTb3S9eHu3Z4J6FonWWfNqsYmoEep+4UuQ4Md/lvtHoGewegDMfEFupX0DUbXOUb6dRoP97KMrICW6HRN12rMs5chZVlyX8JGs6b+XhvM799v/+n/pk/Ob8Nlo1v32IWgLdE7jJk4xigO5KYB7lphy6kS34K6tJZzJyyYavkA7XtZYka9HZD6uie0oUGSzVrXS3fMGuTWICdYqD5mMmSHkA5zIOl+R3fMViGhIN7p1nGuLJHplNZ9pfqAxJig3197e+5bZAy5PRhz30F7riNIE56tuwGy49bfQpQotzuQEWW8QDJZct34ymgvp0ej1LhDQRjaOHqzJKQFk1Gar/T9o0l8BQBta1KzM0T5IcQfmBGfkMYmC6DZdmmAJv/PF929kIojOJc7bYNbdH8zFID6ctx9OqvNM+dy96AzExfTALQ9MJ69kO9NYuIyttVoxznmkPRofiwE6JvcoYZcVrMA6UrEKA6DsJi4cC53FAfPEpkil9UUSEoX5LJ6Fic9TRQa6GwsdfzT5HtQ5mNpjiGJ0nw1VzndfzwLJpXO93yVBcv5aWIj4v4nRG2eF0hPRPkhH339D5fsuiIgdjXYAAAAAElFTkSuQmCC" crossorigin="use-credentials" data-imagetype="AttachmentByCid" data-custom="AAMkADU5ZTg2MTYyLTA1YjktNGNkMS05ZWQ3LTEyOWJiYzVkYmY2YQBGAAAAAAC3uJMQEM8QRbpXunCK%2FEWUBwC7iyd91Nm8SIT4bwQTt9%2FAAAAAAAEMAAC7iyd91Nm8SIT4bwQTt9%2FAAAADQnx3AAABEgAQAKwiM48tNLVIu7%2FLihEKAmg%3D" /></span></p>
</td>
<td style="width: 249pt; height: 78.75pt; padding: 3pt;">
<p style="font-size: 11pt; font-family: Calibri,sans-serif; background-color: white; margin: 0; line-height: 11.25pt;"><strong><span style="color: #1c202a; font-size: 10pt; font-family: Helvetica, sans-serif, serif, EmojiFont;">E NET PRODUCTION Sp. z o.o.</span></strong></p>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; background-color: white; margin: 0; line-height: 11.25pt;"><span style="color: #1c202a; font-size: 10pt; font-family: Helvetica, sans-serif, serif, EmojiFont;">Katowice 40-858</span></p>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; background-color: white; margin: 0; line-height: 11.25pt;"><span style="color: #1c202a; font-size: 10pt; font-family: Helvetica, sans-serif, serif, EmojiFont;">Bracka 28a</span></p>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; background-color: white; margin: 0; line-height: 11.25pt;"><span style="color: #1c202a; font-size: 10pt; font-family: Helvetica, sans-serif, serif, EmojiFont;">mail: <a href="mailto:gtarka@enp.pl" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable"><span style="color: #0563c1;">gtarka@enp.pl </span></a> </span><span style="color: black;">&nbsp;</span></p>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; background-color: white; margin: 0; line-height: 11.25pt;"><u><span style="color: #b81244; font-size: 10pt; font-family: Helvetica, sans-serif, serif, EmojiFont;"><a href="http://www.enp.pl/" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable"><span style="color: #0563c1;">www.enp.pl</span></a></span></u></p>
</td>
</tr>
</tbody>
</table>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; background-color: white; margin: 0;"><span style="color: black; font-size: 10pt; font-family: Tahoma, sans-serif, serif, EmojiFont;">&nbsp;</span></p>
<table style="border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 82.1pt;">
<td style="width: 442.7pt; height: 82.1pt; padding: 0 5.4pt; border-style: solid none none none; border-top-width: 1pt; border-top-color: #CCCCCC;" valign="top">
<p style="font-size: 11pt; font-family: Calibri,sans-serif; margin: 7.5pt 0 0 0; line-height: 115%;"><span style="color: #9d9d9d; font-size: 1pt; font-family: Calibri Light, sans-serif, serif, EmojiFont; line-height: 115%;">&nbsp;</span></p>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; margin: 7.5pt 0 0 0; line-height: 115%;"><span style="color: #9d9d9d; font-size: 9pt; font-family: Calibri Light, sans-serif, serif, EmojiFont; line-height: 115%;">Informacje i załączniki zawarte w tym emailu przeznaczone są wyłącznie dla adresata. Mogą zawierać chronione prawnie, poufne informacje. W przypadku, gdy nie jest Pan/Pani adresatem lub osobą upoważnioną do odbioru niniejszego e-maila, nie należy używać, powielać bądź przesyłać informacji i załącznik&oacute;w. W przypadku omyłkowego otrzymania emaila należy niezwłocznie poinformować o tym nadawcę i usunąć email. </span><span lang="en-US" style="color: #9d9d9d; font-size: 9pt; font-family: Calibri Light, sans-serif, serif, EmojiFont; line-height: 115%;">Dziękujemy. E NET PRODUCTION. </span></p>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; margin: 7.5pt 0 0 0; line-height: 115%;"><span lang="en-US" style="color: #9d9d9d; font-size: 1pt; font-family: Calibri Light, sans-serif, serif, EmojiFont; line-height: 115%;">&nbsp;</span></p>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; margin: 7.5pt 0 0 0; line-height: 115%;"><span lang="en-US" style="color: #9d9d9d; font-size: 9pt; font-family: Calibri Light, sans-serif, serif, EmojiFont; line-height: 115%;">This email is solely intended to the addressees and contains confidential information.Unless stated, the opinions and comments written down in this document are the sender\'s property and not the official vision of our Group. If you receive this email in error,&nbsp; please notify us by sending it back immediately to the email address of the sender and then please delete it from your own system. Please don\'t copy, use or forward the content of this document and its attachments to another person for any reason. Thank you for your understanding. E NET PRODUCTION.</span></p>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; margin: 7.5pt 0 0 0; line-height: 115%;"><span lang="en-US" style="color: #9d9d9d; font-size: 1pt; font-family: Calibri Light, sans-serif, serif, EmojiFont; line-height: 115%;">&nbsp;</span></p>
<p style="font-size: 11pt; font-family: Calibri,sans-serif; margin: 7.5pt 0 0 0; line-height: 250%;"><span lang="en-US" style="color: #70ad47; font-size: 9pt; font-family: Calibri Light, sans-serif, serif, EmojiFont; line-height: 250%;">Please consider the environment before printing this e-mail.</span></p>
</td>
</tr>
</tbody>
</table>
</div>','publishedAt' => '2020-06-15 15:16:08','updatedAt' => '2020-06-15 15:16:08','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '313','user_id' => '1','title' => 'defender blad','slug' => 'defender-blad','content' => '<p><strong><span class="x-hidden-focus">dism /online /cleanup-image /restorehealth</span></strong>&nbsp;</p>
<p>&nbsp;</p>
<p><strong><span class="x-hidden-focus">sfc /scannow&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></strong></p>
<p><strong><a href="https://answers.microsoft.com/pl-pl/windows/forum/all/ochrona-przed-wirusami-i-zagrożeniami-windows/47dc8301-c242-4758-8d7f-9b469534f496">link</a></strong></p>','publishedAt' => '2020-06-15 17:56:46','updatedAt' => '2020-06-15 17:56:46','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '314','user_id' => '1','title' => 'wzorowe repo','slug' => 'wzorowe-repo','content' => '<ul id="bkmrk-https%3A%2F%2Fgithub.com%2Fc">
<li><a href="https://github.com/CodelyTV/php-ddd-example">https://github.com/CodelyTV/php-ddd-example</a></li>
<li><a href="https://github.com/jorge07/ddd-playground">https://github.com/jorge07/ddd-playground</a></li>
</ul>
<p>&nbsp;</p>
<p><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/73672">https://gitlab.enp.me/mshp/adafir/-/merge_requests/73672</a></p>
<p><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/73141">https://gitlab.enp.me/mshp/adafir/-/merge_requests/73141</a></p>
<p><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/73886">https://gitlab.enp.me/mshp/adafir/-/merge_requests/73886</a></p>
<p><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/73904">https://gitlab.enp.me/mshp/adafir/-/merge_requests/73904</a></p>
<p>&nbsp;</p>
<p>https://gitlab.enp.me/mshp/adafir/-/merge_requests/74201</p>','publishedAt' => '2020-06-16 12:37:41','updatedAt' => '2021-02-12 07:44:25','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '315','user_id' => '1','title' => 'sprintf','slug' => 'sprintf','content' => '<pre class="language-php"><code>&lt;?php
$number = 9;
$str = "Beijing";
$ben = "pikawa";

$txt = sprintf("There are %u million bicycles in %shaaaa %s",$number,$str, $ben);
echo $txt;</code></pre>
<p>There are 9 million bicycles in Beijinghaaaa pikawa</p>','publishedAt' => '2020-06-16 15:48:18','updatedAt' => '2020-06-16 15:48:18','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '316','user_id' => '1','title' => 'array filter instancji z tablicy','slug' => 'array-filter-instancji-z-tablicy','content' => '<pre class="language-php"><code>array_filter($maybeOfferDTOs, static function ($offer) {
   return $offer instanceof OfferDTO;
});</code></pre>','publishedAt' => '2020-06-17 11:51:27','updatedAt' => '2020-06-17 11:51:27','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '317','user_id' => '1','title' => 'uruchomienie lini komend z contenera dockera','slug' => 'uruchomienie-lini-komend-z-contenera-dockera','content' => '<pre class="language-markup"><code>docker exec enp_phpfpm_1  php app/admin_alsen/console doctrine:migrations:generate --env=loc</code></pre>','publishedAt' => '2020-06-25 15:14:12','updatedAt' => '2020-06-25 15:15:36','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '318','user_id' => '1','title' => 'enet linki','slug' => 'enet-linki','content' => '<p><a href="https://techdoc.enp.pl/books">dokumentacja</a></p>
<p><strong>Role/uprawnienia</strong></p>
<p><a href="http://admin_enp0026.adafir.loc/user/edit/27101">http://admin_enp0026.adafir.loc/user/edit/27101</a></p>
<p><a href="https://panelterg.enp.pl/securityschema/list">https://panelterg.enp.pl/securityschema/list</a></p>
<p><a href="https://panelterg.enp.pl/role/list">https://panelterg.enp.pl/role/list</a></p>
<p><a href="https://panelterg.enp.pl/user/list">https://panelterg.enp.pl/user/list</a></p>
<p><strong>Dostawy edycja danych przekazywanych do api</strong></p>
<p><a href="https://panelterg.enp.pl/supplier_settings/payment/edit/supplier/54">https://panelterg.enp.pl/supplier_settings/payment/edit/supplier/54</a></p>
<p><strong>Status flow</strong></p>
<p><a href="https://admin_enp0026.adafir.loc/statusflow/configuration/list">https://admin_enp0026.adafir.loc/statusflow/configuration/list</a></p>
<p><strong>Powiadomienia</strong></p>
<p><a href="https://panelterg.enp.pl/notification/list">https://panelterg.enp.pl/notification/list</a></p>
<p><strong>Status bancha pre na tergu</strong></p>
<p><a href="https://mediaexpert.enp.pl/enp/project" target="_blank" rel="noopener">https://mediaexpert.enp.pl/enp/project</a></p>
<p><strong>Estymacje</strong></p>
<p><a href="https://enetproduction.atlassian.net/browse/SALES-1899?jql=project%20%3D%20SALES%20AND%20component%20%3D%20%22Do%20estymacji%20zesp%C3%B3%C5%82%204%22">link</a></p>','publishedAt' => '2020-06-25 15:18:44','updatedAt' => '2020-11-13 10:48:09','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '319','user_id' => '1','title' => 'enet komendy comandy commandy','slug' => 'enet-komendy-comandy-commandy','content' => '<p><a href="http://enp0031a02.adafir.loc/ro/femei/pantofi/incaltaminte-sport/incaltaminte-sport-adidas-advantage-base-ee7511-negru">http://enp0031a02.adafir.loc/ro/femei/pantofi/incaltaminte-sport/incaltaminte-sport-adidas-advantage-base-ee7511-negru</a></p>
<pre class="language-markup"><code>dphp app/admin_ENP0026/console doctrine:migrations:execute --up \'Application\\MigrationsV3\\Client\\ENP0026\\Version20210125145611\'</code></pre>
<pre class="language-markup"><code>dphp bin/dep generator:migration-doctrine</code></pre>
<p>odpalania cycle</p>
<pre class="language-markup"><code>php app/admin_alsen/console  enp:cycle:generate</code></pre>
<p>zmiana bazy</p>
<pre class="language-markup"><code>bin/change-db-on-loc-env.sh ENP0001 d</code></pre>
<p>git</p>
<pre class="language-markup"><code> dphp bin/dep g:b</code></pre>
<pre class="language-markup"><code>npm run compile --zero</code></pre>
<p>uruchamianie test&oacute;w</p>
<pre class="language-markup"><code>dphp bin/dep adafir:tests:phpunit
dphp bin/dep adafir:tests:phpspec
</code></pre>
<p>command do sprawdzenia rzeczy z proda (baz itd )</p>
<pre class="language-markup"><code>dphp bin/dep z:l</code></pre>
<p>komenda do sprawdzenia czy dana oferta nalezy do listy produktowej:</p>
<pre class="language-markup"><code>php app/admin_samsung/console enp:product-list:offer:checker --product-list-id=3046 --offer-id=270834983 --env=prod</code></pre>
<p>komenda do przeladowania koniguracji</p>
<pre class="language-markup"><code>dbash bin/recreate-loc -n</code></pre>
<p>komenda do ponawiania zam&oacute;wień</p>
<pre class="language-markup"><code>php app/admin_ENP0031/console enp:order:update --orderId=64554080,64542176 --env=prod --useHighPriority
</code></pre>
<p>update zamowienia w elasticu</p>
<pre class="language-markup"><code>php app/admin_ENP0026/console enp:es:order:reindex --id=12863039 --env=prod</code></pre>
<p>flaty dla uslugi</p>
<pre class="language-markup"><code>php app/admin_ENP0026/console enp:service:flat 1074 --queue=0 --env=prod </code></pre>
<p>offer id czy jest na liscie produktowej</p>
<pre class="language-markup"><code>php app/admin_ENP0026/console enp:product-list:offer:checker --product-list-id=10 --offer-id=2568304 --env=prod</code></pre>
<div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">
<div>
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">
<div>to taki powazny buduje cały flat</div>
</div>
</div>
<pre class="language-markup"><code>enp:transport:flat:generate​</code></pre>
</div>
</div>
</div>
</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">a ten buduje cache transport&oacute;w używane w koszu</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">
<div>
<pre class="language-markup"><code>enp_transport:build_transport_cache_command</code></pre>
</div>
</div>
</div>
</div>
</div>
<h3><strong>Procesowanie zam&oacute;wienia na tergu</strong></h3>
<p>komenda i wymaga to uruchomienia consumera na rabicie / <strong>Tabela:</strong> supplier_order</p>
<pre class="language-markup"><code>dphp app/admin_ENP0026/console --env=loc enp:rabbitmq:consumer supplier_api_log -m=1 -w</code></pre>
<pre class="language-markup"><code>dphp app/admin_ENP0026/console enp:order:supplier:order:process --supplierId=54 --processSingleOrderId=3106000</code></pre>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">zbuduje lokalnie brakujacy cache</div>
<pre class="language-markup"><code>dphp app/admin_ENP0035/console enp:core:warmup-app-cache --warmer=product_list_flat_dto</code></pre>
<p>zmiana bazy</p>
<pre class="language-markup"><code>./bin/change-db-on-loc-env.sh ENP0026 p</code></pre>
<p>/elastic !!!</p>
<pre class="language-markup"><code>enp:es:order:verify-last-order-statuses --limit=1600 --offset=500</code></pre>
<p>&nbsp;</p>','publishedAt' => '2020-06-29 10:03:05','updatedAt' => '2021-04-01 08:35:58','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '320','user_id' => '1','title' => 'sprawdzanie czy istnieje parameter przy wstrzykiwaniu','slug' => 'sprawdzanie-czy-istnieje-parameter-przy-wstrzykiwaniu','content' => '<pre class="language-javascript"><code>services:
    Enp\\Bundle\\MenuBundle\\Chain\\DestinationManipulatorChain\\Manipulator\\EsOfferListManipulator:
        class: Enp\\Bundle\\MenuBundle\\Chain\\DestinationManipulatorChain\\Manipulator\\EsOfferListManipulator
        arguments:
            - "@=container.hasParameter(\'enp_es_product_offer.enabled\') ? parameter(\'enp_es_product_offer.enabled\') : false"
        tags:
            - { name: menu_item_destination_manipulator, service_key: \'es_offer_list_destination\'}
</code></pre>','publishedAt' => '2020-07-01 16:21:16','updatedAt' => '2020-10-19 13:14:15','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '321','user_id' => '1','title' => 'generowanie raportu lokalizcja','slug' => 'generowanie-raportu-lokalizcja','content' => '<pre class="language-markup"><code>\\Enp\\Bundle\\OrderBundle\\Controller\\ExportController::orderFullExportAction</code></pre>
<p>lista kolumn do exportu:</p>
<pre class="language-markup"><code>Enp\\Bundle\\FilterBundle\\DataObject\\FilterList</code></pre>
<p>plugin achival:</p>
<pre class="language-markup"><code>Enp\\Bundle\\OrderBundle\\Query\\Plugin\\ArchivalQueryPlugin
src/Enp/Bundle/CoreBundle/Query/FinderAbstract.php
\\Enp\\Bundle\\OrderBundle\\Query\\OrderFinder</code></pre>
<p>Wylacznie kolejek:</p>
<pre class="language-markup"><code>Enp/Bundle/Admin/DataExportBundle/Controller/ExportController.php</code></pre>
<p>&nbsp;</p>','publishedAt' => '2020-07-02 08:11:48','updatedAt' => '2021-04-27 11:06:59','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '322','user_id' => '1','title' => 'lista obrazów','slug' => 'lista-obrazow','content' => '<pre class="language-markup"><code>sudo docker image ls</code></pre>','publishedAt' => '2020-07-05 10:36:12','updatedAt' => '2020-07-05 10:36:12','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '324','user_id' => '1','title' => 'mysql group by z max wartością kolumny','slug' => 'mysql-group-by-z-max-wartoscia-kolumny','content' => '<pre class="language-php"><code>  public function findLatestCreationDatesByOrderAndStatusesIds(
        OrderInterface $order,
        array $statusesIds
    ): array {
        $queryBuilder = $this-&gt;getQueryBuilder()
            -&gt;select(
                \\sprintf(\'IDENTITY(%s.status) as status\', $this-&gt;getAlias()),
                \\sprintf(\'max(%s.createdAt) as createdAt\', $this-&gt;getAlias())
            )
            -&gt;andWhere(\\sprintf(\'%s.order = :order\', $this-&gt;getAlias()))
            -&gt;setParameter(
                \'order\',
                $order-&gt;getId(),
                PDO::PARAM_INT
            )
            -&gt;andWhere(sprintf(\'%s.status IN (:statuses)\', $this-&gt;getAlias()))
            -&gt;setParameter(
                \'statuses\',
                $statusesIds,
                Connection::PARAM_INT_ARRAY
            );

        $queryBuilder-&gt;groupBy(sprintf(\'%s.status\', $this-&gt;getAlias()));

        return $queryBuilder-&gt;getQuery()-&gt;getResult();
    }</code></pre>','publishedAt' => '2020-07-13 20:05:01','updatedAt' => '2020-07-13 20:05:01','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '325','user_id' => '1','title' => 'usuniecie kontenera po id','slug' => 'usuniecie-kontenera-po-id','content' => '<pre class="language-markup"><code>docker rmi -f xxxxx</code></pre>
<p>stop all containers<code></code></p>
<pre class="language-markup"><code>docker stop $(docker ps -a -q)</code></pre>
<pre><code></code></pre>
<p>remove all containers<code></code></p>
<pre class="language-markup"><code>docker rm $(docker ps -a -q)</code></pre>
<pre><code></code></pre>
<p>remove all images</p>
<pre class="language-markup"><code>docker rmi -f $(docker images -a -q)</code></pre>','publishedAt' => '2020-07-16 17:34:18','updatedAt' => '2020-07-18 13:04:32','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '326','user_id' => '1','title' => 'firma dane','slug' => 'firma-dane','content' => '<p><a href="https://zarobki.pracuj.pl/zaawansowany-kalkulator-wynagrodzen/7af574d59c553c68ad32384b4877c41828e04548cfe263f48be2a4135c6f31be">kalkulator</a></p>
<div class="full-company-data-box__nip"><strong>NIP:</strong>&nbsp;5771940560</div>
<div class="full-company-data-box__regon"><strong>REGON:</strong>&nbsp;386197868</div>
<div class="full-company-data-box__regon">&nbsp;</div>
<div class="full-company-data-box__regon"><a href="https://www.podatki.gov.pl/generator-mikrorachunku-podatkowego/">link konto</a></div>
<div class="full-company-data-box__regon">&nbsp;</div>
<div class="full-company-data-box__regon">konto zus skladki:</div>
<h5 class="mb-3 maskNrs"><strong>68 60000002026 001 5771940560</strong></h5>
<p>tu można wyszukać</p>
<p><a href="https://eskladka.pl/Home">https://eskladka.pl/Home</a></p>
<p><a href="https://zarobki.pracuj.pl/zaawansowany-kalkulator-wynagrodzen/5498be26ffa70841f4751d2f1e80c513f87a395bb07244ea42a682ae6bface91">Link kalkulator</a></p>
<p><strong>2020</strong></p>
<table style="border-collapse: collapse; width: 100%; height: 252px;" border="1">
<tbody>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">zus</td>
<td style="width: 20%; height: 18px;">dochodowy</td>
<td style="width: 20%; height: 18px;">uwagi</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">styczen</td>
<td style="width: 20%; height: 18px;">-</td>
<td style="width: 20%; height: 18px;">-</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">luty</td>
<td style="width: 20%; height: 18px;">-</td>
<td style="width: 20%; height: 18px;">-</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">marzec</td>
<td style="width: 20%; height: 18px;">-</td>
<td style="width: 20%; height: 18px;">-</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">kwiecen</td>
<td style="width: 20%; height: 18px;">-</td>
<td style="width: 20%; height: 18px;">-</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">maj</td>
<td style="width: 20%; height: 18px;">-</td>
<td style="width: 20%; height: 18px;">-</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">czer</td>
<td style="width: 20%; height: 18px;">15 zaloznie</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">lip</td>
<td style="width: 20%; height: 18px;">362,34</td>
<td style="width: 20%; height: 18px;">373</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">sierp</td>
<td style="width: 20%; height: 18px;">362,34</td>
<td style="width: 20%; height: 18px;">919</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">wrzesien</td>
<td style="width: 20%; height: 18px;">362,34</td>
<td style="width: 20%; height: 18px;">919</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">pazdz</td>
<td style="width: 20%; height: 18px;">362,34</td>
<td style="width: 20%; height: 18px;">919</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">list</td>
<td style="width: 20%; height: 18px;">362,34</td>
<td style="width: 20%; height: 18px;">919</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">grudzien</td>
<td style="width: 20%; height: 18px;">362,34</td>
<td style="width: 20%; height: 18px;">919</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
</tr>
</tbody>
</table>
<p>Dopłaciłem <strong>619</strong> zł to wyliczyłą Iwona</p>
<p>Dopłaciłęm <strong>59</strong> tyle według babki powienienem jeszcze dopłacić.</p>
<p>czyli łacznie dopłaciłęm za 2020: <strong>678</strong></p>
<p><strong>2021</strong></p>
<table style="border-collapse: collapse; width: 100%; height: 252px;" border="1">
<tbody>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">zus</td>
<td style="width: 20%; height: 18px;">dochodowy</td>
<td style="width: 32.4101%; height: 18px;">uwagi</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">styczen</td>
<td style="width: 20%; height: 18px;">362,34 za grudzien</td>
<td style="width: 20%; height: 18px;">877 za grudzień</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">luty</td>
<td style="width: 20%; height: 18px;">627,01 za styczen</td>
<td style="width: 20%; height: 18px;">905</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">marzec</td>
<td style="width: 20%; height: 18px;">627,01 za luty</td>
<td style="width: 20%; height: 18px;">909</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">kwiecen</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 32.4101%; height: 18px;">zn&oacute;w 907 !!! bo te 2 zł roznicy</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">maj</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">czer</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">lip</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">sierp</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">wrzesien</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">pazdz</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">list</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">grudzien</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 20%; height: 18px;">&nbsp;</td>
<td style="width: 32.4101%; height: 18px;">&nbsp;</td>
<td style="width: 7.58993%; height: 18px;">&nbsp;</td>
</tr>
</tbody>
</table>','publishedAt' => '2020-07-20 21:49:52','updatedAt' => '2021-03-13 18:12:16','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '327','user_id' => '1','title' => 'konfiguracja po zmiennych konstruktora w symfony','slug' => 'konfiguracja-po-zmiennych-konstruktora-w-symfony','content' => '<pre class="language-markup"><code> Enp\\Bundle\\OrderBundle\\Service\\Calculator\\OrderRealizationTimeCalculator:
        class: Enp\\Bundle\\OrderBundle\\Service\\Calculator\\OrderRealizationTimeCalculator
        autowire: true
        arguments:
            $configuration: \'%enp_order.configuration_of_calculating_time_of_order_realize%\'
</code></pre>','publishedAt' => '2020-07-21 21:27:13','updatedAt' => '2020-07-21 21:27:13','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '328','user_id' => '1','title' => 'czyszczenei cache adafira','slug' => 'czyszczenei-cache-adafira','content' => '<pre class="language-markup"><code>function rmcache() {
    dbash bin/local-cache-and-logs-clear
    dredis flushall
}</code></pre>','publishedAt' => '2020-07-23 11:08:29','updatedAt' => '2020-11-10 14:00:15','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '329','user_id' => '1','title' => 'php spec komenda symfony','slug' => 'php-spec-komenda-symfony','content' => '<pre class="language-markup"><code>dphp bin/phpspec desc  Enp/Bundle/Front/CartFlowBundle/Chain/GiftCardServiceDecoratorChain</code></pre>','publishedAt' => '2020-07-23 14:09:08','updatedAt' => '2020-07-23 14:09:08','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '330','user_id' => '1','title' => 'form do generowania koszyka','slug' => 'form-do-generowania-koszyka','content' => '<pre class="language-markup"><code>src/Enp/Bundle/Front/CartFlowBundle/Form/Type/Address/Account.php</code></pre>','publishedAt' => '2020-07-27 08:18:25','updatedAt' => '2020-07-27 08:18:25','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '331','user_id' => '1','title' => 'encja symfony','slug' => 'encja-symfony','content' => '<pre class="language-markup"><code>DM\\KlienciBundle\\Entity\\Client:
    type: entity
    repositoryClass: DM\\KlienciBundle\\Repository\\ClientRepository
    table: rb_clients

    id:
        id:
            type: integer
            generator: { strategy: AUTO }

    indexes:
        telefon_komorkowy1_index:
            columns: [ telefon_komorkowy1 ]
        telefon_komorkowy2_index:
            columns: [ telefon_komorkowy2 ]
        telefon_stacjonarny_index:
            columns: [ telefon_stacjonarny ]
        pesel_index:
            columns: [ pesel ]
        data_importu_index:
            columns: [ data_importu ]
        blocked_index:
            columns: [ blocked ]
        bik_index:
            columns: [ bik ]
        komornik_index:
            columns: [ komornik ]
        dochod_index:
            columns: [ dochod ]
        kwota_wnioskowana_index:
            columns: [ kwota_wnioskowana ]
        email_index:
            columns: [ email ]
        komornik_bik_kod_pocztowy_index:
            columns: [ komornik, bik, kod_pocztowy ]
        ubezpieczenie_samochod_koniec_polisy_index:
            columns: [ ubezpieczenie_samochod_koniec_polisy ]
            
    fields:
        #dane techniczne
        data_importu:
            type: datetime
            nullable: false
        blocked:
            type: boolean
            nullable: true
        datetime_create:
            type: datetime
            nullable: true
        datetime_modify:
            type: datetime
            nullable: true
        old_ids:
            type: string
            length: 255
            nullable: true
        send_notification_get_data_date:
            type: datetime
            nullable: true
        contact_agreement:
            type: boolean
            options:
                default: true
        contact_agreement_change_datetime:
            type: datetime
            nullable: true
        agreement_expire_at:
            type: date
            nullable: true

        #dane podstawowe
        imie:
            type: string
            length: 100
            nullable: false
        imie_drugie:
            type: string
            length: 50
            nullable: true
        nazwisko:
            type: string
            length: 100
            nullable: false
        telefon_komorkowy1:
            type: string
            length: 20
            nullable: true
        telefon_komorkowy1_siec:
            type: string
            length: 20
            nullable: true
        telefon_komorkowy1_umowa:
            type: string
            length: 20
            nullable: true
        telefon_komorkowy1_koniec_umowy:
            type: date
            nullable: true
        telefon_komorkowy2:
            type: string
            length: 20
            nullable: true
        telefon_stacjonarny:
            type: string
            length: 20
            nullable: true
        fax:
            type: string
            length: 20
            nullable: true
        pesel:
            type: string
            length: 15
            nullable: true
        email:
            type: string
            length: 150
            nullable: true
        plec:
            type: string
            length: 10
            nullable: true
        nazwisko_panienskie_matki:
            type: string
            length: 100
            nullable: true
        imie_matki:
            type: string
            length: 50
            nullable: true
        imie_ojca:
            type: string
            length: 50
            nullable: true
        urodzenie_data:
            type: string
            length: 20
            nullable: true
        urodzenie_data_auto:
            type: date
            nullable: true
        urodzenie_data_year:
            type: smallint
            nullable: true
        urodzenie_data_month:
            type: smallint
            nullable: true
        urodzenie_data_day:
            type: smallint
            nullable: true
        urodzenie_miejsce:
            type: string
            length: 100
            nullable: true
        urodzenie_kraj:
            type: string
            length: 50
            nullable: true
        stan_cywilny:
            type: string
            length: 30
            nullable: true
        komornik:
            type: string
            length: 20
            nullable: true
        bik:
            type: string
            length: 20
            nullable: true
        oddluzanie:
            type: string
            length: 20
            nullable: true

        # dane dodatkowe
        wojewodztwo:
            type: string
            length: 30
            nullable: true
        miejscowosc:
            type: string
            length: 100
            nullable: true
        poczta:
            type: string
            length: 100
            nullable: true
        kod_pocztowy:
            type: string
            length: 10
            nullable: true
        ulica:
            type: string
            length: 150
            nullable: true
        nr_domu_mieszkania:
            type: string
            length: 20
            nullable: true
        kraj:
            type: string
            length: 40
            nullable: true
        obywatelstwo:
            type: string
            length: 40
            nullable: true
        obywatel_rezydent_podatkowy_usa:
            type: string
            length: 5
            nullable: true
        rozlicza_podatki_obywatel_polski:
            type: string
            length: 5
            nullable: true
        usa_nip:
            type: string
            length: 15
            nullable: true  

        # dane szczegolowe 
        dowod_osobisty_seria_numer:
            type: string
            lenght: 20
            nullable: true
        dowod_osobisty_data_waznosci:
            type: string
            length: 20
            nullable: true
        dowod_osobisty_data_wydania:
            type: string
            length: 20
            nullable: true
        dowod_osobisty_bezterminowy:
            type: string
            length: 5
            nullable: true
        dowod_osobisty_wystawca:
            type: string
            length: 150
            nullable: true
        dokument_tozsamosci_rodzaj:
            type: string
            length: 20
            nullable: true
        paszport_seria_numer:
            type: string
            lenght: 20
            nullable: true
        paszport_data_waznosci:
            type: string
            length: 20
            nullable: true  

        # zatrudnienie
        dochod:
            type: integer
            nullable: true
        dochod_zrodlo:
            type: string
            length: 150
            nullable: true
        dochod_od_kiedy:
            type: string
            length: 20
            nullable: true
        kwota_wnioskowana:
            type: integer
            nullable: true      
        kwota_limitu:
            type: integer
            nullable: true
        rodzaj_ksiegowosci:
            type: string
            length: 100
            nullable: true       

        leadID:
            type: string
            length: 20
            nullable: true
        owner:
            type: string
            length: 100
            nullable: true  
        zgoda_kontakt_telefoniczny:
            type: string
            length: 10
            nullable: true

        #firma
        czy_klient_biznesowy:
            type: string
            length: 5
            nullable: true
        firma_nazwa:
            type: string
            length: 150
            nullable: true
        firma_nazwa_skrocona:
            type: string
            length: 150
            nullable: true
        firma_telefon:
            type: string
            length: 20
            nullable: true
        firma_nip:
            type: string
            length: 15
            nullable: true
        firma_regon:
            type: string
            length: 15
            nullable: true
        firma_pkd:
            type: string
            length: 250
            nullable: true
        firma_krs:
            type: string
            length: 20
            nullable: true
        firma_data_rozpoczecia:
            type: date
            nullable: true
        firma_forma_prawna:
            type: string
            length: 50
            nullable: true
        firma_rodzaj_ksiegowosci:
            type: string
            length: 50
            nullable: true
        firma_email:
            type: string
            length: 100
            nullable: true
        firma_sektor:
            type: string
            length: 100
            nullable: true
        firma_liczba_pracownikow:
            type: string
            length: 50
            nullable: true
        firma_branza:
            type: string
            length: 50
            nullable: true
        firma_forma_prawna:
            type: string
            length: 30
            nullable: true
        firma_sprzedaz_koniec_roku_biezacego:
            type: string
            length: 40
            nullable: true
        firma_limit_w_rachunku_mbank:
            type: string
            length: 20
            nullable: true  

        korespondencyjny_inny:
            type: string
            length: 5
            nullable: true
        korespondencyjny_wojewodztwo:
            type: string
            length: 30
            nullable: true
        korespondencyjny_miejscowosc:
            type: string
            length: 100
            nullable: true
        korespondencyjny_poczta:
            type: string
            length: 100
            nullable: true
        korespondencyjny_kod_pocztowy:
            type: string
            length: 10
            nullable: true
        korespondencyjny_ulica:
            type: string
            length: 150
            nullable: true
        korespondencyjny_nr_domu_mieszkania:
            type: string
            length: 20
            nullable: true
        korespondencyjny_kraj:
            type: string
            length: 40
            nullable: true  

        adres_zamieszkania_wojewodztwo:
            type: string
            length: 20
            nullable: true
        adres_zamieszkania_miejscowosc:
            type: string
            length: 100
            nullable: true
        adres_zamieszkania_poczta:
            type: string
            length: 100
            nullable: true
        adres_zamieszkania_kod_pocztowy:
            type: string
            length: 10
            nullable: true
        adres_zamieszkania_ulica:
            type: string
            length: 150
            nullable: true
        adres_zamieszkania_nr_domu_mieszkania:
            type: string
            length: 20
            nullable: true
        adres_zamieszkania_inny:
            type: string
            length: 10
            nullable: true

        adres_doreczenia_wojewodztwo:
            type: string
            length: 20
            nullable: true
        adres_doreczenia_miejscowosc:
            type: string
            length: 100
            nullable: true
        adres_doreczenia_poczta:
            type: string
            length: 100
            nullable: true
        adres_doreczenia_kod_pocztowy:
            type: string
            length: 10
            nullable: true
        adres_doreczenia_ulica:
            type: string
            length: 150
            nullable: true
        adres_doreczenia_nr_domu_mieszkania:
            type: string
            length: 20
            nullable: true
        doreczenia_inny:
            type: string
            length: 10
            nullable: true  

        uznania_roczne_szacunkowa_wielkosc:
            type: integer
            nullable: true
        srodki_inne_pochodzenie:
            type: string
            length: 150
            nullable: true
        zatrudnienie_forma:
            type: string
            length: 50
            nullable: true
        zatrudnienie_dzialalnosc:
            type: string
            length: 100
            nullable: true
        praca_forma_wyplaty:
            type: string
            length: 50
            nullable: true
        praca_stanowisko:
            type: string
            length: 100
            nullable: true
        praca_pracodawca:
            type: string
            length: 150
            nullable: true
        pracodawca_nip:
            type: string
            length: 15
            nullable: true
        status_bezrobotnego:
            type: string
            length: 50
            nullable: true
        zatrudnienie_inne:
            type: string
            length: 50
            nullable: true   
        kramer:
            type: boolean
            nullable: false
            options:
                default: 0

        ubezpieczenie_samochod_koniec_polisy:
            type: date
            nullable: true

        distributor:
            type: string
            length: 255
            nullable: true
        utm_source:
            type: string
            length: 255
            nullable: true

        lokata_data_konca:
            type: date
            nullable: true
    

    oneToOne:
        ClientPart2:
            targetEntity: DM\\KlienciBundle\\Entity\\ClientPart2
            mappedBy: Client
            cascade: [persist, merge, remove]
        ClientPart3:
            targetEntity: DM\\KlienciBundle\\Entity\\ClientPart3
            mappedBy: Client
            cascade: [persist, merge, remove]
        ClientPart4:
            targetEntity: DM\\KlienciBundle\\Entity\\ClientPart4
            mappedBy: Client
            cascade: [persist, merge, remove]
        ClientPart5:
            targetEntity: DM\\KlienciBundle\\Entity\\ClientPart5
            mappedBy: Client
            cascade: [persist, merge, remove]
        ClientPart6:
            targetEntity: DM\\KlienciBundle\\Entity\\ClientPart6
            mappedBy: Client
            cascade: [persist, merge, remove]

    oneToMany:
        klienci_kategorie:
            targetEntity: DM\\KlienciBundle\\Entity\\KlienciKategorie
            mappedBy: client
            cascade: [persist, merge, remove]
        history:
            targetEntity: DM\\KlienciBundle\\Entity\\History
            mappedBy: client
            cascade: [persist, merge, remove]
        sms_storages:
            targetEntity: DM\\SmsBundle\\Entity\\SmsStorage
            mappedBy: client
            cascade: [ "remove" ]
        agreements:
            targetEntity: DM\\KlienciBundle\\Entity\\ClientAgreement
            mappedBy: client
            cascade: [persist, merge, remove]
        continue_service_logs:
            targetEntity: DM\\ContinueServiceBundle\\Entity\\ContinueServiceLog
            mappedBy: client
            cascade: [remove]
        multistepform_transactions:
            targetEntity: DM\\MultiStepFormBundle\\Entity\\Transaction
            mappedBy: client
            cascade: [remove]    
        client_instytution_stats:
            targetEntity: DM\\KlienciBundle\\Entity\\ClientInstytutionStat
            mappedBy: client
            cascade: [remove]
</code></pre>','publishedAt' => '2020-07-27 10:12:45','updatedAt' => '2020-07-27 10:12:45','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '334','user_id' => '1','title' => 'Przyklad migracji z last insert id','slug' => 'przyklad-migracji-z-last-insert-id','content' => '<pre class="language-php"><code>        $this-&gt;addSql(\'
            INSERT INTO `admin_module_action` (`security_attribute`, `name`, `descr`, `admin_module_id`, `securitySchema`, `cycle_id`)
            VALUES (\\\'VANSELING_ORDER\\\', \\\'Zam&oacute;wienie vanseling\\\', \\\'Zam&oacute;wienie vanseling\\\', 98, 1, NULL);
        \');
        $this-&gt;addSql(\'
            INSERT INTO `admin_role` (`name`, `descr`, `enabled`, `deleted_at`, `system_code`)
            VALUES (\\\'Operator vanseling\\\', \\\'Sprzedaż bezpośrednia od kierowcy\\\', 1, NULL, NULL);
            SET @adminRoleId = LAST_INSERT_ID();
        \');
        $this-&gt;addSql(\'
            INSERT INTO `admin_role_has_admin_module_action` (`admin_role_id`, `admin_module_action_attribute`)
            VALUES (@adminRoleId, \\\'VANSELING_ORDER\\\');
        \');
        $this-&gt;addSql(\'
            INSERT INTO `admin_role_has_admin_module_action` (`admin_role_id`, `admin_module_action_attribute`)
            VALUES (@adminRoleId, \\\'VANSELING_CHANGE_ITEM_PRICE\\\');
        \');</code></pre>','publishedAt' => '2020-08-18 06:53:16','updatedAt' => '2020-11-10 13:59:41','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '335','user_id' => '1','title' => 'migracja z szukaniem tabel o podobnych nazwach','slug' => 'migracja-z-szukaniem-tabel-o-podobnych-nazwach','content' => '<pre class="language-php"><code>&lt;?php

declare(strict_types=1);

namespace Application\\Migrations;

use Doctrine\\DBAL\\Schema\\Schema;
use Enp\\Bundle\\CoreBundle\\Migration\\AbstractEnpMigration;
use PDO;

final class Version20200819084526 extends AbstractEnpMigration
{
    public function up(Schema $schema): void
    {
        foreach ($this-&gt;getOrderExtensionTables() as $tableName) {
            if (empty($tableName)) {
                continue;
            }

            $this-&gt;addSql(sprintf(\'ALTER TABLE %s ADD payment_document_number VARCHAR(255) DEFAULT NULL\', $tableName));
        }
    }

    public function down(Schema $schema): void
    {
        foreach ($this-&gt;getOrderExtensionTables() as $tableName) {
            if (empty($tableName)) {
                continue;
            }
            $this-&gt;addSql(sprintf(\'ALTER TABLE %s DROP payment_document_number\', $tableName));
        }
    }

    private function getOrderExtensionTables(): array
    {
        $tables = $this-&gt;connection-&gt;executeQuery(
            "SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME LIKE \'%\\_order_extension\';"
        )-&gt;fetchAll(PDO::FETCH_COLUMN);
        $tables[] = \'order_extension\';

        return $tables;
    }
}
</code></pre>','publishedAt' => '2020-08-19 16:12:13','updatedAt' => '2020-08-19 16:12:13','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '337','user_id' => '1','title' => 'moje mrki','slug' => 'moje-mrki','content' => '<ol>
<li>Aktualizacja bank&oacute;w płatnosci alsen <a href="https://enetproduction.atlassian.net/browse/SALES-418">https://enetproduction.atlassian.net/browse/SALES-418</a></li>
<li>Alsen liczenie czasu reakcji <a href="https://enetproduction.atlassian.net/browse/SALES-322">https://enetproduction.atlassian.net/browse/SALES-322</a></li>
<li>Modyfikacja formualrza na rumuni <a href="https://enetproduction.atlassian.net/browse/SALES-827">https://enetproduction.atlassian.net/browse/SALES-827</a></li>
<li>Alert <a href="http://gitlab.enp.me/mshp/adafir/-/merge_requests/65649/diffs">http://gitlab.enp.me/mshp/adafir/-/merge_requests/65649/diffs</a></li>
<li>Zadanie z zam&oacute;wieniem typu vanseling <a href="http://gitlab.enp.me/mshp/adafir/-/merge_requests/66093/">vanseling</a></li>
<li>Szybkie zwroty <a href="http://gitlab.enp.me/mshp/adafir/-/merge_requests/66940">szybkie zwroty</a></li>
<li>UDZ <a href="http://gitlab.enp.me/mshp/adafir/-/merge_requests/67380/" target="_blank" rel="noopener">udz</a></li>
<li>Zasilkownia <a href="http://gitlab.enp.me/mshp/adafir/-/merge_requests/68802//diffs">zasilkownia</a> <a href="http://gitlab.enp.me/enp/api-tests/-/merge_requests/67">testy api</a></li>
<li>urawienia mso <a href="http://gitlab.enp.me/mshp/adafir/-/merge_requests/69535">link</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/70070/" target="_blank" rel="noopener">fix notificaction terg</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/69405/" target="_blank" rel="noopener">brico</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/70901">elastic naprawa</a></li>
<li><a href="https://enetproduction.atlassian.net/browse/SALES-1592">samsung zmiana zapytania</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/70974">ccc zmiana zwalidacji </a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/70377/">ccc migracja judget</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/70935">leasing terg</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/71983/">cumulus zmiana cache redisa</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/72731/">dodawanie produktow naprawa</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/72875/">agata bazinga optymalizacja</a></li>
<li><a href="https://enetproduction.atlassian.net/browse/SALES-3599">spike vanseling</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/73600">santander naprawa</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/73551">offerDTO naprawa </a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/73738">nowy vanseling</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/74717">filtry raporty</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/73289">ccc - pfy !!!</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/74539">terg naprawa transort&oacute;w</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/75913">naprawa zasilkovni</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/75104">nowe przejście na sf</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/76731/">folia</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/77151">konflikty usług</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/78108/diffs">raporty_tomek</a></li>
</ol>','publishedAt' => '2020-08-27 12:32:30','updatedAt' => '2021-04-30 07:58:45','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '338','user_id' => '1','title' => 'listy produktów','slug' => 'listy-produktow','content' => '<pre class="language-markup"><code>\\Enp\\Bundle\\TransportBundle\\Service\\TransportService::getTransportList
\\Enp\\Bundle\\TransportBundle\\Service\\TransportService::getTransportMethodList</code></pre>','publishedAt' => '2020-09-02 08:44:55','updatedAt' => '2020-11-10 14:02:50','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '339','user_id' => '1','title' => 'komendy visual studio code','slug' => 'komendy-visual-studio-code','content' => '<p>1) Uruchamianie formatowania kodu:</p>
<pre class="language-markup"><code>Visual Studio Code on Windows - Shift + Alt + F</code></pre>
<pre class="language-markup"><code>Visual Studio Code on MacOS - Shift + Option + F</code></pre>
<pre class="language-markup"><code>Visual Studio Code on Ubuntu - Ctrl + Shift + I</code></pre>
<div id="gtx-trans" style="position: absolute; left: 0px; top: 4.76042px;">
<div class="gtx-trans-icon">&nbsp;</div>
</div>','publishedAt' => '2020-09-05 10:16:17','updatedAt' => '2020-09-05 10:16:17','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '340','user_id' => '1','title' => 'dodanie dockera do wyjątków','slug' => 'dodanie-dockera-do-wyjatkow','content' => '<pre class="language-markup"><code>sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
docker image ls</code></pre>','publishedAt' => '2020-09-05 11:10:21','updatedAt' => '2020-09-05 11:10:21','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '341','user_id' => '1','title' => 'testtt','slug' => 'testtt','content' => '<p>https://panel.prod.ccc.enp.pl/media/cache/resolve/filemanager_original/images/A5_poziom_210222_v2_2.jpg</p>
<pre class="language-markup"><code>{% extends "EnpAdminWebBundle::Default/clearpage_content.html.twig" %}

{% block stylesheets %}
    {% stylesheets
    \'@EnpCoreBundle/Resources/public/js/libs/querybuilder/query-builder.css\'
    \'@EnpAdminDataTableBundle/Resources/public/css/temp.css\'
    \'@EnpAdminWebBundle/Resources/public/js/libs/bootstrap/bootstrap-editable.css\' %}
    &lt;link rel="stylesheet" type="text/css" media="screen" href="{{ asset_url }}"&gt;
    {% endstylesheets %}
{% endblock %}

{% import "EnpAdminWebBundle::elements/formElements/submit_button.html.twig" as setSubmitButton %}

{% set yesButton = "enp.order.edit.yes"|trans %}
{% set cancelButton = "enp.order.edit.cancel"|trans %}

{% block page_content %}

    {% include "EnpOrderBundle::Modals/modals.html.twig" %}

    {#
        @MSG TO GFX FROM AŚ:
        Nadpisane ponieważ jest tam klasa "newinput" - trzeba jakoś to połonaczyć żeby stary "input" dobrze wyglądał
    #}

    {% import "EnpOrderBundle::Order/input.html.twig" as formInput %}
    {% trans_default_domain \'EnpOrderBundle\' %}

    &lt;div class="order-edit-block clearfix2"&gt;
        &lt;div class="col-lg-6"&gt;
            {{ alert_show_order(entity) }}
        &lt;/div&gt;
        &lt;div class="col-lg-6"&gt;
            {{ alert_apply_manual(entity) }}
        &lt;/div&gt;
    &lt;/div&gt;

    {% include "EnpOrderBundle::Order/elements/nav.html.twig" %}

    &lt;div class="order-edit-block"&gt;

        &lt;div class="edit-container clearfix2 g-mb10"&gt;
            {% include "EnpOrderBundle::Order/action/statusButtons.html.twig" %}

            {% if is_granted(\'ORDER_EDIT_RETURN\') %}
                {% include "EnpOrderBundle::Order/elements/return.html.twig" %}
            {% endif %}
            {% if is_granted(\'ORDER_EDIT_COMPLAINT\') %}
                {% include "EnpOrderBundle::Order/elements/complaint.html.twig" %}
            {% endif %}
            &lt;div class="clearfix2 g-mt20 edit-container-data"&gt;
                {% include "EnpOrderBundle::Order/action/topDetailsLeft.html.twig" %}
                {% include "EnpOrderBundle::Order/action/topDetailsRight.html.twig" %}
                {% include "EnpOrderBundle::Order/action/comments.html.twig" ignore missing %}
                {% if is_granted(\'CC_TICKET_LIST\', entity) %}
                    {% include "EnpOrderBundle::Order/elements/notes.html.twig" %}
                {% endif %}

                {% include "EnpOrderBundle::Order/elements/note-modal.html.twig" %}

                {% set hasPinCodeConfirm =  (entity.getPinPersonalReception|length &gt; 0 and (entity.getPinPersonalReception|first).confirmVersion &gt; 0) %}
                {% if hasPinCodeConfirm or is_granted(\'PIN_PERSONAL_RECEPTION_REGENERATE\') %}
                    {% include "EnpOrderBundle::Order/elements/pin-personal.html.twig" %}
                {% endif %}
                {% include "EnpOrderBundle::Order/elements/additionalInfo.html.twig" %}
                {% if is_granted(\'ORDER_EDIT_CONSENT_LIST\') %}
                    {% include "EnpOrderBundle::Consent/orderConsentContent.html.twig" %}
                {% endif %}
                    {% include "EnpENP0026OrderBundle:Order/IntenseInvoice:intenseInvoiceOrder.html.twig" %}

                {% if (is_granted(\'PROFORMA_INVOICE_ORDER_GENERATE_PDF\')) %}
                    {% include "EnpENP0026OrderBundle:Order/OrderTransaction:proFormaSend.html.twig" with {id: entity.id} only %}
                {% endif %}

                {% if is_granted(\'ORDER_INVOICE_COMMENT_VIEW\') or is_granted(\'ORDER_INVOICE_COMMENT_EDIT\') %}
                    {% include "@EnpOrder/Order/action/invoiceComment.html.twig" ignore missing %}
                {% endif %}

            &lt;/div&gt;

            {% include "EnpOrderBundle::Order/action/confirm_phone_number.html.twig" %}
            {% include "EnpOrderBundle::Order/action/customer.html.twig" %}
            {% if hasOperatorMode %}
                {% include "EnpOrderBundle::Order/action/operator.html.twig" %}
            {% endif %}

            {% if (is_granted(\'ORDER_EDIT_OPERATOR_EXEC_SHOW\', entity)
                    or is_granted(\'ORDER_EDIT_OPERATOR_EXEC_ASSIGN_ME\', entity)
                    or is_granted(\'ORDER_EDIT_OPERATOR_EXEC_ASSIGN_OTHERS\', entity)
                )
                and (operatorExec is defined and operatorExec is not null)
            %}
                {% include "EnpOrderBundle::Order/action/operator_exec.html.twig" %}
            {% endif %}

        &lt;/div&gt;

        &lt;div class="col-lg-12 js-orderDetails"&gt;
            {% include "EnpOrderBundle::Order/action/formItems.html.twig" %}
        &lt;/div&gt;

        &lt;div class="js-validateForm"&gt;
            &lt;div class="col-lg-4 order-form-view"&gt;
                {% if is_granted(\'ORDER_EDIT_TRANSPORT_PAYMENT\') %}
                    {% include "EnpOrderBundle::Order/action/transportPayment.html.twig" %}
                {% endif %}
                {% include "EnpOrderBundle::Order/action/addressAccount.html.twig" %}
            &lt;/div&gt;
            &lt;div class="col-lg-4 order-form-view"&gt;
                {% include "EnpOrderBundle::Order/action/pricesDetails.html.twig" %}
                {% include "EnpOrderBundle::Order/action/inquiries.html.twig" %}
                {% include "EnpOrderBundle::Order/action/addressBilling.html.twig" %}
            &lt;/div&gt;
            &lt;div class="col-lg-4 order-form-view"&gt;
                {% include "EnpOrderBundle::Order/action/uploadFiles.html.twig" %}
            &lt;/div&gt;
            &lt;div class="col-lg-4 order-form-view"&gt;
                {% include "EnpOrderBundle::Order/action/addressTransport.html.twig" %}
            &lt;/div&gt;

            {% include "EnpOrderBundle::Order/action/order_separate_modal.html.twig" %}
        &lt;/div&gt;

    &lt;/div&gt;
    {{ EnpDatatable_init({
        collectDataLink: collectDataLink
    }) }}

    {{ EnpDatatatableStartPicker() }}
    {{ EnpDatatableModalMultiPicker(\'groupOfferPickerInModal\', collectDataLink) }}

    {{ EnpDatatableModalMultiPicker(\'enpCustomerPicker\', customerPickerCollectDataLink) }}
    {{ EnpDatatatableEndPicker() }}

{% endblock page_content %}

{% block javascripts %}
    {% trans_default_domain \'EnpOrderBundle\' %}

    &lt;div id="js-order-customer-bind"
         data-add-button-msg=\'{{ \'enp.payment.transaction_details.relate_new_order\'|trans }}\'
         data-delete-button-msg=\'{{ \'enp.payment.transaction_details.order_delete\'|trans }}\'
         data-add-link=\'{{ path(\'enp_order_bind_customer\') }}\'
         data-order-id=\'{{ entity.id }}\'
         data-mapping-granted=\'{{ (is_granted(\'ORDER_BIND_CUSTOMER\')) }}\'
    &gt;
    &lt;/div&gt;

    &lt;div id="js-order-formItem"
         data-action-update=\'{{ path(\'enp_order_update_items\', { id: entity.id }) }}\'
         data-action-delete=\'{{ path(\'enp_order_delete_items\', { id: entity.id }) }}\'
         data-action-add="{{ path(\'enp_order_add_items\', { id: entity.id }) }}"
         data-action-copy="{{ path(\'enp_order_copy_items\', { id: entity.id }) }}"
         data-action-separate="{{ path(\'enp_order_separate_items\', { id: entity.id }) }}"
         data-action-offer-change="{{ path(\'enp_order_change_offer\', { id: entity.id }) }}"
         data-action-add-service="{{ path(\'enp_order_add_service_item\', { id: entity.id }) }}"
         data-actionCommitSoItems="{{ path(\'enp_order_commit_supplier_items\', { id: entity.id }) }}"
         data-action-separate-qty="{{ path(\'enp_order_separate_item_qty\', { id: entity.id }) }}"
         data-action-split-order-qty="{{ path(\'enp_order_separate_qty\', { id: entity.id }) }}"
         data-action-serial-number="{{ path(\'enp_order_serial_number_items\', { id: entity.id }) }}"
    &gt;
    &lt;/div&gt;
    &lt;div id="js-order-transportPayment"
         data-inposts-id="{{ transportInpostsJson }}"
         data-pos-id="{{ posJson }}"
         data-delivery-point-id="{{ deliveryPointTypeJson }}"
    &gt;
    &lt;/div&gt;
    &lt;div id="js-address-data"
         data-action="{{ path(\'enp_customer_address_realation\') }}"
    &gt;
    &lt;/div&gt;
    &lt;div id="js-order-status"
         data-status-check-url="{{ path(\'enp_order_update_status_transition\', {id: entity.id}) }}"
         data-msg-status-note-required="{{ \'enp.order.edit.status_note_is_required\'|trans }}"
         data-error-status-label="{{ \'enp.order.edit.error_status_change\'|trans }}"
    &gt;
    &lt;/div&gt;
    &lt;div id="js-order-payment"&gt;&lt;/div&gt;
    &lt;div id="js-order-transport"&gt;&lt;/div&gt;
    &lt;div id="js-order-currency"&gt;&lt;/div&gt;
    &lt;div id="js-forward-cosi"
         data-forward-action="{{ path(\'enp_order_forward\', { id: entity.id }) }}"
    &gt;
    &lt;/div&gt;
    &lt;div id="js-regenerate-pin"
         data-regenerate-action="{{ path(\'enp_pin_personal_reception_regenerate\', {id: entity.id}) }}"
    &gt;&lt;/div&gt;
    &lt;div id="js-order-consent-data"
         data-refresh-translate="{{ \'enp_order_consent.list.refresh\'|trans({}, \'EnpOrderConsent\') }}"
    &gt;
    &lt;/div&gt;

    {{ parent() }}
    {% javascripts
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_change_delivery_day.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_formItem.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_transportPayment.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_delete_return_data.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_address.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_address_book.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_invoice.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_blacklist.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_promotion_code.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_operator.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_status.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_payment.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_transport.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_currency.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_nav.js\'
    \'@EnpAdminWebBundle/Resources/public/js/app/global/enp.pickerDialog.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_forward.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_flags.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_regenerate_pin.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_copy_address.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_customerPicker.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_warehouse_reservations.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_order_consent_list.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_total_change.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_payment_deadline.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_resend_invoice.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_invoice_comment.js\'
    \'@EnpOrderBundle/Resources/public/js/action/enp.order_action_payment_document_number.js\'
    \'@EnpOrderBundle/Resources/public/js/enp.order_note.js\'

    \'@EnpAdminWebBundle/Resources/public/js/libs/angular/angular-sanitize.min.js\'
    \'@EnpAdminWebBundle/Resources/public/js/libs/angular/angular-cookies.min.js\'
    \'@EnpAdminWebBundle/Resources/public/js/libs/angular/angular-route.min.js\'

    \'@EnpAdminWebBundle/Resources/public/smart/js/plugin/x-editable/x-editable.min.js\'
    \'@EnpAdminWebBundle/Resources/public/smart/js/plugin/x-editable/moment.min.js\'
    \'@EnpAdminWebBundle/Resources/public/smart/js/plugin/x-editable/jquery.mockjax.min.js\'

    \'@EnpAdminDataTableBundle/Resources/public/js/libs/jQuery.extendext.min.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/libs/querybuilder/js/query-builder.min.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/libs/querybuilder/i18n/pl.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/libs/querybuilder/i18n/en.js\'

    \'@EnpAdminWebBundle/Resources/public/js/libs/dragtable/jquery.dragtable.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/app/functions/functions.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/app/app.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/app/filters/filters.js\'

    \'@EnpAdminDataTableBundle/Resources/public/js/app/services/pagination.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/app/services/configuration.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/app/services/translation.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/app/controllers/Pagination.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/app/controllers/MassActions.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/app/controllers/DataTables.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/app/controllers/Filters.js\'
    \'@EnpAdminDataTableBundle/Resources/public/js/app/modules/DataTables/tables.js\'

    \'@EnpContactCenterBundle/Resources/public/js/ticket/ticket.js\'
    \'@EnpContactCenterBundle/Resources/public/js/ticket/ticketCategoryChange.js\'

    \'@EnpAllegroOrderBundle/Resources/public/js/enp.order_commission.js\'

    \'@EnpPromotionActionBundle/Resources/public/js/app/promotion-code.js\'
    %}
    &lt;script src="{{ asset_url }}"&gt;&lt;/script&gt;
    {% endjavascripts %}

    {% include \'EnpOrderBundle::Order/editComments.html.twig\' ignore missing %}
    {% include \'EnpOrderBundle::Order/scriptExt.html.twig\' ignore missing %}

    &lt;script&gt;
		Enp_ContactCenter.init();

		var accordionIcons = {
			header: "fa fa-plus",
			activeHeader: "fa fa-minus"
		};

		$("#return-data-accordion").accordion({
			autoHeight : false,
			heightStyle : "content",
			animate : 300,
			icons: accordionIcons,
			header : "h4"
		});

		$("#complaint-data-accordion").accordion({
			autoHeight : false,
			heightStyle : "content",
			animate : 300,
			icons: accordionIcons,
			header : "h4"
		});

    &lt;/script&gt;
{% endblock %}</code></pre>','publishedAt' => '2020-09-08 13:34:14','updatedAt' => '2021-02-26 13:13:06','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '342','user_id' => '1','title' => 'php spec tets translatora w symfony','slug' => 'php-spec-tets-translatora-w-symfony','content' => '<pre class="language-php"><code>public function it_should_return_true(Order $order, TranslatorInterface $translator): void
    {
        $flagsToBlock = [\'xxxx\', \'aaaa\', \'bbbb\'];

        $order-&gt;hasFlagByCode(\'xxxx\')-&gt;willReturn(false);
        $order-&gt;hasFlagByCode(\'aaaa\')-&gt;willReturn(false);
        $order-&gt;hasFlagByCode(\'bbbb\')-&gt;willReturn(false);

        $translator-&gt;trans(
            \'enp.order.order_quick_return.order_source.error\',
            [
                \'%flagsToBlock%\' =&gt; implode(\',\', $flagsToBlock),
            ],
            \'EnpOrderBundle\'
        )-&gt;willReturn(\'message\');

        $this-&gt;beConstructedWith($flagsToBlock, $translator);
        $this-&gt;check($order)-&gt;shouldReturn(true);
        $this-&gt;getErrorMessage()-&gt;shouldBe(\'message\');
    }</code></pre>','publishedAt' => '2020-09-08 18:59:21','updatedAt' => '2020-10-19 13:12:55','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '343','user_id' => '1','title' => 'php spec test formularza','slug' => 'php-spec-test-formularza','content' => '<pre class="language-php"><code>class FastOrderReturnCreatorSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this-&gt;shouldHaveType(FastOrderReturnCreator::class);
        $this-&gt;shouldBeAnInstanceOf(OrderEditFormInterface::class);
    }

    public function let(
        FormFactoryInterface $formFactory,
        RouterInterface $router
    ): void {
        $this-&gt;beConstructedWith($formFactory, $router);
    }

    public function it_creates_form(
        FormFactoryInterface $formFactory,
        RouterInterface $router,
        OrderInterface $order,
        FormInterface $form
    ): void {
        $orderId = 1;
        $url = \'/order/fast-order-return/1\';

        $order-&gt;getId()-&gt;willReturn($orderId)-&gt;shouldBeCalledOnce();
        $router-&gt;generate(\'enp_enp0026_order_fast_return\', [\'orderId\' =&gt; $orderId])-&gt;willReturn($url);

        $formFactory-&gt;createNamed(
            \'order_return\',
            FastOrderReturnType::class,
            null,
            [
                \'action\' =&gt; $url,
                \'method\' =&gt; Request::METHOD_PUT,
            ]
        )-&gt;willReturn($form);

        $this-&gt;createOrderEditForm($order)-&gt;shouldBe($form);
    }</code></pre>','publishedAt' => '2020-09-08 19:23:25','updatedAt' => '2020-10-19 13:13:09','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '345','user_id' => '1','title' => 'proste cookie w js dla polityki prywatnośći','slug' => 'proste-cookie-w-js-dla-polityki-prywatnosci','content' => '<pre class="language-markup"><code>&lt;!DOCTYPE html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
    &lt;meta charset="UTF-8"&gt;
    &lt;meta name="viewport" content="width=device-width, initial-scale=1.0"&gt;
    &lt;title&gt;Document&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
    &lt;div id="simplecookienotification_v01" style="display:none;z-index: 99999; min-height: 35px; width: 300px; position: fixed; background: rgb(255, 255, 255); border: 1px solid rgb(147, 18, 18); text-align: center; color: rgb(119, 119, 119); border-radius: 10px; right: 10px; bottom: 10px;"&gt;
        &lt;div style="padding:5px; margin-left:5px; margin-right:5px; font-size:12px; font-weight:normal;"&gt;
            &lt;span id="simplecookienotification_v01_powiadomienie"&gt;
                W naszym sklepie stosujemy pliki cookies w celu świadczenia Państwu usług na najwyższym poziomie, w
                spos&oacute;b dostosowany do indywidualnych potrzeb. Korzystanie z witryny bez zmiany ustawień dotyczących
                cookies oznacza, że będą one zamieszczane w Państwa urządzeniu końcowym. Możecie Państwo dokonać w
                każdym czasie zmiany ustawień dotyczących cookies.
            &lt;/span&gt;
            &lt;span id="br_pc_title_html"&gt;
                &lt;br&gt;&lt;/span&gt;
            &lt;a id="simplecookienotification_v01_polityka" href="https://damidomo.pl/content/17-polityka-cookies" style="color: rgb(147, 18, 18);"&gt;Polityka Cookies&lt;/a&gt;
            &lt;span id="br_pc2_title_html"&gt;&amp;nbsp;&amp;nbsp;&lt;/span&gt;
            &lt;br&gt;
            &lt;a id="simplecookienotification_v01_info" href="https://damidomo.pl/content/16-polityka-prywatnosci" style="color: rgb(147, 18, 18);"&gt;Polityka Prywatności&lt;/a&gt;
            &lt;span id="br_pc3_title_html"&gt;&amp;nbsp;&amp;nbsp;&lt;/span&gt;
            &lt;br&gt;
            &lt;div id="jwc_hr1" style="height: 10px; display: block;"&gt;&lt;/div&gt;
            &lt;a id="okbutton" href="#" style="position: relative; background: rgb(147, 18, 18); color: rgb(255, 255, 255); padding: 5px 15px; text-decoration: none; font-size: 14px; font-weight: normal; border: 0px solid rgb(245, 245, 245); border-radius: 5px;"&gt;ROZUMIEM&lt;/a&gt;
            &lt;div id="jwc_hr2" style="height: 10px; display: block;"&gt;&lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
    &lt;script&gt;
        window.addEventListener(\'load\', function() {
            function simplecookienotification_v01_create_cookie(name, value, days) {
                var expires = "";
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = "; expires=" + date.toUTCString();
                }
                document.cookie = name + "=" + (value || "") + expires + "; path=/";
                document.getElementById("simplecookienotification_v01").style.display = "none";
            }
            function simplecookienotification_v01_read_cookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(\';\');
                for (var i = 0; i &lt; ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == \' \') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            }
            var simplecookienotification_v01_jest = simplecookienotification_v01_read_cookie("simplecookienotification_v01");
            if (simplecookienotification_v01_jest == null) {
                document.getElementById("simplecookienotification_v01").style.display = "block";
            }
            document.getElementById("okbutton").addEventListener("click", function(e) {
                e.preventDefault();
                simplecookienotification_v01_create_cookie(\'simplecookienotification_v01\', 1, 360);
            });
        }, true);
    &lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>','publishedAt' => '2020-09-13 12:26:31','updatedAt' => '2020-09-13 12:26:31','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '346','user_id' => '1','title' => 'ładowanie serwisóœ do tablicy po tagach','slug' => 'ladowanie-serwisooe-do-tablicy-po-tagach','content' => '<pre class="language-markup"><code>    Enp\\Bundle\\ENP0026\\OrderBundle\\Service\\OrderFastReturn\\OrderFastReturnService:
        arguments: [!tagged enp.order.order_fast_return.guard]

    Enp\\Bundle\\ENP0026\\OrderBundle\\Service\\OrderFastReturn\\Guard\\OrderCanceledGuard:
        tags:
            - { name: \'enp.order.order_fast_return.guard\' }

    Enp\\Bundle\\ENP0026\\OrderBundle\\Service\\OrderFastReturn\\Guard\\InvoiceAndWayBillGuard:
        autowire: true
        tags:
            - { name: \'enp.order.order_fast_return.guard\' }
</code></pre>','publishedAt' => '2020-09-15 08:45:00','updatedAt' => '2020-09-15 08:45:00','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '347','user_id' => '1','title' => 'przydatne mrki','slug' => 'przydatne-mrki','content' => '<ol>
<li><a href="http://gitlab.enp.me/mshp/adafir/-/merge_requests/66851/diffs">rozbicie item&oacute;w w edycji zam&oacute;wienia strategia</a></li>
<li><a href="https://gitlab.enp.me/mshp/adafir/-/merge_requests/57105/diffs#9f8dfc2ba8d8ee01683c7fb58fdb5b5f25acb3a7">nakładanie flag</a></li>
</ol>
<p>&nbsp;</p>','publishedAt' => '2020-09-17 08:06:54','updatedAt' => '2021-03-23 21:18:32','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '348','user_id' => '1','title' => 'enp hacki','slug' => 'enp-hacki','content' => '<ol>
<li>pokazanie nazyw bazy
<div>dodaj do adresu /enp/project zobaczysz <a tabindex="-1" title="http://m.in/" href="http://m.in/" target="_blank" rel="noreferrer noopener">m.in</a>. nazwe DB</div>
<div>grunt both --app=ENP0019A01&amp;&amp; npm run bothBuilds --base=ENP0019A01 &nbsp;</div>
<div>/bin/recreate-loc -f</div>
</li>
<li>
<div>I wyjście z powrotem do własnego użytkownika Po url dodajeszfgh
<pre class="language-markup"><code>?_change_to_username=_exit​</code></pre>
Przelogowanie się na innego usera w Panelu bez podawania hasła Po url dodajesz <br />
<pre class="language-markup"><code>?_change_to_username=login_użytkownika​</code></pre>
<p>3. Wyłączenie test&oacute;w</p>
<pre class="language-markup"><code>git commit -m "[ci skip]"</code></pre>
<pre class="language-markup"><code>npm install --save-dev grunt-sass@2.1.0</code></pre>
<p>4. Dodawanie produkt&oacute;w</p>
<pre class="language-markup"><code>cart/add?id=offerId</code></pre>
<pre class="language-markup"><code>a na liście odpalasz ?adafir=sql ?</code></pre>
</div>
</li>
</ol>','publishedAt' => '2020-09-22 20:12:46','updatedAt' => '2021-01-20 15:34:04','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '349','user_id' => '1','title' => 'twig extension optymalizacja','slug' => 'twig-extension-optymalizacja','content' => '<div style="font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 12pt; color: #000000; background-color: #ffffff;">w naszej aktualnej wersji symfony (oraz twig) otrzymaliśmy możliwość tworzenia lazy-loaded twig extensions.</div>
<div style="font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 12pt; color: #000000; background-color: #ffffff;">Szczeg&oacute;ły w dokumentacji SF:&nbsp;<a id="LPlnk601442" href="https://symfony.com/doc/3.4/templating/twig_extension.html#creating-lazy-loaded-twig-extensions">https://symfony.com/doc/3.4/templating/twig_extension.html#creating-lazy-loaded-twig-extensions</a></div>
<div style="font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 12pt; color: #000000; background-color: #ffffff;">&nbsp;</div>
<div style="font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 12pt; color: #000000; background-color: #ffffff;">Og&oacute;lnie sprowadza się to do rozdzielnia extensiona na dwie klasy:</div>
<div style="font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 12pt; color: #000000; background-color: #ffffff;">
<ol>
<li>FooExtension - zawierający jedynie listę dostępnych metod</li>
<li>FooRuntime - zawierający logikę metod</li>
</ol>
<div>Przykładowy jeden przerobiony extension znajdziecie w MR:&nbsp;<a id="LPlnk511380" href="http://gitlab.enp.me/mshp/adafir/-/merge_requests/67805">http://gitlab.enp.me/mshp/adafir/-/merge_requests/67805</a></div>
</div>','publishedAt' => '2020-10-01 13:26:08','updatedAt' => '2020-10-01 13:26:08','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '350','user_id' => '1','title' => 'sprawdzenie wersji wkhtmltopdf na linuxie','slug' => 'sprawdzenie-wersji-wkhtmltopdf-na-linuxie','content' => '<div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">&nbsp;</div>
</div>
</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">
<div>
<div>
<div>
<pre class="language-markup"><code>dbash
​
potem wkhtmltopdf -V</code></pre>
</div>
</div>
</div>
</div>
</div>
</div>
</div>','publishedAt' => '2020-10-02 14:13:40','updatedAt' => '2020-10-02 14:13:40','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '351','user_id' => '1','title' => 'moduł do generowania raportów z sql','slug' => 'modul-do-generowania-raportow-z-sql','content' => '<div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div data-tid="messageBodyContent">
<div>
<div>
<p><a title="https://panel.prod.ccc.enp.pl/report/list" href="https://panel.prod.ccc.enp.pl/report/list">https://panel.prod.ccc.enp.pl/report/list</a></p>
</div>
</div>
</div>
</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">
<div>
<div>
<div><a title="https://panel.prod.ccc.enp.pl/report/edit/2" href="https://panel.prod.ccc.enp.pl/report/edit/2">https://panel.prod.ccc.enp.pl/report/edit/2</a></div>
</div>
</div>
</div>
</div>
</div>
</div>','publishedAt' => '2020-10-06 08:53:24','updatedAt' => '2020-10-06 08:53:43','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '352','user_id' => '1','title' => 'cumulus api','slug' => 'cumulus-api','content' => '<pre class="language-markup"><code>Enp\\Bundle\\Service\\Api\\CumulusBundle\\Service\\CumulusApi::getWarranties // dumpowac w panelu na ubezpieczeniach</code></pre>','publishedAt' => '2020-10-08 06:56:05','updatedAt' => '2020-10-08 06:56:05','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '353','user_id' => '1','title' => 'płatności i transporty na koszyku','slug' => 'platnosci-i-transporty-na-koszyku','content' => '<p>//twig z platnosciami</p>
<p>{% set groupDisplayDescription = (stepForm.children.paymentGroup.vars[\'paymentMethodDisplayDescription\'][payment.vars.value])|default(false) %}</p>
<p>{% set groupDescription = stepForm.children.paymentGroup.vars[\'paymentMethodDescription\'][payment.vars.value] %}</p>
<p>{% for payment in stepForm.children.paymentGroup %}</p>
<p>//front</p>
<p data-renderer-start-pos="1">/var/www/adafir/layout/components/cart/js/paymentElements/payment-label.vue<br />/var/www/adafir/layout/components/cart/js/cart-transport.vue</p>','publishedAt' => '2020-10-12 12:26:53','updatedAt' => '2020-10-12 12:26:53','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '354','user_id' => '1','title' => 'stała wstrzyknieta do serwisu','slug' => 'stala-wstrzyknieta-do-serwisu','content' => '<pre class="language-markup"><code>    Enp\\Bundle\\ENP0031\\AddressBundle\\Validator\\Constraints\\ApartmentNumber\\ValidatorDefault:
        class: Enp\\Bundle\\ENP0031\\AddressBundle\\Validator\\Constraints\\ApartmentNumber\\ValidatorDefault
        arguments:
            $maxLenght: 10
        tags:
            - { name: enp_address_validator_apartment_number, service_key: !php/const:Enp\\Bundle\\ENP0031\\AddressBundle\\Validator\\Constraints\\ApartmentNumberValidator::DEFAULT_VALIDATOR_KEY }
</code></pre>','publishedAt' => '2020-10-13 10:44:33','updatedAt' => '2020-10-13 10:44:33','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '355','user_id' => '1','title' => 'paginator tablicy','slug' => 'paginator-tablicy','content' => '<pre class="language-php"><code>class ArrayPaginator
{
    public function getPaginatedList(array $data, int $page, int $limit): array
    {
        if (false === $this-&gt;isValidPaginationInput($page, $limit)) {
            return [];
        }

        $offset = ($page - 1) * $limit;

        return array_slice($data, $offset, $limit);
    }

    private function isValidPaginationInput(int $page, int $limit): bool
    {
        return $page &gt; 0 &amp;&amp; $limit &gt; 0;
    }
}
</code></pre>','publishedAt' => '2020-10-14 19:41:04','updatedAt' => '2020-10-14 19:41:04','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '356','user_id' => '1','title' => 'kalkulator  kilometrów na podstawie długości i szerokosci geograficznej','slug' => 'kalkulator-kilometrow-na-podstawie-dlugosci-i-szerokosci-geograficznej','content' => '<pre class="language-php"><code>class DistanceCalculator
{
    private const EARTH_RADIUS = 6371.009;

    public function calculate(CoordinableInterface $from, CoordinableInterface $to): float
    {
        return $this-&gt;calculateBetweenPoints((float)$from-&gt;getLat(), (float)$from-&gt;getLng(), (float)$to-&gt;getLat(), (float)$to-&gt;getLng());
    }

    public function calculateBetweenPoints(
        float $latitudeFrom,
        float $longitudeFrom,
        float $latitudeTo,
        float $longitudeTo
    ): float {
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * static::EARTH_RADIUS;
    }
}</code></pre>','publishedAt' => '2020-10-15 13:13:23','updatedAt' => '2020-10-15 13:13:23','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '357','user_id' => '1','title' => 'laravel tablica idków','slug' => 'laravel-tablica-idkow','content' => '<pre class="language-php"><code> Category::select(\'id\')-&gt;get()-&gt;modelKeys()</code></pre>','publishedAt' => '2020-10-18 12:49:33','updatedAt' => '2020-10-18 12:49:33','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '358','user_id' => '1','title' => 'adafir usługi','slug' => 'adafir-uslugi','content' => '<p><a href="https://techdoc.enp.pl/books/cumulus/page/akcje-kontekstowe">link</a>&nbsp; <a href="https://techdoc.enp.pl/books/cumulus/page/popularne-b%C5%82%C4%99dy-oraz-ich-przyczyny">link2</a></p>
<p>php app/admin_ENP0026/console enp:service:flat 1074 --queue=0 --env=prod // przebudowa flat uslugi</p>
<p>service_has_product //tabela</p>
<p>1)cumulus<br /><a href="https://prodpanelterg.enp.pl/pim/product-describe/product-specification/edit/2568302">https://prodpanelterg.enp.pl/pim/product-describe/product-specification/edit/2568302</a><br />piszesz im o tym, że atrubuty cumulus_grupakod i cumulus_podgrupakod<br />Enp\\Bundle\\Service\\FrontBundle\\Service\\ServiceCartManager::fetchFlatServicesForCart -&gt; skad pochodza dodatkwoe usugi !!!</p>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">src/Enp/Bundle/OrderBundle/Form/Factory/OrderServicesTypeFactory.php</div>
<div style="box-sizing: border-box; font-family: Menlo, Monaco, Consolas, \'Courier New\', Courier, monospace; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre; widows: 2; word-spacing: 0px;">$dto <span style="background-color: #6888c9;">= </span>$this<span style="background-color: #6888c9;">-&gt;</span>servicesDTOProvider<span style="background-color: #6888c9;">-&gt;</span>getDTOServices<span style="background-color: #6888c9;">(</span>$order<span style="background-color: #6888c9;">)</span>;</div>
<div style="box-sizing: border-box; font-family: Menlo, Monaco, Consolas, \'Courier New\', Courier, monospace; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: pre; widows: 2; word-spacing: 0px;">
<div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;"><br />
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">
<div>
<pre class="language-markup"><code>Enp\\Bundle\\ENP0026\\IntenseEsbServiceBundle\\Processor\\ServiceInPostcodeProcessor
Enp\\Bundle\\Service\\FrontBundle\\Service\\ServiceCartManager::processServiceFlatDataForCart
Enp\\Bundle\\Service\\FrontBundle\\Service\\ServiceCartManager::processServiceFlatDataForCart
Enp\\Bundle\\Service\\FrontBundle\\Service\\ServiceCartManager::getServicesForCart</code></pre>
</div>
</div>
</div>
</div>
</div>
</div>','publishedAt' => '2020-10-23 11:20:04','updatedAt' => '2020-12-11 13:16:40','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '359','user_id' => '1','title' => 'wyłącznie windowsa po czasie','slug' => 'wylacznie-windowsa-po-czasie','content' => '<pre class="language-markup"><code>shutdown -s -t 3600</code></pre>','publishedAt' => '2020-10-25 12:31:29','updatedAt' => '2020-10-25 12:31:29','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '360','user_id' => '1','title' => 'verto enp','slug' => 'verto-enp','content' => '<pre class="language-markup"><code>src/Enp/Bundle/SupplierAPI/VertoBundle/Api/Handler/OrderRequestHandler.php:69</code></pre>
<p><a href="https://techdoc.enp.pl/books/supplierapi/chapter/verto">https://techdoc.enp.pl/books/supplierapi/chapter/verto</a></p>','publishedAt' => '2020-10-26 09:37:33','updatedAt' => '2020-10-26 09:37:33','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '361','user_id' => '1','title' => 'enp dyżury','slug' => 'enp-dyzury','content' => '<p><a href="https://enetspzoo.sharepoint.com/sites/portal/Lists/Dyury%20%20Adafir/calendar.aspx">link dyżury</a></p>
<p><a href="https://enetspzoo.sharepoint.com/sites/portal/Lists/PLAN%20RELEASW/calendar.aspx">https://enetspzoo.sharepoint.com/sites/portal/Lists/PLAN%20RELEASW/calendar.aspx</a></p>','publishedAt' => '2020-10-26 13:06:19','updatedAt' => '2020-12-14 20:35:02','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '362','user_id' => '1','title' => 'stawianie frontu na brico','slug' => 'stawianie-frontu-na-brico','content' => '<pre class="language-markup"><code>UWAGA pro-tip jak odpalić front na brico
function webpack() {
    app=$1
    docker run -v $(pwd):/build adafir/atoms yarn
    docker run -v $(pwd):/build adafir/atoms npm run compile --base="$app" --task=bothBuilds
}
reload basha i p&oacute;źniej w konsoli wołasz: webpack ENP0027A01 cieszysz sie działającym frontem na brico</code></pre>','publishedAt' => '2020-10-27 14:09:25','updatedAt' => '2020-11-10 13:57:46','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '363','user_id' => '1','title' => 'php spec wprawdzenie czy metoda rzucza odpowiedni wyjątek','slug' => 'php-spec-wprawdzenie-czy-metoda-rzucza-odpowiedni-wyjatek','content' => '<pre class="language-markup"><code>$this-&gt;shouldThrow(InvalidArgumentException::class)-&gt;during(\'vote\', [$token, new Order(), [\'xxx\', \'yyy\']]);</code></pre>','publishedAt' => '2020-10-29 10:11:46','updatedAt' => '2020-10-29 10:11:46','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '364','user_id' => '1','title' => 'debugowanie rzeczy usuwanych w nie wiadomy sposób na koszyku','slug' => 'debugowanie-rzeczy-usuwanych-w-nie-wiadomy-sposob-na-koszyku','content' => '<pre class="language-markup"><code>\\Enp\\Bundle\\Service\\FrontBundle\\Service\\UnallowedCartItemsRemover::removeUnallowedCartItemsFromCart
​
\\Enp\\Bundle\\Service\\FrontBundle\\Service\\UnallowedCartItemsRemover::getCartItemsToRemove</code></pre>','publishedAt' => '2020-11-05 12:14:25','updatedAt' => '2021-01-26 14:03:01','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '365','user_id' => '1','title' => 'jak padnie inndb wywalić','slug' => 'jak-padnie-inndb-wywalic','content' => '<pre class="language-markup"><code>ib_logfile1</code></pre>
<pre class="language-markup"><code>ib_logfile0</code></pre>','publishedAt' => '2020-11-05 19:37:24','updatedAt' => '2020-11-07 15:35:25','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '366','user_id' => '1','title' => 'import/export mysql','slug' => 'importexport-mysql','content' => '<p>//export - backup tabeli</p>
<pre class="language-markup"><code>sudo mysqldump -u root -p nazwa_bazy nazwa_tabeli &gt; plik.sql</code></pre>
<pre class="language-markup"><code>./mysqldump.exe -u root -p m4856_cypherlarva auctions &gt; auctions_gt.sql //windows wamp</code></pre>
<p>//import&nbsp;</p>
<pre class="language-markup"><code>sudo mysql -u root -p m4856_cypherlarva &lt; m4856_cypherlarva.sql</code></pre>
<pre class="language-markup"><code>./mysql.exe -u root  m4856_cypherlarva &lt; auctions.sql //windows</code></pre>','publishedAt' => '2020-11-07 15:34:55','updatedAt' => '2020-12-20 19:19:59','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '367','user_id' => '1','title' => 'tablica na obiekt w php','slug' => 'tablica-na-obiekt-w-php','content' => '<pre class="language-php"><code>&lt;?php
class ArrayToObject
{
    public static function convertToObject(array $array): stdClass
    {
        $object = new stdClass();

        foreach ($array as $key =&gt; $value) {
            if (is_array($value)) {
                $value = static::convertToObject($value);
            }

            $object-&gt;$key = $value;
        }

        return $object;
    }
}

$array = [
    \'e1\' =&gt; [\'nume\' =&gt; \'Nitu\', \'prenume\' =&gt; \'Andrei\', \'sex\' =&gt; \'m\', \'varsta\' =&gt; 23, \'data\' =&gt; [
        \'page\' =&gt; 1,
        \'ajax\' =&gt; true,
        \'title\' =&gt; \'test 123\'
    ]],
    \'e2\' =&gt; [\'nume\' =&gt; \'Nae\', \'prenume\' =&gt; \'Ionel\', \'sex\' =&gt; \'m\', \'varsta\' =&gt; 27],
    \'e3\' =&gt; [\'nume\' =&gt; \'Noman\', \'prenume\' =&gt; \'Alice\', \'sex\' =&gt; \'f\', \'varsta\' =&gt; 22],
    \'e4\' =&gt; [\'nume\' =&gt; \'Geangos\', \'prenume\' =&gt; \'Bogdan\', \'sex\' =&gt; \'m\', \'varsta\' =&gt; 23],
    \'e5\' =&gt; [\'nume\' =&gt; \'Vasile\', \'prenume\' =&gt; \'Mihai\', \'sex\' =&gt; \'m\', \'varsta\' =&gt; 25]
];


$obj = ArrayToObject::convertToObject($array);

echo \'&lt;pre&gt;\';
var_dump($obj-&gt;e1-&gt;data-&gt;title);
echo \'&lt;/pre&gt;\';
?&gt;</code></pre>','publishedAt' => '2020-11-07 17:11:53','updatedAt' => '2020-11-07 17:23:06','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '368','user_id' => '1','title' => 'artubut dla botów','slug' => 'artubut-dla-botow','content' => '<pre class="language-markup"><code>&lt;meta name="googlebot" content="noindex" /&gt;</code></pre>
<p><a href="https://developers.google.com/search/reference/robots_meta_tag?hl=pl" target="_blank" rel="noopener">link</a></p>','publishedAt' => '2020-11-07 18:17:42','updatedAt' => '2020-11-07 18:18:05','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '369','user_id' => '1','title' => 'symfony wywołanie settera na serwisie','slug' => 'symfony-wywolanie-settera-na-serwisie','content' => '<pre class="language-markup"><code>services:
    xxxxxxxxxxx:
        calls:
            - [setItemQuantityDecorator, [\'@xxxxxxx\']]</code></pre>','publishedAt' => '2020-11-09 10:20:34','updatedAt' => '2020-11-09 10:20:34','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '370','user_id' => '1','title' => 'retro enp','slug' => 'retro-enp','content' => '<p><a href="https://ideaboardz.com/for/RETRO%20DEV4%20SALES/3423723">link</a></p>
<p><a href="https://techdoc.enp.pl/books/definition-of-done/chapter/backend">https://techdoc.enp.pl/books/definition-of-done/chapter/backend</a></p>','publishedAt' => '2020-11-10 13:47:13','updatedAt' => '2020-12-08 14:26:57','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '371','user_id' => '1','title' => 'greylog enp','slug' => 'greylog-enp','content' => '<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;"><a tabindex="-1" title="http://10.123.10.3:9000/dashboards/5de6d2ee853d1f000bd00974" href="http://10.123.10.3:9000/dashboards/5de6d2ee853d1f000bd00974" target="_blank" rel="noreferrer noopener">http://10.123.10.3:9000/dashboards/5de6d2ee853d1f000bd00974</a> - bugi sales</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;"><a tabindex="-1" title="http://10.123.10.3:9000/dashboards/5b6d43710843a00001c91aeb" href="http://10.123.10.3:9000/dashboards/5b6d43710843a00001c91aeb" target="_blank" rel="noreferrer noopener">http://10.123.10.3:9000/dashboards/5b6d43710843a00001c91aeb</a> - bugi terg</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;"><a href="http://10.123.10.3:9000/messages/graylog_387/6be02b21-356e-11eb-b3d6-02420a960010">http://10.123.10.3:9000/messages/graylog_387/6be02b21-356e-11eb-b3d6-02420a960010</a> // zadowolenie alsen</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;"><a href="https://vobis.pl/enp/logs/prod/100/">https://vobis.pl/enp/logs/prod/100/</a></div>','publishedAt' => '2020-11-16 15:40:42','updatedAt' => '2020-12-15 10:04:19','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '372','user_id' => '1','title' => 'pierwszy klucz z tablicy w php','slug' => 'pierwszy-klucz-z-tablicy-w-php','content' => '<pre class="language-php"><code>&lt;?php
$transport = [\'123\' =&gt; 123333, 1236 =&gt; \'asdad\'];
$mode = current($transport);

var_dump($mode);//123333</code></pre>','publishedAt' => '2020-11-20 08:20:53','updatedAt' => '2020-11-20 08:20:53','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '373','user_id' => '1','title' => 'pim produkty','slug' => 'pim-produkty','content' => '<p><a href="https://panel.sklepsamsung.pl/pim/product-describe/list">https://panel.sklepsamsung.pl/pim/product-describe/list</a></p>
<p>ref_number to kod produktu z tego linku</p>
<p>//command</p>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">\\Enp\\Bundle\\PIM\\ProductBundle\\Command\\RebuildFlatTableCommand</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">tabela
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">pim_product_system_flat</div>
</div>','publishedAt' => '2020-11-20 13:48:12','updatedAt' => '2020-11-27 09:07:13','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '374','user_id' => '1','title' => 'terg pin','slug' => 'terg-pin','content' => '<div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;"><br />
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">
<div>
<div>
<div><a title="https://prodpanelterg.enp.pl/shop/order/edit/api_log/15186259" href="https://prodpanelterg.enp.pl/shop/order/edit/api_log/15186259">https://prodpanelterg.enp.pl/shop/order/edit/api_log/15186259</a></div>
<div><a href="https://enetproduction.atlassian.net/browse/TASUP-3820">https://enetproduction.atlassian.net/browse/TASUP-3820</a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<p>// HODPIN w logach api</p>
<p>tabela w bazie <strong>pin_personal_reception</strong></p>','publishedAt' => '2020-11-24 12:53:11','updatedAt' => '2020-11-25 19:33:56','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '375','user_id' => '1','title' => 'kgu terg','slug' => 'kgu-terg','content' => '<p>Enp\\Bundle\\ENP0026\\IntenseEsbServiceBundle\\Query\\ServiceInKGUDataFinder</p>
<p>tabela <strong>intense_esb_service</strong></p>','publishedAt' => '2020-11-25 19:33:23','updatedAt' => '2020-11-25 19:33:45','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '376','user_id' => '1','title' => 'sla enp','slug' => 'sla-enp','content' => '<p><a href="https://enetproduction.atlassian.net/browse/TASUP-3311">https://enetproduction.atlassian.net/browse/TASUP-3311</a> //alerty<br /><a href="https://enetproduction.atlassian.net/browse/TASUP-3362">https://enetproduction.atlassian.net/browse/TASUP-3362</a> // listy produktowe<br /><a href="https://enetproduction.atlassian.net/browse/TASUP-3370">https://enetproduction.atlassian.net/browse/TASUP-3370</a> // zła konfiguracja produktu<br /><a href="https://enetproduction.atlassian.net/browse/TASUP-3366">https://enetproduction.atlassian.net/browse/TASUP-3366</a> //tooltipy<br /><a href="https://enetproduction.atlassian.net/browse/TASUP-3370">https://enetproduction.atlassian.net/browse/TASUP-3370</a><br /><a href="https://enetproduction.atlassian.net/browse/ENPSUP-33166">https://enetproduction.atlassian.net/browse/ENPSUP-33166</a><br /><a href="https://enetproduction.atlassian.net/browse/ENPSUP-33177">https://enetproduction.atlassian.net/browse/ENPSUP-33177</a><br /><a href="https://enetproduction.atlassian.net/browse/ENPSUP-33831">https://enetproduction.atlassian.net/browse/ENPSUP-33831</a> //ccc rozmiar<br /><a href="https://enetproduction.atlassian.net/browse/ENPSUP-33774">https://enetproduction.atlassian.net/browse/ENPSUP-33774</a> //watek sla od kludiusza<br /><a href="https://enetproduction.atlassian.net/browse/TASUP-3820">https://enetproduction.atlassian.net/browse/TASUP-3820</a> //PIN TERG<br /><a href="https://enetproduction.atlassian.net/browse/ENPSUP-33886">https://enetproduction.atlassian.net/browse/ENPSUP-33886</a> //empik szmit<br /><a href="https://enetproduction.atlassian.net/servicedesk/customer/portal/4/ENPSUP-33391">https://enetproduction.atlassian.net/servicedesk/customer/portal/4/ENPSUP-33391</a> //emik szmit powiazany<br /><strong>25.11</strong><br /><a href="https://enetproduction.atlassian.net/browse/ENPSUP-33961">https://enetproduction.atlassian.net/browse/ENPSUP-33961</a> /// crm <br /><a href="https://enetproduction.atlassian.net/browse/TASUP-3852">https://enetproduction.atlassian.net/browse/TASUP-3852</a> //udz terg<br /><strong>26.11</strong><br /><a href="https://enetproduction.atlassian.net/browse/ENPSUP-33977">https://enetproduction.atlassian.net/browse/ENPSUP-33977 //elastic </a> <br /><a href="https://enetproduction.atlassian.net/browse/ENPSUP-33981">https://enetproduction.atlassian.net/browse/ENPSUP-33981</a> //darmowa dostaw marketing</p>
<p><a href="https://enetproduction.atlassian.net/browse/ENPSUP-33915">https://enetproduction.atlassian.net/browse/ENPSUP-33915</a> //powiadomiania ccc</p>
<p><strong>13.01.21</strong></p>
<p><a href="https://enetproduction.atlassian.net/browse/TASUP-4140">https://enetproduction.atlassian.net/browse/TASUP-4140</a> //faktura papierowa terg</p>
<p><a href="https://enetproduction.atlassian.net/browse/ENPSUP-34701">https://enetproduction.atlassian.net/browse/ENPSUP-34701</a> brico paragony</p>
<p><strong>//=====================TERG TABELA=====================================</strong></p>
<p><a href="https://terg-monitoring.adafir.eu/tiled-charts/?menuIndex=1&amp;submenuIndex=-1&amp;eaContext=52b12bb">https://terg-monitoring.adafir.eu/tiled-charts/?menuIndex=1&amp;submenuIndex=-1&amp;eaContext=52b12bb</a></p>
<p><strong>Verto wątki</strong></p>
<p><a href="https://enetproduction.atlassian.net/browse/ENPSUP-33982">https://enetproduction.atlassian.net/browse/ENPSUP-33982</a></p>
<p><strong>//===============================COMMANDY===============================================</strong></p>
<p><strong>Elastic</strong>:</p>
<p>php app/admin_ENP0031/console enp:es:order:reindex --id=65379140,65369926,65370404,65372418,65372570 --env=prod</p>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">src/Enp/Bundle/ESOrderBundle/Queue/ESOrderProcessor.php:23</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">php app/admin_ENP0031/console enp:order:update --orderId=65247258 --env=prod --useHighPriority</div>
<p>enp:delete_orders_by_test_email</p>
<p>php app/admin_ENP0026/console enp:service:flat 303 --queue=0 --env=master</p>
<p>//===========================linki</p>
<p><a href="https://enetproduction.atlassian.net/issues/?jql=project%20in%20(%22TERG-ADAFIR%20SUPPORT%22%2C%20%22ENP%20Support%22)%20AND%20Obszar%20in%20(SALES%2C%20SALES-FRONT)%20AND%20status%20not%20in%20(Resolved%2C%20Closed%2C%20%22Waiting%20for%20release%22)%20ORDER%20BY%20cf%5B14001%5D%20DESC%2C%20updated%20DESC%2C%20priority%20DESC">sla_per_osoba</a></p>
<p><a href="https://enetproduction.atlassian.net/secure/Dashboard.jspa?selectPageId=34669">sla_tablica</a></p>','publishedAt' => '2020-11-26 16:18:58','updatedAt' => '2021-04-30 15:10:11','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '377','user_id' => '1','title' => 'monit order mediamarkt tetowe zamówienia','slug' => 'monit-order-mediamarkt-tetowe-zamowienia','content' => '<div style="box-sizing: border-box; font-family: Menlo, Monaco, Consolas, \'Courier New\', Courier, monospace; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<pre class="language-markup"><code>enp:delete_orders_by_email mediasaturn@monit24.pl 2</code></pre>
</div>
<div style="box-sizing: border-box; font-family: Menlo, Monaco, Consolas, \'Courier New\', Courier, monospace; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<pre class="language-markup"><code>src/Enp/Bundle/MSH/OrderBundle/Command/MonitOrderDeleteCommand.php</code></pre>
<pre class="language-markup"><code>src/Enp/Bundle/MSH/OrderBundle/Command/MonitOrderDeleteCommand.php:157</code></pre>
</div>','publishedAt' => '2020-11-27 09:39:10','updatedAt' => '2020-11-27 09:40:01','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '378','user_id' => '1','title' => 'logi elastica','slug' => 'logi-elastica','content' => '<pre class="language-markup"><code>Enp\\Bundle\\ESOrderBundle\\Queue\\ESOrderReceiver::executeMsg</code></pre>
<pre class="language-markup"><code>src/Enp/Bundle/ESOrderBundle/Queue/ESOrderProcessor.php</code></pre>','publishedAt' => '2020-11-27 09:42:06','updatedAt' => '2020-11-27 09:45:02','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '379','user_id' => '1','title' => 'npm zmiana limitu obserwowanych plików','slug' => 'npm-zmiana-limitu-obserwowanych-plikow','content' => '<pre class="language-markup"><code>echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf &amp;&amp; sudo sysctl -p</code></pre>','publishedAt' => '2020-11-28 12:02:57','updatedAt' => '2020-11-28 12:02:57','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '380','user_id' => '1','title' => 'prettier vue koniguracja','slug' => 'prettier-vue-koniguracja','content' => '<pre class="language-markup"><code>{
    "html.format.wrapLineLength": 0,
    "editor.wordWrapColumn": 205,
    "yaml.format.printWidth": 205,
    "json.schemas": [],
    "vetur.format.defaultFormatterOptions": {
        "js-beautify-html": {
            "wrap_attributes": "force-expand-multiline"
        },
        "prettyhtml": {
            "printWidth": 205,
            "singleQuote": false,
            "wrapAttributes": false,
            "sortAttributes": false,
        },
        "prettier": {
            "semi": false,
            "singleQuote": true,
            "eslintIntegration": true,
            "trailingComma": "none",
            "printWidth": 205
        }
    },
    "prettier.printWidth": 205
}</code></pre>','publishedAt' => '2020-11-28 16:56:22','updatedAt' => '2020-11-28 16:56:51','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '381','user_id' => '1','title' => 'estymacje enp','slug' => 'estymacje-enp','content' => '<p><a href="https://enetproduction.atlassian.net/secure/Dashboard.jspa?selectPageId=34684">https://enetproduction.atlassian.net/secure/Dashboard.jspa?selectPageId=34684#</a></p>','publishedAt' => '2020-12-02 12:24:38','updatedAt' => '2020-12-02 12:24:38','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '382','user_id' => '1','title' => 'Facet śledził taksówką swoją żonę','slug' => 'facet-sledzil-taksowka-swoja-zone','content' => '<p style="text-align: center;">Facet śledził taks&oacute;wką swoją żonę.<br />Prawda wyszła na jaw: jego żona pracuje w agencji towarzyskiej!<br />Zaskoczony facet m&oacute;wi do taks&oacute;wkarza:<br />- Chcesz pan zarobić st&oacute;wę?<br />- Jasne. Co mam robić?<br />- Wejść do tego burdelu, zabrać moją żonę, wsadzić ją do taks&oacute;wki i zawieźć nas oboje do domu.<br />Taks&oacute;wkarz zabrał się do pracy.<br />Kilka minut p&oacute;źniej drzwi agencji otworzyły się z hukiem i pojawił się taks&oacute;wkarz trzymający za włosy wijącą się kobietę.<br />Otworzył drzwi samochodu, wrzucił ją do środka i powiedział:<br />- Trzymaj ją pan!<br />Facet krzyczy do taks&oacute;wkarza:<br />- Ale to nie jest moja żona!<br />- Wiem kurwa, to moja! Teraz idę po pańską!</p>','publishedAt' => '2020-12-02 22:27:16','updatedAt' => '2021-01-14 23:40:06','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '383','user_id' => '1','title' => 'php podcasty','slug' => 'php-podcasty','content' => '<p><a href="https://podcasts.apple.com/pl/podcast/better-software-design/id1508298430">https://podcasts.apple.com/pl/podcast/better-software-design/id1508298430</a></p>','publishedAt' => '2020-12-07 11:01:36','updatedAt' => '2020-12-07 11:01:36','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '384','user_id' => '1','title' => 'enp logowanie czasu','slug' => 'enp-logowanie-czasu','content' => '<p><a href="https://enetproduction.atlassian.net/browse/TERG4-427">https://enetproduction.atlassian.net/browse/TERG4-427</a> //sla logownie czasu<br /><a href="https://enetproduction.atlassian.net/plugins/servlet/ac/timereports/timereports-report#!filterOrProjectId=project_SALES&amp;filterOrProjectId=project_NC&amp;filterOrProjectId=project_TL&amp;groupByField=workeduser&amp;group=SALES-ZESPOL-4">https://enetproduction.atlassian.net/plugins/servlet/ac/timereports/timereports-report#!filterOrProjectId=project_SALES&amp;filterOrProjectId=project_NC&amp;filterOrProjectId=project_TL&amp;groupByField=workeduser&amp;group=SALES-ZESPOL-4</a></p>','publishedAt' => '2020-12-09 07:35:11','updatedAt' => '2020-12-09 07:35:11','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '385','user_id' => '1','title' => 'Php ciekawostki','slug' => 'php-ciekawostki','content' => '<p><strong><a href="https://stackoverflow.com/questions/3819398/php-exec-command-or-similar-to-not-wait-for-result">https://stackoverflow.com/questions/3819398/php-exec-command-or-similar-to-not-wait-for-resulthttps://stackoverflow.com/questions/3819398/php-exec-command-or-similar-to-not-wait-for-result</a></strong></p>','publishedAt' => '2020-12-09 22:41:10','updatedAt' => '2020-12-09 22:41:10','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '386','user_id' => '1','title' => 'płątności limitery cache itd','slug' => 'platnosci-limitery-cache-itd','content' => '<pre class="language-markup"><code>Enp\\Bundle\\PaymentBundle\\Model\\PaymentGroupResolverAbstract</code></pre>
<pre class="language-markup"><code>Enp\\Bundle\\PaymentApi\\TpayBundle\\Limiter\\TpayPaymentLimiter</code></pre>','publishedAt' => '2020-12-10 09:02:02','updatedAt' => '2020-12-15 17:16:33','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '387','user_id' => '1','title' => 'enp karty podarunkowe','slug' => 'enp-karty-podarunkowe','content' => '<div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;"><br />
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">&nbsp;</div>
</div>
</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">
<div>&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">
<div>tabela gift_card</div>
</div>
</div>
</div>
<div>​</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">
<div>
<div>
<div>
<pre>WHERE generated_by_order_id = 18566987
</pre>
</div>
</div>
</div>
</div>
</div>
</div>
<div>​</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">
<div data-tid="messageBodyContainer">
<div data-tid="messageBodyContent">
<div>kolumna additional_data</div>
</div>
</div>
</div>','publishedAt' => '2020-12-15 13:41:14','updatedAt' => '2020-12-15 13:41:14','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '388','user_id' => '1','title' => 'listener docrinowy do elastica','slug' => 'listener-docrinowy-do-elastica','content' => '<pre class="language-markup"><code>Enp\\Bundle\\OrderBundle\\EventListener\\OrderExportFlatDataListener</code></pre>','publishedAt' => '2020-12-17 16:29:33','updatedAt' => '2020-12-17 16:29:33','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '389','user_id' => '1','title' => 'sprawdzanie transportów transport','slug' => 'sprawdzanie-transportow-transport','content' => '<pre class="language-markup"><code>Enp/Bundle/Front/CartFlowBundle/EventListener/TransportPaymentVerifyListener.php:347</code></pre>
<pre class="language-markup"><code>Enp\\Bundle\\ENP0026\\HomeDelivery\\CartBundle\\EventListener\\PostcodeListener::onAddressCompleted</code></pre>
<pre class="language-markup"><code>Enp/Bundle/Front/CartFlowBundle/Form/EventListener/Address/Account.php</code></pre>
<p>możę trzeba też cache zbudować dla transprot&oacute;w<br />php app/admin_samsung/console enp:transport:flat:generate -<br />php app/admin_samsung/console enp_transport:build_transport_cache_command</p>
<p>tylko miast samsung dać admin_alsen<br />do cykli command dphp app/admin_alsen/console enp:cycle:generate<br />jak mogę sobie na media expert zmienic stan magazynowy produktu?<br />w tabeli warehouse_stock</p>
<p>php app/admin_ENP0031/console<br />php app/admin_ENP0031/console enp:transport:flat:generate</p>
<p>debugowanie !!!!<br />Enp\\Bundle\\PaymentBundle\\Model\\PaymentGroupResolverAbstract::shouldExcludePaymentGroup</p>
<p>ccc transport y trzeba dodac cennicki!!!<br />http://panel.pre.ccc.enp.pl/transport/pricelist/list<br />http://panel.pre.ccc.enp.pl/transport/pricelist/edit/8</p>
<p>\\Enp\\Bundle\\TransportBundle\\Service\\TransportService::getTransportMethodList</p>
<p><br />http://panel.pre.ccc.enp.pl/transport/transport/edit/56<br />http://panel.pre.ccc.enp.pl/transport/group/edit/42<br />http://panel.pre.ccc.enp.pl/transport/integration/edit/52<br />http://panel.pre.ccc.enp.pl/transport/pricelist/edit/15<br />https://pre.pl.ccc-ecom.enp.pl/sk/damske/topanky/sportove/rekreacna-obuv-adidas-court-bold-fx3488-biela<br /><a href="https://pre.pl.ccc-ecom.enp.pl/pl/cart/delivery-today/postcode/availability?postcode=00-000">https://pre.pl.ccc-ecom.enp.pl/pl/cart/delivery-today/postcode/availability?postcode=00-000</a></p>
<p>&nbsp;</p>
<p>/var/www/adafir/src/Enp/Bundle/Front/CartFlowBundle/Resources/config/service/listener.yml</p>','publishedAt' => '2020-12-17 20:51:04','updatedAt' => '2021-03-12 21:13:26','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '390','user_id' => '1','title' => 'kalendarze','slug' => 'kalendarze','content' => '<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;"><a href="https://techdoc.enp.pl/books/kalendarz-dostaw/page/debug-kalendarza---od-czego-zacz%C4%85%C4%87">https://techdoc.enp.pl/books/kalendarz-dostaw/page/debug-kalendarza---od-czego-zacz%C4%85%C4%87</a></div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">&nbsp;</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">src/Enp/Bundle/Sale/DeliveryCalendar/DCCartBundle/Bus/Handler/SubmitDeliveryCalendarHandler.php</div>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">src/Enp/Bundle/Sale/DeliveryCalendar/DCCartBundle/EventListener/DeliveryCalendarContextCleaningSubscriber.php</div>
<p style="margin: 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-style: initial; text-decoration-color: initial;">Enp\\Bundle\\Sale\\DeliveryCalendar\\DCCartBundle\\Bus\\Handler\\SubmitDeliveryCalendarHandler::changeCartTransportToCalendarBaseGridTransport</p>
<p><a href="https://www.mediaexpert.pl/enp/logs/prod.supplier_api_error/1000/">https://www.mediaexpert.pl/enp/logs/prod.supplier_api_error/1000/</a></p>','publishedAt' => '2020-12-18 11:13:55','updatedAt' => '2021-02-17 10:26:10','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '392','user_id' => '1','title' => 'przydatne miejscaw kodzie','slug' => 'przydatne-miejscaw-kodzie','content' => '<pre class="language-markup"><code>src/Enp/Bundle/OrderBundle/Enum/ItemSystemDataEnum.php</code></pre>
<pre class="language-markup"><code>Enp\\Bundle\\OrderBundle\\Applicator\\OrderFlagApplicator</code></pre>
<pre class="language-markup"><code>Enp\\Bundle\\OrderBundle\\Controller\\Action\\ItemsController::deleteAction</code></pre>
<pre class="language-markup"><code>Enp/Bundle/OrderBundle/Queue/OrderChange/OrderChangeProcessor.php</code></pre>
<pre class="language-markup"><code>Enp\\Bundle\\OrderBundle\\Provider\\Strategy\\AbstractRealizationRuleStockStrategy</code></pre>
<pre class="language-markup"><code>src/Enp/Bundle/OrderBundle/Traits/GroupedAdditionalTrait.php</code></pre>','publishedAt' => '2020-12-23 15:16:44','updatedAt' => '2021-03-31 09:08:25','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '393','user_id' => '1','title' => 'migracja array','slug' => 'migracja-array','content' => '<pre class="language-markup"><code>    public function down(Schema $schema): void
    {
        $this-&gt;addSql(
            "DELETE FROM flag WHERE system_code in (:codes)",
            [
                \'codes\' =&gt; [
                    ENP0026OrderFlagEnum::TERG_LEASING,
                    ENP0026OrderFlagEnum::TERG_LOAN_LEASING,
                ],
            ],
            [
                \'codes\' =&gt; Connection::PARAM_STR_ARRAY,
            ]
        );
    }</code></pre>','publishedAt' => '2020-12-24 09:32:14','updatedAt' => '2020-12-24 09:32:14','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '394','user_id' => '1','title' => 'restart karty sieciowej linux','slug' => 'restart-karty-sieciowej-linux','content' => '<pre class="language-markup"><code>sudo rfkill unblock all</code></pre>','publishedAt' => '2020-12-28 13:20:13','updatedAt' => '2020-12-28 13:20:13','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '395','user_id' => '1','title' => 'widoki problem enp','slug' => 'widoki-problem-enp','content' => '<pre class="language-markup"><code>select * from view v
join admin_group_has_view aghv on v.id = aghv.view_id
order by id desc

select ag.*, v.* from `view` v
join admin_group_has_view g on v.id = g.view_id
join admin_group ag on g.admin_group_id = ag.i</code></pre>
<p><a href="https://enetproduction.atlassian.net/browse/TASUP-4091">https://enetproduction.atlassian.net/browse/TASUP-4091</a></p>
<p><span style="color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">\\Enp\\Bundle\\Admin\\ViewBundle\\Service\\ViewManager::getRestrictedFilters</span></p>','publishedAt' => '2020-12-28 23:13:02','updatedAt' => '2021-02-22 14:58:05','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '396','user_id' => '1','title' => 'Goecoding enp','slug' => 'goecoding-enp','content' => '<p style="margin: 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">Od strony backendu proces kt&oacute;ry ostatni zaczął być bardziej promowany</p>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><a class="external-link" style="color: #0052cc; text-decoration: none;" href="http://enp0019a01.adafir.loc/v2/products/searchnearpos?product=225150&amp;address=52.114503,19.423561&amp;distance=9000&amp;onlyAvailable=1" rel="nofollow noreferrer">http://enp0019a01.adafir.loc/v2/products/searchnearpos?product=225150&amp;address=52.114503,19.423561&amp;distance=9000&amp;onlyAvailable=1</a></p>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">src/Enp/Bundle/ProductOfferBundle/Controller/Front/NearPosOffersController.php:52</p>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">kończy sie to odpytaniem do api google</p>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">vendor/willdurand/geocoder/src/Geocoder/Provider/GoogleMaps.php:72</p>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><tt>"https://maps.googleapis.com/maps/api/geocode/json?address=52.114503%2C19.423561"</tt></p>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">bez sensu jest troche to ze pytamy o wsp&oacute;łrzędne kt&oacute;re już mamy - może arto było by jakos to sprawdzić, czy adres to nie wsp&oacute;łrzędne.</p>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">Na szybko można też spr&oacute;bowac włączyć cache w bazinga - jak tutaj:</p>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">app/admin/config/geocoder/config.yml:7</p>
<p>&nbsp;</p>','publishedAt' => '2021-01-07 11:00:52','updatedAt' => '2021-01-07 11:00:52','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '397','user_id' => '1','title' => 'zapytania enp','slug' => 'zapytania-enp','content' => '<p>Dostepnośc hod w danym dniu na danym kodzie pocztowym:</p>
<pre class="language-markup"><code>SELECT
	hdd.service_id AS HOD_service_id,
	hppc.post_code,
	hdd.shop_code,
	hdd.day,
	hs.name,
	hs.service_index
FROM hod_pos_post_code hppc
JOIN hod_delivery_day hdd ON(hppc.shop_code = hdd.shop_code)
JOIN hod_service hs ON(hdd.service_id = hs.id)
WHERE 
	hppc.post_code = \'00-001\'
	AND hdd.day = \'2021-01-24\'
	# id usługi z tabeli hod_service
	# AND hdd.service_id IN (1,3,4,6,7,8,10,11,13,15,19,22,43,44,71)
	# AND hdd.service_id &gt; 0
GROUP BY hdd.service_id</code></pre>
<p>Stany magazynowe danego profuktu</p>
<pre class="language-markup"><code>select stock_shop, stock_shipment, name from warehouse_stock ws
join warehouse w on ws.warehouse_id = w.id
where ws.product_id = 21360508 
# and name like \'P303%\'
AND ws.warehouse_id IN (196)
#GROUP BY (ws.warehouse_id)</code></pre>
<p>&nbsp;</p>','publishedAt' => '2021-01-12 11:55:21','updatedAt' => '2021-03-23 21:16:05','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '398','user_id' => '1','title' => 'hod enp','slug' => 'hod-enp','content' => '<pre class="language-markup"><code>SELECT
	hdd.service_id AS HOD_service_id,
	hppc.post_code,
	hdd.shop_code,
	hdd.day,
	hs.name,
	hs.service_index
FROM hod_pos_post_code hppc
JOIN hod_delivery_day hdd ON(hppc.shop_code = hdd.shop_code)
JOIN hod_service hs ON(hdd.service_id = hs.id)
WHERE 
	hppc.post_code = \'00-001\'
	AND hdd.day = \'2021-01-24\'
	# id usługi z tabeli hod_service
	# AND hdd.service_id IN (1,3,4,6,7,8,10,11,13,15,19,22,43,44,71)
	# AND hdd.service_id &gt; 0
GROUP BY hdd.service_id</code></pre>
<div>
<div>
<pre class="language-markup"><code>\\Enp\\Bundle\\ENP0026\\HomeDelivery\\APIBundle\\Command\\EcShopsPostCodesDataGetCommand
\\Enp\\Bundle\\ENP0026\\HomeDelivery\\APIBundle\\Command\\EcCalendarGetCommand</code></pre>
</div>
<div><a href="https://www.mediaexpert.pl/enp/logs/prod.supplier_api_error/1000/">https://www.mediaexpert.pl/enp/logs/prod.supplier_api_error/1000/</a></div>
<div><a href="https://enetproduction.atlassian.net/browse/TASUP-4165">https://enetproduction.atlassian.net/browse/TASUP-4165</a></div>
<div><a href="https://enetproduction.atlassian.net/browse/TASUP-4205">https://enetproduction.atlassian.net/browse/TASUP-4205</a></div>
<div>&nbsp;</div>
<div>&nbsp;</div>
</div>','publishedAt' => '2021-01-12 14:00:31','updatedAt' => '2021-01-12 14:00:31','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '399','user_id' => '1','title' => 'brico paragony','slug' => 'brico-paragony','content' => '<p style="margin: 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><a class="external-link" style="color: #0052cc; text-decoration: none;" href="https://panel.bricomarche.pl/shop/order/edit/api_log/75380" rel="nofollow noreferrer">https://panel.bricomarche.pl/shop/order/edit/api_log/75380</a><br />Paragon był już raz pobrany i w momencie pobrania paragonu tworzymy wpis w tabeli supplier_order. A zapytanie pobierające dane z tego posa leci w tym miejscu \\Enp\\Bundle\\OrderBundle\\Query\\OrderFinder::getOrdersWithoutSupplierOrder</p>
<div class="code panel" style="margin: 9px 0px; padding: 0px; border: 1px solid #cccccc; background: #f5f5f5; font-size: 12px; line-height: 1.33333; font-family: monospace; border-radius: 3px; color: #172b4d; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
<div class="codeContent panelContent" style="margin: 0px; padding: 9px 12px;">
<pre class="code-java" style="margin: 0px; padding: 0px; max-height: 30em; overflow: auto; white-space: pre-wrap; overflow-wrap: normal;">SELECT DISTINCT o.id FROM `order` o
LEFT JOIN order_has_flag ohf ON ohf.order_id = o.id
LEFT JOIN pos p ON p.id = o.realize_pos_id
LEFT JOIN `supplier_order` so ON o.id = so.order_id AND so.deleted_at IS NULL
LEFT JOIN flag f ON ohf.flag_id = f.id
WHERE (o.deleted_at IS NULL)
AND (f.system_code != <span class="code-quote" style="color: #009100;">\'incomplete\'</span> OR f.id IS NULL)
AND (so.id IS NULL)
AND (p.code = <span class="code-quote" style="color: #009100;">\'7369\'</span>)
AND ( f.system_code = <span class="code-quote" style="color: #009100;">\'order_ready_for_erp\'</span>) LIMIT 25

</pre>
</div>
</div>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">Zwraca to ten endpoint:</p>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><a class="external-link" style="color: #0052cc; text-decoration: none;" href="https://admin_enp0027.adafir.loc/api/storeline/orders" rel="nofollow noreferrer">https://admin_enp0027.adafir.loc/api/storeline/orders</a></p>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">Można pominąć autoryzajce robiąc takiego hacka w tym pliku app/admin/config/security.yml i zakomentowąć api routy:<br /><span class="image-wrap"><a id="337510_thumb" style="color: #0052cc; text-decoration: none;" title="Zrzut ekranu z 2021-01-13 15-11-36.png" href="https://enetproduction.atlassian.net/secure/attachment/337510/337510_Zrzut+ekranu+z+2021-01-13+15-11-36.png"><img class="" style="margin: 0px; padding: 0px; border: 0px;" src="https://enetproduction.atlassian.net/secure/thumbnail/337510/Zrzut+ekranu+z+2021-01-13+15-11-36.png?default=false" alt="Zrzut ekranu z 2021-01-13 15-11-36.png" /></a></span><br />a potem w controlerze debugowac to:<br />Enp\\Bundle\\SupplierAPI\\StorelineBundle\\Controller\\OrderController::getOrders</p>
<p style="margin: 12px 0px 0px; padding: 0px; color: #172b4d; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #f4f5f7; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">A ja finalnie ustawiłem w tabeli supplier_order deleted_at dla wpis&oacute;w dla tego zam&oacute;wienia co umożliwiło pobranie danych jeszcze raz.</p>
<p>&nbsp;</p>','publishedAt' => '2021-01-13 14:28:29','updatedAt' => '2021-01-13 14:28:29','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '400','user_id' => '1','title' => 'zmiana kodu ubezpieczenia','slug' => 'zmiana-kodu-ubezpieczenia','content' => '<p>eplatnik / kreatory / obsluga ubezpieczonegodra</p>
<p><a href="https://www.pit.pl/ulga-zus-na-start/skladki-zus-po-zakonczeniu-ulgi-na-start-961334">https://www.pit.pl/ulga-zus-na-start/skladki-zus-po-zakonczeniu-ulgi-na-start-961334</a></p>
<p><a href="https://zus.pox.pl/zus/koniec-ulgi-na-start-i-przejscie-na-skladki-preferencyjne.htm">https://zus.pox.pl/zus/koniec-ulgi-na-start-i-przejscie-na-skladki-preferencyjne.htm</a></p>
<p><a href="https://www.mala-firma.pl/dla-poczatkujacych/jak-wypelnic-druk/442-jak-wypelnic-zus-zua">https://www.mala-firma.pl/dla-poczatkujacych/jak-wypelnic-druk/442-jak-wypelnic-zus-zua</a></p>','publishedAt' => '2021-01-19 21:07:36','updatedAt' => '2021-01-19 21:07:36','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '401','user_id' => '1','title' => 'symfony route sposob','slug' => 'symfony-route-sposob','content' => '<p>https://stackoverflow.com/questions/54495546/symfony-flex-overriding-twigs-routing-path-function?fbclid=IwAR225-8nqneyzkPC_k_e_lFSJn07RvClzZyLXNdOK1yyYVaYWkQy-osyq_Q</p>','publishedAt' => '2021-01-21 21:53:01','updatedAt' => '2021-01-21 21:53:01','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '402','user_id' => '1','title' => 'spike usługi widoczne na koszyku','slug' => 'spike-uslugi-widoczne-na-koszyku','content' => '<p><a href="https://enetproduction.atlassian.net/browse/SALES-3417">https://enetproduction.atlassian.net/browse/SALES-3417</a></p>','publishedAt' => '2021-01-26 14:01:31','updatedAt' => '2021-01-26 14:01:31','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '403','user_id' => '1','title' => 'dostawa dzisiaj','slug' => 'dostawa-dzisiaj','content' => '<pre class="language-markup"><code>Enp\\Bundle\\Front\\CartFlowBundle\\Validator\\DeliveryTodayAvailabilityValidator
Enp\\Bundle\\Front\\CartFlowBundle\\Controller\\DeliveryTodayController::getPostcodeAvailabilityAction</code></pre>
<p><a href="http://panel.pre.ccc.enp.pl/transport/postcode-warehouse-mapping-list/list">http://panel.pre.ccc.enp.pl/transport/postcode-warehouse-mapping-list/list</a> //lista</p>
<div style="box-sizing: border-box; font-family: \'Segoe UI\', system-ui, \'Apple Color Emoji\', \'Segoe UI Emoji\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">dla Warszawy -&gt; 00-010</div>
<p><a href="https://pre.pl.ccc-ecom.enp.pl/pl/damskie/buty/sportowe/obuwie-sportowe-vans-vn0a3mvz1861-czarny">https://pre.pl.ccc-ecom.enp.pl/pl/damskie/buty/sportowe/obuwie-sportowe-vans-vn0a3mvz1861-czarny</a></p>','publishedAt' => '2021-01-29 15:48:35','updatedAt' => '2021-02-01 10:15:58','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '404','user_id' => '1','title' => 'sprawdzenie czy istnieje id w innej tabeli','slug' => 'sprawdzenie-czy-istnieje-id-w-innej-tabeli','content' => '<pre class="language-markup"><code>SELECT o.id FROM  `order` o 
WHERE o.payment_id = 112
AND NOT EXISTS (SELECT 1 FROM payment_santander_status WHERE payment_santander_status.order_id = o.id)
limit 1000</code></pre>','publishedAt' => '2021-02-12 10:51:22','updatedAt' => '2021-02-12 10:51:22','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '406','user_id' => '1','title' => 'zaytanie po dacie','slug' => 'zaytanie-po-dacie','content' => '<p>Przed</p>
<pre class="language-markup"><code>SELECT
    MONTH(o.created_at) AS month,
    COUNT(1) AS \'total in month\',
    ((SELECT COUNT(1) FROM `order` AS o1 FORCE INDEX (idx_created_at) WHERE o1.customer_id IS NOT NULL AND MONTH(o1.created_at) = MONTH(o.created_at) AND YEAR(o1.created_at) = 2020) / COUNT(1)) * 100 AS \'% logged in\',
    ((SELECT COUNT(1) FROM `order` AS o1 FORCE INDEX (idx_created_at) WHERE o1.customer_id IS NULL AND MONTH(o1.created_at) = MONTH(o.created_at) AND YEAR(o1.created_at) = 2020) / COUNT(1)) * 100 AS \'% guests\'
FROM `order` AS o
FORCE INDEX (idx_created_at)
WHERE YEAR(o.created_at) = 2020
GROUP BY MONTH(o.created_at);</code></pre>
<p>Po</p>
<pre class="language-markup"><code>SELECT
    MONTH(o.created_at) AS month,
    COUNT(1) AS \'total in month\',
    ((SELECT COUNT(1) FROM `order` AS o1 FORCE INDEX (idx_created_at) WHERE o1.customer_id IS NOT NULL AND MONTH(o1.created_at) = MONTH(o.created_at) AND YEAR(o1.created_at) = 2020) / COUNT(1)) * 100 AS \'% logged in\',
    ((SELECT COUNT(1) FROM `order` AS o1 FORCE INDEX (idx_created_at) WHERE o1.customer_id IS NULL AND MONTH(o1.created_at) = MONTH(o.created_at) AND YEAR(o1.created_at) = 2020) / COUNT(1)) * 100 AS \'% guests\'
FROM `order` AS o
FORCE INDEX (idx_created_at)
WHERE YEAR(o.created_at) = 2020
GROUP BY MONTH(o.created_at);</code></pre>
<p>&nbsp;</p>','publishedAt' => '2021-03-12 21:12:46','updatedAt' => '2021-03-12 21:12:46','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '407','user_id' => '1','title' => 'kod pocztowy zamiana','slug' => 'kod-pocztowy-zamiana','content' => '<pre class="language-markup"><code>02134 -&gt; 02-134

str.replace(/(\\d{2})(\\d{3})/, "$1-$2") lub str.replace(/(..)/, &rdquo;$1-&rdquo;)</code></pre>','publishedAt' => '2021-03-20 16:35:42','updatedAt' => '2021-03-20 16:35:42','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '408','user_id' => '1','title' => 'ustawienie wersji phpna linuxie','slug' => 'ustawienie-wersji-phpna-linuxie','content' => '<pre class="language-markup"><code>sudo update-alternatives --config php

update-alternatives --set php /usr/bin/php7.4
</code></pre>
<p>&nbsp;</p>
<pre class="language-markup"><code>//aktywacja wersji
sudo a2enmod php8.0

dezaktywacja
//sudo a2dismod php8.0</code></pre>','publishedAt' => '2021-03-26 17:27:59','updatedAt' => '2021-03-26 17:45:30','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '409','user_id' => '1','title' => 'link aktywność w google','slug' => 'link-aktywnosc-w-google','content' => '<p><a href="https://myactivity.google.com/activitycontrols/webandapp">link</a></p>','publishedAt' => '2021-04-02 21:39:32','updatedAt' => '2021-04-02 21:39:32','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '412','user_id' => '1','title' => 'merge poza pętlą','slug' => 'merge-poza-petla','content' => '<pre class="language-php"><code>    public function xxx(OrderInterface $order, ?string $postCodeToCheck): void
    {
        $returnOfDamagedEquipmentServicesToRemoveLocal = [];

        foreach ($this-&gt;guardsCollection as $singleGuard) {
                $returnOfDamagedEquipmentServicesToRemoveLocal[] = $singleGuard-&gt;getReturnOfDamagedEquipmentServicesToRemove();
            }
        }

        if (empty($returnOfDamagedEquipmentServicesToRemoveLocal)) {
            return;
        }

        $this-&gt;returnOfDamagedEquipmentServicesToRemove = array_merge(
            ...$returnOfDamagedEquipmentServicesToRemoveLocal
        );
    }
</code></pre>','publishedAt' => '2021-04-14 20:30:47','updatedAt' => '2021-04-16 23:24:24','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '413','user_id' => '1','title' => 'sortowanie z zachowaniem kluczy','slug' => 'sortowanie-z-zachowaniem-kluczy','content' => '<pre class="language-php"><code>$servicesIds = [];

foreach ($conflictedData as $key =&gt; $singleGroup) {
   uasort($singleGroup, fn($a, $b) =&gt; $a-&gt;getCreatedAt() &gt; $b-&gt;getCreatedAt());
   unset($conflictedData[$key][array_key_first($singleGroup)]);
   $servicesIds[] = array_keys($conflictedData[$key]);
}

return array_merge(...$servicesIds);</code></pre>','publishedAt' => '2021-04-16 13:19:51','updatedAt' => '2021-04-16 23:23:56','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '414','user_id' => '1','title' => 'ipost w adafirze','slug' => 'ipost-w-adafirze','content' => '<pre class="language-markup"><code>\\Enp\\Bundle\\TransportAPI\\InpostBundle\\Api\\InpostClient</code></pre>
<pre class="language-markup"><code>\\Enp\\Bundle\\TransportAPI\\InpostBundle\\Provider\\InpostProvider</code></pre>
<p>&nbsp;</p>','publishedAt' => '2021-04-19 12:01:32','updatedAt' => '2021-04-19 12:01:32','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '415','user_id' => '1','title' => 'repozytoria','slug' => 'repozytoria','content' => '<pre class="language-markup"><code>https://github.com/hgraca/explicit-architecture-php/tree/master/src</code></pre>','publishedAt' => '2021-04-19 21:18:40','updatedAt' => '2021-04-19 21:18:40','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '416','user_id' => '1','title' => 'test cache i prywatnej metody','slug' => 'test-cache-i-prywatnej-metody','content' => '<pre class="language-php"><code>    public function it_is_get_data_test(
        ServiceConflictsFinder $serviceConflictsFinder,
        CacheProviderInterface $cacheProvider
    ): void {
        $serviceId = 1;
        $serviceId2 = 2;

        $serviceConflictsFinder-&gt;getData($serviceId)-&gt;willReturn([2, 3])-&gt;shouldBeCalledOnce();
        $serviceConflictsFinder-&gt;getData($serviceId2)-&gt;willReturn([7, 8])-&gt;shouldBeCalledOnce();
        $cacheProvider-&gt;load(
            sprintf(\'service_conflict_data_%s\', $serviceId),
            CacheLifetime::HOUR,
            Argument::that(
                function ($serviceId) {
                    call_user_func($serviceId);

                    return [2, 3];
                }
            )
        )-&gt;willReturn([2, 3])-&gt;shouldBeCalledOnce();
        $cacheProvider-&gt;load(
            sprintf(\'service_conflict_data_%s\', $serviceId2),
            CacheLifetime::HOUR,
            Argument::that(
                function ($serviceId2) {
                    call_user_func($serviceId2);

                    return [7, 8];
                }
            )
        )-&gt;willReturn([7, 8])-&gt;shouldBeCalledOnce();

        $this-&gt;getDataOfConflictsServices([1, 2])-&gt;shouldBe([1 =&gt; [2, 3], 2 =&gt; [7, 8]]);
    }</code></pre>','publishedAt' => '2021-04-20 19:52:10','updatedAt' => '2021-04-20 19:52:10','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '417','user_id' => '1','title' => 'mso rzeczy offline order','slug' => 'mso-rzeczy-offline-order','content' => '<p>offline order</p>
<p><a href="https://admin_msh.adafir.loc/offline_order/new">https://admin_msh.adafir.loc/offline_order/new</a></p>
<p>https://enetproduction.atlassian.net/browse/ENPSUP-36353</p>','publishedAt' => '2021-04-29 14:39:09','updatedAt' => '2021-04-29 14:39:09','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0'),
  array('id' => '418','user_id' => '1','title' => 'enp spark','slug' => 'enp-spark','content' => '<pre class="language-markup"><code>https://techdoc.enp.pl/books/spark/page/instalacja-projektu
https://techdoc.enp.pl/books/spark/page/node-version-manager

https://www.mediaexpert.pl/smartfony-i-zegarki/smartwatche-i-zegarki/smartwatche
https://www.mediaexpert.pl/komputery-i-tablety/tablety-i-e-booki/tablety
https://www.mediaexpert.pl/agd-do-zabudowy/plyty-do-zabudowy
https://www.mediaexpert.pl/dom-i-ogrod/myjki-wysokocisnieniowe_i_akcesoria/myjki-wysokocisnieniowe
https://www.mediaexpert.pl/gaming/gry
https://www.mediaexpert.pl/telewizory-i-rtv/sluchawki/wszystkie-sluchawki
https://www.mediaexpert.pl/agd-male &lt;- wszystkie podkategorie
          
{
  "grant_type": "password",
  "client_id": "{{client_id}}",
  "client_secret": "{{client_secret}}",
  "username": "xxxxx",
  "password": "xxxxx",
  "source": "normal"
}

Enp\\Bundle\\ENP0026\\Admin\\ControlPanelBundle\\Form\\Type\\Prototype\\GeocoderMapsType

https://panelterg.enp.pl/statusflow/transition/edit/922/configuration/4/status/38

test:

04433188084
spark config:reconfigure
</code></pre>','publishedAt' => '2021-04-30 15:07:17','updatedAt' => '2021-04-30 15:07:17','headtitle' => NULL,'keyworks' => NULL,'description' => NULL,'img' => NULL,'type' => '2','number_of_comments' => '0')
);
