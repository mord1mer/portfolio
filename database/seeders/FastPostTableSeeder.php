<?php

use Illuminate\Database\Seeder;
use App\Http\Model\Post;
use App\Domain\Pages\PageTypes;
use Illuminate\Support\Str;

class FastPostTableSeeder extends Seeder
{
    public function run()
    {
        require base_path('database/seeders/fastpost/fast_posts.php');

        foreach ($fast_posts as $post) {
            Post::insert([
                'user_id' => 1,
                'title' => $post['title'],
                'slug' => Str::slug($post['title']),
                'content' => $post['content'],
                'publishedAt' => $post['publishedAt'],
                'updatedAt' => $post['updatedAt'],
                'headtitle' => $post['title'],
                'keyworks' => '',
                'type' => PageTypes::FAST_POST,
                'description' => '',
                'img' => '',
            ]);
        }
    }
}
