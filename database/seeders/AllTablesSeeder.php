<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Http\Model\Category;
use App\Http\Model\Comment;
use App\Http\Model\Post;
use App\User;
use Illuminate\Database\Seeder;
use App\Http\Model\Tag;
use Illuminate\Support\Facades\DB;

class AllTablesSeeder extends Seeder
{
    public function run(): void
    {
        require base_path('database/seeders/all/data.php');

        Category::insert($categories);
        Tag::insert($tags);
        User::insert($users);
        Post::insert($posts);
        Comment::insert($comments);

        DB::table('category_post')->insert($category_post);
        DB::table('post_tag')->insert($post_tag);
    }
}
