<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Domain\Pages\PageTypes;

class FastPostSeed extends Seeder
{
    public function run()
    {
        require base_path('database/seeders/fast_post_tags/fast_post_tag.php');

        foreach ($fast_post_tag as $tag) {
            //dd($tag);
            DB::table('post_tag')->insert([
                'post_id' => $tag['fast_post_id'],
                'tag_id' => $tag['tag_id'],
                'type' => PageTypes::FAST_POST
            ]);
        }
    }
}
