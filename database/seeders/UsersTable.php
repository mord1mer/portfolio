<?php

use Illuminate\Database\Seeder;
use App\Domain\User\UserRoles;
use Illuminate\Support\Facades\DB;

class UsersTable extends Seeder
{
    public function run()
    {
        DB::table('users')->insert(
            [
                'name' => 'Grzesiek Tarka',
                'email' => 'grzesiekneo2@o2.pl',
                'roles' => json_encode([UserRoles::ROLE_SUPER_ADMIN]),
                'password' => '$2y$10$DeQGzUOgE0/7Ldc0wIeId.6g0hWLcdTRB.DFiF6qWv/CcPX/h4D.6',
            ],
            [
                'name' => 'Janusz Januszewicz',
                'email' => 'janusz@o2.pl',
                'roles' => json_encode([UserRoles::ROLE_REDACTOR]),
                'password' => '$2y$10$DeQGzUOgE0/7Ldc0wIeId.6g0hWLcdTRB.DFiF6qWv/CcPX/h4D.6',
            ],
            [
                'name' => 'Asia Junicka',
                'email' => 'asia@o2.pl',
                'roles' => json_encode([UserRoles::ROLE_REDACTOR]),
                'password' => '$2y$10$DeQGzUOgE0/7Ldc0wIeId.6g0hWLcdTRB.DFiF6qWv/CcPX/h4D.6',
            ]
        );
    }
}
