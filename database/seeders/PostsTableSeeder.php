<?php

use App\Domain\Pages\PageTypes;
use App\Http\Model\Post;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PostsTableSeeder extends Seeder
{
    public function run() {
        $faker = Faker::create('pl_PL');

        $numberOfPosts = 20;
        $numberOfOrginalPost = 3;
        $numberOfComments = 1000;

        $title = 'Jak stworzyć swój własny helper w laravelu (5.5)';
        $content = file_get_contents(base_path('database/seeders/posts/1.php'));
        $post = Post::insert([
            'user_id' => 1,
            'title' => $title,
            'slug' => Str::slug($title),
            'content' => $content,
            'publishedAt' => date("Y-m-d H:i:s"),
            'updatedAt' => date("Y-m-d H:i:s"),
            'headtitle' => $title,
            'keyworks' => 'laravel, helpery, php',
            'type' => PageTypes::BLOG_POST,
            'description' => 'W wielu projektach nie jednokrotnie przychodzi moment w którym potrzebujesz stworzyć szybką funkcje/helpera przyspieszającą twoją pracę. Dziś zajmiemy się implementacją takiej funkcji w popularnej "larvie"',
            'img' => 'img/upload/3/titleimg/neo-urban-1734495_1920.jpg',
        ]);

        DB::table('post_tag')->insert([
            ['post_id' => DB::getPdo()->lastInsertId(), 'tag_id' => 8, 'type' => PageTypes::BLOG_POST],
            ['post_id' => DB::getPdo()->lastInsertId(), 'tag_id' => 5, 'type' => PageTypes::BLOG_POST],
        ]);

        DB::table('category_post')->insert(
            ['post_id' => 1, 'category_id' => 1]
        );

        //=======================================================================================
        //=====================================2=================================================
        //=======================================================================================
        //=======================================================================================
        $title = 'Przeprowadzanie obliczeń matematycznych w sql';
        $content = file_get_contents(base_path('database/seeders/posts/2.php'));
        Post::insert([
            'user_id' => 1,
            'title' => $title,
            'slug' => Str::slug($title),
            'content' => $content,
            'publishedAt' => date("Y-m-d H:i:s"),
            'updatedAt' => date("Y-m-d H:i:s"),
            'headtitle' => $title,
            'keyworks' => 'sql, operacje matematyczne',
            'type' => PageTypes::BLOG_POST,
            'description' => 'Przeprowadzanie obliczeń matematycznych w aplikacji możliwe a nawet konieczne na samym poziomie bazy danych. Przyspiesza to działanie naszej aplikacjii w znaczący sposób. W dzisiejszym wpisie zajmiemy się przykładem takiej operacji',
            'img' => 'img/upload/5/titleimg/book-1853677_1920.jpg',
        ]);

        $lastId = DB::getPdo()->lastInsertId();

        DB::table('post_tag')->insert([
            ['post_id' => $lastId, 'tag_id' => 12, 'type' => PageTypes::BLOG_POST],
        ]);

        DB::table('category_post')->insert(
            ['post_id' => $lastId, 'category_id' => 1]
        );
        //=======================================================================================
        //=====================================3=================================================
        //=======================================================================================
        //=======================================================================================
        $title = 'Jak wyłaczyć ekran logowania w systemie windows';
        $content = file_get_contents(base_path('database/seeders/posts/3.php'));
        Post::insert([
            'user_id' => 1,
            'title' => $title,
            'slug' => Str::slug($title),
            'content' => $content,
            'publishedAt' => date("Y-m-d H:i:s"),
            'updatedAt' => date("Y-m-d H:i:s"),
            'headtitle' => $title,
            'keyworks' => 'windows, wyłącznie logowania',
            'type' => PageTypes::BLOG_POST,
            'description' => 'W przypadku użycia komputera w domu przez zaufanych użytkowników panel logowania do systemu windows może być tylko nie potrzebną stratą czasu. Dziś zajmiemy się wyłączeniem takiego panelu dzięki czemu windows po uruchomieniu komputera włączy się bezpośrednio i zaoszczędzi nam',
            'img' => 'img/upload/4/titleimg/login-1203603_1280.jpg',
        ]);
        $lastId = DB::getPdo()->lastInsertId();
        DB::table('post_tag')->insert([
            ['post_id' => $lastId, 'tag_id' => 11, 'type' => PageTypes::BLOG_POST],
        ]);

        DB::table('category_post')->insert(
            ['post_id' => $lastId, 'category_id' => 2]
        );

        for ($user_id = 1; $user_id <= $numberOfPosts; $user_id++) {
            $post = new Post;
            $post->user_id = rand(1, $numberOfOrginalPost);
            $post->title = 'cypher' . $user_id;
            $post->slug = 'cypher' . $user_id;
            $post->content = $faker->text($maxNbChars = 5000);
            $post->publishedAt = date("Y-m-d H:i:s");
            $post->updatedAt = date("Y-m-d H:i:s");
            $post->headtitle = 'headtitle' . $user_id;
            $post->keyworks = 'key works test asd' . $user_id;
            $post->type = PageTypes::BLOG_POST;
            $post->description = $faker->text($maxNbChars = 100);

            $post->save();

            $lastId = $post->id;

            DB::table('category_post')->insert(
                ['post_id' => $lastId, 'category_id' => rand(1, 2)]
            );

            DB::table('post_tag')->insert([
                ['post_id' => $lastId, 'tag_id' => rand(1, 28), 'type' => PageTypes::BLOG_POST],
            ]);
        }
        //========================================================================================
        //========================================================================================
        //========================================================================================
        //========================================================================================

        $phpUniTestComment = new \App\Http\Model\Comment();
        $phpUniTestComment->post_id = 1;
        $phpUniTestComment->comment_author = 'Grzesiek T';
        $phpUniTestComment->comment_author_email = 'grzesiekneo2@o2.pl';
        $phpUniTestComment->comment_author_email_md5 = md5('grzesiekneo2@o2.pl');
        $phpUniTestComment->comment_author_ip = '192.168.110';
        //========================================================================================
        $phpUniTestComment->comment_content = 'Fajny komentarz nie ?';
        $phpUniTestComment->comment_parent = 0;
        $phpUniTestComment->comment_agent = 'user agent';
        $phpUniTestComment->publishedAt = date('2018-08-09 21:18:14');
        $phpUniTestComment->save();

        $firstIdWithPostType = Post::select('id')->where('type', 1)->limit(1)->first()->id;
        $maxIdToRandPostType = $firstIdWithPostType + $numberOfPosts + $numberOfOrginalPost - 1;

        for ($comments = 1; $comments <= $numberOfComments; $comments++) {
            $comment = new \App\Http\Model\Comment();
            $comment->post_id = rand($firstIdWithPostType, $maxIdToRandPostType);
            $comment->comment_author = $faker->userName;
            $comment->comment_author_email = $faker->email;
            $comment->comment_author_email_md5 = md5($faker->email);
            $comment->comment_author_ip = $faker->ipv4;
            //========================================================================================
            $comment->comment_content = $faker->text($maxNbChars = 200);
            $comment->comment_parent = 0;
            $comment->comment_agent = $faker->userAgent;
            $comment->publishedAt = date('Y-m-d H:i:s');
            $comment->save();
        }
    }

}
