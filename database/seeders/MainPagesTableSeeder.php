<?php

use App\Http\Model\Post;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Domain\Pages\PageTypes;
use Illuminate\Support\Facades\Blade;

class MainPagesTableSeeder extends Seeder
{
    public function run()
    {
        $content = file_get_contents(base_path('database/seeders/main_pages/aboutMe.php'));
        $title = 'Grzegorz Tarka - kim jestem i moja historia';
        $description = 'Witam nazywam się Grzegorz, programowaniem zajmuje się od nieco ponad roku, jednak trudno powiedzieć mi kiedy to wszystko się zaczęło bo w zasadzie od zawsze interesowałem sie informatyką i programowaniem.';

        $post = new Post;
        $post->user_id = 1;
        $post->title = $title;
        $post->slug = 'o-mnie';
        $post->content = $content;
        $post->publishedAt = date("Y-m-d H:i:s");
        $post->updatedAt = date("Y-m-d H:i:s");
        $post->headtitle = $title;
        $post->keyworks = '';
        $post->type = PageTypes::MAIN_PAGE;
        $post->description = $description;
        $post->save();
        //========================================================
        //========================================================
        $title = 'Grzesiek Tarka-programista webowy';
        $description = 'Tworzenie wyjątkowych stron internetowych. Freelancer/ web developer. Aplikacje webowe frontend i backend. Projekty w PHP,Javascript, CSS, HTML.';
        $content = file_get_contents(base_path('database/seeders/main_pages/home.php'));

        $post = new Post;
        $post->user_id = 1;
        $post->title = $title;
        $post->slug = 'home';
        $post->content = $content;
        $post->publishedAt = date("Y-m-d H:i:s");
        $post->updatedAt = date("Y-m-d H:i:s");
        $post->headtitle = $title;
        $post->keyworks = '';
        $post->type = PageTypes::MAIN_PAGE;
        $post->description = $description;
        $post->save();
        //========================================================
        //========================================================
        $title = 'Grzegorz Tarka - kim jestem i moja historia2';
        $description = 'Witam nazywam się Grzegorz, programowaniem zajmuje się od nieco ponad roku, jednak trudno powiedzieć mi kiedy to wszystko się zaczęło bo w zasadzie od zawsze interesowałem sie informatyką i programowaniem.';

        $content = file_get_contents(base_path('database/seeders/main_pages/portfolio.php'));

        $post = new Post;
        $post->user_id = 1;
        $post->title = $title;
        $post->slug = 'portfolio';
        $post->content = $content;
        $post->publishedAt = date("Y-m-d H:i:s");
        $post->updatedAt = date("Y-m-d H:i:s");
        $post->headtitle = $title;
        $post->keyworks = '';
        $post->type = PageTypes::MAIN_PAGE;
        $post->description = $description;
        $post->save();
    }
}
