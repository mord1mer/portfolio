<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.9.5
 */

/**
 * Database `m6035_portfolio_moja`
 */

/* `m6035_portfolio_moja`.`tags` */
$tags = array(
  array('id' => '1','name' => 'html','slug' => 'html','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '2','name' => 'css','slug' => 'css','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '3','name' => 'javascript','slug' => 'javascript','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '4','name' => 'jquery','slug' => 'jquery','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '5','name' => 'php','slug' => 'php','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '6','name' => 'ajax','slug' => 'ajax','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '7','name' => 'oop','slug' => 'oop','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '8','name' => 'sql','slug' => 'sql','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '9','name' => 'laravel','slug' => 'laravel','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '10','name' => 'bootstrap','slug' => 'bootstrap','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '11','name' => 'wyrazenia regularne','slug' => 'wyrazenia-regularne','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '12','name' => 'pdo','slug' => 'pdo','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '13','name' => 'htaccess','slug' => 'htaccess','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '14','name' => 'composer','slug' => 'composer','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '15','name' => 'js/jquery','slug' => 'jsjquery','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '16','name' => 'zdjecia','slug' => 'zdjecia','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '17','name' => 'windows','slug' => 'windows','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '18','name' => 'linux','slug' => 'linux','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '19','name' => 'symfony','slug' => 'symfony','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '20','name' => 'git','slug' => 'git','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '21','name' => 'behat','slug' => 'behat','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '22','name' => 'wzorce projektowe','slug' => 'wzorce-projektowe','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '23','name' => 'inne','slug' => 'inne','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '24','name' => 'apache','slug' => 'apache','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '25','name' => 'phpmyadmin','slug' => 'phpmyadmin','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '26','name' => 'curl','slug' => 'curl','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '27','name' => 'twig','slug' => 'twig','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '28','name' => 'phpunit','slug' => 'phpunit','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '29','name' => 'ct8','slug' => 'ct8','showInPost' => '0','showInFastPost' => '1'),
  array('id' => '30','name' => 'phpstan','slug' => 'phpstan','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '31','name' => 'doctrine','slug' => 'doctrine','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '32','name' => 'xml','slug' => 'xml','showInPost' => '0','showInFastPost' => '1'),
  array('id' => '33','name' => 'soap','slug' => 'soap','showInPost' => '0','showInFastPost' => '1'),
  array('id' => '34','name' => 'szukanie po stringach w php','slug' => 'szukanie-po-stringach-w-php','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '35','name' => 'visual studio code','slug' => 'visual-studio-code','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '36','name' => 'wamp','slug' => 'wamp','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '37','name' => 'vue','slug' => 'vue','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '38','name' => 'css flex','slug' => 'css-flex','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '39','name' => 'php wzorce narzędzia','slug' => 'php-wzorce-narzedzia','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '40','name' => 'złote myśli','slug' => 'zlote-mysli','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '41','name' => 'docker','slug' => 'docker','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '42','name' => 'rabbit','slug' => 'rabbit','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '43','name' => 'cqrs','slug' => 'cqrs','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '44','name' => 'redis','slug' => 'redis','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '45','name' => 'edukacja','slug' => 'edukacja','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '46','name' => 'funkcje php','slug' => 'funkcje-php','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '47','name' => 'enet','slug' => 'enet','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '48','name' => 'enetcode','slug' => 'enetcode','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '49','name' => 'firma','slug' => 'firma','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '50','name' => 'phpspec','slug' => 'phpspec','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '51','name' => 'cookies','slug' => 'cookies','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '52','name' => 'virtual host','slug' => 'virtual-host','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '53','name' => 'seo','slug' => 'seo','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '54','name' => 'enp sla','slug' => 'enp-sla','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '55','name' => 'kawały','slug' => 'kawaly','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '56','name' => 'podcast','slug' => 'podcast','showInPost' => '1','showInFastPost' => '1'),
  array('id' => '57','name' => 'google','slug' => 'google','showInPost' => '1','showInFastPost' => '1')
);
