<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('post_id')->unsigned();
            $table->integer('comment_parent')->unsigned();
            $table->integer('number_of_responses')->unsigned()->default(0);
            $table->string('comment_author', 100);
            $table->string('comment_author_email', 100);
            $table->string('comment_author_email_md5', 32);
            $table->string('comment_author_url', 100)->nullable();
            $table->string('comment_author_ip', 100);
            $table->text('comment_content');
            $table->string('comment_agent', 255);
            $table->datetime('publishedAt');

            $table->softDeletes();

            $table->foreign('post_id')->references('id')->on('posts');
            $table->index(['post_id', 'comment_parent']);
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
