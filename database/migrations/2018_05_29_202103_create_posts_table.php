<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    public function up(): void
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title', 100)->unique();
            $table->string('slug')->unique();
            $table->mediumText('content')->nullable();
            $table->datetime('publishedAt');
            $table->datetime('updatedAt');
            $table->string('headtitle', 100)->nullable();
            $table->string('keyworks', 200)->nullable();
            $table->string('description', 278)->nullable();
            $table->string('img', 255)->nullable();
            $table->smallInteger('type');
            $table->tinyInteger('show_added_comments')->default(0);
            $table->tinyInteger('let_add_comments')->default(0);
            $table->integer('number_of_comments')->unsigned()->default(0);
            $table->foreign('user_id')->references('id')->on('users');
            //$table->foreign('category_id')->references('id')->on('categories');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('posts');
    }
}
